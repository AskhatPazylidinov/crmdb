﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductCode
    {
        public int ProductId { get; set; }
        public string CreditTypeCode { get; set; }

        public virtual CreditType CreditTypeCodeNavigation { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
