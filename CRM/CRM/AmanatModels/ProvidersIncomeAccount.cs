﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProvidersIncomeAccount
    {
        public int OfficeId { get; set; }
        public int ProviderId { get; set; }
        public string IncomeAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
