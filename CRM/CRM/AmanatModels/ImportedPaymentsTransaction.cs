﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportedPaymentsTransaction
    {
        public long PaymentId { get; set; }
        public long Position { get; set; }
        public string ClearingFileName { get; set; }
    }
}
