﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InternalPerson
    {
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ApprovedByUserId { get; set; }
        public int? ApprovedByExcludeUserId { get; set; }
        public int? FraudCode1 { get; set; }
        public int? FraudCode2 { get; set; }
        public int? FraudCode3 { get; set; }
        public DateTime FraudDate { get; set; }
        public string FraudUserPosition { get; set; }
        public string FraudUserName { get; set; }
        public decimal FraudSum { get; set; }
        public decimal CompensationSum { get; set; }
        public int RatingTypeId { get; set; }
        public byte? DetectedBy { get; set; }
        public string RatingComment { get; set; }

        public virtual User ApprovedByExcludeUser { get; set; }
        public virtual User ApprovedByUser { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Reason FraudCode1Navigation { get; set; }
        public virtual Reason FraudCode2Navigation { get; set; }
        public virtual Reason FraudCode3Navigation { get; set; }
        public virtual RatingType RatingType { get; set; }
    }
}
