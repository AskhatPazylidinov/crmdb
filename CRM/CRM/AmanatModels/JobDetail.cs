﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class JobDetail
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Result { get; set; }

        public virtual Job Job { get; set; }
    }
}
