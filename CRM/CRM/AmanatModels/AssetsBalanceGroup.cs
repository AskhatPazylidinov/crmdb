﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetsBalanceGroup
    {
        public int TypeId { get; set; }
        public int AccountType { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
        public virtual Type Type { get; set; }
    }
}
