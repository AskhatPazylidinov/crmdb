﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SubStatusesType
    {
        public SubStatusesType()
        {
            HistoriesSubStatuses = new HashSet<HistoriesSubStatus>();
            SubStatusesExpiredReasons = new HashSet<SubStatusesExpiredReason>();
            SubStatusesLimitsForUsers = new HashSet<SubStatusesLimitsForUser>();
            SubStatusesLimitsOnRoles = new HashSet<SubStatusesLimitsOnRole>();
            SubStatusesPeriods = new HashSet<SubStatusesPeriod>();
            SubStatusesReasons = new HashSet<SubStatusesReason>();
            SubStatusesRequestsLimitsForUsers = new HashSet<SubStatusesRequestsLimitsForUser>();
            SubStatusesRequestsLimitsOnRoles = new HashSet<SubStatusesRequestsLimitsOnRole>();
            SubStatusesWorkflowDestSubStatuses = new HashSet<SubStatusesWorkflow>();
            SubStatusesWorkflowSourceSubStatuses = new HashSet<SubStatusesWorkflow>();
        }

        public int SubStatusId { get; set; }
        public string Typename { get; set; }
        public string Description { get; set; }
        public int? StandardStatusId { get; set; }
        public int OrderId { get; set; }

        public virtual ICollection<HistoriesSubStatus> HistoriesSubStatuses { get; set; }
        public virtual ICollection<SubStatusesExpiredReason> SubStatusesExpiredReasons { get; set; }
        public virtual ICollection<SubStatusesLimitsForUser> SubStatusesLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesLimitsOnRole> SubStatusesLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesPeriod> SubStatusesPeriods { get; set; }
        public virtual ICollection<SubStatusesReason> SubStatusesReasons { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsForUser> SubStatusesRequestsLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsOnRole> SubStatusesRequestsLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesWorkflow> SubStatusesWorkflowDestSubStatuses { get; set; }
        public virtual ICollection<SubStatusesWorkflow> SubStatusesWorkflowSourceSubStatuses { get; set; }
    }
}
