﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondGovernment
    {
        public SocialFondGovernment()
        {
            SocialFondDepartments = new HashSet<SocialFondDepartment>();
        }

        public int GovernmentId { get; set; }
        public string GovernmentName { get; set; }
        public string GovernmentCode { get; set; }

        public virtual ICollection<SocialFondDepartment> SocialFondDepartments { get; set; }
    }
}
