﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BanksAction
    {
        public int CreditId { get; set; }
        public DateTime ActionDate { get; set; }
        public int ActionUserId { get; set; }
        public string Actions { get; set; }
        public int Id { get; set; }
        public DateTime ModifyDate { get; set; }
        public string PlanActions { get; set; }
        public string DelayReason { get; set; }
        public bool IsOvk { get; set; }

        public virtual User ActionUser { get; set; }
        public virtual History Credit { get; set; }
    }
}
