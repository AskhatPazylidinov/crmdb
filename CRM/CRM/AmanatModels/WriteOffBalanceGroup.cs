﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class WriteOffBalanceGroup
    {
        public byte AccountTypeId { get; set; }
        public string ResidentBalanceGroup { get; set; }
        public string NonResidentBalanceGroup { get; set; }

        public virtual BalanceGroup NonResidentBalanceGroupNavigation { get; set; }
        public virtual BalanceGroup ResidentBalanceGroupNavigation { get; set; }
    }
}
