﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsGenderType
    {
        public int GenderTypeId { get; set; }
        public int CibgenderTypeId { get; set; }
    }
}
