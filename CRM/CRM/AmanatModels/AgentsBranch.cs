﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsBranch
    {
        public AgentsBranch()
        {
            AgentsUsers = new HashSet<AgentsUser>();
            InterBranchTransfersLimits = new HashSet<InterBranchTransfersLimit>();
            TransfersOperations = new HashSet<TransfersOperation>();
        }

        public int AgentBranchId { get; set; }
        public int AgentId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public virtual Agent Agent { get; set; }
        public virtual ICollection<AgentsUser> AgentsUsers { get; set; }
        public virtual ICollection<InterBranchTransfersLimit> InterBranchTransfersLimits { get; set; }
        public virtual ICollection<TransfersOperation> TransfersOperations { get; set; }
    }
}
