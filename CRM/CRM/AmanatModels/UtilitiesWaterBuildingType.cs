﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesWaterBuildingType
    {
        public UtilitiesWaterBuildingType()
        {
            UtilitiesWaterPayments = new HashSet<UtilitiesWaterPayment>();
        }

        public int BuildingTypeId { get; set; }
        public string Typename { get; set; }
        public string ExportPrefix { get; set; }

        public virtual ICollection<UtilitiesWaterPayment> UtilitiesWaterPayments { get; set; }
    }
}
