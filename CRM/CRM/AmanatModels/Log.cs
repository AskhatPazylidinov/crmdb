﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Log
    {
        public Log()
        {
            LogStatuses = new HashSet<LogStatus>();
        }

        public long Id { get; set; }
        public string SenderName { get; set; }
        public string ReceiverPhone { get; set; }
        public string Message { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual ICollection<LogStatus> LogStatuses { get; set; }
    }
}
