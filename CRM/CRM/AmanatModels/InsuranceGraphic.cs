﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceGraphic
    {
        public int InsuranceId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte Period { get; set; }
        public decimal Rate { get; set; }
        public int InsureTypeId { get; set; }

        public virtual InsuranceChange InsuranceChange { get; set; }
    }
}
