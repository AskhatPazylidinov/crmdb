﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InternetBankingOperationStep
    {
        public int Id { get; set; }
        public int OperationId { get; set; }
        public string Description { get; set; }
        public int Step { get; set; }
        public bool IsComplete { get; set; }
    }
}
