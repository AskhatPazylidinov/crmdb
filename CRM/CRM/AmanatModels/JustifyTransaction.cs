﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class JustifyTransaction
    {
        public int UserId { get; set; }
        public long Position { get; set; }
    }
}
