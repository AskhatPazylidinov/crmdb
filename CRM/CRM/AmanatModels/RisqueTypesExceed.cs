﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RisqueTypesExceed
    {
        public byte CustomerTypeId { get; set; }
        public byte RiskTypeId { get; set; }
        public int Period { get; set; }
    }
}
