﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankType1
    {
        public byte Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
