﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportedPayment
    {
        public long PaymentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime ValueDate { get; set; }
        public string SenderFullName { get; set; }
        public string SenderIdentificationNumber { get; set; }
        public string SenderOkpo { get; set; }
        public string SenderAccountNo { get; set; }
        public string ReceiverBik { get; set; }
        public string ReceiverAccountNo { get; set; }
        public string ReceiverFullName { get; set; }
        public string PaymentComment { get; set; }
        public string PaymentCode { get; set; }
        public decimal TransferSumm { get; set; }
    }
}
