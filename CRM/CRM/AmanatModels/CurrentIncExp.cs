﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CurrentIncExp
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public int? TypeId { get; set; }
        public decimal? Value { get; set; }
        public bool? InPercent { get; set; }
        public int? AccountTypeId { get; set; }
        public string AccountNo { get; set; }
        public int? CurrencyId { get; set; }
        public string Reason { get; set; }
        public bool? Tax { get; set; }
        public bool? ErSs { get; set; }
        public bool? EeSs { get; set; }
        public bool? Tdc { get; set; }
        public bool? Ed { get; set; }
        public int? Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? StatusUserId { get; set; }
        public int? Direction { get; set; }
        public decimal? FixedSum { get; set; }
        public decimal? FixedTaxSum { get; set; }
        public decimal? FixedErSs { get; set; }
        public decimal? FixedEeSs { get; set; }
        public bool? AutoGenerated { get; set; }
        public decimal? WorkedHours { get; set; }
        public int? AvanceId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? OfficeScope { get; set; }
        public int? GroupId { get; set; }
        public int? Period { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
