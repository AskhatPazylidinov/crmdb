﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PositionAccountPair
    {
        public int BranchId { get; set; }
        public int CurrencyId { get; set; }
        public string PositionNo { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
