﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccumulativeAccount
    {
        public int BranchId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public int CurrencyId { get; set; }
        public string OdbaccountNo { get; set; }
        public int AccountId { get; set; }
    }
}
