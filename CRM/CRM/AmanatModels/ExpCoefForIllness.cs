﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExpCoefForIllness
    {
        public int MinExp { get; set; }
        public int MaxExp { get; set; }
        public decimal Coef { get; set; }
    }
}
