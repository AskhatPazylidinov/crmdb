﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InBranchActivityType
    {
        public string InBranch { get; set; }
        public string TypeName { get; set; }
    }
}
