﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PositionAccountPairView
    {
        public int BranchId { get; set; }
        public int CurrencyId { get; set; }
        public string PositionNo { get; set; }
        public string BalanceGroup { get; set; }
    }
}
