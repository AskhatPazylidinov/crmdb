﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalLoansPaymentAccount
    {
        public string MainAccountNo { get; set; }
        public int MainAccountCurrencyId { get; set; }
        public string PaymentAccountNo { get; set; }
        public int PaymentAccountCurrencyId { get; set; }

        public virtual DepositAccount MainAccount { get; set; }
        public virtual Account1 PaymentAccount { get; set; }
    }
}
