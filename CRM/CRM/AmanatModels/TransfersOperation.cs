﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersOperation
    {
        public TransfersOperation()
        {
            TransfersOperationsTransactions = new HashSet<TransfersOperationsTransaction>();
        }

        public long OperationId { get; set; }
        public long TransferId { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public int? AgentUserId { get; set; }
        public int? AgentBranchId { get; set; }

        public virtual AgentsBranch AgentBranch { get; set; }
        public virtual AgentsUser AgentUser { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual InterTransfer Transfer { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<TransfersOperationsTransaction> TransfersOperationsTransactions { get; set; }
    }
}
