﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProvidersComissionsPartition
    {
        public int ProviderId { get; set; }
        public byte DirectionTypeId { get; set; }
        public decimal ProviderComission { get; set; }
        public decimal BankComission { get; set; }

        public virtual ServiceProvider Provider { get; set; }
    }
}
