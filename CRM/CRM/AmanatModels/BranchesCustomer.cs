﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesCustomer
    {
        public int BranchId { get; set; }
        public int CustomerId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
