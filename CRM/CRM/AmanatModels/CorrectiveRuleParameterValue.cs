﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrectiveRuleParameterValue
    {
        public int CorrectiveRuleId { get; set; }
        public byte ScoringParameterTypeId { get; set; }
        public byte OperationId { get; set; }
        public string ScoreParameterValue { get; set; }

        public virtual CorrectiveRule CorrectiveRule { get; set; }
    }
}
