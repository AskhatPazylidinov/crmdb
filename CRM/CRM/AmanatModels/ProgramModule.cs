﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProgramModule
    {
        public ProgramModule()
        {
            ActiveBranches = new HashSet<ActiveBranch>();
            BranchesCloseDays = new HashSet<BranchesCloseDay>();
            CreateTransactionsInModuleRoles = new HashSet<CreateTransactionsInModuleRole>();
            CreateTransactionsInModuleUsers = new HashSet<CreateTransactionsInModuleUser>();
            RoleLimits = new HashSet<RoleLimit>();
            UserLimits = new HashSet<UserLimit>();
            Users = new HashSet<User>();
        }

        public int ModuleId { get; set; }
        public string Description { get; set; }
        public int ExtendedTypeId { get; set; }

        public virtual ICollection<ActiveBranch> ActiveBranches { get; set; }
        public virtual ICollection<BranchesCloseDay> BranchesCloseDays { get; set; }
        public virtual ICollection<CreateTransactionsInModuleRole> CreateTransactionsInModuleRoles { get; set; }
        public virtual ICollection<CreateTransactionsInModuleUser> CreateTransactionsInModuleUsers { get; set; }
        public virtual ICollection<RoleLimit> RoleLimits { get; set; }
        public virtual ICollection<UserLimit> UserLimits { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
