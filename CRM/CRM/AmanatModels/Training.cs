﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Training
    {
        public int TrainingId { get; set; }
        public int EmployeeId { get; set; }
        public int TypeId { get; set; }
        public string NameOfTrainingCenter { get; set; }
        public string Theme { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Reason { get; set; }
        public decimal? Cost { get; set; }
        public string Place { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public int Term { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int StatusUserId { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
