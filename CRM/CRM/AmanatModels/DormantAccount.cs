﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DormantAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ApprovedUserId { get; set; }
        public int? UserId { get; set; }
        public DateTime? ActivateDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
