﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Broker
    {
        public int BrokerId { get; set; }
        public string BrokerName { get; set; }
    }
}
