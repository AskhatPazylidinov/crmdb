﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesMortgage
    {
        public int SecurityMortgageId { get; set; }
        public int MortgageId { get; set; }
        public int SecurityId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Mortgage Mortgage { get; set; }
        public virtual Security Security { get; set; }
        public virtual SecuritiesMortgagesReplacement SecuritiesMortgagesReplacement { get; set; }
    }
}
