﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationTransactionsView
    {
        public int Id { get; set; }
        public DateTime DateOper { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public decimal SumV { get; set; }
        public string TypeOper { get; set; }
        public decimal? SumFee { get; set; }
        public string DeviceCode { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string CardAccountNo { get; set; }
        public string CodeLogon { get; set; }
        public byte FileType { get; set; }
        public string ParentDeviceCode { get; set; }
        public string LineNumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceName { get; set; }
        public string MerchantId { get; set; }
        public string Cmi { get; set; }
        public int? ParentId { get; set; }
        public string Mcc { get; set; }
        public long? Position { get; set; }
        public bool? HasTransaction { get; set; }
    }
}
