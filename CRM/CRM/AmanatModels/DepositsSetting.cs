﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsSetting
    {
        public int EmployeeId { get; set; }
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal TransferSumm { get; set; }
        public string Comment { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
