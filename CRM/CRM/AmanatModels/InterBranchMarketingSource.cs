﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchMarketingSource
    {
        public long TransferId { get; set; }
        public int UserId { get; set; }
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }

        public virtual InterTransfer Transfer { get; set; }
    }
}
