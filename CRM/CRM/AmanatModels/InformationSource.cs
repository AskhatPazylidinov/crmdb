﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InformationSource
    {
        public InformationSource()
        {
            Cards = new HashSet<Card>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Card> Cards { get; set; }
    }
}
