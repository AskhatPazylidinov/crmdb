﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Recall
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Reason { get; set; }
        public int? VacationId { get; set; }

        public virtual VacationsApproved Vacation { get; set; }
    }
}
