﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositoryAccount
    {
        public int BranchId { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int AccountId { get; set; }
        public int? CustomerTypeId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
