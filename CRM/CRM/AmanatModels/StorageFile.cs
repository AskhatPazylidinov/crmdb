﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StorageFile
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public int? FileState { get; set; }
    }
}
