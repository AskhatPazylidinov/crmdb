﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InfoSystemRequestLog
    {
        public long LogId { get; set; }
        public int UserId { get; set; }
        public int OperationId { get; set; }
        public string PaymentDescription { get; set; }
        public DateTime LogDate { get; set; }
        public string RequestTypeName { get; set; }
        public int ResponseResultStatus { get; set; }
    }
}
