﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int OperationId { get; set; }

        public virtual DetailedOperation Operation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
