﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Insider
    {
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Relationship { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
