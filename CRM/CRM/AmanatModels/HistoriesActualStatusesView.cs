﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesActualStatusesView
    {
        public int CreditId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int? UserId { get; set; }
    }
}
