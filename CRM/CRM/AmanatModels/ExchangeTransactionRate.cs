﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExchangeTransactionRate
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public decimal OperationRate { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? SumV { get; set; }
        public int? CrossCurrencyId { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
