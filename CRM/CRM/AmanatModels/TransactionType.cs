﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionType
    {
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }
}
