﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PosTerminalOperation
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int VerifiedUserId { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string VerifyCode { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual User VerifiedUser { get; set; }
    }
}
