﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafeType
    {
        public SafeType()
        {
            IndividualGuaranteeSumms = new HashSet<IndividualGuaranteeSumm>();
            IndividualOverdueTariffs = new HashSet<IndividualOverdueTariff>();
            IndividualSafeTariffsValues = new HashSet<IndividualSafeTariffsValue>();
            SafeTariffs = new HashSet<SafeTariff>();
            Saves = new HashSet<Safe>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<IndividualGuaranteeSumm> IndividualGuaranteeSumms { get; set; }
        public virtual ICollection<IndividualOverdueTariff> IndividualOverdueTariffs { get; set; }
        public virtual ICollection<IndividualSafeTariffsValue> IndividualSafeTariffsValues { get; set; }
        public virtual ICollection<SafeTariff> SafeTariffs { get; set; }
        public virtual ICollection<Safe> Saves { get; set; }
    }
}
