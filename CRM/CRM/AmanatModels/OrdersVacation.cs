﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrdersVacation
    {
        public int EmployeeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
        public string ConfirmComment { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
