﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployeesExtraAllowance
    {
        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public int AllowanceTypeId { get; set; }
        public decimal ExtraSumm { get; set; }

        public virtual AllowancesType AllowanceType { get; set; }
        public virtual PaysheetsEmployee PaysheetsEmployee { get; set; }
    }
}
