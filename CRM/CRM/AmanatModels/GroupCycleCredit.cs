﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GroupCycleCredit
    {
        public int Id { get; set; }
        public int CreditId { get; set; }
        public int CycleId { get; set; }
        public bool IsLeader { get; set; }
        public string GroupAgreementNo { get; set; }

        public virtual History Credit { get; set; }
        public virtual GroupCycle Cycle { get; set; }
    }
}
