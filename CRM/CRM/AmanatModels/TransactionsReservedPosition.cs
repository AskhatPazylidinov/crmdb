﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsReservedPosition
    {
        public long StartPosition { get; set; }
        public long EndPosition { get; set; }
    }
}
