﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferDirection
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
