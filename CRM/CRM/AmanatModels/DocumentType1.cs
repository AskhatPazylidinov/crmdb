﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentType1
    {
        public DocumentType1()
        {
            Customers = new HashSet<Customer>();
            CustomersChanges = new HashSet<CustomersChange>();
            DocumentTypeIds = new HashSet<DocumentTypeId>();
            DocumentTypesMapping1s = new HashSet<DocumentTypesMapping1>();
        }

        public int DocumentTypeId { get; set; }
        public string TypeName { get; set; }
        public string ShortType { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<CustomersChange> CustomersChanges { get; set; }
        public virtual ICollection<DocumentTypeId> DocumentTypeIds { get; set; }
        public virtual ICollection<DocumentTypesMapping1> DocumentTypesMapping1s { get; set; }
    }
}
