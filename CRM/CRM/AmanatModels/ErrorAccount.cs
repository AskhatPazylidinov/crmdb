﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ErrorAccount
    {
        public string DebetAccountNo { get; set; }
        public string CreditAccountNo { get; set; }
    }
}
