﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OverallPhonesLimit
    {
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal TotalLimit { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
