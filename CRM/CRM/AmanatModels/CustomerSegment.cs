﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerSegment
    {
        public int SegmentId { get; set; }
        public string SegmentName { get; set; }
    }
}
