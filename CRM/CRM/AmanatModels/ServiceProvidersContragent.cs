﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersContragent
    {
        public ServiceProvidersContragent()
        {
            ServiceProvidersContragentsBranches = new HashSet<ServiceProvidersContragentsBranch>();
            ServiceProvidersContragentsComissions = new HashSet<ServiceProvidersContragentsComission>();
            ServiceProvidersContragentsContacts = new HashSet<ServiceProvidersContragentsContact>();
        }

        public int ProviderId { get; set; }
        public int ContragentId { get; set; }
        public string UtilityCode { get; set; }
        public string UserRecomendation { get; set; }
        public decimal? ProviderComissionPartition { get; set; }
        public decimal? BankComissionPartition { get; set; }
        public byte? IncomeCashSymbol { get; set; }
        public byte? WithdrawCashSymbol { get; set; }

        public virtual ContragentType Contragent { get; set; }
        public virtual ServiceProvider Provider { get; set; }
        public virtual ICollection<ServiceProvidersContragentsBranch> ServiceProvidersContragentsBranches { get; set; }
        public virtual ICollection<ServiceProvidersContragentsComission> ServiceProvidersContragentsComissions { get; set; }
        public virtual ICollection<ServiceProvidersContragentsContact> ServiceProvidersContragentsContacts { get; set; }
    }
}
