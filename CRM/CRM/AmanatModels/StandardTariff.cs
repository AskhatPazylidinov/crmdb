﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StandardTariff
    {
        public StandardTariff()
        {
            IndividualTariff2s = new HashSet<IndividualTariff2>();
            IndividualTariffValues = new HashSet<IndividualTariffValue>();
            StandardTariffValues = new HashSet<StandardTariffValue>();
            Tariff1s = new HashSet<Tariff1>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool InCount { get; set; }
        public int IncomeOperationType { get; set; }

        public virtual ICollection<IndividualTariff2> IndividualTariff2s { get; set; }
        public virtual ICollection<IndividualTariffValue> IndividualTariffValues { get; set; }
        public virtual ICollection<StandardTariffValue> StandardTariffValues { get; set; }
        public virtual ICollection<Tariff1> Tariff1s { get; set; }
    }
}
