﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeLogView
    {
        public int Id { get; set; }
        public int LogType { get; set; }
        public string EntityName { get; set; }
        public string ContextInfo { get; set; }
        public byte Action { get; set; }
        public int UserId { get; set; }
        public int ChangeLogId { get; set; }
        public string PropertyName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime DateChanged { get; set; }
        public string UserName { get; set; }
        public string Key { get; set; }
    }
}
