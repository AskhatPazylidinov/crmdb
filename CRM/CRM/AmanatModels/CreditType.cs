﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditType
    {
        public CreditType()
        {
            ProductCodes = new HashSet<ProductCode>();
        }

        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductCode> ProductCodes { get; set; }
    }
}
