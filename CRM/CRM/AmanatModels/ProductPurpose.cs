﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductPurpose
    {
        public int ProductId { get; set; }
        public int PurposeTypeId { get; set; }

        public virtual Product1 Product { get; set; }
        public virtual Purpose PurposeType { get; set; }
    }
}
