﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OffshoreTransferWarning
    {
        public int WarningId { get; set; }
        public int? SwiftId { get; set; }
        public int? TelexId { get; set; }
        public int? FastTransferId { get; set; }
        public int? SendCountryId { get; set; }
        public int? RecCountryId { get; set; }
        public int? ProxCountryId { get; set; }
        public int? ContrNationalityCountryId { get; set; }
        public int? ContrRegistrationCountryId { get; set; }
        public int? ContrResidenceCountryId { get; set; }
        public int? ContrBusinessCountryId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual Country2 ContrBusinessCountry { get; set; }
        public virtual Country2 ContrNationalityCountry { get; set; }
        public virtual Country2 ContrRegistrationCountry { get; set; }
        public virtual Country2 ContrResidenceCountry { get; set; }
        public virtual Country2 ProxCountry { get; set; }
        public virtual Country2 RecCountry { get; set; }
        public virtual Country2 SendCountry { get; set; }
    }
}
