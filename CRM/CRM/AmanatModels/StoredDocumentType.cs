﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StoredDocumentType
    {
        public StoredDocumentType()
        {
            StoredDocuments = new HashSet<StoredDocument>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<StoredDocument> StoredDocuments { get; set; }
    }
}
