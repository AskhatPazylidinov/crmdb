﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Workday
    {
        public DateTime Date { get; set; }
        public byte DayTypeId { get; set; }
    }
}
