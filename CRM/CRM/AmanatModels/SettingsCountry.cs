﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCountry
    {
        public int CountryId { get; set; }
        public int CibcountryId { get; set; }

        public virtual Country2 Country { get; set; }
    }
}
