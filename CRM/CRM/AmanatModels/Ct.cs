﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Ct
    {
        public string Tag { get; set; }
        public string ModificationFlag { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}
