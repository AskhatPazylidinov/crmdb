﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankConversionsParticipant
    {
        public int OperationId { get; set; }
        public bool IsDebetOperation { get; set; }
        public string BankCode { get; set; }
        public int CurrencyId { get; set; }
        public decimal OperationSumm { get; set; }
        public DateTime ValueDate { get; set; }
        public byte StatusId { get; set; }

        public virtual SwiftCorrBank BankCodeNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual CrossBankConversion Operation { get; set; }
    }
}
