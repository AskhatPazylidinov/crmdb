﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerManager
    {
        public int CompanyId { get; set; }
        public int PersonId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Position { get; set; }
        public bool IsNominal { get; set; }
        public bool FirstSign { get; set; }

        public virtual Customer Company { get; set; }
        public virtual Customer Person { get; set; }
    }
}
