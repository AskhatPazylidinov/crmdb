﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PensionsFundsSetting
    {
        public int EmployeeId { get; set; }
        public byte PayrollTypeId { get; set; }
        public int PensionFundId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal TransferSumm { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
