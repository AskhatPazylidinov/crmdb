﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalAccountGroupBalance
    {
        public string AccountGroup { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public int SourceCurrencyId { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }
        public decimal DtSumV { get; set; }
        public decimal DtSumN { get; set; }
        public decimal CtSumV { get; set; }
        public decimal CtSumN { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual Currency3 SourceCurrency { get; set; }
    }
}
