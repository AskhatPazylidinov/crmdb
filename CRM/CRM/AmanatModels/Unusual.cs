﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Unusual
    {
        public int KyCode { get; set; }
        public int KyParentcode { get; set; }
        public string FlName { get; set; }
    }
}
