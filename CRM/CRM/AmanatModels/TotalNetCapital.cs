﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TotalNetCapital
    {
        public DateTime ActualDate { get; set; }
        public decimal Capital { get; set; }
    }
}
