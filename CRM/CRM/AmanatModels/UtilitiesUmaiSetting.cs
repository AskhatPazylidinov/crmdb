﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesUmaiSetting
    {
        public int Id { get; set; }
        public bool HasOnlineProcessing { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Signature { get; set; }
        public long UmaiTerminalId { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyAddress { get; set; }
        public int? ProxyPort { get; set; }
        public bool UseProxyAuthorization { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }
        public string HttpAuthorizationCode { get; set; }
    }
}
