﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class VacationStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
