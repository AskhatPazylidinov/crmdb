﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Schedule1
    {
        public Schedule1()
        {
            Position1s = new HashSet<Position1>();
        }

        public int Id { get; set; }
        public int? DepId { get; set; }
        public string Name { get; set; }
        public decimal? Salary { get; set; }
        public int? CurrencyId { get; set; }
        public int? OfficeId { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string CloseReason { get; set; }
        public string OpenReason { get; set; }
        public string Shortname { get; set; }
        public decimal? SalaryMax { get; set; }
        public int? StatusId { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }

        public virtual Department2 Dep { get; set; }
        public virtual ICollection<Position1> Position1s { get; set; }
    }
}
