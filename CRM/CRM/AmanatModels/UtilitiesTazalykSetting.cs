﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesTazalykSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
    }
}
