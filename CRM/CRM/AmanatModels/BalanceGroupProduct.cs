﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BalanceGroupProduct
    {
        public int ProductId { get; set; }
        public string ResidentGroup { get; set; }
        public string NonResidentGroup { get; set; }
        public int TypeAccount { get; set; }

        public virtual Product Product { get; set; }
        public virtual CardsAccountType TypeAccountNavigation { get; set; }
    }
}
