﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocument
    {
        public int DocumentId { get; set; }
        public string DisplayName { get; set; }
        public string FileName { get; set; }
        public bool CanRunOnEmptyCard { get; set; }
        public int? GroupId { get; set; }
        public int? OrientedCustomerTypeId { get; set; }
        public bool? ToPdf { get; set; }

        public virtual ExtraDocumentGroup Group { get; set; }
    }
}
