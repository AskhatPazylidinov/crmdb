﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MortgagesField
    {
        public int PropertyId { get; set; }
        public string IntegrationName { get; set; }

        public virtual Property Property { get; set; }
    }
}
