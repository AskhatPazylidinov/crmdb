﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class WithdrawTariffsByBranch
    {
        public int Id { get; set; }
        public byte CustomerTypeId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public int AccountBranchId { get; set; }
        public decimal AccountBranchComission { get; set; }
        public int AccountBranchComissionCalculationTypeId { get; set; }
        public decimal? AccountBranchMinSumm { get; set; }
        public byte AccountBranchMinSummCurrencyType { get; set; }
        public decimal? AccountBranchMaxSumm { get; set; }
        public byte AccountBranchMaxSummCurrencyType { get; set; }
        public int TransactionBranchId { get; set; }
        public decimal TransactionBranchComission { get; set; }
        public int TransactionBranchComissionCalculationTypeId { get; set; }
        public decimal? TransactionBranchMinSumm { get; set; }
        public byte TransactionBranchMinSummCurrencyType { get; set; }
        public decimal? TransactionBranchMaxSumm { get; set; }
        public byte TransactionBranchMaxSummCurrencyType { get; set; }

        public virtual Branch AccountBranch { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Branch TransactionBranch { get; set; }
    }
}
