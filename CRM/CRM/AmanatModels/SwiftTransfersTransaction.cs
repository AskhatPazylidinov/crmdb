﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfersTransaction
    {
        public long OperationId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual SwiftTransfersOperation Operation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
