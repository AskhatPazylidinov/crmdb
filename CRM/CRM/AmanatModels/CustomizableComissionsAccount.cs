﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomizableComissionsAccount
    {
        public int ComissionId { get; set; }
        public int BranchId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual CustomizableComission Comission { get; set; }
    }
}
