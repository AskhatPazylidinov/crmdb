﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersStatus
    {
        public int CustomerId { get; set; }
        public int StatusCategory { get; set; }
        public int StatusTypeId { get; set; }
        public DateTime StatusDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual CustomersStatusType StatusType { get; set; }
    }
}
