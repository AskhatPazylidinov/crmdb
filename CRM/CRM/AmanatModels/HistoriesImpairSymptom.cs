﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesImpairSymptom
    {
        public int CreditId { get; set; }
        public int ImpairSymptomId { get; set; }
        public DateTime StatusDate { get; set; }
        public int? UserId { get; set; }

        public virtual History Credit { get; set; }
        public virtual ImpairSymptom ImpairSymptom { get; set; }
    }
}
