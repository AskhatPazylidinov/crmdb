﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeGroupIncExp
    {
        public OfficeGroupIncExp()
        {
            OfficeGroupIncExpOperations = new HashSet<OfficeGroupIncExpOperation>();
            OfficeGroupIncExpTransactions = new HashSet<OfficeGroupIncExpTransaction>();
        }

        public int GroupId { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public DateTime CalcDate { get; set; }
        public int StatusId { get; set; }
        public string Comment { get; set; }
        public decimal Value { get; set; }
        public int? TypeId { get; set; }
        public bool IsAvance { get; set; }
        public bool? IsPercent { get; set; }
        public bool? Tax { get; set; }
        public bool? Sfee { get; set; }
        public bool? Sfer { get; set; }
        public int? AccountTypeId { get; set; }
        public int? Period { get; set; }

        public virtual ICollection<OfficeGroupIncExpOperation> OfficeGroupIncExpOperations { get; set; }
        public virtual ICollection<OfficeGroupIncExpTransaction> OfficeGroupIncExpTransactions { get; set; }
    }
}
