﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrderType
    {
        public OrderType()
        {
            Order1s = new HashSet<Order1>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string TemplatePath { get; set; }

        public virtual ICollection<Order1> Order1s { get; set; }
    }
}
