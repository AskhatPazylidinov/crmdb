﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Aliment1
    {
        public int AlimentId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal PercentSumm { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
