﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ScheduledPaymentsLog
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public int PaymentId { get; set; }
        public bool Result { get; set; }
        public long? Position { get; set; }
        public string Comment { get; set; }
        public DateTime? PaymentDate { get; set; }
    }
}
