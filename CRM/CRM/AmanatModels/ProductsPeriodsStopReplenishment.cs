﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsPeriodsStopReplenishment
    {
        public int ProductId { get; set; }
        public int StartPeriod { get; set; }
        public int? EndPeriod { get; set; }
        public byte StopDepositDaysBeforeEnd { get; set; }
    }
}
