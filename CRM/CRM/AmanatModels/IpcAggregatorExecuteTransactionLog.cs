﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcAggregatorExecuteTransactionLog
    {
        public int Id { get; set; }
        public Guid ClientTransactionId { get; set; }
        public Guid? ExternalTransactionId { get; set; }
        public string RecipientName { get; set; }
        public string ProviderTransactionId { get; set; }
        public string CardNo { get; set; }
        public decimal Amount { get; set; }
        public long? WalletOperationId { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime TransactionDate { get; set; }

        public virtual WalletOperation WalletOperation { get; set; }
    }
}
