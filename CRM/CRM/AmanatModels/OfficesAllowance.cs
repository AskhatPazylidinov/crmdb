﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesAllowance
    {
        public int OfficeId { get; set; }
        public int AllowanceTypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal AllowanceSumm { get; set; }

        public virtual AllowancesType AllowanceType { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
