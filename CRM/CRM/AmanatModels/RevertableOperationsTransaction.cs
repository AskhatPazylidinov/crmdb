﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperationsTransaction
    {
        public long RevertId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual RevertableOperation Revert { get; set; }
    }
}
