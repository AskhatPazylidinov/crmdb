﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersOffice
    {
        public int CustomerId { get; set; }
        public int? OfficeId { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
