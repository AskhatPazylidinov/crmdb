﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariffDetailsHistory
    {
        public int Id { get; set; }
        public int TariffId { get; set; }
        public string MainAccountNo { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? TariffCurrencyId { get; set; }
        public int? TariffStart { get; set; }
        public int? TariffEnd { get; set; }
        public decimal? TariffRate { get; set; }
        public bool? IsPercent { get; set; }
    }
}
