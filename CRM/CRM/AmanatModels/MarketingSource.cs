﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MarketingSource
    {
        public MarketingSource()
        {
            Histories = new HashSet<History>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<History> Histories { get; set; }
    }
}
