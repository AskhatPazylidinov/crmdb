﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesMonitoring
    {
        public HistoriesMonitoring()
        {
            HistoriesMonitoringsStatuses = new HashSet<HistoriesMonitoringsStatus>();
        }

        public int MonitoringId { get; set; }
        public int CreditId { get; set; }
        public int TypeId { get; set; }
        public int Mmonth { get; set; }
        public int Myear { get; set; }

        public virtual History Credit { get; set; }
        public virtual MonitoringsType Type { get; set; }
        public virtual ICollection<HistoriesMonitoringsStatus> HistoriesMonitoringsStatuses { get; set; }
    }
}
