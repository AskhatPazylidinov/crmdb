﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComissinEcvayer
    {
        public int CardType { get; set; }
        public int DeviceType { get; set; }
        public byte CardOfBank { get; set; }
        public int OperType { get; set; }
        public int CurrencyId { get; set; }
        public byte ComissionTypeEcvayer { get; set; }
        public decimal ComissionEcvayer { get; set; }
        public byte? MinComissionType { get; set; }
        public decimal? MinComission { get; set; }
        public byte ComissionType { get; set; }

        public virtual CardType CardTypeNavigation { get; set; }
        public virtual DeviceType DeviceTypeNavigation { get; set; }
        public virtual OperationType OperTypeNavigation { get; set; }
    }
}
