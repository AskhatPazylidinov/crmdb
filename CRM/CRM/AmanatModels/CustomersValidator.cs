﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersValidator
    {
        public CustomersValidator()
        {
            CustomersValidatorsObjects = new HashSet<CustomersValidatorsObject>();
        }

        public string ValidatorName { get; set; }
        public byte CustomerTypeId { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomersValidatorsObject> CustomersValidatorsObjects { get; set; }
    }
}
