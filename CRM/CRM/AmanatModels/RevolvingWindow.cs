﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevolvingWindow
    {
        public int CreditId { get; set; }
        public DateTime BillingStartDate { get; set; }
        public DateTime? BillingEndDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
