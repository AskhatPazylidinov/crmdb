﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditSub
    {
        public string Inn { get; set; }
        public int? CustomerId { get; set; }
        public int? CreditId { get; set; }
        public long? Flag { get; set; }
        public DateTime? DateBeg { get; set; }
        public DateTime? DateEnd { get; set; }
        public decimal? SumSub { get; set; }
    }
}
