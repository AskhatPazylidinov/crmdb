﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StartCalculatedFine
    {
        public int CreditId { get; set; }
        public decimal StartFinesSumV { get; set; }

        public virtual History Credit { get; set; }
    }
}
