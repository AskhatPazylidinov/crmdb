﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCreditsOverdue
    {
        public int CurrencyId { get; set; }
        public decimal MinimalOverdueSum { get; set; }
    }
}
