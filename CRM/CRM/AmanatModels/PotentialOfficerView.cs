﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PotentialOfficerView
    {
        public int PotentialId { get; set; }
        public int UserId { get; set; }
        public byte OfficerType { get; set; }
        public int? ParentPotentialId { get; set; }
        public DateTime? DateFrom { get; set; }
        public int OfficeId { get; set; }
        public int BranchId { get; set; }
        public string OfficeName { get; set; }
        public string Fullname { get; set; }
        public string ParentFullname { get; set; }
    }
}
