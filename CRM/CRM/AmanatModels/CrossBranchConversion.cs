﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBranchConversion
    {
        public CrossBranchConversion()
        {
            CrossBranchConversionStatuses = new HashSet<CrossBranchConversionStatus>();
            CrossBranchConversionsSumms = new HashSet<CrossBranchConversionsSumm>();
            CrossBranchConversionsTransactions = new HashSet<CrossBranchConversionsTransaction>();
        }

        public int OperationId { get; set; }
        public DateTime ConversionDate { get; set; }
        public int BranchId { get; set; }
        public decimal ExchangeRate { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ICollection<CrossBranchConversionStatus> CrossBranchConversionStatuses { get; set; }
        public virtual ICollection<CrossBranchConversionsSumm> CrossBranchConversionsSumms { get; set; }
        public virtual ICollection<CrossBranchConversionsTransaction> CrossBranchConversionsTransactions { get; set; }
    }
}
