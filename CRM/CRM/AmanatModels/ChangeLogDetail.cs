﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeLogDetail
    {
        public int Id { get; set; }
        public int ChangeLogId { get; set; }
        public string Property { get; set; }
        public string PropertyName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime DateChanged { get; set; }

        public virtual ChangeLog ChangeLog { get; set; }
    }
}
