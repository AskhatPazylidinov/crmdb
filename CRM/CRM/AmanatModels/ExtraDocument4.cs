﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocument4
    {
        public int ExtraDocumentId { get; set; }
        public string DisplayName { get; set; }
        public string Filename { get; set; }
        public bool CanRunOnEmptyGarantee { get; set; }
        public int? OrientedCustomerTypeId { get; set; }
    }
}
