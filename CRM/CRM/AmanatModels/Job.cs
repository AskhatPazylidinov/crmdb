﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Job
    {
        public Job()
        {
            JobDetails = new HashSet<JobDetail>();
            JobResults = new HashSet<JobResult>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int UserId { get; set; }
        public byte JobState { get; set; }
        public string Result { get; set; }
        public byte Service { get; set; }
        public string ContextKey { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<JobDetail> JobDetails { get; set; }
        public virtual ICollection<JobResult> JobResults { get; set; }
    }
}
