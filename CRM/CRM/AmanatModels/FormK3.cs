﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FormK3
    {
        public int Type { get; set; }
        public string BalanceGroup { get; set; }
        public int DaysCount { get; set; }
        public byte TypeK3 { get; set; }

        public virtual TypeK3 TypeNavigation { get; set; }
    }
}
