﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SendLog
    {
        public int LogId { get; set; }
        public DateTime LogDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public string SendFileName { get; set; }
        public string ResponseFileName { get; set; }

        public virtual User User { get; set; }
    }
}
