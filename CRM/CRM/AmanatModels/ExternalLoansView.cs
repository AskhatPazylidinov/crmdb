﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalLoansView
    {
        public int ProgramId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int Period { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal AvailableSumV { get; set; }
        public decimal? InterestRate { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int CustomerId { get; set; }
        public string AgreementNo { get; set; }
    }
}
