﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DuplicateCustomersView
    {
        public string SelectIndex { get; set; }
        public int CustomerId { get; set; }
        public int DuplicateCustomerId { get; set; }
        public bool IsIdentificationNumberMatch { get; set; }
        public bool IsCustomerNameMatch { get; set; }
        public bool IsDocumentMatch { get; set; }
        public bool IsDateOfBirthMatch { get; set; }
        public int TotalMatch { get; set; }
        public string IdentificationNumber { get; set; }
        public string DuplicateIdentificationNumber { get; set; }
        public string Document { get; set; }
        public string DuplicateDocument { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DuplicateDateOfBirth { get; set; }
        public string CustomerName { get; set; }
        public string DuplicateCustomerName { get; set; }
    }
}
