﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Agent
    {
        public Agent()
        {
            AgentsBranches = new HashSet<AgentsBranch>();
            InterBranchTransfersLimits = new HashSet<InterBranchTransfersLimit>();
            InterTransferReceiverAgents = new HashSet<InterTransfer>();
            InterTransferSenderAgents = new HashSet<InterTransfer>();
        }

        public int AgentId { get; set; }
        public string AgentName { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<AgentsBranch> AgentsBranches { get; set; }
        public virtual ICollection<InterBranchTransfersLimit> InterBranchTransfersLimits { get; set; }
        public virtual ICollection<InterTransfer> InterTransferReceiverAgents { get; set; }
        public virtual ICollection<InterTransfer> InterTransferSenderAgents { get; set; }
    }
}
