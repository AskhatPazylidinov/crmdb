﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersInfo
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? ResidenceCountryId { get; set; }
        public int? ResidenceCityId { get; set; }
        public string ResidenceCityName { get; set; }
        public string ResidenceStreet { get; set; }
        public string ResidenceHouse { get; set; }
        public string ResidenceHousing { get; set; }
        public string ResidenceBuilding { get; set; }
        public string ResidenceFlat { get; set; }
        public bool? IsOwnerResidence { get; set; }
        public int CreditId { get; set; }
        public string Rating { get; set; }
        public string Branch { get; set; }
        public string Office { get; set; }
        public string Officer { get; set; }
        public string AccountNo { get; set; }
        public decimal? Sum { get; set; }
        public string Currency { get; set; }
        public byte Status { get; set; }
        public DateTime? IssueDate { get; set; }
        public int? SubStatus { get; set; }
        public string SubStatusName { get; set; }
        public DateTime StatusDate { get; set; }
        public string GroupName { get; set; }
        public int? Cycle { get; set; }
        public int? RejectType { get; set; }
        public string RejectReason { get; set; }
        public DateTime? RejectDate { get; set; }
    }
}
