﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialBranchAccount
    {
        public int AccountId { get; set; }
        public int BranchId { get; set; }
        public int AccountTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string OdbaccountNo { get; set; }
    }
}
