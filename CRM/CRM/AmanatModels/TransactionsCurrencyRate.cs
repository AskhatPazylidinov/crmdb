﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsCurrencyRate
    {
        public long Position { get; set; }
        public decimal CurrencyRate { get; set; }
    }
}
