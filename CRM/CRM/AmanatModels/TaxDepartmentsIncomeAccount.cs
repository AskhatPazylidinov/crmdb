﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDepartmentsIncomeAccount
    {
        public int OfficeId { get; set; }
        public string IncomeAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int Id { get; set; }
        public int? TaxDepartmentId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
