﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SavingAccount1
    {
        public int BranchId { get; set; }
        public string OdbaccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int AccountId { get; set; }
    }
}
