﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchCode
    {
        public int BranchId { get; set; }
        public string Code { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
