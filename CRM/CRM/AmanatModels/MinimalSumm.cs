﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MinimalSumm
    {
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal MinSumm { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
