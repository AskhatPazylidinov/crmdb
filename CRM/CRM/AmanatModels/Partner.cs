﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Partner
    {
        public int CustomerId { get; set; }
        public string PartnerName { get; set; }
        public string CityName { get; set; }
        public int? CountryCode { get; set; }
    }
}
