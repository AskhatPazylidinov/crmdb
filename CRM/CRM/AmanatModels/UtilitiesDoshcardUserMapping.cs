﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesDoshcardUserMapping
    {
        public int UserId { get; set; }
        public string DoshcardLogin { get; set; }
        public string DoshcardPassword { get; set; }

        public virtual User User { get; set; }
    }
}
