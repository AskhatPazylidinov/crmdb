﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferOffice
    {
        public int OldOfficeId { get; set; }
        public int NewOfficeId { get; set; }
        public DateTime MigrateDate { get; set; }

        public virtual Office1 NewOffice { get; set; }
        public virtual Office1 OldOffice { get; set; }
    }
}
