﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnersBalanceGroup
    {
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
