﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsAccount
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int TypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal SumV { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual History Credit { get; set; }
    }
}
