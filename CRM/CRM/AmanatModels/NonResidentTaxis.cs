﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonResidentTaxis
    {
        public NonResidentTaxis()
        {
            NonResidentTaxIncExpTypes = new HashSet<NonResidentTaxIncExpType>();
            NonResidentTaxSettings = new HashSet<NonResidentTaxSetting>();
        }

        public int TaxId { get; set; }
        public string FiscalName { get; set; }
        public int CurrencyId { get; set; }
        public string DepositAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<NonResidentTaxIncExpType> NonResidentTaxIncExpTypes { get; set; }
        public virtual ICollection<NonResidentTaxSetting> NonResidentTaxSettings { get; set; }
    }
}
