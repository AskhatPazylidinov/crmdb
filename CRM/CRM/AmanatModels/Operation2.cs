﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Operation2
    {
        public long Position { get; set; }
        public short OperationType { get; set; }
        public int UserId { get; set; }
        public DateTime VerifiedDate { get; set; }
        public int OfficeId { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
