﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RefOper1
    {
        public RefOper1()
        {
            Swift103Transfers = new HashSet<Swift103Transfer>();
        }

        public string Code { get; set; }
        public string DebetCode { get; set; }
        public string CreditCode { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Swift103Transfer> Swift103Transfers { get; set; }
    }
}
