﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnersContract
    {
        public int CompanyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AgreementNo { get; set; }

        public virtual Partner2 Company { get; set; }
    }
}
