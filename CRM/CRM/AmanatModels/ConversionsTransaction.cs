﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionsTransaction
    {
        public int ConversionId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Conversion Conversion { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
