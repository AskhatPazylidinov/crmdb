﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EnrollmentsView
    {
        public string OfficeName { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public decimal SumV { get; set; }
        public string CustomerName { get; set; }
        public string Comment { get; set; }
        public long Position { get; set; }
        public bool? Checks { get; set; }
        public string Id { get; set; }
        public DateTime TransactionDate { get; set; }
        public int BranchId { get; set; }
        public string Currency { get; set; }
        public int OperationType { get; set; }
    }
}
