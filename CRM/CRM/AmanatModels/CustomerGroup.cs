﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerGroup
    {
        public int CustomerId { get; set; }
        public int GroupId { get; set; }
    }
}
