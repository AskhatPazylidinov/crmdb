﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class KibuniqueId
    {
        public string UniqueIdflex { get; set; }
        public int CreditId { get; set; }
        public int TranchId { get; set; }
    }
}
