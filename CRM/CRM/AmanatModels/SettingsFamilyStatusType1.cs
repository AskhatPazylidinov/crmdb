﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsFamilyStatusType1
    {
        public int FamilyTypeId { get; set; }
        public int CibfamilyTypeId { get; set; }

        public virtual FamilyStatusType FamilyType { get; set; }
    }
}
