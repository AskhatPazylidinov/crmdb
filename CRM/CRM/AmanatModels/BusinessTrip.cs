﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BusinessTrip
    {
        public BusinessTrip()
        {
            BusinessTripDates = new HashSet<BusinessTripDate>();
        }

        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string Reason { get; set; }
        public int? OrderId { get; set; }
        public int? IncomeId { get; set; }
        public string OrderPdf { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public int? StatusId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? StatusUserId { get; set; }
        public string Place { get; set; }
        public string ResponisibleEmployee { get; set; }
        public int? ResponisibleEmployeeId { get; set; }
        public decimal? AddSalaryPercent { get; set; }
        public decimal? AddSalarySumm { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual ICollection<BusinessTripDate> BusinessTripDates { get; set; }
    }
}
