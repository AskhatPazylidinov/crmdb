﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency1
    {
        public int ProductId { get; set; }
        public int CurencyId { get; set; }

        public virtual Currency3 Curency { get; set; }
        public virtual Product Product { get; set; }
    }
}
