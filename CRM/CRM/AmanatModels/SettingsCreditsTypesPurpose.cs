﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCreditsTypesPurpose
    {
        public int ProductId { get; set; }
        public int CibpurposeTypeId { get; set; }
    }
}
