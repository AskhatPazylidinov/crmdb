﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrganizationCorrAccount
    {
        public string CorrAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? BranchId { get; set; }
        public int CorrAccountTypeId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Account1 C { get; set; }
    }
}
