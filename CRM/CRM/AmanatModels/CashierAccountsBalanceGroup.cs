﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashierAccountsBalanceGroup
    {
        public int OfficeTypeId { get; set; }
        public int CurrencyTypeId { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
