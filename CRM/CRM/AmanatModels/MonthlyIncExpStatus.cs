﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MonthlyIncExpStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
