﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankConversionsTransaction
    {
        public int OperationId { get; set; }
        public byte ValueDateId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual CrossBankConversion Operation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
