﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PreviousTransactionDate
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime LastTransactionDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
