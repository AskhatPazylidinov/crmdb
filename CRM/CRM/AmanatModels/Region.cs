﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Region
    {
        public string RegionId { get; set; }
        public string RegionDesc { get; set; }
        public string CountryCode { get; set; }
    }
}
