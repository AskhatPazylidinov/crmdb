﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsMonitoringsSetting1
    {
        public int GaranteeTypeId { get; set; }
        public int MonitoringTypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int Interval { get; set; }
        public int? MaxMonitorings { get; set; }

        public virtual GarantType GaranteeType { get; set; }
        public virtual MonitoringsType1 MonitoringType { get; set; }
    }
}
