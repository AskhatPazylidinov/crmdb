﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsNightCashDocument
    {
        public int NightDocumentNo { get; set; }
        public DateTime GenerateDateTime { get; set; }
    }
}
