﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchReservedAccountsBalanceGroup
    {
        public int ReservedAccountTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountGroup { get; set; }

        public virtual AccountGroup AccountGroupNavigation { get; set; }
    }
}
