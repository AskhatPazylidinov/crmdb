﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesTable
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public decimal? WorkedHours { get; set; }
        public decimal? Overflow { get; set; }
        public string Comment { get; set; }
        public bool? Approved { get; set; }
        public byte? TypeId { get; set; }
        public string Marker { get; set; }
        public bool? Fixed { get; set; }
        public byte? DayTypeId { get; set; }
        public int? BranchId { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
