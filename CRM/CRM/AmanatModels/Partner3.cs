﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Partner3
    {
        public Partner3()
        {
            PartnersAccount1s = new HashSet<PartnersAccount1>();
            PayBalance1s = new HashSet<PayBalance1>();
            PayBalanceTemps = new HashSet<PayBalanceTemp>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
        }

        public int Id { get; set; }
        public int Code { get; set; }
        public string PartnerName { get; set; }
        public string CityName { get; set; }
        public int? CountryCode { get; set; }

        public virtual ICollection<PartnersAccount1> PartnersAccount1s { get; set; }
        public virtual ICollection<PayBalance1> PayBalance1s { get; set; }
        public virtual ICollection<PayBalanceTemp> PayBalanceTemps { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
    }
}
