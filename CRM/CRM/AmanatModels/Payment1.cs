﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Payment1
    {
        public Payment1()
        {
            ChangeHistories = new HashSet<ChangeHistory>();
            PaymentsOperations = new HashSet<PaymentsOperation>();
        }

        public long PaymentId { get; set; }
        public DateTime DateP { get; set; }
        public byte? PaymentTypeId { get; set; }
        public byte PaymentDirectionId { get; set; }
        public int? CustomerId { get; set; }
        public string SenderFullName { get; set; }
        public string ReceiverFullName { get; set; }
        public string SenderBik { get; set; }
        public string ReceiverBik { get; set; }
        public string SenderAccountNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public string PaymentComment { get; set; }
        public string PaymentCode { get; set; }
        public decimal TransferSumm { get; set; }
        public string Pnumber { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentFileRef { get; set; }
        public int? Format { get; set; }
        public byte? SendingTypeId { get; set; }
        public bool IsUnclarifiedPayment { get; set; }

        public virtual InputPaymentSenderInfo InputPaymentSenderInfo { get; set; }
        public virtual ICollection<ChangeHistory> ChangeHistories { get; set; }
        public virtual ICollection<PaymentsOperation> PaymentsOperations { get; set; }
    }
}
