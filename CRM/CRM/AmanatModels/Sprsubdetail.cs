﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Sprsubdetail
    {
        public string Bic { get; set; }
        public int? RegionId { get; set; }
        public string CorrNbkf { get; set; }
    }
}
