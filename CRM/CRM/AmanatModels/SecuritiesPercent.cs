﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesPercent
    {
        public int SecurityCustomerId { get; set; }
        public DateTime PercentDate { get; set; }
        public decimal SumV { get; set; }

        public virtual SecuritiesCustomer SecurityCustomer { get; set; }
    }
}
