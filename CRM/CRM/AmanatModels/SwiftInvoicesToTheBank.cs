﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftInvoicesToTheBank
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int CountryId { get; set; }
        public int CurrencyId { get; set; }
        public decimal ComissionSumm { get; set; }
        public string BankCode { get; set; }

        public virtual Country2 Country { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
