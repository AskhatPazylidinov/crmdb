﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesCustomer
    {
        public int CreditId { get; set; }
        public int CustomerId { get; set; }
        public bool IsLeader { get; set; }
        public decimal RequestSumm { get; set; }
        public string CreditPurpose { get; set; }
        public decimal? ApprovedSumm { get; set; }
        public int? HistoryObjectId { get; set; }

        public virtual History Credit { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
