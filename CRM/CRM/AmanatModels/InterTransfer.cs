﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterTransfer
    {
        public InterTransfer()
        {
            ReceiverChangeLogs = new HashSet<ReceiverChangeLog>();
            TransfersOperations = new HashSet<TransfersOperation>();
        }

        public long TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public string PaymentCode { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrencyId { get; set; }
        public decimal TotalComission { get; set; }
        public decimal SenderComission { get; set; }
        public decimal BankComission { get; set; }
        public string PaymentComment { get; set; }
        public int SenderCustomerId { get; set; }
        public int? SenderAttorneyId { get; set; }
        public int ReceiverCustomerId { get; set; }
        public string SenderAccountNo { get; set; }
        public bool? IsCashDeposit { get; set; }
        public string ReceiverAccountNo { get; set; }
        public int SenderBranchId { get; set; }
        public int? ReceiverBranchId { get; set; }
        public int? SenderAgentId { get; set; }
        public int? ReceiverAgentId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
        public virtual Agent ReceiverAgent { get; set; }
        public virtual Branch ReceiverBranch { get; set; }
        public virtual Customer ReceiverCustomer { get; set; }
        public virtual Agent SenderAgent { get; set; }
        public virtual Customer SenderAttorney { get; set; }
        public virtual Branch SenderBranch { get; set; }
        public virtual Customer SenderCustomer { get; set; }
        public virtual Currency3 TransferCurrency { get; set; }
        public virtual InterBranchMarketingSource InterBranchMarketingSource { get; set; }
        public virtual InterTransfersVerification InterTransfersVerification { get; set; }
        public virtual ICollection<ReceiverChangeLog> ReceiverChangeLogs { get; set; }
        public virtual ICollection<TransfersOperation> TransfersOperations { get; set; }
    }
}
