﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Reserf
    {
        public int CreditId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime CalcDate { get; set; }
        public byte TypeId { get; set; }
        public int ReserveTypeId { get; set; }
        public decimal ReserveSumV { get; set; }
        public bool? IsError { get; set; }
        public string ReserveReason { get; set; }
        public decimal ReserveRate { get; set; }

        public virtual History Credit { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual ReservesType ReserveType { get; set; }
    }
}
