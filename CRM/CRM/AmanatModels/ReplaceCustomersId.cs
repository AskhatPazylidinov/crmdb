﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplaceCustomersId
    {
        public int OldCustomerId { get; set; }
        public int NewCustomerId { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
