﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ContragentType
    {
        public ContragentType()
        {
            ServiceProvidersContragents = new HashSet<ServiceProvidersContragent>();
            UtilitiesContragentPayments = new HashSet<UtilitiesContragentPayment>();
            UtilitiesElsomPayments = new HashSet<UtilitiesElsomPayment>();
            UtilitiesEpayPayments = new HashSet<UtilitiesEpayPayment>();
            UtilitiesMobilnikPayments = new HashSet<UtilitiesMobilnikPayment>();
            UtilitiesQiwiPayments = new HashSet<UtilitiesQiwiPayment>();
            UtilitiesQuickPayPayments = new HashSet<UtilitiesQuickPayPayment>();
            UtilitiesUmaiPayments = new HashSet<UtilitiesUmaiPayment>();
        }

        public int ContragentTypeId { get; set; }
        public string Typename { get; set; }
        public string LogoFileName { get; set; }
        public int? CategoryId { get; set; }
        public string Placeholder { get; set; }
        public string InputValidate { get; set; }
        public int InputType { get; set; }
        public DateTime ModifyDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public int DisplayPriority { get; set; }

        public virtual UtilitiesCategory Category { get; set; }
        public virtual ICollection<ServiceProvidersContragent> ServiceProvidersContragents { get; set; }
        public virtual ICollection<UtilitiesContragentPayment> UtilitiesContragentPayments { get; set; }
        public virtual ICollection<UtilitiesElsomPayment> UtilitiesElsomPayments { get; set; }
        public virtual ICollection<UtilitiesEpayPayment> UtilitiesEpayPayments { get; set; }
        public virtual ICollection<UtilitiesMobilnikPayment> UtilitiesMobilnikPayments { get; set; }
        public virtual ICollection<UtilitiesQiwiPayment> UtilitiesQiwiPayments { get; set; }
        public virtual ICollection<UtilitiesQuickPayPayment> UtilitiesQuickPayPayments { get; set; }
        public virtual ICollection<UtilitiesUmaiPayment> UtilitiesUmaiPayments { get; set; }
    }
}
