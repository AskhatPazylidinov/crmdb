﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesCustomer
    {
        public SecuritiesCustomer()
        {
            SecuritiesPercents = new HashSet<SecuritiesPercent>();
        }

        public int SecurityCustomerId { get; set; }
        public int SecurityId { get; set; }
        public int CustomerId { get; set; }
        public decimal InvestmentSumm { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TransferAccountNo { get; set; }
        public int? TransferAccountCurrencyId { get; set; }
        public int SecuritiesQuantity { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Security Security { get; set; }
        public virtual SecuritiesAccount SecuritiesAccount { get; set; }
        public virtual ICollection<SecuritiesPercent> SecuritiesPercents { get; set; }
    }
}
