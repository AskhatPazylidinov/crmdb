﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafeOverduesTariff
    {
        public DateTime ChangeDate { get; set; }
        public int SafeTypeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public decimal FineSummPerDay { get; set; }

        public virtual SafeTariff SafeTariff { get; set; }
    }
}
