﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftComission
    {
        public SwiftComission()
        {
            IndividualTariff4s = new HashSet<IndividualTariff4>();
            SwiftAccounts = new HashSet<SwiftAccount>();
            SwiftTransfersComissions = new HashSet<SwiftTransfersComission>();
            Tariff4s = new HashSet<Tariff4>();
        }

        public int ComissionId { get; set; }
        public int SystemId { get; set; }
        public string ComissionName { get; set; }
        public string ComissionComment { get; set; }
        public byte? ComissionTypeId { get; set; }

        public virtual ICollection<IndividualTariff4> IndividualTariff4s { get; set; }
        public virtual ICollection<SwiftAccount> SwiftAccounts { get; set; }
        public virtual ICollection<SwiftTransfersComission> SwiftTransfersComissions { get; set; }
        public virtual ICollection<Tariff4> Tariff4s { get; set; }
    }
}
