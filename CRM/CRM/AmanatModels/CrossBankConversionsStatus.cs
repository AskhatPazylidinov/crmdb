﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankConversionsStatus
    {
        public int OperationId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual CrossBankConversion Operation { get; set; }
        public virtual User User { get; set; }
    }
}
