﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class WriteOffContrAccount
    {
        public int BranchId { get; set; }
        public byte AccountTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
