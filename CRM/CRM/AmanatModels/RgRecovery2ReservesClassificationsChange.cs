﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RgRecovery2ReservesClassificationsChange
    {
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
    }
}
