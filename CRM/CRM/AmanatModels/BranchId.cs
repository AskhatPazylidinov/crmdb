﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchId
    {
        public int BranchId1 { get; set; }
        public int ExternalBranchId { get; set; }
        public int ExternalCustomerId { get; set; }

        public virtual Branch BranchId1Navigation { get; set; }
    }
}
