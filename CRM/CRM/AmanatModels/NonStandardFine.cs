﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonStandardFine
    {
        public int CreditId { get; set; }

        public virtual History Credit { get; set; }
    }
}
