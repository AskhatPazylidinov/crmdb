﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionTransferCode
    {
        public int Id { get; set; }
        public string SenderBranchCode { get; set; }
        public string ReceiverBranchCode { get; set; }
    }
}
