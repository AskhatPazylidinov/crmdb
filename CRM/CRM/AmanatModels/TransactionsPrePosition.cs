﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsPrePosition
    {
        public long Position { get; set; }
        public DateTime GenerateDatetime { get; set; }
    }
}
