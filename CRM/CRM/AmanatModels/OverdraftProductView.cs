﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OverdraftProductView
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
