﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class K3depositAllowedView
    {
        public int DepositProductId { get; set; }
        public byte K3type { get; set; }
        public string Name { get; set; }
    }
}
