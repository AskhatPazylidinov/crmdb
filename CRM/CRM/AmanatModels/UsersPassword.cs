﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersPassword
    {
        public int UserId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string Password { get; set; }

        public virtual User User { get; set; }
    }
}
