﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DiplomaType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
