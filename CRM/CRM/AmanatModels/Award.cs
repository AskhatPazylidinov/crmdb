﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Award
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public string DocType { get; set; }
        public string Type { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
