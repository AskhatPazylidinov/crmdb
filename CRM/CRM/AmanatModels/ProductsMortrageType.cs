﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsMortrageType
    {
        public int ProductId { get; set; }
        public int MortrageTypeId { get; set; }

        public virtual MortrageType MortrageType { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
