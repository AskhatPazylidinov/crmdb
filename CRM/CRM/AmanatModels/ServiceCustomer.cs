﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceCustomer
    {
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public int? EnableUserId { get; set; }
        public int? DisableUserId { get; set; }
        public string AccountNo { get; set; }
        public int? CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual BankService1 ServiceType { get; set; }
    }
}
