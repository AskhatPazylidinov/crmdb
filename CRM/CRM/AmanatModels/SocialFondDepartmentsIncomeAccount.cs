﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondDepartmentsIncomeAccount
    {
        public int Id { get; set; }
        public int OfficeId { get; set; }
        public int? SocialFondDepartmentId { get; set; }
        public string IncomeAccountNo { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
