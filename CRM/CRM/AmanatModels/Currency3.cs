﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency3
    {
        public Currency3()
        {
            Account1s = new HashSet<Account1>();
            AccountGroupBalanceCurrencies = new HashSet<AccountGroupBalance>();
            AccountGroupBalanceSourceCurrencies = new HashSet<AccountGroupBalance>();
            AccountsSourceCurrencies = new HashSet<AccountsSourceCurrency>();
            AllowedViewCompanies = new HashSet<AllowedViewCompany>();
            BalanceGroupsBalances = new HashSet<BalanceGroupsBalance>();
            CardsRegistries = new HashSet<CardsRegistry>();
            CashOfficesLimits = new HashSet<CashOfficesLimit>();
            ComplianceExchangeLimits = new HashSet<ComplianceExchangeLimit>();
            Contracts = new HashSet<Contract>();
            CrossBankConversionsParticipants = new HashSet<CrossBankConversionsParticipant>();
            CrossBranchConversionsSumms = new HashSet<CrossBranchConversionsSumm>();
            Currency1s = new HashSet<Currency1>();
            Currency2s = new HashSet<Currency2>();
            CurrencyRateCrossCurrencies = new HashSet<CurrencyRate>();
            CurrencyRateCurrencies = new HashSet<CurrencyRate>();
            CurrencyRatesHistoryCrossCurrencies = new HashSet<CurrencyRatesHistory>();
            CurrencyRatesHistoryCurrencies = new HashSet<CurrencyRatesHistory>();
            CustomerExtraWorkCurrencies = new HashSet<Customer>();
            CustomerWorkCurrencies = new HashSet<Customer>();
            Denominations = new HashSet<Denomination>();
            DepositBonusesComissions = new HashSet<DepositBonusesComission>();
            EditApprovedParamsLimitsForUsers = new HashSet<EditApprovedParamsLimitsForUser>();
            EditApprovedParamsLimitsOnRoles = new HashSet<EditApprovedParamsLimitsOnRole>();
            Emissions = new HashSet<Emission>();
            EstimatedMonthTurnovers = new HashSet<EstimatedMonthTurnover>();
            GaranteesMarketPrices = new HashSet<GaranteesMarketPrice>();
            Histories = new HashSet<History>();
            IncomesStructures = new HashSet<IncomesStructure>();
            IndividualTariff2s = new HashSet<IndividualTariff2>();
            IndividualTariff3s = new HashSet<IndividualTariff3>();
            IndividualTariff4s = new HashSet<IndividualTariff4>();
            InterBranchTransfersLimits = new HashSet<InterBranchTransfersLimit>();
            InterTransfers = new HashSet<InterTransfer>();
            IssueLoanLimitsForUsers = new HashSet<IssueLoanLimitsForUser>();
            IssueLoanLimitsOnRoles = new HashSet<IssueLoanLimitsOnRole>();
            Limit1s = new HashSet<Limit1>();
            Limits = new HashSet<Limit>();
            MarketRateCrossCurrencies = new HashSet<MarketRate>();
            MarketRateCurrencies = new HashSet<MarketRate>();
            MoneyTransfers = new HashSet<MoneyTransfer>();
            NettingOperations = new HashSet<NettingOperation>();
            Odbaccounts = new HashSet<Odbaccount>();
            OfficesExchangeRates = new HashSet<OfficesExchangeRate>();
            OfficesExchangeRatesHistories = new HashSet<OfficesExchangeRatesHistory>();
            OfficesFlowOperations = new HashSet<OfficesFlowOperation>();
            OfficesPlannedIssuings = new HashSet<OfficesPlannedIssuing>();
            OnlineOperatorsWithdrawTariffs = new HashSet<OnlineOperatorsWithdrawTariff>();
            OperationalAccountGroupBalanceCurrencies = new HashSet<OperationalAccountGroupBalance>();
            OperationalAccountGroupBalanceSourceCurrencies = new HashSet<OperationalAccountGroupBalance>();
            OperationalBalanceGroupBalances = new HashSet<OperationalBalanceGroupBalance>();
            OperationalReserves = new HashSet<OperationalReserf>();
            OrtCurrencyReserves = new HashSet<OrtCurrencyReserf>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
            PaysheetsEmployees = new HashSet<PaysheetsEmployee>();
            PlanningInFlows = new HashSet<PlanningInFlow>();
            PositionAccountPairs = new HashSet<PositionAccountPair>();
            ProductBonusesOnCooperationDates = new HashSet<ProductBonusesOnCooperationDate>();
            ProductsCurrencies = new HashSet<ProductsCurrency>();
            ProductsCurrenciesReplenishmentLimits = new HashSet<ProductsCurrenciesReplenishmentLimit>();
            ProductsCurrenciesWithdrawTariffs = new HashSet<ProductsCurrenciesWithdrawTariff>();
            ProductsFines = new HashSet<ProductsFine>();
            ProductsInterests = new HashSet<ProductsInterest>();
            ProductsIssueComissions = new HashSet<ProductsIssueComission>();
            ReplenishmentIndividualTariffs = new HashSet<ReplenishmentIndividualTariff>();
            ReplenishmentTariffs = new HashSet<ReplenishmentTariff>();
            Reserves = new HashSet<Reserf>();
            ReservesClassificationsSettings = new HashSet<ReservesClassificationsSetting>();
            RoleLimits = new HashSet<RoleLimit>();
            Salaries = new HashSet<Salary>();
            Securities = new HashSet<Security>();
            SecuritiesAccounts = new HashSet<SecuritiesAccount>();
            SecuritiesMortrages = new HashSet<SecuritiesMortrage>();
            StandardWithdrawTariffs = new HashSet<StandardWithdrawTariff>();
            StorageTransfers = new HashSet<StorageTransfer>();
            SubStatusesLimitsForUsers = new HashSet<SubStatusesLimitsForUser>();
            SubStatusesLimitsOnRoles = new HashSet<SubStatusesLimitsOnRole>();
            SubStatusesRequestsLimitsForUsers = new HashSet<SubStatusesRequestsLimitsForUser>();
            SubStatusesRequestsLimitsOnRoles = new HashSet<SubStatusesRequestsLimitsOnRole>();
            Swift103Transfers = new HashSet<Swift103Transfer>();
            Swift202Transfers = new HashSet<Swift202Transfer>();
            Swift300TransferCurrencyBoughts = new HashSet<Swift300Transfer>();
            Swift300TransferCurrencySolds = new HashSet<Swift300Transfer>();
            SwiftCorrBankAccounts = new HashSet<SwiftCorrBankAccount>();
            SwiftInvoicesToTheBanks = new HashSet<SwiftInvoicesToTheBank>();
            Tariff1s = new HashSet<Tariff1>();
            Tariff2s = new HashSet<Tariff2>();
            Tariff3s = new HashSet<Tariff3>();
            Tariff4s = new HashSet<Tariff4>();
            TrainingCourses = new HashSet<TrainingCourse>();
            TranchesLimits = new HashSet<TranchesLimit>();
            TransactionTemplateOperationCurrencies = new HashSet<TransactionTemplateOperation>();
            TransactionTemplateOperationTransactionTemplates = new HashSet<TransactionTemplateOperation>();
            TransactionTemplates = new HashSet<TransactionTemplate>();
            Transfers = new HashSet<Transfer>();
            UserLimits = new HashSet<UserLimit>();
            WithdrawTariffsByBranches = new HashSet<WithdrawTariffsByBranch>();
        }

        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string Symbol { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string NameC1 { get; set; }
        public string NameC2 { get; set; }
        public string NameC3 { get; set; }
        public int? CountryId { get; set; }
        public byte GenderTypeId { get; set; }
        public string Prbocode { get; set; }
        public bool? IsUseByCurrencyRate { get; set; }

        public virtual Country2 Country { get; set; }
        public virtual MinimalOverduePercentsSumm MinimalOverduePercentsSumm { get; set; }
        public virtual SettingsCurrency SettingsCurrency { get; set; }
        public virtual ICollection<Account1> Account1s { get; set; }
        public virtual ICollection<AccountGroupBalance> AccountGroupBalanceCurrencies { get; set; }
        public virtual ICollection<AccountGroupBalance> AccountGroupBalanceSourceCurrencies { get; set; }
        public virtual ICollection<AccountsSourceCurrency> AccountsSourceCurrencies { get; set; }
        public virtual ICollection<AllowedViewCompany> AllowedViewCompanies { get; set; }
        public virtual ICollection<BalanceGroupsBalance> BalanceGroupsBalances { get; set; }
        public virtual ICollection<CardsRegistry> CardsRegistries { get; set; }
        public virtual ICollection<CashOfficesLimit> CashOfficesLimits { get; set; }
        public virtual ICollection<ComplianceExchangeLimit> ComplianceExchangeLimits { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
        public virtual ICollection<CrossBankConversionsParticipant> CrossBankConversionsParticipants { get; set; }
        public virtual ICollection<CrossBranchConversionsSumm> CrossBranchConversionsSumms { get; set; }
        public virtual ICollection<Currency1> Currency1s { get; set; }
        public virtual ICollection<Currency2> Currency2s { get; set; }
        public virtual ICollection<CurrencyRate> CurrencyRateCrossCurrencies { get; set; }
        public virtual ICollection<CurrencyRate> CurrencyRateCurrencies { get; set; }
        public virtual ICollection<CurrencyRatesHistory> CurrencyRatesHistoryCrossCurrencies { get; set; }
        public virtual ICollection<CurrencyRatesHistory> CurrencyRatesHistoryCurrencies { get; set; }
        public virtual ICollection<Customer> CustomerExtraWorkCurrencies { get; set; }
        public virtual ICollection<Customer> CustomerWorkCurrencies { get; set; }
        public virtual ICollection<Denomination> Denominations { get; set; }
        public virtual ICollection<DepositBonusesComission> DepositBonusesComissions { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsForUser> EditApprovedParamsLimitsForUsers { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsOnRole> EditApprovedParamsLimitsOnRoles { get; set; }
        public virtual ICollection<Emission> Emissions { get; set; }
        public virtual ICollection<EstimatedMonthTurnover> EstimatedMonthTurnovers { get; set; }
        public virtual ICollection<GaranteesMarketPrice> GaranteesMarketPrices { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<IncomesStructure> IncomesStructures { get; set; }
        public virtual ICollection<IndividualTariff2> IndividualTariff2s { get; set; }
        public virtual ICollection<IndividualTariff3> IndividualTariff3s { get; set; }
        public virtual ICollection<IndividualTariff4> IndividualTariff4s { get; set; }
        public virtual ICollection<InterBranchTransfersLimit> InterBranchTransfersLimits { get; set; }
        public virtual ICollection<InterTransfer> InterTransfers { get; set; }
        public virtual ICollection<IssueLoanLimitsForUser> IssueLoanLimitsForUsers { get; set; }
        public virtual ICollection<IssueLoanLimitsOnRole> IssueLoanLimitsOnRoles { get; set; }
        public virtual ICollection<Limit1> Limit1s { get; set; }
        public virtual ICollection<Limit> Limits { get; set; }
        public virtual ICollection<MarketRate> MarketRateCrossCurrencies { get; set; }
        public virtual ICollection<MarketRate> MarketRateCurrencies { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransfers { get; set; }
        public virtual ICollection<NettingOperation> NettingOperations { get; set; }
        public virtual ICollection<Odbaccount> Odbaccounts { get; set; }
        public virtual ICollection<OfficesExchangeRate> OfficesExchangeRates { get; set; }
        public virtual ICollection<OfficesExchangeRatesHistory> OfficesExchangeRatesHistories { get; set; }
        public virtual ICollection<OfficesFlowOperation> OfficesFlowOperations { get; set; }
        public virtual ICollection<OfficesPlannedIssuing> OfficesPlannedIssuings { get; set; }
        public virtual ICollection<OnlineOperatorsWithdrawTariff> OnlineOperatorsWithdrawTariffs { get; set; }
        public virtual ICollection<OperationalAccountGroupBalance> OperationalAccountGroupBalanceCurrencies { get; set; }
        public virtual ICollection<OperationalAccountGroupBalance> OperationalAccountGroupBalanceSourceCurrencies { get; set; }
        public virtual ICollection<OperationalBalanceGroupBalance> OperationalBalanceGroupBalances { get; set; }
        public virtual ICollection<OperationalReserf> OperationalReserves { get; set; }
        public virtual ICollection<OrtCurrencyReserf> OrtCurrencyReserves { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
        public virtual ICollection<PaysheetsEmployee> PaysheetsEmployees { get; set; }
        public virtual ICollection<PlanningInFlow> PlanningInFlows { get; set; }
        public virtual ICollection<PositionAccountPair> PositionAccountPairs { get; set; }
        public virtual ICollection<ProductBonusesOnCooperationDate> ProductBonusesOnCooperationDates { get; set; }
        public virtual ICollection<ProductsCurrency> ProductsCurrencies { get; set; }
        public virtual ICollection<ProductsCurrenciesReplenishmentLimit> ProductsCurrenciesReplenishmentLimits { get; set; }
        public virtual ICollection<ProductsCurrenciesWithdrawTariff> ProductsCurrenciesWithdrawTariffs { get; set; }
        public virtual ICollection<ProductsFine> ProductsFines { get; set; }
        public virtual ICollection<ProductsInterest> ProductsInterests { get; set; }
        public virtual ICollection<ProductsIssueComission> ProductsIssueComissions { get; set; }
        public virtual ICollection<ReplenishmentIndividualTariff> ReplenishmentIndividualTariffs { get; set; }
        public virtual ICollection<ReplenishmentTariff> ReplenishmentTariffs { get; set; }
        public virtual ICollection<Reserf> Reserves { get; set; }
        public virtual ICollection<ReservesClassificationsSetting> ReservesClassificationsSettings { get; set; }
        public virtual ICollection<RoleLimit> RoleLimits { get; set; }
        public virtual ICollection<Salary> Salaries { get; set; }
        public virtual ICollection<Security> Securities { get; set; }
        public virtual ICollection<SecuritiesAccount> SecuritiesAccounts { get; set; }
        public virtual ICollection<SecuritiesMortrage> SecuritiesMortrages { get; set; }
        public virtual ICollection<StandardWithdrawTariff> StandardWithdrawTariffs { get; set; }
        public virtual ICollection<StorageTransfer> StorageTransfers { get; set; }
        public virtual ICollection<SubStatusesLimitsForUser> SubStatusesLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesLimitsOnRole> SubStatusesLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsForUser> SubStatusesRequestsLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsOnRole> SubStatusesRequestsLimitsOnRoles { get; set; }
        public virtual ICollection<Swift103Transfer> Swift103Transfers { get; set; }
        public virtual ICollection<Swift202Transfer> Swift202Transfers { get; set; }
        public virtual ICollection<Swift300Transfer> Swift300TransferCurrencyBoughts { get; set; }
        public virtual ICollection<Swift300Transfer> Swift300TransferCurrencySolds { get; set; }
        public virtual ICollection<SwiftCorrBankAccount> SwiftCorrBankAccounts { get; set; }
        public virtual ICollection<SwiftInvoicesToTheBank> SwiftInvoicesToTheBanks { get; set; }
        public virtual ICollection<Tariff1> Tariff1s { get; set; }
        public virtual ICollection<Tariff2> Tariff2s { get; set; }
        public virtual ICollection<Tariff3> Tariff3s { get; set; }
        public virtual ICollection<Tariff4> Tariff4s { get; set; }
        public virtual ICollection<TrainingCourse> TrainingCourses { get; set; }
        public virtual ICollection<TranchesLimit> TranchesLimits { get; set; }
        public virtual ICollection<TransactionTemplateOperation> TransactionTemplateOperationCurrencies { get; set; }
        public virtual ICollection<TransactionTemplateOperation> TransactionTemplateOperationTransactionTemplates { get; set; }
        public virtual ICollection<TransactionTemplate> TransactionTemplates { get; set; }
        public virtual ICollection<Transfer> Transfers { get; set; }
        public virtual ICollection<UserLimit> UserLimits { get; set; }
        public virtual ICollection<WithdrawTariffsByBranch> WithdrawTariffsByBranches { get; set; }
    }
}
