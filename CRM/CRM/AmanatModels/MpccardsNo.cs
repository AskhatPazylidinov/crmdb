﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MpccardsNo
    {
        public int BranchId { get; set; }
        public int EmployeeNo { get; set; }
        public string AccountNo { get; set; }
        public string EmployeeName { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
