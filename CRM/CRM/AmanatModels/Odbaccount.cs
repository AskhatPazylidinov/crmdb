﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Odbaccount
    {
        public string OdbaccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string BalanceGroup { get; set; }
        public long Kodkl { get; set; }
        public string AccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
