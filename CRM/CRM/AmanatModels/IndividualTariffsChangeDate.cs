﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariffsChangeDate
    {
        public IndividualTariffsChangeDate()
        {
            IndividualTariff2s = new HashSet<IndividualTariff2>();
        }

        public DateTime ChangeDate { get; set; }

        public virtual ICollection<IndividualTariff2> IndividualTariff2s { get; set; }
    }
}
