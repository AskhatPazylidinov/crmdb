﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesSubStatus
    {
        public int Id { get; set; }
        public int CreditId { get; set; }
        public int SubStatusId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public int? PrevSubStatusId { get; set; }
        public int? ExpiredReasonId { get; set; }
        public int? ReasonId { get; set; }

        public virtual History Credit { get; set; }
        public virtual SubStatusesExpiredReason ExpiredReason { get; set; }
        public virtual SubStatusesReason Reason { get; set; }
        public virtual SubStatusesType SubStatus { get; set; }
        public virtual User User { get; set; }
    }
}
