﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Contractor
    {
        public int CustomerId { get; set; }
        public string Description { get; set; }
        public string ExternalBikcode { get; set; }
        public string ExternalAccountNo { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
