﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetsMovement
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Note { get; set; }
        public int AssetId { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
