﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncomeAccount1
    {
        public int OfficeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public byte IncomeAccountTypeId { get; set; }
        public string OdbaccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int AccountId { get; set; }
    }
}
