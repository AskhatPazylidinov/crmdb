﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Emission
    {
        public Emission()
        {
            EmissionsFairPrices = new HashSet<EmissionsFairPrice>();
        }

        public int EmissionId { get; set; }
        public int EmitterId { get; set; }
        public string EmissionName { get; set; }
        public decimal NominalPricePerItem { get; set; }
        public int CurrencyId { get; set; }
        public string Isincode { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime EndDate { get; set; }
        public byte CalculationTypeId { get; set; }
        public decimal CouponRate { get; set; }
        public int CouponPaymentPeriod { get; set; }
        public long TotalVolume { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Emitter Emitter { get; set; }
        public virtual ICollection<EmissionsFairPrice> EmissionsFairPrices { get; set; }
    }
}
