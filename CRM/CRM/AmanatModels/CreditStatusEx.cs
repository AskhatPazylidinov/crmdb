﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditStatusEx
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
