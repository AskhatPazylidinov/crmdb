﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CountryCurrency
    {
        public int RiaCountryId { get; set; }
        public string RiaCurrencyCode { get; set; }

        public virtual RiaCurrency RiaCurrencyCodeNavigation { get; set; }
    }
}
