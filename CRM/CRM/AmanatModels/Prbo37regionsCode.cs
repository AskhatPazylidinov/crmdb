﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Prbo37regionsCode
    {
        public int RegionId { get; set; }
        public int NationalBankRegionId { get; set; }

        public virtual Region1 Region { get; set; }
    }
}
