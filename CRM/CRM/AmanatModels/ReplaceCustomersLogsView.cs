﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplaceCustomersLogsView
    {
        public int Id { get; set; }
        public int OldCustomerId { get; set; }
        public string OldCustomerName { get; set; }
        public int NewCustomerId { get; set; }
        public string NewCustomerName { get; set; }
        public DateTime OperationDate { get; set; }
        public DateTime CreateDateForMerge { get; set; }
        public int? ErrNumber { get; set; }
        public int? ErrLine { get; set; }
        public string ErrMessage { get; set; }
        public int UserId { get; set; }
        public string CreateUser { get; set; }
        public int? CreateUserOfficeId { get; set; }
        public int? CreateUserBranchId { get; set; }
        public string CreateUserOffice { get; set; }
        public string CreateUserBranch { get; set; }
        public int ApproveUserId { get; set; }
        public string ApproveUser { get; set; }
    }
}
