﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceType
    {
        public InsuranceType()
        {
            HistoriesInsurancesPolicies = new HashSet<HistoriesInsurancesPolicy>();
            InsuranceInterests = new HashSet<InsuranceInterest>();
            InsurancesCompaniesDocuments = new HashSet<InsurancesCompaniesDocument>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<HistoriesInsurancesPolicy> HistoriesInsurancesPolicies { get; set; }
        public virtual ICollection<InsuranceInterest> InsuranceInterests { get; set; }
        public virtual ICollection<InsurancesCompaniesDocument> InsurancesCompaniesDocuments { get; set; }
    }
}
