﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserInfo
    {
        public int Id { get; set; }
        public int MoneySystemId { get; set; }
        public int? UserId { get; set; }
        public string RequesterId { get; set; }
        public string PrincipalId { get; set; }

        public virtual MoneySystem MoneySystem { get; set; }
        public virtual User User { get; set; }
    }
}
