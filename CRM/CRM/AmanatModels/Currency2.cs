﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency2
    {
        public int ProductId { get; set; }
        public int CurrencyId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
