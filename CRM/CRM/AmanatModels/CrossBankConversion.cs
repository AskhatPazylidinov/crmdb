﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankConversion
    {
        public CrossBankConversion()
        {
            CrossBankConversionsParticipants = new HashSet<CrossBankConversionsParticipant>();
            CrossBankConversionsStatuses = new HashSet<CrossBankConversionsStatus>();
            CrossBankConversionsTransactions = new HashSet<CrossBankConversionsTransaction>();
        }

        public int OperationId { get; set; }
        public byte ConversionTypeId { get; set; }
        public string OrderNo { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public string ContragentBank { get; set; }

        public virtual SwiftCorrBank ContragentBankNavigation { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<CrossBankConversionsParticipant> CrossBankConversionsParticipants { get; set; }
        public virtual ICollection<CrossBankConversionsStatus> CrossBankConversionsStatuses { get; set; }
        public virtual ICollection<CrossBankConversionsTransaction> CrossBankConversionsTransactions { get; set; }
    }
}
