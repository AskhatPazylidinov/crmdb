﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsOperationType
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeMpc { get; set; }
        public byte IsMain { get; set; }
        public byte IsComsission { get; set; }
        public byte IsExchange { get; set; }
        public byte? Priorety { get; set; }
        public bool InCash { get; set; }

        public virtual User User { get; set; }
    }
}
