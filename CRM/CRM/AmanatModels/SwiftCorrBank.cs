﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftCorrBank
    {
        public SwiftCorrBank()
        {
            CorrBankRatings = new HashSet<CorrBankRating>();
            CrossBankConversions = new HashSet<CrossBankConversion>();
            CrossBankConversionsParticipants = new HashSet<CrossBankConversionsParticipant>();
            K3depositsInBanks = new HashSet<K3depositsInBank>();
            SwiftCorrBankAccounts = new HashSet<SwiftCorrBankAccount>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string SwiftCode { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public int? CustomerId { get; set; }

        public virtual ICollection<CorrBankRating> CorrBankRatings { get; set; }
        public virtual ICollection<CrossBankConversion> CrossBankConversions { get; set; }
        public virtual ICollection<CrossBankConversionsParticipant> CrossBankConversionsParticipants { get; set; }
        public virtual ICollection<K3depositsInBank> K3depositsInBanks { get; set; }
        public virtual ICollection<SwiftCorrBankAccount> SwiftCorrBankAccounts { get; set; }
    }
}
