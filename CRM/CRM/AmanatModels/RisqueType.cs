﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RisqueType
    {
        public RisqueType()
        {
            CustomersRisqueTypes = new HashSet<CustomersRisqueType>();
        }

        public int RisqueTypeId { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<CustomersRisqueType> CustomersRisqueTypes { get; set; }
    }
}
