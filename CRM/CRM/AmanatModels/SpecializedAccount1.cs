﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecializedAccount1
    {
        public int OfficeId { get; set; }
        public byte TypeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
