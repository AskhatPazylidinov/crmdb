﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RelativesLoan
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string LoanState { get; set; }
        public int OverdueDays { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
