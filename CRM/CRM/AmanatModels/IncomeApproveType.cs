﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncomeApproveType
    {
        public IncomeApproveType()
        {
            Histories = new HashSet<History>();
            ProductsApproveTypes = new HashSet<ProductsApproveType>();
            ProductsInterests = new HashSet<ProductsInterest>();
            ProductsReviewComissions = new HashSet<ProductsReviewComission>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<ProductsApproveType> ProductsApproveTypes { get; set; }
        public virtual ICollection<ProductsInterest> ProductsInterests { get; set; }
        public virtual ICollection<ProductsReviewComission> ProductsReviewComissions { get; set; }
    }
}
