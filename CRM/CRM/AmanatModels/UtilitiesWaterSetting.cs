﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesWaterSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
        public string ExportPrefix { get; set; }
    }
}
