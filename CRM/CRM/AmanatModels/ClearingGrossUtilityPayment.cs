﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ClearingGrossUtilityPayment
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public long Position { get; set; }
        public byte ProviderType { get; set; }
    }
}
