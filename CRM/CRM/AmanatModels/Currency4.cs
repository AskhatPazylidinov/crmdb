﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency4
    {
        public int KyCode { get; set; }
        public string FlCodelat { get; set; }
        public string FlName { get; set; }
    }
}
