﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualAutoProlongation
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsMainSummWithPercents { get; set; }
        public int ProlongationsCount { get; set; }
        public int UserId { get; set; }
        public int? ApprovedUserId { get; set; }
        public bool IsApproved { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
