﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MarketRate
    {
        public DateTime OperationDate { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public byte TypeId { get; set; }
        public DateTime RateDate { get; set; }
        public int? CustomerId { get; set; }
        public decimal? BuyRate { get; set; }
        public decimal? SellRate { get; set; }
        public int CrossCurrencyId { get; set; }
        public int UserId { get; set; }
        public bool IsApprove { get; set; }
        public int LinkOfficeId { get; set; }
        public bool IsHeadOfficeData { get; set; }
        public bool? IsUsed { get; set; }
        public long Id { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual Currency3 CrossCurrency { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
    }
}
