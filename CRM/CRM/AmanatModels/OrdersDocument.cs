﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrdersDocument
    {
        public byte OrderTypeId { get; set; }
        public string Filename { get; set; }
        public byte? MinStatusToDisplay { get; set; }
    }
}
