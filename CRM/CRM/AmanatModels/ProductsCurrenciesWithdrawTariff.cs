﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCurrenciesWithdrawTariff
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public int ComissionCalculationTypeId { get; set; }
        public decimal? MinSumm { get; set; }
        public byte MinSummCurrencyType { get; set; }
        public decimal? MaxSumm { get; set; }
        public byte MaxSummCurrencyType { get; set; }
        public int? CashIncomeType { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProductsCurrency ProductsCurrency { get; set; }
    }
}
