﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InviteInternetBankingUser
    {
        public int LoginId { get; set; }
        public int CustomerId { get; set; }
        public int InvitingEmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public int UserId { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
