﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsBalanceGroup1
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public byte AccountTypeId { get; set; }
        public string ResidentGroup { get; set; }
        public string NonResidentGroup { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
