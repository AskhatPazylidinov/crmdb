﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositoryAccountsBalanceGroup
    {
        public int AccountTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccoountGroup { get; set; }

        public virtual AccountGroup AccoountGroupNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
