﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocument3
    {
        public int ExtraDocumentId { get; set; }
        public string DisplayName { get; set; }
        public string Filename { get; set; }
        public bool CanRunOnEmptyDeposit { get; set; }
        public int? OrientedCustomerTypeId { get; set; }
        public bool? ToPdf { get; set; }
    }
}
