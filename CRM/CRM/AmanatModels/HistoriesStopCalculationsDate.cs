﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesStopCalculationsDate
    {
        public int CreditId { get; set; }
        public byte StopTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
