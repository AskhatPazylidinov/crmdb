﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Garantee1
    {
        public int? OldGaranteeId { get; set; }
        public int? NewGaranteeId { get; set; }
    }
}
