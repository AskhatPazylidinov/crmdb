﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Area
    {
        public Area()
        {
            City1s = new HashSet<City1>();
        }

        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public string Code { get; set; }
        public int RegionId { get; set; }
        public int? ParentAreaId { get; set; }
        public byte AreaTypeId { get; set; }

        public virtual Region1 Region { get; set; }
        public virtual ICollection<City1> City1s { get; set; }
    }
}
