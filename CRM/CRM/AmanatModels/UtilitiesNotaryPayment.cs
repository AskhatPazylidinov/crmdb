﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesNotaryPayment
    {
        public int OperationId { get; set; }
        public string FullCustomerName { get; set; }
        public string ServiceName { get; set; }
        public string IdentificationNumber { get; set; }
        public string PaymentComment { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
