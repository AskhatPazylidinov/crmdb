﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GarantType
    {
        public GarantType()
        {
            GarantInBranches = new HashSet<GarantInBranch>();
            Garantees = new HashSet<Garantee>();
            GuaranteeTypeIds = new HashSet<GuaranteeTypeId>();
            ProductsDocument3s = new HashSet<ProductsDocument3>();
            ProductsMonitoringsSetting1s = new HashSet<ProductsMonitoringsSetting1>();
            Properties = new HashSet<Property>();
        }

        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GarantInBranch> GarantInBranches { get; set; }
        public virtual ICollection<Garantee> Garantees { get; set; }
        public virtual ICollection<GuaranteeTypeId> GuaranteeTypeIds { get; set; }
        public virtual ICollection<ProductsDocument3> ProductsDocument3s { get; set; }
        public virtual ICollection<ProductsMonitoringsSetting1> ProductsMonitoringsSetting1s { get; set; }
        public virtual ICollection<Property> Properties { get; set; }
    }
}
