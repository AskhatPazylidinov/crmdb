﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositMarketingSource
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int UserId { get; set; }
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
