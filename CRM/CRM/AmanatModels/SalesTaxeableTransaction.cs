﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SalesTaxeableTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
