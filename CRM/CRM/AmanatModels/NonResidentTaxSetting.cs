﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonResidentTaxSetting
    {
        public int TaxId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal BaseRate { get; set; }
        public decimal ThresholdAmount { get; set; }
        public decimal ThresholdRate { get; set; }

        public virtual NonResidentTaxis Tax { get; set; }
    }
}
