﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Payroll
    {
        public Payroll()
        {
            PayrollsEmployees = new HashSet<PayrollsEmployee>();
        }

        public int PayrollId { get; set; }
        public DateTime PayrollDate { get; set; }
        public byte IncomeTypeId { get; set; }

        public virtual ICollection<PayrollsEmployee> PayrollsEmployees { get; set; }
    }
}
