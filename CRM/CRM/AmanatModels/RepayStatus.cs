﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RepayStatus
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
