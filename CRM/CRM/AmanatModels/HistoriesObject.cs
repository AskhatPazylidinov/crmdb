﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesObject
    {
        public int CreditObjectId { get; set; }
        public string CreditObjectName { get; set; }
        public int PurposeId { get; set; }

        public virtual Purpose Purpose { get; set; }
    }
}
