﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TranchesLimitChangeOperation
    {
        public int CreditId { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool IsIncome { get; set; }
        public decimal ChangeSumm { get; set; }
        public long? Position { get; set; }

        public virtual History Credit { get; set; }
    }
}
