﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashMachine
    {
        public CashMachine()
        {
            UsersCashierMachines = new HashSet<UsersCashierMachine>();
        }

        public string MachineName { get; set; }
        public string MachineDescription { get; set; }
        public string ProcessAssemblyName { get; set; }

        public virtual ICollection<UsersCashierMachine> UsersCashierMachines { get; set; }
    }
}
