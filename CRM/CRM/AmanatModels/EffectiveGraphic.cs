﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EffectiveGraphic
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int GraphicId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public byte? PaymentType { get; set; }
        public decimal? EffectiveRate { get; set; }
        public decimal? EffectiveRateForBank { get; set; }

        public virtual History Credit { get; set; }
    }
}
