﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PotentialBlackVisitorsOperation
    {
        public long Id { get; set; }
        public DateTime CheckDate { get; set; }
        public int CustomerId { get; set; }
        public int PersonId { get; set; }
        public int? OperationUserId { get; set; }
        public string OperationNote { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User OperationUser { get; set; }
        public virtual Person Person { get; set; }
    }
}
