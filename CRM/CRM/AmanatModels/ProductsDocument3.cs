﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsDocument3
    {
        public int DocumentId { get; set; }
        public int GarantTypeId { get; set; }
        public string Filename { get; set; }
        public byte EventTypeId { get; set; }
        public bool? ToPdf { get; set; }

        public virtual GarantType GarantType { get; set; }
    }
}
