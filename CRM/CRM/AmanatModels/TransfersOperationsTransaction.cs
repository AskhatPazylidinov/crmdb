﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersOperationsTransaction
    {
        public long OperationId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual TransfersOperation Operation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
