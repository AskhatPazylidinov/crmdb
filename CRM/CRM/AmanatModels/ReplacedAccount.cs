﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplacedAccount
    {
        public string AccountPrefix { get; set; }
        public string ReplaceAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual User ApprovedUser { get; set; }
        public virtual User CreatedUser { get; set; }
    }
}
