﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InternalSystemName
    {
        public int Id { get; set; }
        public string TransferSystemName { get; set; }
    }
}
