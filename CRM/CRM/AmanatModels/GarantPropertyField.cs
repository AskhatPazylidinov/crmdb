﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GarantPropertyField
    {
        public int? OldFid { get; set; }
        public int? NewFid { get; set; }
    }
}
