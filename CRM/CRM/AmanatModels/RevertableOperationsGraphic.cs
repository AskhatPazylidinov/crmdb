﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperationsGraphic
    {
        public long RevertId { get; set; }
        public int GraphicId { get; set; }

        public virtual RevertableOperation Revert { get; set; }
    }
}
