﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesReservesClassification
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ClassificationId { get; set; }

        public virtual ReservesClassification Classification { get; set; }
        public virtual History Credit { get; set; }
    }
}
