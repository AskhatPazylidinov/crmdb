﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogStatus
    {
        public long Id { get; set; }
        public long LogId { get; set; }
        public DateTime SendingTime { get; set; }
        public byte StatusId { get; set; }
        public string ErrorMessage { get; set; }

        public virtual Log Log { get; set; }
    }
}
