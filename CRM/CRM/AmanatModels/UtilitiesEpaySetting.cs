﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesEpaySetting
    {
        public int Id { get; set; }
        public bool HasOnlineProcessing { get; set; }
        public string OnlineProcessingUserIds { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string EpayAddress { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyAddress { get; set; }
        public int? ProxyPort { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }
        public bool UseProxyAuthorization { get; set; }
    }
}
