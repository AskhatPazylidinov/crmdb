﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RowsetRowOper
    {
        public int KyRowId { get; set; }
        public int KyRowsetId { get; set; }
        public DateTime? FlOperdate { get; set; }
        public int? FlOpercode { get; set; }
        public string FlOpercodes { get; set; }
        public double? FlSumcur { get; set; }
        public string FlCurcodes { get; set; }
        public double? FlSumtgt { get; set; }
        public double? FlSum { get; set; }
        public double? FlShareqty { get; set; }
        public double? FlSharecapital { get; set; }
        public string FlReason { get; set; }
        public string FlLimitcodes { get; set; }
        public string FlShadycodes { get; set; }
        public int? FlStatuscode { get; set; }
        public string FlExtrainfo { get; set; }
    }
}
