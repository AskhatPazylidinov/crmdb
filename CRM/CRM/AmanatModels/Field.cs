﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Field
    {
        public string FieldName { get; set; }
        public string Description { get; set; }
        public bool? CalcField { get; set; }
    }
}
