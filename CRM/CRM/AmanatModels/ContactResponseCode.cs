﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ContactResponseCode
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
