﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class K4offBalanceGroup
    {
        public string OffBalanceGroup { get; set; }

        public virtual BalanceGroup OffBalanceGroupNavigation { get; set; }
    }
}
