﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesDoshcardGroup
    {
        public UtilitiesDoshcardGroup()
        {
            UtilitiesDoshcardPayments = new HashSet<UtilitiesDoshcardPayment>();
        }

        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string AccountNo { get; set; }
        public byte CommissionType { get; set; }
        public decimal Commission { get; set; }

        public virtual ICollection<UtilitiesDoshcardPayment> UtilitiesDoshcardPayments { get; set; }
    }
}
