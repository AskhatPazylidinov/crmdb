﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeLogContextMap
    {
        public string EntityType { get; set; }
        public string FieldsName { get; set; }
    }
}
