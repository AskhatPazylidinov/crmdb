﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReportsLog
    {
        public long LogId { get; set; }
        public int UserId { get; set; }
        public string ReportName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public virtual User User { get; set; }
    }
}
