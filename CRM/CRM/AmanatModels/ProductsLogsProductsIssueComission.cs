﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsIssueComission
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int MortrageTypeId { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
