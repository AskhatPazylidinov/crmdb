﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HolidayWorkday
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime HolidayDate { get; set; }
        public DateTime? WorkDate { get; set; }
        public string Reason { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
