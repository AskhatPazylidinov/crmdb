﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BalanceGroup
    {
        public BalanceGroup()
        {
            AssetsBalanceGroups = new HashSet<AssetsBalanceGroup>();
            CashierAccountsBalanceGroups = new HashSet<CashierAccountsBalanceGroup>();
            ConversionsAccountsBalanceGroups = new HashSet<ConversionsAccountsBalanceGroup>();
            DepositoryAccountsBalanceGroups = new HashSet<DepositoryAccountsBalanceGroup>();
            IncomeOperationTypesBalanceGroups = new HashSet<IncomeOperationTypesBalanceGroup>();
            InterBranchTransfersAccountsBalanceGroups = new HashSet<InterBranchTransfersAccountsBalanceGroup>();
            InterBranchTransfersIncomeAccountsBalanceGroups = new HashSet<InterBranchTransfersIncomeAccountsBalanceGroup>();
            MoneyTransfersAccountsBalanceGroups = new HashSet<MoneyTransfersAccountsBalanceGroup>();
            MortgagesAccountsBalanceGroups = new HashSet<MortgagesAccountsBalanceGroup>();
            OfficesPlannedIncomes = new HashSet<OfficesPlannedIncome>();
            PlainAccountsTemplates = new HashSet<PlainAccountsTemplate>();
            ProductsBalanceGroup1NonResidentGroupNavigations = new HashSet<ProductsBalanceGroup1>();
            ProductsBalanceGroup1ResidentGroupNavigations = new HashSet<ProductsBalanceGroup1>();
            ProductsBalanceGroupNonResidentGroupNavigations = new HashSet<ProductsBalanceGroup>();
            ProductsBalanceGroupResidentGroupNavigations = new HashSet<ProductsBalanceGroup>();
            ProductsPurposesBalanceGroupNonResidentGroupNavigations = new HashSet<ProductsPurposesBalanceGroup>();
            ProductsPurposesBalanceGroupResidentGroupNavigations = new HashSet<ProductsPurposesBalanceGroup>();
            ReservesAccountsBalanceGroup1s = new HashSet<ReservesAccountsBalanceGroup1>();
            ReservesAccountsBalanceGroupExpenseBalanceGroupNavigations = new HashSet<ReservesAccountsBalanceGroup>();
            ReservesAccountsBalanceGroupReserveBalanceGroupNavigations = new HashSet<ReservesAccountsBalanceGroup>();
            SecuritiesAccountsBalanceGroups = new HashSet<SecuritiesAccountsBalanceGroup>();
            SecuritiesBalanceGroups = new HashSet<SecuritiesBalanceGroup>();
            SpecializedAccountsBalanceGroups = new HashSet<SpecializedAccountsBalanceGroup>();
            WriteOffBalanceGroupNonResidentBalanceGroupNavigations = new HashSet<WriteOffBalanceGroup>();
            WriteOffBalanceGroupResidentBalanceGroupNavigations = new HashSet<WriteOffBalanceGroup>();
        }

        public string Code { get; set; }
        public string RuName { get; set; }
        public byte TypeId { get; set; }
        public byte? TaxTypeId { get; set; }

        public virtual AllowedBalanceGroup AllowedBalanceGroup { get; set; }
        public virtual BicBalanceGroup BicBalanceGroup { get; set; }
        public virtual Form36BalGroup Form36BalGroup { get; set; }
        public virtual K4offBalanceGroup K4offBalanceGroup { get; set; }
        public virtual PartnersBalanceGroup PartnersBalanceGroup { get; set; }
        public virtual ProtectionAgencyBalanceGroup ProtectionAgencyBalanceGroup { get; set; }
        public virtual ICollection<AssetsBalanceGroup> AssetsBalanceGroups { get; set; }
        public virtual ICollection<CashierAccountsBalanceGroup> CashierAccountsBalanceGroups { get; set; }
        public virtual ICollection<ConversionsAccountsBalanceGroup> ConversionsAccountsBalanceGroups { get; set; }
        public virtual ICollection<DepositoryAccountsBalanceGroup> DepositoryAccountsBalanceGroups { get; set; }
        public virtual ICollection<IncomeOperationTypesBalanceGroup> IncomeOperationTypesBalanceGroups { get; set; }
        public virtual ICollection<InterBranchTransfersAccountsBalanceGroup> InterBranchTransfersAccountsBalanceGroups { get; set; }
        public virtual ICollection<InterBranchTransfersIncomeAccountsBalanceGroup> InterBranchTransfersIncomeAccountsBalanceGroups { get; set; }
        public virtual ICollection<MoneyTransfersAccountsBalanceGroup> MoneyTransfersAccountsBalanceGroups { get; set; }
        public virtual ICollection<MortgagesAccountsBalanceGroup> MortgagesAccountsBalanceGroups { get; set; }
        public virtual ICollection<OfficesPlannedIncome> OfficesPlannedIncomes { get; set; }
        public virtual ICollection<PlainAccountsTemplate> PlainAccountsTemplates { get; set; }
        public virtual ICollection<ProductsBalanceGroup1> ProductsBalanceGroup1NonResidentGroupNavigations { get; set; }
        public virtual ICollection<ProductsBalanceGroup1> ProductsBalanceGroup1ResidentGroupNavigations { get; set; }
        public virtual ICollection<ProductsBalanceGroup> ProductsBalanceGroupNonResidentGroupNavigations { get; set; }
        public virtual ICollection<ProductsBalanceGroup> ProductsBalanceGroupResidentGroupNavigations { get; set; }
        public virtual ICollection<ProductsPurposesBalanceGroup> ProductsPurposesBalanceGroupNonResidentGroupNavigations { get; set; }
        public virtual ICollection<ProductsPurposesBalanceGroup> ProductsPurposesBalanceGroupResidentGroupNavigations { get; set; }
        public virtual ICollection<ReservesAccountsBalanceGroup1> ReservesAccountsBalanceGroup1s { get; set; }
        public virtual ICollection<ReservesAccountsBalanceGroup> ReservesAccountsBalanceGroupExpenseBalanceGroupNavigations { get; set; }
        public virtual ICollection<ReservesAccountsBalanceGroup> ReservesAccountsBalanceGroupReserveBalanceGroupNavigations { get; set; }
        public virtual ICollection<SecuritiesAccountsBalanceGroup> SecuritiesAccountsBalanceGroups { get; set; }
        public virtual ICollection<SecuritiesBalanceGroup> SecuritiesBalanceGroups { get; set; }
        public virtual ICollection<SpecializedAccountsBalanceGroup> SpecializedAccountsBalanceGroups { get; set; }
        public virtual ICollection<WriteOffBalanceGroup> WriteOffBalanceGroupNonResidentBalanceGroupNavigations { get; set; }
        public virtual ICollection<WriteOffBalanceGroup> WriteOffBalanceGroupResidentBalanceGroupNavigations { get; set; }
    }
}
