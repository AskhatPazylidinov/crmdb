﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserLogView
    {
        public long LogId { get; set; }
        public DateTime LogDate { get; set; }
        public string LoginName { get; set; }
        public string UserName { get; set; }
        public short LogStatusId { get; set; }
        public string Description { get; set; }
        public string Ipaddress { get; set; }
    }
}
