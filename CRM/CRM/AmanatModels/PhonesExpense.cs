﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PhonesExpense
    {
        public int EmployeeId { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string Phone { get; set; }
        public decimal ExpenseSumm { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Phone PhoneNavigation { get; set; }
    }
}
