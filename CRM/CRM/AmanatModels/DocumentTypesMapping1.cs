﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentTypesMapping1
    {
        public int ContactDocumentTypeId { get; set; }
        public int DocumentTypeId { get; set; }

        public virtual DocumentType ContactDocumentType { get; set; }
        public virtual DocumentType1 DocumentType { get; set; }
    }
}
