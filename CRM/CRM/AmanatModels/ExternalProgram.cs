﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalProgram
    {
        public ExternalProgram()
        {
            Histories = new HashSet<History>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<History> Histories { get; set; }
    }
}
