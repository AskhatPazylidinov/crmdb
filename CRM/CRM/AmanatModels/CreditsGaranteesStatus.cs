﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsGaranteesStatus
    {
        public int CreditGaranteeRelationId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int? UserId { get; set; }

        public virtual CreditsGarantee CreditGaranteeRelation { get; set; }
        public virtual User User { get; set; }
    }
}
