﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsEarlyPaymentComission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte Period { get; set; }
        public decimal? PartialComission { get; set; }
        public byte? PartialComissionType { get; set; }
        public decimal? FullPaymentComission { get; set; }
        public byte? FullPaymentComissionType { get; set; }

        public virtual ProductsChange ProductsChange { get; set; }
    }
}
