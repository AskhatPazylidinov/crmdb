﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LoansEarlyPayment
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal EarlyPaidAmount { get; set; }
        public decimal BaseDeptToPay { get; set; }
    }
}
