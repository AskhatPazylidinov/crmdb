﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OnlineTransactionsView
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public string DocumentNo { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string BalanceGroup { get; set; }
        public decimal SumV { get; set; }
        public int OfficeId { get; set; }
        public int BranchId { get; set; }
        public int UserId { get; set; }
        public int UserOfficeId { get; set; }
        public int UserBranchId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime DateV { get; set; }
        public string Comment { get; set; }
        public bool? IsConfirmed { get; set; }
        public bool? IsWithdrawal { get; set; }
        public int? IpcExecuteTransactionPaymentId { get; set; }
        public bool IsSentToIpc { get; set; }
    }
}
