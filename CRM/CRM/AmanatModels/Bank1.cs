﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Bank1
    {
        public Bank1()
        {
            BankServices = new HashSet<BankService>();
        }

        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public int? ParentId { get; set; }
        public string PpCode { get; set; }
        public string Bic { get; set; }
        public string Name { get; set; }
        public string CityHead { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Phone { get; set; }
        public string NameRus { get; set; }
        public int CountryId { get; set; }
        public int Deleted { get; set; }
        public int ContactType { get; set; }
        public int CanRevoke { get; set; }
        public int CanChange { get; set; }
        public int GetMoney { get; set; }
        public string SendCurrency { get; set; }
        public string RecCurrency { get; set; }
        public int CityId { get; set; }
        public int IsUniqueTransferIdentity { get; set; }

        public virtual City City { get; set; }
        public virtual Country1 Country { get; set; }
        public virtual ICollection<BankService> BankServices { get; set; }
    }
}
