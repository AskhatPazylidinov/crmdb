﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositAccount
    {
        public DepositAccount()
        {
            AllowedDepositsWithdrawOffices = new HashSet<AllowedDepositsWithdrawOffice>();
            CapitalizedPercents = new HashSet<CapitalizedPercent>();
            CustomerAwayReasons = new HashSet<CustomerAwayReason>();
            CustomersWithdrawTariffs = new HashSet<CustomersWithdrawTariff>();
            DepositsMortrages = new HashSet<DepositsMortrage>();
            DormantAccounts = new HashSet<DormantAccount>();
            Extensions = new HashSet<Extension>();
            ExternalLoansGraphics = new HashSet<ExternalLoansGraphic>();
            Graphic1s = new HashSet<Graphic1>();
            IgnoresTaxesOnPercents = new HashSet<IgnoresTaxesOnPercent>();
            IndividualAutoProlongations = new HashSet<IndividualAutoProlongation>();
            IndividualCapitalizationDays = new HashSet<IndividualCapitalizationDay>();
            IndividualInterestRates = new HashSet<IndividualInterestRate>();
            IndividualStornoRates = new HashSet<IndividualStornoRate>();
            IndividualWithdrawalLimitsDates = new HashSet<IndividualWithdrawalLimitsDate>();
            InterTransfers = new HashSet<InterTransfer>();
            InviteCustomer1s = new HashSet<InviteCustomer1>();
            JointAccountCustomers = new HashSet<JointAccountCustomer>();
            MinimalSumms = new HashSet<MinimalSumm>();
            OpenTransactions = new HashSet<OpenTransaction>();
            Percent1s = new HashSet<Percent1>();
            PrivateCustomerChecks = new HashSet<PrivateCustomerCheck>();
            PublicOfferDocumentReferences = new HashSet<PublicOfferDocumentReference>();
            RatesBasedOnBalanceDates = new HashSet<RatesBasedOnBalanceDate>();
            RestoredAccounts = new HashSet<RestoredAccount>();
        }

        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string PercentAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public int ProgramId { get; set; }
        public int Period { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal AvailableSumV { get; set; }
        public byte JointTypeId { get; set; }
        public string CurrentAccountNo { get; set; }
        public int? PurposeId { get; set; }
        public string AgreementNo { get; set; }
        public int UserId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Account1 Account1Navigation { get; set; }
        public virtual Account1 Curren { get; set; }
        public virtual Product2 Program { get; set; }
        public virtual AccountsChange AccountsChange { get; set; }
        public virtual DepositMarketingSource DepositMarketingSource { get; set; }
        public virtual ExternalLoansCredit ExternalLoansCredit { get; set; }
        public virtual ExternalLoansPaymentAccount ExternalLoansPaymentAccount { get; set; }
        public virtual PreviousTransactionDate PreviousTransactionDate { get; set; }
        public virtual RevertableCloseOperationsAccount RevertableCloseOperationsAccount { get; set; }
        public virtual StartCalculatedPercent StartCalculatedPercent { get; set; }
        public virtual StartNonComissionableSumm StartNonComissionableSumm { get; set; }
        public virtual ICollection<AllowedDepositsWithdrawOffice> AllowedDepositsWithdrawOffices { get; set; }
        public virtual ICollection<CapitalizedPercent> CapitalizedPercents { get; set; }
        public virtual ICollection<CustomerAwayReason> CustomerAwayReasons { get; set; }
        public virtual ICollection<CustomersWithdrawTariff> CustomersWithdrawTariffs { get; set; }
        public virtual ICollection<DepositsMortrage> DepositsMortrages { get; set; }
        public virtual ICollection<DormantAccount> DormantAccounts { get; set; }
        public virtual ICollection<Extension> Extensions { get; set; }
        public virtual ICollection<ExternalLoansGraphic> ExternalLoansGraphics { get; set; }
        public virtual ICollection<Graphic1> Graphic1s { get; set; }
        public virtual ICollection<IgnoresTaxesOnPercent> IgnoresTaxesOnPercents { get; set; }
        public virtual ICollection<IndividualAutoProlongation> IndividualAutoProlongations { get; set; }
        public virtual ICollection<IndividualCapitalizationDay> IndividualCapitalizationDays { get; set; }
        public virtual ICollection<IndividualInterestRate> IndividualInterestRates { get; set; }
        public virtual ICollection<IndividualStornoRate> IndividualStornoRates { get; set; }
        public virtual ICollection<IndividualWithdrawalLimitsDate> IndividualWithdrawalLimitsDates { get; set; }
        public virtual ICollection<InterTransfer> InterTransfers { get; set; }
        public virtual ICollection<InviteCustomer1> InviteCustomer1s { get; set; }
        public virtual ICollection<JointAccountCustomer> JointAccountCustomers { get; set; }
        public virtual ICollection<MinimalSumm> MinimalSumms { get; set; }
        public virtual ICollection<OpenTransaction> OpenTransactions { get; set; }
        public virtual ICollection<Percent1> Percent1s { get; set; }
        public virtual ICollection<PrivateCustomerCheck> PrivateCustomerChecks { get; set; }
        public virtual ICollection<PublicOfferDocumentReference> PublicOfferDocumentReferences { get; set; }
        public virtual ICollection<RatesBasedOnBalanceDate> RatesBasedOnBalanceDates { get; set; }
        public virtual ICollection<RestoredAccount> RestoredAccounts { get; set; }
    }
}
