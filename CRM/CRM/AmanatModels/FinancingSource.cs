﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FinancingSource
    {
        public FinancingSource()
        {
            Histories = new HashSet<History>();
        }

        public int SourceId { get; set; }
        public string SourceName { get; set; }
        public bool IsActive { get; set; }
        public string ShortName { get; set; }

        public virtual ICollection<History> Histories { get; set; }
    }
}
