﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeletedCardsRegistry
    {
        public int CardId { get; set; }
        public DateTime WithdrawDate { get; set; }
        public string CheckNumber { get; set; }
        public int CurrencyId { get; set; }
        public decimal WithdrawSum { get; set; }
        public decimal CommissionSum { get; set; }
        public int UserId { get; set; }
        public int CardType { get; set; }
        public string CardBin { get; set; }
        public string LogonCode { get; set; }
        public int OperType { get; set; }
        public DateTime OperationTime { get; set; }
        public int OfficeId { get; set; }
        public DateTime DeleteTime { get; set; }
        public int DeleteUserId { get; set; }

        public virtual User DeleteUser { get; set; }
    }
}
