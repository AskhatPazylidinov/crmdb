﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialServicesBankDetail
    {
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }
        public string ExternalBikcode { get; set; }
        public string ExternalAccountNo { get; set; }

        public virtual TaxDepartment1 Department { get; set; }
        public virtual TaxService Service { get; set; }
    }
}
