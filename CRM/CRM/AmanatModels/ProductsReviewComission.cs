﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsReviewComission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ApproveTypeId { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }

        public virtual IncomeApproveType ApproveType { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
