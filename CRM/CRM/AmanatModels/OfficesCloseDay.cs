﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesCloseDay
    {
        public DateTime CloseDate { get; set; }
        public int OfficeId { get; set; }
        public int ModuleId { get; set; }
        public byte CloseTypeId { get; set; }
        public int? UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
    }
}
