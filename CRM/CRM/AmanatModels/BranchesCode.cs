﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesCode
    {
        public int BranchId { get; set; }
        public int BranchCode { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
