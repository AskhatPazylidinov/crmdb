﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesIncomeSetting
    {
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int MinIncomePercents { get; set; }
        public int MaxIncomePercents { get; set; }
    }
}
