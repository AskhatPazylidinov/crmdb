﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PrivateCustomerRequisite
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerDocumentNo { get; set; }
    }
}
