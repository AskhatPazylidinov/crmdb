﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BatchCounter
    {
        public DateTime BankDate { get; set; }
        public int BatchNumber { get; set; }
        public bool IsMonthBatch { get; set; }
        public bool IsTestBatch { get; set; }
    }
}
