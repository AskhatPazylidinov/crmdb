﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RecipientsExceedingLimit
    {
        public int OfficeId { get; set; }
        public string Recipients { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
