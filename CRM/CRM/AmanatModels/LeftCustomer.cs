﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LeftCustomer
    {
        public int CustomerId { get; set; }
        public DateTime LeaveDate { get; set; }
        public string LeaveComment { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
