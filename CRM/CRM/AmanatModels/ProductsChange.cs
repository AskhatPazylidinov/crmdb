﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsChange
    {
        public ProductsChange()
        {
            GuaranteeIssueComissions = new HashSet<GuaranteeIssueComission>();
            ProductsCustomizableComissions = new HashSet<ProductsCustomizableComission>();
            ProductsEarlyPaymentComissions = new HashSet<ProductsEarlyPaymentComission>();
            ProductsFines = new HashSet<ProductsFine>();
            ProductsInterests = new HashSet<ProductsInterest>();
            ProductsIssueComissions = new HashSet<ProductsIssueComission>();
            ProductsPartnerCommissions = new HashSet<ProductsPartnerCommission>();
            ProductsReviewComissions = new HashSet<ProductsReviewComission>();
        }

        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }
        public string Description { get; set; }
        public bool HasReviewComission { get; set; }
        public bool HasGrantComission { get; set; }
        public byte? PeriodsCount { get; set; }
        public bool AllowsEarlyPartialPayment { get; set; }
        public bool AllowsEarlyFullPayment { get; set; }
        public decimal? MinimumEarlyAmount { get; set; }
        public bool HasTrancheIssueComission { get; set; }
        public bool HasTrancheSupportComission { get; set; }
        public decimal? TrancheIssueComission { get; set; }
        public byte? TrancheIssueComissionType { get; set; }
        public decimal? TrancheSupportComission { get; set; }
        public byte? TrancheSupportComissionType { get; set; }
        public decimal? RateForNotUsedTranche { get; set; }
        public byte DaysToStartFines { get; set; }
        public bool HasFineForLatePercentsPayment { get; set; }
        public bool HasFineForLateMainSummPayment { get; set; }
        public decimal? ReturnComission { get; set; }
        public byte? ReturnComissionType { get; set; }
        public byte? EarlyDaysDiscount { get; set; }
        public decimal? MinimumIssueComission { get; set; }
        public decimal? MaximumIssueComission { get; set; }
        public bool HasFineForLateGuaranteePayment { get; set; }
        public decimal? FineForLateGuaranteePayment { get; set; }
        public byte? FineForLateGuaranteePaymentType { get; set; }
        public byte? FinesDaysToWithdrawGuaranteePayment { get; set; }
        public decimal? MaxFinesSumm { get; set; }
        public bool? HasSalesTaxOnIssueComission { get; set; }
        public int? RateIntervalSummTypeId { get; set; }
        public decimal? FineForLatePercentsPayment { get; set; }
        public byte? FineForLatePercentsPaymentType { get; set; }
        public byte? FineForLatePercentsCalculationType { get; set; }
        public byte? FineForLatePercentsBaseType { get; set; }
        public decimal? FineForLateMainSummPayment { get; set; }
        public byte? FineForLateMainSummPaymentType { get; set; }
        public byte? FineForLateMainSummCalculationType { get; set; }
        public byte? FineForLateMainSummBaseType { get; set; }
        public int? AfterDaysDiscount { get; set; }
        public byte? MaxFinesSummBaseType { get; set; }
        public decimal? ProductEffectiveRate { get; set; }
        public decimal? PercentOfMainSum { get; set; }
        public int? AmountDaysForPeriodOfAllowance { get; set; }
        public decimal? MinValueOfGuarantee { get; set; }
        public int? MaxValueOfParallelLoans { get; set; }

        public virtual Product1 Product { get; set; }
        public virtual ICollection<GuaranteeIssueComission> GuaranteeIssueComissions { get; set; }
        public virtual ICollection<ProductsCustomizableComission> ProductsCustomizableComissions { get; set; }
        public virtual ICollection<ProductsEarlyPaymentComission> ProductsEarlyPaymentComissions { get; set; }
        public virtual ICollection<ProductsFine> ProductsFines { get; set; }
        public virtual ICollection<ProductsInterest> ProductsInterests { get; set; }
        public virtual ICollection<ProductsIssueComission> ProductsIssueComissions { get; set; }
        public virtual ICollection<ProductsPartnerCommission> ProductsPartnerCommissions { get; set; }
        public virtual ICollection<ProductsReviewComission> ProductsReviewComissions { get; set; }
    }
}
