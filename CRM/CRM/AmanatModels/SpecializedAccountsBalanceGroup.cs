﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecializedAccountsBalanceGroup
    {
        public int SpecialTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountGroup { get; set; }

        public virtual AccountGroup AccountGroupNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
