﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Amortization
    {
        public int AssetId { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal Sum { get; set; }
        public decimal Balance { get; set; }
        public int LifeTimePast { get; set; }
        public decimal SumV { get; set; }
        public bool IsStopped { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
