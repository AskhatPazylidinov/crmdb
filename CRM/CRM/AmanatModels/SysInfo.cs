﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SysInfo
    {
        public DateTime DateOd { get; set; }
        public int CurrencyOd { get; set; }
        public long CurrentInnerAccNo { get; set; }
        public int FinancialInstitutionCode { get; set; }
        public int MainOfficeId { get; set; }
        public int OnlineBankUserId { get; set; }
        public DateTime BankStartDate { get; set; }
        public int CountryOd { get; set; }
    }
}
