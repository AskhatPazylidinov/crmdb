﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GaranteesOfficer
    {
        public int GaranteeId { get; set; }
        public byte GaranteeOfficerTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? UserId { get; set; }

        public virtual Garantee Garantee { get; set; }
        public virtual User User { get; set; }
    }
}
