﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerStatusesHistory
    {
        public int CustomerId { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int UserId { get; set; }
        public int? ReasonId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual CustomerStatusesReason Reason { get; set; }
        public virtual CustomerStatus Status { get; set; }
        public virtual User User { get; set; }
    }
}
