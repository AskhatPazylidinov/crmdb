﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsPeriodsCalculationType
    {
        public int ProductId { get; set; }
        public int StartPeriod { get; set; }
        public int? EndPeriod { get; set; }
        public byte CalculationTypeId { get; set; }

        public virtual Product2 Product { get; set; }
    }
}
