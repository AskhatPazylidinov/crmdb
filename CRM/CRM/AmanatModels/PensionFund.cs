﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PensionFund
    {
        public int FundId { get; set; }
        public string Fundname { get; set; }
    }
}
