﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
