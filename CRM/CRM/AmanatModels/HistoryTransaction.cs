﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoryTransaction
    {
        public int HistoryId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual OperationsHistory History { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
