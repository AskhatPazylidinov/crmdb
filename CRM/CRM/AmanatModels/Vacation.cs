﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Vacation
    {
        public Vacation()
        {
            VacationsCalculations = new HashSet<VacationsCalculation>();
        }

        public int OrderId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Order Order { get; set; }
        public virtual ICollection<VacationsCalculation> VacationsCalculations { get; set; }
    }
}
