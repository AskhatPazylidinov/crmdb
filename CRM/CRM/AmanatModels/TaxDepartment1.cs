﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDepartment1
    {
        public TaxDepartment1()
        {
            SpecialServicesBankDetails = new HashSet<SpecialServicesBankDetail>();
            TaxDepartmentsComissions = new HashSet<TaxDepartmentsComission>();
            TaxDepartmentsTransitAccounts = new HashSet<TaxDepartmentsTransitAccount>();
            TaxPayment1s = new HashSet<TaxPayment1>();
            TaxServicesComissions = new HashSet<TaxServicesComission>();
            UtilitiesImportFiles = new HashSet<UtilitiesImportFile>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public bool IsOkmotCodeRequired { get; set; }
        public string ExternalBikcode { get; set; }
        public string ExternalAccountNo { get; set; }
        public bool TakeUserCommentAsClearingComment { get; set; }
        public bool? IsBudgetPayments { get; set; }
        public int TaxDistrictId { get; set; }
        public string ProcessorAssemblyName { get; set; }

        public virtual TaxDistrict TaxDistrict { get; set; }
        public virtual TaxOkmotCode TaxOkmotCode { get; set; }
        public virtual ICollection<SpecialServicesBankDetail> SpecialServicesBankDetails { get; set; }
        public virtual ICollection<TaxDepartmentsComission> TaxDepartmentsComissions { get; set; }
        public virtual ICollection<TaxDepartmentsTransitAccount> TaxDepartmentsTransitAccounts { get; set; }
        public virtual ICollection<TaxPayment1> TaxPayment1s { get; set; }
        public virtual ICollection<TaxServicesComission> TaxServicesComissions { get; set; }
        public virtual ICollection<UtilitiesImportFile> UtilitiesImportFiles { get; set; }
    }
}
