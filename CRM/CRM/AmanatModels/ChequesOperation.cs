﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChequesOperation
    {
        public int CustomerId { get; set; }
        public string ChequeSeries { get; set; }
        public string ChequePage { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
