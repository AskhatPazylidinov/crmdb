﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProvidersTransitAccount
    {
        public int BranchId { get; set; }
        public int ProviderId { get; set; }
        public string TransitAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
