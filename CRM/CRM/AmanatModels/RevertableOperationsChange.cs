﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperationsChange
    {
        public long RevertId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte ChangeTypeId { get; set; }

        public virtual RevertableOperation Revert { get; set; }
    }
}
