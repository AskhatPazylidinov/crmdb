﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsUtility
    {
        public int OperationId { get; set; }
        public int UtilityId { get; set; }

        public virtual DetailedOperation Operation { get; set; }
        public virtual UtilitiesRegistry Utility { get; set; }
    }
}
