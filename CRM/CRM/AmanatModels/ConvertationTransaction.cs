﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConvertationTransaction
    {
        public int ConvertationId { get; set; }
        public long Position { get; set; }

        public virtual Convertation Convertation { get; set; }
    }
}
