﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SubsideGraphic
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int GraphicId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal SubsidePercentsSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
