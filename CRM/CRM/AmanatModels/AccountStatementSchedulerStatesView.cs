﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountStatementSchedulerStatesView
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public string Branch { get; set; }
        public int OfficeId { get; set; }
        public string Office { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public string AccountNo { get; set; }
        public string Symbol { get; set; }
        public string Period { get; set; }
        public string Addresses { get; set; }
        public DateTime DateSend { get; set; }
        public string Details { get; set; }
        public bool IsSend { get; set; }
    }
}
