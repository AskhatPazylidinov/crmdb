﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportTransactionPosition
    {
        public long TransactionId { get; set; }
        public long Position { get; set; }

        public virtual ImportTransaction Transaction { get; set; }
    }
}
