﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsSpecialAccount
    {
        public int ProductId { get; set; }
        public byte AccountTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
