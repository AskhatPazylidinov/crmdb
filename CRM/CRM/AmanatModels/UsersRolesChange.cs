﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersRolesChange
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? UserSet { get; set; }

        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
