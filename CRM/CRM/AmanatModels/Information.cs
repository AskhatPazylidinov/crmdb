﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Information
    {
        public Information()
        {
            Histories = new HashSet<History>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? CanIssueSeveralLoans { get; set; }
        public int? ProductId { get; set; }
        public bool? IsIssueComissionRound { get; set; }
        public bool? NotUseCurrentAccountBalance { get; set; }

        public virtual ICollection<History> Histories { get; set; }
    }
}
