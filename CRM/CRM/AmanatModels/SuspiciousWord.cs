﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SuspiciousWord
    {
        public string CheckWord { get; set; }
        public bool IsSuspicious { get; set; }
        public string ShadyCodes { get; set; }
    }
}
