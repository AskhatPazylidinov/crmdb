﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Setting2
    {
        public string Address { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ProxyAddress { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }
    }
}
