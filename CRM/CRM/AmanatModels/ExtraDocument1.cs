﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocument1
    {
        public int ExtraDocumentId { get; set; }
        public string DisplayName { get; set; }
        public string Filename { get; set; }
        public byte CanRunOnEmptyCredit { get; set; }
        public bool IsGuarantorFile { get; set; }
        public int? GroupId { get; set; }
        public bool? InPdf { get; set; }

        public virtual ExtraDocumentGroup1 Group { get; set; }
    }
}
