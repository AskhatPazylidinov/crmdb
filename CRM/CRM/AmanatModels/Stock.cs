﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Stock
    {
        public int TypeId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Balance { get; set; }
        public int? QuantityOut { get; set; }
        public decimal? BalanceIn { get; set; }
        public decimal? BalanceOut { get; set; }
        public int MeasureId { get; set; }
        public int ContractorId { get; set; }
        public int? CurrencyInOut { get; set; }
    }
}
