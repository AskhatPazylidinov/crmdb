﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionTypeMpc
    {
        public int CodeId { get; set; }
        public string Transaction { get; set; }
        public string Description { get; set; }
    }
}
