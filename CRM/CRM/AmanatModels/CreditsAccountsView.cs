﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsAccountsView
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int TypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal SumV { get; set; }
        public string AccountName { get; set; }
        public int OfficeId { get; set; }
        public string SrtOffice { get; set; }
        public string SrtCurrency { get; set; }
        public int BranchId { get; set; }
        public string SrtBranch { get; set; }
        public int? OfficerId { get; set; }
    }
}
