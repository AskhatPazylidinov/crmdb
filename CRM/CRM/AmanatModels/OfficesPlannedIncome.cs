﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesPlannedIncome
    {
        public int OfficeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string BalanceGroup { get; set; }
        public decimal PlannedSumm { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
