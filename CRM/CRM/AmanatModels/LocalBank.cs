﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class LocalBank
    {
        public int BicCode { get; set; }
        public string SwiftCode { get; set; }
        public byte ParticipationTypeId { get; set; }
        public string CorrespondentAccountNo { get; set; }
        public string BankName { get; set; }
        public bool AcceptsGross { get; set; }
        public bool AcceptsClearing { get; set; }
        public string Address { get; set; }
    }
}
