﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Vocode
    {
        public Vocode()
        {
            Swift103Transfers = new HashSet<Swift103Transfer>();
        }

        public string CodeVo { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }

        public virtual ICollection<Swift103Transfer> Swift103Transfers { get; set; }
    }
}
