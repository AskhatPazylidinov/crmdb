﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesClassification
    {
        public ReservesClassification()
        {
            HistoriesReservesClassifications = new HashSet<HistoriesReservesClassification>();
            ProductsReservesClassifications = new HashSet<ProductsReservesClassification>();
            ReservesClassificationsChanges = new HashSet<ReservesClassificationsChange>();
        }

        public int TypeId { get; set; }
        public string ClassificationName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<HistoriesReservesClassification> HistoriesReservesClassifications { get; set; }
        public virtual ICollection<ProductsReservesClassification> ProductsReservesClassifications { get; set; }
        public virtual ICollection<ReservesClassificationsChange> ReservesClassificationsChanges { get; set; }
    }
}
