﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesStatus
    {
        public int SecurityId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }

        public virtual Security Security { get; set; }
        public virtual User User { get; set; }
    }
}
