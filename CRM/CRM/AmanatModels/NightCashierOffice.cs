﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NightCashierOffice
    {
        public int OfficeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SpecialCashierType { get; set; }
        public bool IsActive { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
