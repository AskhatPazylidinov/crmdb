﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesClassificationsRule
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int ReserveTypeId { get; set; }
        public int ReserveRuleId { get; set; }

        public virtual ReservesType ReserveType { get; set; }
        public virtual ReservesClassificationsChange ReservesClassificationsChange { get; set; }
    }
}
