﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncomeAccount
    {
        public int OfficeId { get; set; }
        public int CustomerTypeId { get; set; }
        public int OperationTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
