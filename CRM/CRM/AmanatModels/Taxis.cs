﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Taxis
    {
        public byte Code { get; set; }
        public string Name { get; set; }
        public decimal Rait { get; set; }
    }
}
