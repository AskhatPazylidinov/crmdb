﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekPhoneSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
    }
}
