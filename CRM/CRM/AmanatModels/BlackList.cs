﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BlackList
    {
        public BlackList()
        {
            FuzzyCompareCoeffs = new HashSet<FuzzyCompareCoeff>();
            People = new HashSet<Person>();
            Status1s = new HashSet<Status1>();
        }

        public int BlackListId { get; set; }
        public string Name { get; set; }
        public int DepartmentId { get; set; }
        public string Description { get; set; }
        public bool IsNameDisplayed { get; set; }
        public byte? StopActionId { get; set; }
        public byte? BlackListLoadProvider { get; set; }
        public DateTime? ExternalDate { get; set; }
        public bool UseDllLogic { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<FuzzyCompareCoeff> FuzzyCompareCoeffs { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual ICollection<Status1> Status1s { get; set; }
    }
}
