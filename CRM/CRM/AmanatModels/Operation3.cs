﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Operation3
    {
        public long Position { get; set; }
        public DateTime? FlOperdate { get; set; }
        public int? FlOpercode { get; set; }
        public string FlOpercodes { get; set; }
        public string FlLimitcodes { get; set; }
        public string FlShadycodes { get; set; }
        public bool? Exported { get; set; }
        public string FlExtrainfo { get; set; }
        public int? ModeTypeId { get; set; }
        public int? CustomerId { get; set; }
    }
}
