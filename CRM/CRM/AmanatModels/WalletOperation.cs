﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class WalletOperation
    {
        public WalletOperation()
        {
            IpcAggregatorExecuteTransactionLogs = new HashSet<IpcAggregatorExecuteTransactionLog>();
            WalletOperationTransactions = new HashSet<WalletOperationTransaction>();
        }

        public long OperationId { get; set; }
        public long ExternalOperationId { get; set; }
        public DateTime? ExternalOperationDateTime { get; set; }
        public decimal Sum { get; set; }
        public DateTime? OperationDateTime { get; set; }
        public string ElcartOperationId { get; set; }
        public bool IsCardIssuedInCurrentBank { get; set; }
        public string Comment { get; set; }

        public virtual ICollection<IpcAggregatorExecuteTransactionLog> IpcAggregatorExecuteTransactionLogs { get; set; }
        public virtual ICollection<WalletOperationTransaction> WalletOperationTransactions { get; set; }
    }
}
