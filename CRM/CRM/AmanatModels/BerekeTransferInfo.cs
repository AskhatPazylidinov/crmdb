﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BerekeTransferInfo
    {
        public int Id { get; set; }
        public long TransferId { get; set; }
        public int BerekeDocKey { get; set; }
        public string BerekeDocNo { get; set; }
        public DateTime TransactionDate { get; set; }
        public string PickUpPointCode { get; set; }
        public int? SenderClientKey { get; set; }
        public int? ReceiverClientKey { get; set; }

        public virtual MoneyTransfer Transfer { get; set; }
    }
}
