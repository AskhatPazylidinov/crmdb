﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OnlineOperation
    {
        public long OperationId { get; set; }
        public DateTime OperationDate { get; set; }
        public string Reference { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal OperationSum { get; set; }
        public decimal BankComission { get; set; }
        public string SenderSurname { get; set; }
        public string SenderName { get; set; }
        public string SenderOtchestvo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? DocumentValidTill { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactAddress { get; set; }
        public string CountryCode2 { get; set; }
        public string PaymentComment { get; set; }
        public byte? PaidStatus { get; set; }
        public long? Position { get; set; }
        public string Comment { get; set; }
        public string SendPoint { get; set; }
        public string PickupPoint { get; set; }
        public long? TransferId { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
