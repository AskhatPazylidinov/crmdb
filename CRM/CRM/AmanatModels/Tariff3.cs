﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Tariff3
    {
        public int TariffId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int MoneySystemId { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal TotalComission { get; set; }
        public byte TotalComissionTypeId { get; set; }
        public decimal SenderComission { get; set; }
        public byte SenderComissionTypeId { get; set; }
        public decimal ReceiverComission { get; set; }
        public byte ReceiverComissionTypeId { get; set; }

        public virtual TariffsChangeDate4 ChangeDateNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual MoneySystem MoneySystem { get; set; }
    }
}
