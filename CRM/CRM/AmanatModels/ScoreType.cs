﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ScoreType
    {
        public ScoreType()
        {
            CorrectiveRuleCurrentScoreTypes = new HashSet<CorrectiveRule>();
            CorrectiveRuleNewScoreTypes = new HashSet<CorrectiveRule>();
        }

        public int ScoreTypeId { get; set; }
        public int ScoringId { get; set; }
        public string EngName { get; set; }
        public string RusName { get; set; }
        public int? ScoreMinValue { get; set; }

        public virtual ICollection<CorrectiveRule> CorrectiveRuleCurrentScoreTypes { get; set; }
        public virtual ICollection<CorrectiveRule> CorrectiveRuleNewScoreTypes { get; set; }
    }
}
