﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonCashAccount
    {
        public int CustomerId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
