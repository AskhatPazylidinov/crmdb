﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeGroupIncExpOperation
    {
        public int GroupId { get; set; }
        public int EmployeeId { get; set; }
        public decimal Value { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual OfficeGroupIncExp Group { get; set; }
    }
}
