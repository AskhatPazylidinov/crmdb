﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowedOperationsInRole
    {
        public int RoleId { get; set; }
        public int OperationId { get; set; }

        public virtual SecuredOperation Operation { get; set; }
        public virtual Role Role { get; set; }
    }
}
