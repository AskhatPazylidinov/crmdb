﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Relative
    {
        public int CustomerId { get; set; }
        public int RelativeId { get; set; }
        public int RelativeTypeId { get; set; }
        public int? UserId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Customer RelativeNavigation { get; set; }
        public virtual RelativeType RelativeType { get; set; }
    }
}
