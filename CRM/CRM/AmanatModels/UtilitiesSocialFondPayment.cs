﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesSocialFondPayment
    {
        public int OperationId { get; set; }
        public string FullCustomerName { get; set; }
        public string PersonalAccountNo { get; set; }
        public string Okpocode { get; set; }
        public int ServiceId { get; set; }
        public int PayerTypeId { get; set; }
        public int PaymentTypeId { get; set; }
        public int DepartmentId { get; set; }
        public long? RegistrationNo { get; set; }

        public virtual SocialFondDepartment Department { get; set; }
        public virtual SocialFondService Service { get; set; }
    }
}
