﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesRule
    {
        public ReservesRule()
        {
            ReservesRulesIndicatorsCalcValues = new HashSet<ReservesRulesIndicatorsCalcValue>();
        }

        public int ReserveRuleId { get; set; }
        public string RuleName { get; set; }
        public string Description { get; set; }
        public decimal ReserveRate { get; set; }

        public virtual ICollection<ReservesRulesIndicatorsCalcValue> ReservesRulesIndicatorsCalcValues { get; set; }
    }
}
