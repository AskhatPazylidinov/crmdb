﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionLimitsByRole
    {
        public int RoleId { get; set; }
        public DateTime LimitDate { get; set; }
        public decimal StartSumN { get; set; }
        public decimal EndSumN { get; set; }
        public int UserId { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
