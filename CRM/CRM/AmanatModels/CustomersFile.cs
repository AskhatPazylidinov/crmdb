﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersFile
    {
        public int CustomerId { get; set; }
        public byte Type { get; set; }
        public string FileName { get; set; }
        public byte? ModuleId { get; set; }
        public string Context { get; set; }
        public int Id { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
