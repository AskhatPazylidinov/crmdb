﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneyTransfersWithFilter
    {
        public long TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public int MoneySystemId { get; set; }
        public string PaymentCode { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrencyId { get; set; }
        public decimal TotalComission { get; set; }
        public decimal BankComission { get; set; }
        public decimal? ComissionReturnedOnCancel { get; set; }
        public string PaymentComment { get; set; }
        public int? SenderCustomerId { get; set; }
        public string SenderCustomerName { get; set; }
        public int? ReceiverCustomerId { get; set; }
        public string ReceiverCustomerName { get; set; }
        public int ContragentCountryId { get; set; }
        public byte DirectionTypeId { get; set; }
        public string ContrAccountNo { get; set; }
        public decimal SumOnContrAccount { get; set; }
        public string Locality { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public int StatusId { get; set; }
        public int? ApproveUserId { get; set; }
        public DateTime OperationDate { get; set; }
        public long? Position { get; set; }
        public string OfficeName { get; set; }
    }
}
