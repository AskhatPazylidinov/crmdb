﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafesLeasing
    {
        public SafesLeasing()
        {
            LeasingsPayments = new HashSet<LeasingsPayment>();
            SafesAttorneys = new HashSet<SafesAttorney>();
            TransferLeasingOldLeases = new HashSet<TransferLeasing>();
        }

        public int LeaseId { get; set; }
        public int SafeId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? CloseDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Safe Safe { get; set; }
        public virtual TransferLeasing TransferLeasingNewLease { get; set; }
        public virtual ICollection<LeasingsPayment> LeasingsPayments { get; set; }
        public virtual ICollection<SafesAttorney> SafesAttorneys { get; set; }
        public virtual ICollection<TransferLeasing> TransferLeasingOldLeases { get; set; }
    }
}
