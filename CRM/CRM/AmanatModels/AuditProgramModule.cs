﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AuditProgramModule
    {
        public int Id { get; set; }
        public int ProgramModule { get; set; }
        public int ProgramModuleForCheck { get; set; }
        public int CheckUnitType { get; set; }
    }
}
