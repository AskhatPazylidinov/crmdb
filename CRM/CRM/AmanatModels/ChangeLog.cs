﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeLog
    {
        public ChangeLog()
        {
            ChangeLogDetails = new HashSet<ChangeLogDetail>();
        }

        public int Id { get; set; }
        public int LogType { get; set; }
        public string Key { get; set; }
        public string Entity { get; set; }
        public string EntityName { get; set; }
        public string ContextInfo { get; set; }
        public byte Action { get; set; }
        public int UserId { get; set; }

        public virtual ICollection<ChangeLogDetail> ChangeLogDetails { get; set; }
    }
}
