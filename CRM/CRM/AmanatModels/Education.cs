﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Education
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int InstituteId { get; set; }
        public string EducationType { get; set; }
        public string Profession { get; set; }
        public string Qualification { get; set; }
        public bool? Status { get; set; }
        public int? StudyType { get; set; }
        public string Diplom { get; set; }
        public string DiplomSvid { get; set; }
        public int? StartYear { get; set; }
        public int? EndYear { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual Institution Institute { get; set; }
    }
}
