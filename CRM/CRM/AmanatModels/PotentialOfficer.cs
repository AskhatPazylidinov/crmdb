﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PotentialOfficer
    {
        public int PotentialId { get; set; }
        public int UserId { get; set; }
        public byte OfficerType { get; set; }
        public int? ParentPotentialId { get; set; }
        public DateTime? DateFrom { get; set; }
    }
}
