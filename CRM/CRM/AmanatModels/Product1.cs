﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Product1
    {
        public Product1()
        {
            Currency2s = new HashSet<Currency2>();
            EditApprovedParamsLimitsForUsers = new HashSet<EditApprovedParamsLimitsForUser>();
            EditApprovedParamsLimitsOnRoles = new HashSet<EditApprovedParamsLimitsOnRole>();
            Histories = new HashSet<History>();
            IssueLoanLimitsForUsers = new HashSet<IssueLoanLimitsForUser>();
            IssueLoanLimitsOnRoles = new HashSet<IssueLoanLimitsOnRole>();
            ProductCodes = new HashSet<ProductCode>();
            ProductPurposes = new HashSet<ProductPurpose>();
            ProductsApproveTypes = new HashSet<ProductsApproveType>();
            ProductsBalanceGroups = new HashSet<ProductsBalanceGroup>();
            ProductsChanges = new HashSet<ProductsChange>();
            ProductsDocument1s = new HashSet<ProductsDocument1>();
            ProductsGuaranteePurposes = new HashSet<ProductsGuaranteePurpose>();
            ProductsGuaranteesDocuments = new HashSet<ProductsGuaranteesDocument>();
            ProductsInsuranceCompanies = new HashSet<ProductsInsuranceCompany>();
            ProductsMonitoringsSettings = new HashSet<ProductsMonitoringsSetting>();
            ProductsMortrageTypes = new HashSet<ProductsMortrageType>();
            ProductsPaymentSources = new HashSet<ProductsPaymentSource>();
            ProductsPurposesBalanceGroups = new HashSet<ProductsPurposesBalanceGroup>();
            ProductsReservesClassifications = new HashSet<ProductsReservesClassification>();
            ProductsSpecialAccounts = new HashSet<ProductsSpecialAccount>();
            SubStatusesLimitsForUsers = new HashSet<SubStatusesLimitsForUser>();
            SubStatusesLimitsOnRoles = new HashSet<SubStatusesLimitsOnRole>();
            SubStatusesPeriods = new HashSet<SubStatusesPeriod>();
            SubStatusesRequestsLimitsForUsers = new HashSet<SubStatusesRequestsLimitsForUser>();
            SubStatusesRequestsLimitsOnRoles = new HashSet<SubStatusesRequestsLimitsOnRole>();
            TranchesLimits = new HashSet<TranchesLimit>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsForIndividuals { get; set; }
        public bool IsForCompanies { get; set; }
        public bool IsForResidents { get; set; }
        public bool IsForNonResidents { get; set; }
        public bool IsOwnMoneyRequired { get; set; }
        public byte? OwnMoneyAmount { get; set; }
        public byte CreditTypeId { get; set; }
        public byte? MinPeopleInGroup { get; set; }
        public byte? MaxPeopleInGroup { get; set; }
        public bool IsTranchesAllowed { get; set; }
        public byte? MaxTranches { get; set; }
        public byte? MaxTranchesAtOneTime { get; set; }
        public string AdditionalInformation { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime OpenOrderDate { get; set; }
        public string CloseOrderNo { get; set; }
        public DateTime? CloseOrderDate { get; set; }
        public int? MaxOverdueDaysForTrancheIssue { get; set; }
        public decimal? DiscountForPositiveCredit { get; set; }
        public decimal? DiscountInterestLimit { get; set; }
        public string RemarksForUser { get; set; }
        public byte? MaxTranchePeriod { get; set; }
        public bool HasPartners { get; set; }
        public byte? PartnerTypeId { get; set; }
        public bool? IsTrancheExtendable { get; set; }
        public byte? MaxDaysForTrancheCancellation { get; set; }
        public bool IsMortgageProduct { get; set; }
        public bool IsBusinessProduct { get; set; }
        public bool? IsInBonusSystem { get; set; }
        public byte ProductTypeId { get; set; }
        public byte IssueComissionPaymentTypeId { get; set; }
        public byte CalculationType { get; set; }
        public bool? UseOffBalanceAccounts { get; set; }
        public byte PercentsBaseTypeId { get; set; }
        public bool? IsForIndividualEnterpreneurs { get; set; }
        public byte? PercentsCalculationSchemeTypeId { get; set; }
        public byte? IssueTranchTypeId { get; set; }
        public string NameEng { get; set; }
        public bool? IsGraphicRequired { get; set; }
        public int? PeriodForTrancheCancellationCheck { get; set; }
        public int? OverduePeriodsForTrancheCancellation { get; set; }
        public byte? BeforeRevolvingWindowDays { get; set; }
        public byte? RevolvingWindowDays { get; set; }
        public byte? LastPeriodForStartReductionLimit { get; set; }
        public bool AllowFromInternetBanking { get; set; }
        public int? MaxOverduePeriodsForLimitRestore { get; set; }
        public string ImageName { get; set; }
        public bool CanIssueOnlyOneTranch { get; set; }
        public bool? UseDiscountAccount { get; set; }
        public bool? IsPercentsCalculationSourceExternal { get; set; }
        public bool? IsFullPaymentAllowed { get; set; }

        public virtual ExportProduct ExportProduct { get; set; }
        public virtual ProductsGroupCode ProductsGroupCode { get; set; }
        public virtual ICollection<Currency2> Currency2s { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsForUser> EditApprovedParamsLimitsForUsers { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsOnRole> EditApprovedParamsLimitsOnRoles { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<IssueLoanLimitsForUser> IssueLoanLimitsForUsers { get; set; }
        public virtual ICollection<IssueLoanLimitsOnRole> IssueLoanLimitsOnRoles { get; set; }
        public virtual ICollection<ProductCode> ProductCodes { get; set; }
        public virtual ICollection<ProductPurpose> ProductPurposes { get; set; }
        public virtual ICollection<ProductsApproveType> ProductsApproveTypes { get; set; }
        public virtual ICollection<ProductsBalanceGroup> ProductsBalanceGroups { get; set; }
        public virtual ICollection<ProductsChange> ProductsChanges { get; set; }
        public virtual ICollection<ProductsDocument1> ProductsDocument1s { get; set; }
        public virtual ICollection<ProductsGuaranteePurpose> ProductsGuaranteePurposes { get; set; }
        public virtual ICollection<ProductsGuaranteesDocument> ProductsGuaranteesDocuments { get; set; }
        public virtual ICollection<ProductsInsuranceCompany> ProductsInsuranceCompanies { get; set; }
        public virtual ICollection<ProductsMonitoringsSetting> ProductsMonitoringsSettings { get; set; }
        public virtual ICollection<ProductsMortrageType> ProductsMortrageTypes { get; set; }
        public virtual ICollection<ProductsPaymentSource> ProductsPaymentSources { get; set; }
        public virtual ICollection<ProductsPurposesBalanceGroup> ProductsPurposesBalanceGroups { get; set; }
        public virtual ICollection<ProductsReservesClassification> ProductsReservesClassifications { get; set; }
        public virtual ICollection<ProductsSpecialAccount> ProductsSpecialAccounts { get; set; }
        public virtual ICollection<SubStatusesLimitsForUser> SubStatusesLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesLimitsOnRole> SubStatusesLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesPeriod> SubStatusesPeriods { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsForUser> SubStatusesRequestsLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsOnRole> SubStatusesRequestsLimitsOnRoles { get; set; }
        public virtual ICollection<TranchesLimit> TranchesLimits { get; set; }
    }
}
