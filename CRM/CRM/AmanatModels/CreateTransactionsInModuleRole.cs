﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreateTransactionsInModuleRole
    {
        public int RoleId { get; set; }
        public int ProgramModuleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual ProgramModule ProgramModule { get; set; }
        public virtual Role Role { get; set; }
    }
}
