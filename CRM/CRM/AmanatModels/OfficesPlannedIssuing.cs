﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesPlannedIssuing
    {
        public int OfficeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal PlannedSumm { get; set; }
        public int PlannedCurrencyId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Currency3 PlannedCurrency { get; set; }
    }
}
