﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitsByAmountsAndDay
    {
        public DateTime ChangeDate { get; set; }
        public int NumOfDays { get; set; }
        public byte PaymentDirection { get; set; }
        public decimal? TransferSumm { get; set; }

        public virtual LimitsByAmountsAndDaysChangeDate ChangeDateNavigation { get; set; }
    }
}
