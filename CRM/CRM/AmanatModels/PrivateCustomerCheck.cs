﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PrivateCustomerCheck
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Comment { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
