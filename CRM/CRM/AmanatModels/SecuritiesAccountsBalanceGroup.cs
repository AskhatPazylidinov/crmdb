﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesAccountsBalanceGroup
    {
        public byte AccountTypeId { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
