﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Child
    {
        public int ChildId { get; set; }
        public int EmployeeId { get; set; }
        public string BirthDocumentNo { get; set; }
        public DateTime BirthDate { get; set; }
        public bool? HasDeduction { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
