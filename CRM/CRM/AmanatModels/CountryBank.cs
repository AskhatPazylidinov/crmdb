﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CountryBank
    {
        public int KyId { get; set; }
        public int? KyCountryCode { get; set; }
        public string FlName { get; set; }
        public string FlAddress { get; set; }
        public string FlBic { get; set; }
        public string FlDetailsWebaddress { get; set; }
        public string FlDetailsEmail { get; set; }
    }
}
