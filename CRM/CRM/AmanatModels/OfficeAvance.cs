﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeAvance
    {
        public int OfficeId { get; set; }
        public DateTime AvanceDate { get; set; }
        public int Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public long? Position { get; set; }
        public bool? Bonus { get; set; }
    }
}
