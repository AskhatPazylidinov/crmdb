﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GuaranteePurpose
    {
        public GuaranteePurpose()
        {
            Histories = new HashSet<History>();
            ProductsGuaranteePurposes = new HashSet<ProductsGuaranteePurpose>();
            ProductsGuaranteesDocuments = new HashSet<ProductsGuaranteesDocument>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<ProductsGuaranteePurpose> ProductsGuaranteePurposes { get; set; }
        public virtual ICollection<ProductsGuaranteesDocument> ProductsGuaranteesDocuments { get; set; }
    }
}
