﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PrevLoansCount
    {
        public int CustomerId { get; set; }
        public int TotalPrevLoans { get; set; }
    }
}
