﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCreditsPurpose
    {
        public int PurposeTypeId { get; set; }
        public int CibpurposeTypeId { get; set; }

        public virtual Purpose PurposeType { get; set; }
    }
}
