﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GuaranteeType
    {
        public GuaranteeType()
        {
            GuaranteeTypeIds = new HashSet<GuaranteeTypeId>();
        }

        public string Code { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GuaranteeTypeId> GuaranteeTypeIds { get; set; }
    }
}
