﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Tranch
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime? IssueDate { get; set; }
        public decimal SumTranch { get; set; }
        public byte Period { get; set; }
        public decimal InterestRate { get; set; }

        public virtual History Credit { get; set; }
    }
}
