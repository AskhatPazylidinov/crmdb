﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IntegrationServiceTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public long TransactionId { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
