﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsDifference
    {
        public int OperationId { get; set; }
        public decimal DiffAmount { get; set; }

        public virtual DetailedOperation Operation { get; set; }
    }
}
