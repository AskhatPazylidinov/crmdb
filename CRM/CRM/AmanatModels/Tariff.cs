﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Tariff
    {
        public int TariffId { get; set; }
        public int OfficeId { get; set; }
        public byte? PaymentTypeId { get; set; }
        public byte PaymentDirectionId { get; set; }
        public DateTime ChangeDate { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionTypeId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public byte? SendingTypeId { get; set; }

        public virtual TariffsChangeDate ChangeDateNavigation { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
