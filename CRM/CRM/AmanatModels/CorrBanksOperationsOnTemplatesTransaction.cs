﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrBanksOperationsOnTemplatesTransaction
    {
        public int OperationId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
