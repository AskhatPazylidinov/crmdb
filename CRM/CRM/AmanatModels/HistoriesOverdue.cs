﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesOverdue
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime OperationDate { get; set; }
        public DateTime? MainSummStartOverdueDate { get; set; }
        public int? MainSummOverdueDays { get; set; }
        public decimal? MainOverdueSumm { get; set; }
        public DateTime? PercentSummStartOverdueDate { get; set; }
        public int? PercentSummOverdueDays { get; set; }
        public decimal? PercentBalanceOverdueSumm { get; set; }
        public decimal? PercentOffSystemOverdueSumm { get; set; }
        public decimal? PercentTotalOverdueSumm { get; set; }
        public DateTime? StartOverdueDate { get; set; }
        public int? OverdueDays { get; set; }
        public decimal? TotalBalanceOverdueSumm { get; set; }
        public decimal? TotalOverdueSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
