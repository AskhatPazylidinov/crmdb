﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InternetBankingOperationHistory
    {
        public int Id { get; set; }
        public DateTime ModifyDate { get; set; }
        public int OperationId { get; set; }
        public long InternalId { get; set; }
        public int InternalType { get; set; }
        public long? DocumentId { get; set; }
        public int Step { get; set; }
    }
}
