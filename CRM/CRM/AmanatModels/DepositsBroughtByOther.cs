﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsBroughtByOther
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int BroughtUserId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual User BroughtUser { get; set; }
    }
}
