﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondSetting
    {
        public int Id { get; set; }
        public bool UseIntegration { get; set; }
        public string Url { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
