﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersNotification
    {
        public int CreditId { get; set; }
        public int NotificationType { get; set; }
        public DateTime BankDate { get; set; }
        public int CustomerId { get; set; }
        public string CustomerPhone { get; set; }
        public long? NotificationId { get; set; }

        public virtual History Credit { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
