﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsChange1
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool IsInvestable { get; set; }
        public bool IsWithdrawable { get; set; }
        public bool IsPercentWithdrawable { get; set; }
        public byte WithdrawPercentsPeriod { get; set; }
        public int StopDepositDaysBeforeEnd { get; set; }
        public byte StartWithdrawMonths { get; set; }
        public int ProlongationsCount { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
