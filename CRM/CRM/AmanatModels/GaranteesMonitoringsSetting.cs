﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GaranteesMonitoringsSetting
    {
        public int GaranteeId { get; set; }
        public int MonitoringTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Interval { get; set; }
        public int? MaxMonitorings { get; set; }

        public virtual Garantee Garantee { get; set; }
        public virtual MonitoringsType1 MonitoringType { get; set; }
    }
}
