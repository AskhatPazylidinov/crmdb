﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchTransfersLimit
    {
        public int LimitId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? AgentId { get; set; }
        public int? AgentBranchId { get; set; }
        public int CurrencyId { get; set; }
        public byte DirectionTypeId { get; set; }
        public int DaysInterval { get; set; }
        public decimal LimitValue { get; set; }

        public virtual Agent Agent { get; set; }
        public virtual AgentsBranch AgentBranch { get; set; }
        public virtual InterBranchTransfersLimitsChangeDate ChangeDateNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
