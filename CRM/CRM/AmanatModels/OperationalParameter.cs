﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalParameter
    {
        public int CustomerId { get; set; }
        public int CreditId { get; set; }
        public int ScoringId { get; set; }
        public int Cycle { get; set; }
        public int? Age { get; set; }
        public int? LoanProductId { get; set; }
        public int? RatingCode { get; set; }
        public int RatingOverdue { get; set; }
        public int? WorstOverduesL6m { get; set; }
        public int? WorstPercentOverduesL6m { get; set; }
        public int? OverdueDays { get; set; }
        public int? PercentOverdueDays { get; set; }
        public bool? NoPayments { get; set; }
        public bool? IsExclude { get; set; }
        public string ExcludeComment { get; set; }
        public bool? IsToCalculate { get; set; }
        public int? RatingOverdueDaysCount { get; set; }
        public decimal? LoanBalance { get; set; }
    }
}
