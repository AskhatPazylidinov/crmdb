﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OpenTransaction
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime OpenDate { get; set; }
        public long Position { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
