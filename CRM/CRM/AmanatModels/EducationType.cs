﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EducationType
    {
        public EducationType()
        {
            Customers = new HashSet<Customer>();
            EducationType3s = new HashSet<EducationType3>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<EducationType3> EducationType3s { get; set; }
    }
}
