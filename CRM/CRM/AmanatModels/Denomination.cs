﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Denomination
    {
        public Denomination()
        {
            OperationsDenominations = new HashSet<OperationsDenomination>();
            StorageAmounts = new HashSet<StorageAmount>();
        }

        public int Id { get; set; }
        public decimal NoteValue { get; set; }
        public int CurrencyId { get; set; }
        public bool IsCoin { get; set; }
        public bool IsBad { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ICollection<OperationsDenomination> OperationsDenominations { get; set; }
        public virtual ICollection<StorageAmount> StorageAmounts { get; set; }
    }
}
