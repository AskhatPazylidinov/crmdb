﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Illness1
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? StatusId { get; set; }
        public int? DaysCount { get; set; }
        public int? IncomeId { get; set; }
        public string OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Pdffile { get; set; }
        public decimal? PaymentSum { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? StatusUserId { get; set; }
        public string ResponisibleEmployee { get; set; }
        public int? ResponisibleEmployeeId { get; set; }
        public string Issuer { get; set; }
        public string DocNo { get; set; }
        public decimal? PaymentSumFromGov { get; set; }
        public int? IncomeId2 { get; set; }
        public decimal? AddSalaryPercent { get; set; }
        public decimal? AddSalarySumm { get; set; }
        public bool? IsFullPercentExperience { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
