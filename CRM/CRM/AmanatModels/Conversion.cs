﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Conversion
    {
        public Conversion()
        {
            ConversionsAccounts = new HashSet<ConversionsAccount>();
            ConversionsStatuses = new HashSet<ConversionsStatus>();
            ConversionsTransactions = new HashSet<ConversionsTransaction>();
        }

        public int ConversionId { get; set; }
        public int CustomerId { get; set; }
        public DateTime ConversionDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public decimal? CrossRate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ConversionsAccount> ConversionsAccounts { get; set; }
        public virtual ICollection<ConversionsStatus> ConversionsStatuses { get; set; }
        public virtual ICollection<ConversionsTransaction> ConversionsTransactions { get; set; }
    }
}
