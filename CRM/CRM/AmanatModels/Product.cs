﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Product
    {
        public Product()
        {
            BalanceGroupProducts = new HashSet<BalanceGroupProduct>();
            Cards = new HashSet<Card>();
            Currency1s = new HashSet<Currency1>();
            ProductCardTypes = new HashSet<ProductCardType>();
            ProductChanges = new HashSet<ProductChange>();
            ProductInBranches = new HashSet<ProductInBranch>();
            ProductsBranches = new HashSet<ProductsBranch>();
            ProductsCompanies = new HashSet<ProductsCompany>();
            ProductsDocuments = new HashSet<ProductsDocument>();
        }

        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public bool? IsForIndividuals { get; set; }
        public bool IsForCompanies { get; set; }
        public bool? IsForResidents { get; set; }
        public bool IsForNonResidents { get; set; }
        public bool IsPromo { get; set; }
        public DateTime? OpenDate { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime? CloseDate { get; set; }
        public string CloseOrderNo { get; set; }
        public int Period { get; set; }
        public int? LoanProductId { get; set; }
        public bool IsIndividualEntrepreneur { get; set; }

        public virtual ICollection<BalanceGroupProduct> BalanceGroupProducts { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
        public virtual ICollection<Currency1> Currency1s { get; set; }
        public virtual ICollection<ProductCardType> ProductCardTypes { get; set; }
        public virtual ICollection<ProductChange> ProductChanges { get; set; }
        public virtual ICollection<ProductInBranch> ProductInBranches { get; set; }
        public virtual ICollection<ProductsBranch> ProductsBranches { get; set; }
        public virtual ICollection<ProductsCompany> ProductsCompanies { get; set; }
        public virtual ICollection<ProductsDocument> ProductsDocuments { get; set; }
    }
}
