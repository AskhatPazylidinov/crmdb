﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Citiestemp
    {
        public int? CityId { get; set; }
        public int? RegionId { get; set; }
        public string CityName { get; set; }
        public string Code { get; set; }
        public string PostalCode { get; set; }
        public byte? CityType { get; set; }
        public int? AreaId { get; set; }
    }
}
