﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsAccountsView
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string PercentAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public int ProgramId { get; set; }
        public int Period { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal AvailableSumV { get; set; }
        public byte JointTypeId { get; set; }
        public string CurrentAccountNo { get; set; }
        public int? PurposeId { get; set; }
        public string AgreementNo { get; set; }
        public int OfficeId { get; set; }
        public string AccountName { get; set; }
        public string SrtOffice { get; set; }
        public string SrtCurrency { get; set; }
        public int BranchId { get; set; }
        public string SrtBranch { get; set; }
    }
}
