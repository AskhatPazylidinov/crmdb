﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class City1
    {
        public City1()
        {
            Branches = new HashSet<Branch>();
            CustomerBirthCities = new HashSet<Customer>();
            CustomerBusinessCities = new HashSet<Customer>();
            CustomerRegistrationCities = new HashSet<Customer>();
            CustomerResidenceCities = new HashSet<Customer>();
            Offices = new HashSet<Office>();
        }

        public int CityId { get; set; }
        public int? RegionId { get; set; }
        public string CityName { get; set; }
        public string Code { get; set; }
        public string PostalCode { get; set; }
        public byte CityType { get; set; }
        public int? AreaId { get; set; }

        public virtual Area Area { get; set; }
        public virtual Region1 Region { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<Customer> CustomerBirthCities { get; set; }
        public virtual ICollection<Customer> CustomerBusinessCities { get; set; }
        public virtual ICollection<Customer> CustomerRegistrationCities { get; set; }
        public virtual ICollection<Customer> CustomerResidenceCities { get; set; }
        public virtual ICollection<Office> Offices { get; set; }
    }
}
