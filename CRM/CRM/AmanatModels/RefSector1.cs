﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RefSector1
    {
        public RefSector1()
        {
            PayBalance1s = new HashSet<PayBalance1>();
            PayBalanceTemps = new HashSet<PayBalanceTemp>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PayBalance1> PayBalance1s { get; set; }
        public virtual ICollection<PayBalanceTemp> PayBalanceTemps { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
    }
}
