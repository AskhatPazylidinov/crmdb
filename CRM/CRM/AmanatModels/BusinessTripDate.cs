﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BusinessTripDate
    {
        public int BusinessTripId { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }

        public virtual BusinessTrip BusinessTrip { get; set; }
    }
}
