﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecializedAccount
    {
        public int OfficeId { get; set; }
        public int SpecialTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
