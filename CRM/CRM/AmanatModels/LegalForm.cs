﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class LegalForm
    {
        public int OdbFormTypeId { get; set; }
        public int LegalFormId { get; set; }
    }
}
