﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OwnershipType
    {
        public OwnershipType()
        {
            Customers = new HashSet<Customer>();
        }

        public int OwnershipTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
