﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsHistory
    {
        public OperationsHistory()
        {
            HistoryTransactions = new HashSet<HistoryTransaction>();
        }

        public int HistoryId { get; set; }
        public int AssetId { get; set; }
        public int OperationId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DocumentDescription { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public decimal? OperationSumm { get; set; }
        public int? Quantity { get; set; }
        public int? OfficeId { get; set; }

        public virtual Asset Asset { get; set; }
        public virtual ICollection<HistoryTransaction> HistoryTransactions { get; set; }
    }
}
