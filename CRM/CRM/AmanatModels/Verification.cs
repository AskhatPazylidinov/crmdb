﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Verification
    {
        public int CustomerId { get; set; }
        public DateTime VerificationDate { get; set; }
        public byte VerificatorTypeId { get; set; }
        public int? UserId { get; set; }
        public byte RiskTypeId { get; set; }
        public string Remarks { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
