﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UnistreamUserInfo
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Guid UnistreamCustomerId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
