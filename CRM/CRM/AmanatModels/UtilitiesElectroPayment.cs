﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElectroPayment
    {
        public int OperationId { get; set; }
        public string PersonalAccountNo { get; set; }
        public string FullCustomerName { get; set; }
        public string Address { get; set; }
        public int ServiceTypeId { get; set; }
        public int DepartmentId { get; set; }
        public decimal ToPaySumm { get; set; }
        public decimal FinesSumm { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual UtilitiesElectroDepartment Department { get; set; }
        public virtual UtilitiesRegistry Operation { get; set; }
        public virtual UtilitiesElectroServiceType ServiceType { get; set; }
    }
}
