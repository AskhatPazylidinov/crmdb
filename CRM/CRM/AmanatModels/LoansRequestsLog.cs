﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LoansRequestsLog
    {
        public int CreditId { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public string OfficeName { get; set; }
        public string Region { get; set; }
        public DateTime RequestDate { get; set; }
        public string AccountNo { get; set; }
        public string Product { get; set; }
        public int? GroupCode { get; set; }
        public string GroupName { get; set; }
        public byte? ScheduleType { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Guarantors { get; set; }
        public string Garantees { get; set; }
        public string Operator { get; set; }
        public string ApprovingPerson { get; set; }
        public decimal RequestSumm { get; set; }
        public decimal? ApprovedSumm { get; set; }
        public string RequestCurrency { get; set; }
        public byte? RequestPeriod { get; set; }
        public byte? ApprovedPeriod { get; set; }
        public int RequestCurrencyId { get; set; }
        public int? ApprovedCurrencyId { get; set; }
        public decimal? RequestRate { get; set; }
        public decimal? ApprovedRate { get; set; }
        public byte Status { get; set; }
        public int? SubStatus { get; set; }
        public DateTime? CreateRequestDate { get; set; }
        public DateTime? ApproveDate { get; set; }
        public DateTime? RejectDate { get; set; }
        public DateTime? IssueDate { get; set; }
        public string PaymentSourse { get; set; }
        public string SubStatusName { get; set; }
        public int? RejectType { get; set; }
        public string RejectReason { get; set; }
        public byte? NoticeType { get; set; }
        public int? Cycle { get; set; }
        public string Officer { get; set; }
        public string HeadOfficer { get; set; }
        public int? NumberDaysReview { get; set; }
        public string ExpiredReason { get; set; }
        public string Purpose { get; set; }
    }
}
