﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RelativesLog
    {
        public DateTime OperationDate { get; set; }
        public bool Insert { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public int CustomerId { get; set; }
        public int RelativeId { get; set; }
        public int RelativeTypeId { get; set; }
        public int UserId { get; set; }
    }
}
