﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Transaction
    {
        public int OperationId { get; set; }
        public long Position { get; set; }

        public virtual Operation1 Operation { get; set; }
    }
}
