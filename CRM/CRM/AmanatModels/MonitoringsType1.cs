﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MonitoringsType1
    {
        public MonitoringsType1()
        {
            GaranteesMonitorings = new HashSet<GaranteesMonitoring>();
            GaranteesMonitoringsSettings = new HashSet<GaranteesMonitoringsSetting>();
            ProductsMonitoringsSetting1s = new HashSet<ProductsMonitoringsSetting1>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<GaranteesMonitoring> GaranteesMonitorings { get; set; }
        public virtual ICollection<GaranteesMonitoringsSetting> GaranteesMonitoringsSettings { get; set; }
        public virtual ICollection<ProductsMonitoringsSetting1> ProductsMonitoringsSetting1s { get; set; }
    }
}
