﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankServiceActiveList
    {
        public int? BranchId { get; set; }
        public int? OfficeId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime StartDate { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactPhone2 { get; set; }
        public byte? ReceiveLanguage { get; set; }
        public bool? IsAllowSmsReceive { get; set; }
        public bool? IsAllowMarketingReceive { get; set; }
        public string Name { get; set; }
        public bool? LoanCustomer { get; set; }
        public bool? DepositCustomer { get; set; }
    }
}
