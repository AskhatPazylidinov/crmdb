﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FieldsCalcValue
    {
        public int Id { get; set; }
        public int FieldValueId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public int? OperationId { get; set; }
    }
}
