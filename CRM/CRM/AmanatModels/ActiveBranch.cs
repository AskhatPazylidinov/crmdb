﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ActiveBranch
    {
        public int BranchId { get; set; }
        public int ModuleId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ProgramModule Module { get; set; }
    }
}
