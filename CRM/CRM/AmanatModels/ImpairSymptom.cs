﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImpairSymptom
    {
        public ImpairSymptom()
        {
            HistoriesImpairSymptoms = new HashSet<HistoriesImpairSymptom>();
        }

        public int ImpairSymptomId { get; set; }
        public string Code { get; set; }
        public string Specification { get; set; }
        public bool? Financial { get; set; }
        public bool? Garantees { get; set; }

        public virtual ICollection<HistoriesImpairSymptom> HistoriesImpairSymptoms { get; set; }
    }
}
