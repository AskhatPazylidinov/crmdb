﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCompany
    {
        public ProductsCompany()
        {
            Cards = new HashSet<Card>();
        }

        public int ProductId { get; set; }
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
    }
}
