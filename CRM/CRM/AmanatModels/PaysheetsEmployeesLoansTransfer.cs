﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployeesLoansTransfer
    {
        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public int CreditId { get; set; }
        public decimal TransferSumm { get; set; }

        public virtual History Credit { get; set; }
        public virtual PaysheetsEmployee PaysheetsEmployee { get; set; }
    }
}
