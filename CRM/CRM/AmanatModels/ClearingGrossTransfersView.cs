﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ClearingGrossTransfersView
    {
        public long PaymentId { get; set; }
        public int BranchId { get; set; }
        public int? OfficeId { get; set; }
        public string OfficeName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public byte? PaymentTypeId { get; set; }
        public byte PaymentDirectionId { get; set; }
        public byte? StatusId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime DateP { get; set; }
        public string PaymentCode { get; set; }
        public string Pnumber { get; set; }
        public decimal TransferSumm { get; set; }
        public string PaymentComment { get; set; }
        public DateTime? OperationDate { get; set; }
        public DateTime? StatusDate { get; set; }
        public string ConfirmUserName { get; set; }
        public string SenderFullName { get; set; }
        public string ReceiverFullName { get; set; }
        public string CustomerName { get; set; }
        public bool? IsReciverOk { get; set; }
        public int? Format { get; set; }
        public string SenderAccountNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public string SenderBik { get; set; }
        public string ReceiverBik { get; set; }
        public string LastUser { get; set; }
        public string CreatedUser { get; set; }
        public bool? IsInternetBankingOperation { get; set; }
        public bool? IsCashierConfirm { get; set; }
        public byte? SendingTypeId { get; set; }
        public bool IsUnclarifiedPayment { get; set; }
    }
}
