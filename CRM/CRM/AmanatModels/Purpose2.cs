﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Purpose2
    {
        public int PurposeTypeId { get; set; }
        public int NbkrtypeId { get; set; }
        public int Report { get; set; }
    }
}
