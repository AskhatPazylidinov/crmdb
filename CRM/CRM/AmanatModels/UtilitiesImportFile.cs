﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesImportFile
    {
        public UtilitiesImportFile()
        {
            ImportedTaxPayments = new HashSet<ImportedTaxPayment>();
            ImportedUtilitiesPayments = new HashSet<ImportedUtilitiesPayment>();
            TerminalTaxPayments = new HashSet<TerminalTaxPayment>();
        }

        public int ImportFileId { get; set; }
        public DateTime UploadDate { get; set; }
        public int? ProviderId { get; set; }
        public string Filename { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public int? TaxDepartmentId { get; set; }
        public int OfficeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
        public virtual TaxDepartment1 TaxDepartment { get; set; }
        public virtual ICollection<ImportedTaxPayment> ImportedTaxPayments { get; set; }
        public virtual ICollection<ImportedUtilitiesPayment> ImportedUtilitiesPayments { get; set; }
        public virtual ICollection<TerminalTaxPayment> TerminalTaxPayments { get; set; }
    }
}
