﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InfoSystemPayment
    {
        public int OperationId { get; set; }
        public string CustomerCode { get; set; }
    }
}
