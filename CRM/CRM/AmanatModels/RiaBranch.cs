﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiaBranch
    {
        public int BranchId { get; set; }
        public string RiaBranchCode { get; set; }
    }
}
