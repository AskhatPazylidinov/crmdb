﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsCardPayment
    {
        public long PaymentId { get; set; }
        public long CardPaymentId { get; set; }
    }
}
