﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashOfficesLimit
    {
        public int OfficeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal CashLimit { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
