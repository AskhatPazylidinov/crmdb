﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class WriteOffLoan
    {
        public int Id { get; set; }
        public string WriteOffOrderNumber { get; set; }
        public bool Hopelessness { get; set; }
        public DateTime CollectionEffortsSuspensionDate { get; set; }
        public string CollectionEffortsSuspensionGrounds { get; set; }
        public int CreditId { get; set; }
        public DateTime? WriteOffDate { get; set; }
    }
}
