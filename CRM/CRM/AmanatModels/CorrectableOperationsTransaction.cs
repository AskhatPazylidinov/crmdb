﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrectableOperationsTransaction
    {
        public long WrongPosition { get; set; }
        public long CorrectPosition { get; set; }
    }
}
