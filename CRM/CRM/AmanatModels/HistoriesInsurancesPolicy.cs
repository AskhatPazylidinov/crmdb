﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesInsurancesPolicy
    {
        public int Id { get; set; }
        public int CreditId { get; set; }
        public int TypeId { get; set; }
        public int InsuranceId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PolicyNo { get; set; }
        public decimal PartInPercents { get; set; }
        public string Comment { get; set; }

        public virtual History Credit { get; set; }
        public virtual InsuranceCompany Insurance { get; set; }
        public virtual InsuranceType Type { get; set; }
    }
}
