﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashFlowOperationType
    {
        public CashFlowOperationType()
        {
            CashFlowTransactions = new HashSet<CashFlowTransaction>();
        }

        public int OperationTypeId { get; set; }
        public int TypeId { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }

        public virtual CashFlowType Type { get; set; }
        public virtual ICollection<CashFlowTransaction> CashFlowTransactions { get; set; }
    }
}
