﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProvidersFixedCommissionsPartition
    {
        public int ProviderId { get; set; }
        public byte DirectionTypeId { get; set; }
        public decimal PaymentCommissionToPartition { get; set; }
        public decimal ProviderCommission { get; set; }
        public decimal BankCommission { get; set; }

        public virtual ServiceProvider Provider { get; set; }
    }
}
