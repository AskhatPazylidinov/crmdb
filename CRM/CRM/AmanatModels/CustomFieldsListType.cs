﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomFieldsListType
    {
        public int Id { get; set; }
        public string ListType { get; set; }
        public string Value { get; set; }
        public bool IsTableOrView { get; set; }
        public int? TypeValueOrder { get; set; }
    }
}
