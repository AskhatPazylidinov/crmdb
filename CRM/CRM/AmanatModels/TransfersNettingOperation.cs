﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersNettingOperation
    {
        public DateTime OperationDate { get; set; }
        public int BranchId { get; set; }
        public long Position { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
