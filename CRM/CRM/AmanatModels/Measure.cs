﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Measure
    {
        public int MeasureId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
    }
}
