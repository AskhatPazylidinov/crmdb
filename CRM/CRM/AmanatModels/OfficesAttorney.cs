﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesAttorney
    {
        public OfficesAttorney()
        {
            OfficesAttorneysDeclensions = new HashSet<OfficesAttorneysDeclension>();
        }

        public int AttorneyId { get; set; }
        public int OfficeId { get; set; }
        public string AttorneyPosition { get; set; }
        public string AttorneyNo { get; set; }
        public DateTime AttorneyIssueDate { get; set; }
        public int CustomerId { get; set; }
        public DateTime? AttorneyEndDate { get; set; }
        public bool IsDefault { get; set; }
        public DateTime AttorneyStartDate { get; set; }

        public virtual ICollection<OfficesAttorneysDeclension> OfficesAttorneysDeclensions { get; set; }
    }
}
