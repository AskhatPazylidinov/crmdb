﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RequestsLog2
    {
        public long LogId { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public DateTime RequestDate { get; set; }
        public long FileId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
