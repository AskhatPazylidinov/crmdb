﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportTaxPaymentCode
    {
        public int ImportServiceCode { get; set; }
        public int ServiceId { get; set; }

        public virtual TaxService Service { get; set; }
    }
}
