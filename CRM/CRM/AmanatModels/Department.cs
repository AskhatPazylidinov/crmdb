﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Department
    {
        public Department()
        {
            BlackLists = new HashSet<BlackList>();
        }

        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public string Recipients { get; set; }

        public virtual ICollection<BlackList> BlackLists { get; set; }
    }
}
