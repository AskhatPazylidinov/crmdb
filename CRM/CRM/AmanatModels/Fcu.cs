﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Fcu
    {
        public Fcu()
        {
            OfficeIds = new HashSet<OfficeId>();
        }

        public int BranchCode { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OfficeId> OfficeIds { get; set; }
    }
}
