﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafesAttorney
    {
        public int LeaseId { get; set; }
        public int AttorneyCustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AttorneyNo { get; set; }
        public string Description { get; set; }
        public bool IsCancelled { get; set; }

        public virtual Customer AttorneyCustomer { get; set; }
        public virtual SafesLeasing Lease { get; set; }
    }
}
