﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DisabledAccount
    {
        public DisabledAccount()
        {
            AllowedViewAccounts = new HashSet<AllowedViewAccount>();
            RolesAllowedViewAccounts = new HashSet<RolesAllowedViewAccount>();
        }

        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public bool? IsShow { get; set; }
        public bool? IsAllowCreateTransaction { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual ICollection<AllowedViewAccount> AllowedViewAccounts { get; set; }
        public virtual ICollection<RolesAllowedViewAccount> RolesAllowedViewAccounts { get; set; }
    }
}
