﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitsByAmountsAndDaysChangeDate
    {
        public LimitsByAmountsAndDaysChangeDate()
        {
            LimitsByAmountsAndDays = new HashSet<LimitsByAmountsAndDay>();
        }

        public DateTime ChangeDate { get; set; }

        public virtual ICollection<LimitsByAmountsAndDay> LimitsByAmountsAndDays { get; set; }
    }
}
