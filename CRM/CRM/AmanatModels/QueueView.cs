﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class QueueView
    {
        public long Id { get; set; }
        public string SenderName { get; set; }
        public string ReceiverPhone { get; set; }
        public string Message { get; set; }
        public DateTime CreationDate { get; set; }
        public byte? StatusId { get; set; }
        public bool? IsProcessing { get; set; }
    }
}
