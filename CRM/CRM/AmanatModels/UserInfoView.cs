﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserInfoView
    {
        public int Id { get; set; }
        public int MoneySystemId { get; set; }
        public int? UserId { get; set; }
        public string RequesterId { get; set; }
        public string PrincipalId { get; set; }
        public string Fullname { get; set; }
        public string DomainUsername { get; set; }
    }
}
