﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersCustomer
    {
        public int UserId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
