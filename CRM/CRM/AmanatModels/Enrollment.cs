﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Enrollment
    {
        public string AccountNo { get; set; }
        public decimal SumV { get; set; }
        public DateTime? DateOper { get; set; }
    }
}
