﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OtherPropertyInstallment
    {
        public int Id { get; set; }
        public int? ProchkaId { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Payment { get; set; }
        public decimal? Straf { get; set; }
        public decimal? RealPayment { get; set; }

        public virtual OtherProperty Prochka { get; set; }
    }
}
