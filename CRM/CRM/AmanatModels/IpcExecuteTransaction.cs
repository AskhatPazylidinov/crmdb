﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransaction
    {
        public int Id { get; set; }
        public int CurrencyId { get; set; }
        public decimal SumV { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public int? ApprovedUserId { get; set; }
        public bool IsProcess { get; set; }
        public DateTime BankDate { get; set; }
        public int? BatchNum { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string InternalNo { get; set; }
        public string ProcessMessage { get; set; }

        public virtual IpcExecuteTransactionPayment IpcExecuteTransactionPayment { get; set; }
        public virtual IpcExecuteTransactionTransfer IpcExecuteTransactionTransfer { get; set; }
    }
}
