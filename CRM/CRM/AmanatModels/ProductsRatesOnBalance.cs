﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsRatesOnBalance
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartBalance { get; set; }
        public decimal? EndBalance { get; set; }
        public decimal InterestRate { get; set; }
        public int MonthPeriod { get; set; }

        public virtual ProductsCurrency ProductsCurrency { get; set; }
    }
}
