﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCapitalizationsDate
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte CapitalizeMonth { get; set; }
        public byte CapitalizeDay { get; set; }

        public virtual ProductsChange1 ProductsChange1 { get; set; }
    }
}
