﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FinalTurnover
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public DateTime ImpactDate { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
