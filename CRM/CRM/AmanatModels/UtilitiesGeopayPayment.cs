﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesGeopayPayment
    {
        public int OperationId { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public long GeoPayTransactionId { get; set; }

        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
