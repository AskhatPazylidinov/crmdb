﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersRisqueType
    {
        public int CustomerId { get; set; }
        public int RisqueTypeId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual RisqueType RisqueType { get; set; }
    }
}
