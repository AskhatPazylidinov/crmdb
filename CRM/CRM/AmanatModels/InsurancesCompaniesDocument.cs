﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsurancesCompaniesDocument
    {
        public int DocumentId { get; set; }
        public int InsuranceId { get; set; }
        public int InsuranceTypeId { get; set; }
        public byte DocumentTypeId { get; set; }
        public string Filename { get; set; }

        public virtual InsuranceCompany Insurance { get; set; }
        public virtual InsuranceType InsuranceType { get; set; }
    }
}
