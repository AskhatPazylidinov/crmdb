﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RefSector
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
