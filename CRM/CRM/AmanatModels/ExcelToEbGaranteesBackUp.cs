﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExcelToEbGaranteesBackUp
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public string GarantTypeName { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? GarantSum { get; set; }
        public decimal? MarketSum { get; set; }
        public string Location { get; set; }
        public string LoanAccountNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? OfficeId { get; set; }
        public int? CustomerId { get; set; }
        public int? GarantTypeId { get; set; }
        public int? GaranteeId { get; set; }
        public int? CreditId { get; set; }
    }
}
