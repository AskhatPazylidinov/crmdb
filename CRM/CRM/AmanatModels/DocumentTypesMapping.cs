﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentTypesMapping
    {
        public string BerekeDocumentTypeId { get; set; }
        public int DocumentTypeId { get; set; }
    }
}
