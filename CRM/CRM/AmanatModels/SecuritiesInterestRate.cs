﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesInterestRate
    {
        public int SecurityId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal InterestRate { get; set; }

        public virtual Security Security { get; set; }
    }
}
