﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesClassificationsSetting
    {
        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ReserveTypeId { get; set; }
        public int MinOverdueDays { get; set; }
        public int? MaxOverdueDays { get; set; }
        public decimal Rate { get; set; }
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public bool CanSetManuallyOnly { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ReservesType ReserveType { get; set; }
    }
}
