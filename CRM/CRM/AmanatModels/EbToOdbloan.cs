﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EbToOdbloan
    {
        public int CreditId { get; set; }
        public long DgPozn { get; set; }

        public virtual History Credit { get; set; }
    }
}
