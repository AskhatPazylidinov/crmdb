﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProcessedWorkDay
    {
        public DateTime WorkDate { get; set; }
        public bool Processed { get; set; }
    }
}
