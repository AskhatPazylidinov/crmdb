﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsGroupCode
    {
        public int ProductId { get; set; }
        public string GroupCode { get; set; }

        public virtual Product1 Product { get; set; }
    }
}
