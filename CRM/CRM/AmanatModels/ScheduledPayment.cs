﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ScheduledPayment
    {
        public int PaymentId { get; set; }
        public byte PaymentTypeId { get; set; }
        public byte? PaymentDay { get; set; }
        public byte? PaymentMonth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DebetCurrencyId { get; set; }
        public string DebitAccountNo { get; set; }
        public int CreditCurrencyId { get; set; }
        public string CreditAccountNo { get; set; }
        public byte TransactionDirection { get; set; }
        public decimal Sum { get; set; }
        public string Comment { get; set; }
        public bool Approved { get; set; }
        public bool Locked { get; set; }
        public int UserId { get; set; }

        public virtual Account1 Credit { get; set; }
        public virtual Account1 Deb { get; set; }
        public virtual SchedulePaymentRepeat SchedulePaymentRepeat { get; set; }
    }
}
