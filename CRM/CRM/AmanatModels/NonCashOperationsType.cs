﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonCashOperationsType
    {
        public NonCashOperationsType()
        {
            NonCashOperations = new HashSet<NonCashOperation>();
        }

        public byte TypeId { get; set; }
        public string NameIncome { get; set; }
        public string CommentIncome { get; set; }
        public string NameWithdraw { get; set; }
        public string CommentWithdraw { get; set; }
        public bool IsRateRequired { get; set; }

        public virtual ICollection<NonCashOperation> NonCashOperations { get; set; }
    }
}
