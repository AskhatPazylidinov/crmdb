﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate3
    {
        public TariffsChangeDate3()
        {
            Tariff2s = new HashSet<Tariff2>();
        }

        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }

        public virtual ICollection<Tariff2> Tariff2s { get; set; }
    }
}
