﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerVerificationDateExternalSystem
    {
        public int CustomerId { get; set; }
        public int BlackListId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? StartDate { get; set; }
        public byte Status { get; set; }
    }
}
