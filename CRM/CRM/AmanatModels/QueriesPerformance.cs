﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class QueriesPerformance
    {
        public long LogId { get; set; }
        public DateTime RunDate { get; set; }
        public decimal RunTimeSeconds { get; set; }
        public string BatchText { get; set; }
        public string StatementText { get; set; }
        public string QueryPlan { get; set; }
        public DateTime OperationDate { get; set; }
    }
}
