﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrtSetting
    {
        public int Type { get; set; }
        public string Article { get; set; }

        public virtual Form36 ArticleNavigation { get; set; }
        public virtual OrtType TypeNavigation { get; set; }
    }
}
