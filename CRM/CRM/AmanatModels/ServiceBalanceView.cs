﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceBalanceView
    {
        public string CustomerName { get; set; }
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? IsCharge { get; set; }
        public decimal? AmountAbs { get; set; }
        public string AmountType { get; set; }
    }
}
