﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrdersPosition
    {
        public int OrderId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Order Order { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
