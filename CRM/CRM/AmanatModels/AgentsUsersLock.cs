﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsUsersLock
    {
        public int AgentUserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual AgentsUser AgentUser { get; set; }
    }
}
