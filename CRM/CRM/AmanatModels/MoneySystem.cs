﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneySystem
    {
        public MoneySystem()
        {
            MoneyTransfers = new HashSet<MoneyTransfer>();
            MoneyTransfersAccounts = new HashSet<MoneyTransfersAccount>();
            MoneyTransfersDocuments = new HashSet<MoneyTransfersDocument>();
            NettingOperations = new HashSet<NettingOperation>();
            Tariff3s = new HashSet<Tariff3>();
            TaxesOnNonResidentsIncomes = new HashSet<TaxesOnNonResidentsIncome>();
            UserInfos = new HashSet<UserInfo>();
        }

        public int SystemId { get; set; }
        public string MoneySystemName { get; set; }
        public bool IsOutsideCountrySystem { get; set; }
        public string NationalBankName { get; set; }
        public string OperatorName { get; set; }
        public int? ContragentBankId { get; set; }
        public bool? IsActive { get; set; }

        public virtual PartnerBank ContragentBank { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransfers { get; set; }
        public virtual ICollection<MoneyTransfersAccount> MoneyTransfersAccounts { get; set; }
        public virtual ICollection<MoneyTransfersDocument> MoneyTransfersDocuments { get; set; }
        public virtual ICollection<NettingOperation> NettingOperations { get; set; }
        public virtual ICollection<Tariff3> Tariff3s { get; set; }
        public virtual ICollection<TaxesOnNonResidentsIncome> TaxesOnNonResidentsIncomes { get; set; }
        public virtual ICollection<UserInfo> UserInfos { get; set; }
    }
}
