﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsView
    {
        public long? Position { get; set; }
        public short? Positionn { get; set; }
        public long PaymentId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string CardNo { get; set; }
        public bool IsWithdraw { get; set; }
        public bool IsThroughCash { get; set; }
        public decimal Amount { get; set; }
        public bool? IsPaid { get; set; }
        public DateTime OperationDate { get; set; }
        public string UserName { get; set; }
    }
}
