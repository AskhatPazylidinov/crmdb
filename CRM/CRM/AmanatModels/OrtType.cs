﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrtType
    {
        public OrtType()
        {
            OrtSettings = new HashSet<OrtSetting>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OrtSetting> OrtSettings { get; set; }
    }
}
