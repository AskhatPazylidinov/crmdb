﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersView
    {
        public int TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public decimal TransferSum { get; set; }
        public int TransferCurrencyId { get; set; }
        public decimal BankComission { get; set; }
        public string PaymentComment { get; set; }
        public int SenderCustomerId { get; set; }
        public string SenderAccountNo { get; set; }
        public int ReceiverCustomerId { get; set; }
        public string ReceiverCardNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public string SenderCustomerName { get; set; }
        public string ReceiverCustomerName { get; set; }
        public byte ActualStatus { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
    }
}
