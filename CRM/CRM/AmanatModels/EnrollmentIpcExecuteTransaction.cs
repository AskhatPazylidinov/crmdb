﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EnrollmentIpcExecuteTransaction
    {
        public long OperationId { get; set; }
        public int IpcExecuteTransactionId { get; set; }

        public virtual IpcExecuteTransactionPayment IpcExecuteTransaction { get; set; }
        public virtual EnrollmentOperation Operation { get; set; }
    }
}
