﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesExchangeRate
    {
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime RateDate { get; set; }
        public decimal BuyRate { get; set; }
        public decimal SellRate { get; set; }
        public int UserId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
