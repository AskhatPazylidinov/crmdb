﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IllnessStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
