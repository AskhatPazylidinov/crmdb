﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PlanningInFlow
    {
        public int OfficeId { get; set; }
        public int PlanYear { get; set; }
        public int PlanMonth { get; set; }
        public int ProgramId { get; set; }
        public int Period { get; set; }
        public decimal PlannedSumm { get; set; }
        public int PlannedCurrencyId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Currency3 PlannedCurrency { get; set; }
        public virtual Product2 Program { get; set; }
    }
}
