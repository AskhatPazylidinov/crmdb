﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowancesType
    {
        public AllowancesType()
        {
            EmployeesAllowancesIgnores = new HashSet<EmployeesAllowancesIgnore>();
            OfficesAllowances = new HashSet<OfficesAllowance>();
            PaysheetsEmployeesExtraAllowances = new HashSet<PaysheetsEmployeesExtraAllowance>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<EmployeesAllowancesIgnore> EmployeesAllowancesIgnores { get; set; }
        public virtual ICollection<OfficesAllowance> OfficesAllowances { get; set; }
        public virtual ICollection<PaysheetsEmployeesExtraAllowance> PaysheetsEmployeesExtraAllowances { get; set; }
    }
}
