﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ScoreParameterValue
    {
        public int Id { get; set; }
        public int ScoringId { get; set; }
        public byte ScoringParameterTypeId { get; set; }
        public byte OperationId { get; set; }
        public string ScoreParameterValue1 { get; set; }
        public int ScoreValue { get; set; }
    }
}
