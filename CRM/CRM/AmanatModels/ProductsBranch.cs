﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsBranch
    {
        public int ProductId { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Product Product { get; set; }
    }
}
