﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HigherEducation
    {
        public int EducationId { get; set; }
        public int EmployeeId { get; set; }
        public string UniversityName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FacultyName { get; set; }
        public string SpecialityName { get; set; }
        public string DiplomaNo { get; set; }
        public string Comment { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
