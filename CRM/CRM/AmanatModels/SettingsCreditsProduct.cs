﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCreditsProduct
    {
        public int ProductId { get; set; }
        public int CibproductTypeId { get; set; }
    }
}
