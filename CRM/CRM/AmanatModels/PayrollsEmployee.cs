﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PayrollsEmployee
    {
        public int PayrollId { get; set; }
        public int EmployeeId { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Payroll Payroll { get; set; }
    }
}
