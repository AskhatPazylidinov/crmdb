﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsDocument2
    {
        public int DocumentId { get; set; }
        public int ProductId { get; set; }
        public string Filename { get; set; }
        public byte EventTypeId { get; set; }
        public bool? ToPdf { get; set; }

        public virtual Product2 Product { get; set; }
    }
}
