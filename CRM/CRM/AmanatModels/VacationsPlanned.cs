﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class VacationsPlanned
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
