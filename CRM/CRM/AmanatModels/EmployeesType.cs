﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesType
    {
        public int EmployeeId { get; set; }
        public DateTime ActualDate { get; set; }
        public byte TypeId { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
