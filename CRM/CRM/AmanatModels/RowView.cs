﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RowView
    {
        public string MiltiSelectId { get; set; }
        public int RowsetId { get; set; }
        public int RowId { get; set; }
        public int? Num { get; set; }
        public bool ToSfr { get; set; }
        public DateTime? Date { get; set; }
        public double? SumCur { get; set; }
        public string ShadyCodes { get; set; }
        public string OperCode { get; set; }
        public string CurCodes { get; set; }
        public string Reason { get; set; }
    }
}
