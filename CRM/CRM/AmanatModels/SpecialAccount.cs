﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialAccount
    {
        public int OfficeId { get; set; }
        public int TypeId { get; set; }
        public int AccountType { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? SubGroupId { get; set; }
        public string Comment { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual Type Type { get; set; }
    }
}
