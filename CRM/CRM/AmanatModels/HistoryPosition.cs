﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoryPosition
    {
        public int EmployeeId { get; set; }
        public DateTime AppDate { get; set; }
        public string Department { get; set; }
        public string Posittion { get; set; }
        public string Reason { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
