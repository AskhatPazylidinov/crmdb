﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountsView
    {
        public string Symbol { get; set; }
        public string AccountNo { get; set; }
        public string CustomerName { get; set; }
        public int CurrencyId { get; set; }
    }
}
