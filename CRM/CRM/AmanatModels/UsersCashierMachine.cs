﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersCashierMachine
    {
        public int UserId { get; set; }
        public string MachineName { get; set; }

        public virtual CashMachine MachineNameNavigation { get; set; }
        public virtual User User { get; set; }
    }
}
