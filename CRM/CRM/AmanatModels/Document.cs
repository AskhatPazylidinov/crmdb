﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Document
    {
        public int DocumentId { get; set; }
        public string Filename { get; set; }
        public byte EventTypeId { get; set; }
    }
}
