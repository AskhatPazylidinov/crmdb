﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsInsuranceCompany
    {
        public int ProductId { get; set; }
        public int InsuranceId { get; set; }

        public virtual InsuranceCompany Insurance { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
