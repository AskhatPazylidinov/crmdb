﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OtherPropertyRestrictionType
    {
        public OtherPropertyRestrictionType()
        {
            OtherProperties = new HashSet<OtherProperty>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OtherProperty> OtherProperties { get; set; }
    }
}
