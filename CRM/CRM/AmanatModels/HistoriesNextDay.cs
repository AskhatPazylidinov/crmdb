﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesNextDay
    {
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public bool IsComplete { get; set; }

        public virtual User User { get; set; }
    }
}
