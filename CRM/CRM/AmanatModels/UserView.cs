﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserView
    {
        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public string UserName { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public int? DefaultModuleId { get; set; }
        public string WorkDir { get; set; }
        public DateTime PasswordExpiryDate { get; set; }
        public bool HasToChangePasswordAfterLogin { get; set; }
        public bool SignIn { get; set; }
        public string DomainUsername { get; set; }
        public int BranchId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? RoleId { get; set; }
        public DateTime? BankDate { get; set; }
        public bool? IsLocked { get; set; }
    }
}
