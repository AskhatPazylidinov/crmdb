﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionBatch
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int BatchNum { get; set; }
    }
}
