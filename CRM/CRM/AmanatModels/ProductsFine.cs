﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsFine
    {
        public int FinesId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? FineForLatePercentsPayment { get; set; }
        public byte? FineForLatePercentsPaymentType { get; set; }
        public byte? FineForLatePercentsCalculationType { get; set; }
        public byte? FineForLatePercentsBaseType { get; set; }
        public decimal? FineForLateMainSummPayment { get; set; }
        public byte? FineForLateMainSummPaymentType { get; set; }
        public byte? FineForLateMainSummCalculationType { get; set; }
        public byte? FineForLateMainSummBaseType { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
