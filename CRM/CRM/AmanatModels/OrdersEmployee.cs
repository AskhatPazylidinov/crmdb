﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrdersEmployee
    {
        public int OrderId { get; set; }
        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Order Order { get; set; }
    }
}
