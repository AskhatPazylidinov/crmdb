﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCustomizableComission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ComissionId { get; set; }
        public decimal ComissionValue { get; set; }
        public byte ComissionTypeId { get; set; }

        public virtual CustomizableComission Comission { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
