﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Revaluation
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
    }
}
