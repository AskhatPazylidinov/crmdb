﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Partner1
    {
        public string PartnerKey { get; set; }
        public string PartnerDesc { get; set; }
        public string PartnerAddress { get; set; }
        public string PartnerPhone { get; set; }
        public string PartnerHead { get; set; }
        public DateTime SignedDate { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public string Bic { get; set; }
        public string AccountPhone { get; set; }
    }
}
