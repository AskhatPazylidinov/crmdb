﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Limit1
    {
        public int LimitId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? BranchId { get; set; }
        public int CurrencyId { get; set; }
        public byte DirectionTypeId { get; set; }
        public int DaysInterval { get; set; }
        public decimal LimitValue { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual LimitsChangeDate ChangeDateNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
