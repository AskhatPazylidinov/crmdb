﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeeAccountType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
