﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StartCalculatedPercent
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumV { get; set; }
        public decimal PaidTaxSumV { get; set; }
        public decimal PaidTaxSumN { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
