﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraAgreeementsReplacement
    {
        public int OfficeId { get; set; }
        public string ReplaceParam { get; set; }
        public string ReplaceValue { get; set; }
    }
}
