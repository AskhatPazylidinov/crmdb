﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfersComission
    {
        public long OperationId { get; set; }
        public int ComissionId { get; set; }
        public int CurrencyId { get; set; }
        public decimal ComissionSumm { get; set; }

        public virtual SwiftComission Comission { get; set; }
        public virtual SwiftTransfersOperation Operation { get; set; }
    }
}
