﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsRate
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int StartPeriod { get; set; }
        public decimal StartSumm { get; set; }
        public int EndPeriod { get; set; }
        public decimal InterestRate { get; set; }
        public bool? IsActive { get; set; }
        public decimal? BonusRate { get; set; }
        public decimal? EndSumm { get; set; }
    }
}
