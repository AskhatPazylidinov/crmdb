﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsMonitoringsSetting
    {
        public int ProductId { get; set; }
        public int TypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int Interval { get; set; }
        public int? MaxMonitorings { get; set; }

        public virtual Product1 Product { get; set; }
        public virtual MonitoringsType Type { get; set; }
    }
}
