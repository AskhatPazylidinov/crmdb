﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsGarantee
    {
        public CreditsGarantee()
        {
            CreditsGaranteesStatuses = new HashSet<CreditsGaranteesStatus>();
            GaranteesMonitorings = new HashSet<GaranteesMonitoring>();
        }

        public int RelationId { get; set; }
        public int CreditId { get; set; }
        public int GaranteeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal GarantSum { get; set; }
        public decimal MarketSum { get; set; }
        public DateTime RateDate { get; set; }
        public bool? UseInGaranteeDocuments { get; set; }

        public virtual History Credit { get; set; }
        public virtual Garantee Garantee { get; set; }
        public virtual ICollection<CreditsGaranteesStatus> CreditsGaranteesStatuses { get; set; }
        public virtual ICollection<GaranteesMonitoring> GaranteesMonitorings { get; set; }
    }
}
