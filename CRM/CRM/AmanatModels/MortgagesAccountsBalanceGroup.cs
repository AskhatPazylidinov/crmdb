﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MortgagesAccountsBalanceGroup
    {
        public byte AccountTypeId { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
