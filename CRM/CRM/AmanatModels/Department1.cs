﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Department1
    {
        public Department1()
        {
            CreditsDepartments = new HashSet<CreditsDepartment>();
        }

        public int DepartmentId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CreditsDepartment> CreditsDepartments { get; set; }
    }
}
