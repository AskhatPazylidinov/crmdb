﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PlanReserf
    {
        public int CreditId { get; set; }
        public int ReserveTypeId { get; set; }

        public virtual History Credit { get; set; }
        public virtual ReservesType ReserveType { get; set; }
    }
}
