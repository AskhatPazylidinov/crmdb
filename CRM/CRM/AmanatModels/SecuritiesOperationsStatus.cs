﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesOperationsStatus
    {
        public long OperationId { get; set; }
        public byte StatusId { get; set; }
        public int UserId { get; set; }
        public DateTime RunDate { get; set; }

        public virtual SecuritiesOperation Operation { get; set; }
        public virtual User User { get; set; }
    }
}
