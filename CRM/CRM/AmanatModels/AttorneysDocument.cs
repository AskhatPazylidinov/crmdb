﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AttorneysDocument
    {
        public int DocumentId { get; set; }
        public string FileName { get; set; }
    }
}
