﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportBicCodeFile
    {
        public ImportBicCodeFile()
        {
            ImportBicCodes = new HashSet<ImportBicCode>();
        }

        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime ImportBankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public int? FileImportTypeId { get; set; }

        public virtual ICollection<ImportBicCode> ImportBicCodes { get; set; }
    }
}
