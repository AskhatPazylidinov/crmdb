﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeId
    {
        public int OfficeId1 { get; set; }
        public int BranchCode { get; set; }

        public virtual Fcu BranchCodeNavigation { get; set; }
        public virtual Office1 OfficeId1Navigation { get; set; }
    }
}
