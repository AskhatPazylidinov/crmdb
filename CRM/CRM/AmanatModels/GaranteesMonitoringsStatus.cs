﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GaranteesMonitoringsStatus
    {
        public int MonitoringId { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int PerformUserId { get; set; }
        public DateTime OperationDate { get; set; }
        public string Comment { get; set; }

        public virtual GaranteesMonitoring Monitoring { get; set; }
        public virtual User PerformUser { get; set; }
    }
}
