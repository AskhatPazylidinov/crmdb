﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsInternalPaymentCode
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int PaymentCode { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
