﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class City
    {
        public City()
        {
            Bank1s = new HashSet<Bank1>();
        }

        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public string CityHead { get; set; }
        public string CityEng { get; set; }
        public int CountryId { get; set; }
        public string SendCurrency { get; set; }
        public string RecCurrency { get; set; }
        public string PpCode { get; set; }

        public virtual Country1 Country { get; set; }
        public virtual ICollection<Bank1> Bank1s { get; set; }
    }
}
