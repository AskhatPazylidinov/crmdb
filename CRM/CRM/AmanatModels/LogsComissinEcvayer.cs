﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsComissinEcvayer
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int CardType { get; set; }
        public int DeviceType { get; set; }
        public byte CardOfBank { get; set; }
        public int OperType { get; set; }
        public int CurrencyId { get; set; }
        public byte ComissionTypeEcvayer { get; set; }
        public decimal ComissionEcvayer { get; set; }
        public byte? MinComissionType { get; set; }
        public decimal? MinComission { get; set; }
        public byte ComissionType { get; set; }

        public virtual User User { get; set; }
    }
}
