﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ContrAgent
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Summa { get; set; }
        public int? CurrencyId { get; set; }
        public string Reason { get; set; }
        public string ExpenseAccountNo { get; set; }
        public string DebtAccountNo { get; set; }
        public bool? Active { get; set; }
    }
}
