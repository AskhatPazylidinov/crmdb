﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsSubside
    {
        public int? CustomerId { get; set; }
        public int? CreditId { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal? SumV { get; set; }
        public string AccountNo1 { get; set; }
        public string AccountNo2 { get; set; }
        public string AccountNo3 { get; set; }
        public decimal? CurrentBalance1 { get; set; }
        public decimal? CurrentBalance2 { get; set; }
        public decimal? CurrentBalance3 { get; set; }
        public DateTime? CloseDate1 { get; set; }
        public DateTime? CloseDate2 { get; set; }
        public DateTime? CloseDate3 { get; set; }
    }
}
