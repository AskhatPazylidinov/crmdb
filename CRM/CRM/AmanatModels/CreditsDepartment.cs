﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsDepartment
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public int DepartmentId { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual History Credit { get; set; }
        public virtual Department1 Department { get; set; }
    }
}
