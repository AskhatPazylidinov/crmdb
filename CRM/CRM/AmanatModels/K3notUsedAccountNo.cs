﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class K3notUsedAccountNo
    {
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public byte TypeK3 { get; set; }
    }
}
