﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class K3depositAllowed
    {
        public int DepositProductId { get; set; }
        public byte K3type { get; set; }

        public virtual Product2 DepositProduct { get; set; }
    }
}
