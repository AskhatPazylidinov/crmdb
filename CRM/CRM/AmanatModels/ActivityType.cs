﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ActivityType
    {
        public ActivityType()
        {
            CustomerActivityTypes = new HashSet<Customer>();
            CustomerExtraWorkActivityTypes = new HashSet<Customer>();
            CustomerPersonActivityTypes = new HashSet<Customer>();
            InverseParentActivityType = new HashSet<ActivityType>();
        }

        public int ActivityTypeId { get; set; }
        public string TypeName { get; set; }
        public bool IsLicensed { get; set; }
        public bool IsHighRisk { get; set; }
        public string InBranch { get; set; }
        public string OnBranch { get; set; }
        public int? ParentActivityTypeId { get; set; }
        public int? NationalBankActivityTypeId { get; set; }
        public bool IsDefault { get; set; }

        public virtual ActivityType ParentActivityType { get; set; }
        public virtual SettingsActivityType SettingsActivityType { get; set; }
        public virtual ICollection<Customer> CustomerActivityTypes { get; set; }
        public virtual ICollection<Customer> CustomerExtraWorkActivityTypes { get; set; }
        public virtual ICollection<Customer> CustomerPersonActivityTypes { get; set; }
        public virtual ICollection<ActivityType> InverseParentActivityType { get; set; }
    }
}
