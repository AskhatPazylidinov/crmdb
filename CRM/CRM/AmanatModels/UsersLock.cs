﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersLock
    {
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ApprovedUserId { get; set; }
        public DateTime LockDate { get; set; }

        public virtual User User { get; set; }
    }
}
