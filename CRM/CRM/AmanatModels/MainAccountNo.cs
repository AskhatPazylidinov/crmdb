﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MainAccountNo
    {
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
    }
}
