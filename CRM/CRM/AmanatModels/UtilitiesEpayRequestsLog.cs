﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesEpayRequestsLog
    {
        public long LogId { get; set; }
        public DateTime BankDate { get; set; }
        public string UtilityCode { get; set; }
        public string PersonalAccountNo { get; set; }
        public decimal PaymentSumm { get; set; }
        public long TransactionId { get; set; }
        public DateTime EventDate { get; set; }
        public string OperationType { get; set; }
        public int StatusCode { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }

        public virtual User User { get; set; }
    }
}
