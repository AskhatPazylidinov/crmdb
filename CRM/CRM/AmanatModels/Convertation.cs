﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Convertation
    {
        public Convertation()
        {
            ConvertationTransactions = new HashSet<ConvertationTransaction>();
        }

        public int Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime DateOper { get; set; }
        public string DeviceCode { get; set; }
        public string CardNumber { get; set; }
        public int CurrencyId { get; set; }
        public decimal MainSum { get; set; }
        public decimal ConvertSum { get; set; }
        public decimal Vss { get; set; }
        public bool IsCalc { get; set; }

        public virtual ICollection<ConvertationTransaction> ConvertationTransactions { get; set; }
    }
}
