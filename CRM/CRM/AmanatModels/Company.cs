﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Company
    {
        public Company()
        {
            AllowedViewCompanies = new HashSet<AllowedViewCompany>();
            Cards = new HashSet<Card>();
            ProductsCompanies = new HashSet<ProductsCompany>();
        }

        public int Code { get; set; }
        public string Name { get; set; }
        public string CompanyCode { get; set; }
        public string BinCode { get; set; }
        public string AgrementCode { get; set; }
        public string AccountConditionSet { get; set; }
        public int MinBal { get; set; }
        public int NonReduceBal { get; set; }
        public string CardConditionSet { get; set; }
        public string CardRiskLevel { get; set; }
        public bool? IsView { get; set; }
        public DateTime? CloseDate { get; set; }
        public string BillingCycleId { get; set; }
        public string DesignCode { get; set; }
        public string ChipApplicationId { get; set; }
        public bool Cn { get; set; }

        public virtual ICollection<AllowedViewCompany> AllowedViewCompanies { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
        public virtual ICollection<ProductsCompany> ProductsCompanies { get; set; }
    }
}
