﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankService
    {
        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public int BankId { get; set; }
        public int ServiceId { get; set; }

        public virtual Bank1 Bank { get; set; }
        public virtual Service Service { get; set; }
    }
}
