﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesRating
    {
        public int CreditId { get; set; }
        public DateTime RatingDate { get; set; }
        public int? UserId { get; set; }
        public int CustomerId { get; set; }
        public byte CustomerTypeId { get; set; }
        public int RatingTypeId { get; set; }
        public int OverdueCount { get; set; }
        public int OverdueDaysCount { get; set; }
        public DateTime OperationDate { get; set; }
        public string Comment { get; set; }

        public virtual History Credit { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual RatingType RatingType { get; set; }
        public virtual User User { get; set; }
    }
}
