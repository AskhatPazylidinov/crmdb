﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersChangeLog
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string TypeChange { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime ChangedDate { get; set; }
        public int ChangedUserId { get; set; }
        public int OfficeChangedUserId { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }
        public DateTime? ApprovedDate { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
