﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NightCashiersOperationDate
    {
        public int OfficeId { get; set; }
        public DateTime OperationDate { get; set; }
        public int SpecialCashierType { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
