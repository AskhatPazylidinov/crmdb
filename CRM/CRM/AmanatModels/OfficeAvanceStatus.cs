﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeAvanceStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
