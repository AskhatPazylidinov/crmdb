﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Delay
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public string Reason { get; set; }
        public int MinCount { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
