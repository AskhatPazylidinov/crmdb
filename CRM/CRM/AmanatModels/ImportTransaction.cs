﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportTransaction
    {
        public ImportTransaction()
        {
            ImportTransactionPositions = new HashSet<ImportTransactionPosition>();
        }

        public long TransactionId { get; set; }
        public int TransactionFileId { get; set; }
        public int RowNo { get; set; }
        public int DebetCurrencyId { get; set; }
        public string DebetAccountId { get; set; }
        public decimal DebetSumV { get; set; }
        public decimal DebetSumN { get; set; }
        public int CreditCurrencyId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal CreditSumV { get; set; }
        public decimal CreditSumN { get; set; }
        public string Comment { get; set; }

        public virtual Account1 Credit { get; set; }
        public virtual Account1 Debet { get; set; }
        public virtual ImportTransactionsFile TransactionFile { get; set; }
        public virtual ICollection<ImportTransactionPosition> ImportTransactionPositions { get; set; }
    }
}
