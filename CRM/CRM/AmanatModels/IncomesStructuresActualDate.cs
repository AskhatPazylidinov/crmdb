﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncomesStructuresActualDate
    {
        public IncomesStructuresActualDate()
        {
            IncomesStructures = new HashSet<IncomesStructure>();
        }

        public int CreditId { get; set; }
        public DateTime ActualDate { get; set; }

        public virtual History Credit { get; set; }
        public virtual ICollection<IncomesStructure> IncomesStructures { get; set; }
    }
}
