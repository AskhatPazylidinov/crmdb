﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CurrentIncExpStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
