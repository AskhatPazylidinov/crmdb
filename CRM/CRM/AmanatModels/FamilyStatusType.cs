﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FamilyStatusType
    {
        public FamilyStatusType()
        {
            Customers = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual SettingsFamilyStatusType1 SettingsFamilyStatusType1 { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
