﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesSecuritiesDiscountRate
    {
        public int StartMonthsToEndDate { get; set; }
        public int? EndMonthsToEndDate { get; set; }
        public decimal DiscountRate { get; set; }
    }
}
