﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalCashDesk
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
