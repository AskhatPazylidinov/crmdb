﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsStornoRate
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int StartDepositPeriod { get; set; }
        public int EndStornoPeriod { get; set; }
        public int StartStornoPeriod { get; set; }
        public decimal RecalcRate { get; set; }
        public byte RecalcTypeId { get; set; }
        public decimal StartDepositSumm { get; set; }
    }
}
