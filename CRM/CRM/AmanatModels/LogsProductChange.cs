﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsProductChange
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int ProductId { get; set; }
        public DateTime DateChange { get; set; }
        public int OperationType { get; set; }
        public int DeviceType { get; set; }
        public byte BankType { get; set; }
        public byte ComisionType { get; set; }
        public decimal BankValue { get; set; }
        public decimal NbkrValue { get; set; }

        public virtual User User { get; set; }
    }
}
