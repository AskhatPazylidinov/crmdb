﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsGroupCode1
    {
        public int ProductId { get; set; }
        public string GroupCode { get; set; }

        public virtual Product2 Product { get; set; }
    }
}
