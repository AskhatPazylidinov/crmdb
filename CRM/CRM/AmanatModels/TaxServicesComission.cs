﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxServicesComission
    {
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }

        public virtual TaxDepartment1 Department { get; set; }
        public virtual TaxService Service { get; set; }
    }
}
