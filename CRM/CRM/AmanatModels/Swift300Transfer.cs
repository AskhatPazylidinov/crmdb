﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Swift300Transfer
    {
        public long TransferId { get; set; }
        public decimal ExchangeRate { get; set; }
        public int CurrencyBoughtId { get; set; }
        public decimal AmountBought { get; set; }
        public int CurrencySoldId { get; set; }
        public decimal AmountSold { get; set; }

        public virtual Currency3 CurrencyBought { get; set; }
        public virtual Currency3 CurrencySold { get; set; }
        public virtual SwiftTransfer Transfer { get; set; }
    }
}
