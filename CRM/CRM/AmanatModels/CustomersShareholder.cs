﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersShareholder
    {
        public int CompanyId { get; set; }
        public int ShareholderId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsNominal { get; set; }
        public decimal SumValue { get; set; }
        public decimal PercentValue { get; set; }

        public virtual Customer Company { get; set; }
        public virtual Customer Shareholder { get; set; }
    }
}
