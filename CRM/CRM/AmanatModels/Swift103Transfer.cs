﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Swift103Transfer
    {
        public long TransferId { get; set; }
        public int SystemId { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrencyId { get; set; }
        public byte ExpenseTypeId { get; set; }
        public string PaymentComment { get; set; }
        public int? SenderCustomerId { get; set; }
        public string SenderCustomerName { get; set; }
        public string SenderAccountNo { get; set; }
        public byte? SenderCustomerTypeId { get; set; }
        public int? SenderAttorneyId { get; set; }
        public bool SenderIsResident { get; set; }
        public int? ReceiverCustomerId { get; set; }
        public string ReceiverCustomerName { get; set; }
        public string ReceiverAccountNo { get; set; }
        public byte? ReceiverCustomerTypeId { get; set; }
        public bool ReceiverIsResident { get; set; }
        public bool? IsCashDeposit { get; set; }
        public string OperCode { get; set; }
        public string ReasonCode { get; set; }
        public byte TrType { get; set; }
        public string SenderAddress { get; set; }
        public string SenderCountryCode { get; set; }
        public int? ContractId { get; set; }
        public string ReferenceInfo { get; set; }
        public string RelatedReferenceInfo { get; set; }
        public long? TempTransferId { get; set; }
        public string CodeVo { get; set; }
        public int? PurposeId { get; set; }
        public string KppOrBin { get; set; }
        public string VoOrKnp { get; set; }

        public virtual Vocode CodeVoNavigation { get; set; }
        public virtual Contract Contract { get; set; }
        public virtual RefOper1 OperCodeNavigation { get; set; }
        public virtual Customer ReceiverCustomer { get; set; }
        public virtual Customer SenderCustomer { get; set; }
        public virtual SwiftSystem System { get; set; }
        public virtual Currency3 TransferCurrency { get; set; }
        public virtual LimitExceedVerification LimitExceedVerification { get; set; }
    }
}
