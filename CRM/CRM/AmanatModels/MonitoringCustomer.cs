﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MonitoringCustomer
    {
        public int CustomerId { get; set; }
        public string Description { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
