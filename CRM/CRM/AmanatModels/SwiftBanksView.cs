﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftBanksView
    {
        public string Bic { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string AccountNo { get; set; }
        public string City { get; set; }
    }
}
