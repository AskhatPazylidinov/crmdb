﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Operation
    {
        public Operation()
        {
            TypeOperations = new HashSet<TypeOperation>();
        }

        public int OperationId { get; set; }
        public byte OperationTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<TypeOperation> TypeOperations { get; set; }
    }
}
