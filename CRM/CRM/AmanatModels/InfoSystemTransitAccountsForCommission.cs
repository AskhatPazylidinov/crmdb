﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InfoSystemTransitAccountsForCommission
    {
        public int BranchId { get; set; }
        public string TransitAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int InfoSystemProviderId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual ServiceProvider InfoSystemProvider { get; set; }
    }
}
