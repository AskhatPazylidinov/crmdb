﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeBonu
    {
        public int OfficeId { get; set; }
        public DateTime BonusDate { get; set; }
        public int Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? AvanceId { get; set; }
        public int? TypeId { get; set; }
        public decimal? Value { get; set; }
        public bool? InPercent { get; set; }
        public int? AccountTypeId { get; set; }
        public string Reason { get; set; }
        public bool? Tax { get; set; }
        public bool? ErSf { get; set; }
        public bool? EeSf { get; set; }
    }
}
