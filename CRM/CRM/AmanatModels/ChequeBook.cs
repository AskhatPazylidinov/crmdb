﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChequeBook
    {
        public int CustomerId { get; set; }
        public DateTime IssueDate { get; set; }
        public string CheckSeries { get; set; }
        public string StartPage { get; set; }
        public string EndPage { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
