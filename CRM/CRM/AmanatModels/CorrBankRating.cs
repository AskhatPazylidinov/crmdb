﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrBankRating
    {
        public int CorrBankRatingId { get; set; }
        public DateTime StartDate { get; set; }
        public int RatingId { get; set; }
        public string CorrBankCode { get; set; }

        public virtual SwiftCorrBank CorrBankCodeNavigation { get; set; }
    }
}
