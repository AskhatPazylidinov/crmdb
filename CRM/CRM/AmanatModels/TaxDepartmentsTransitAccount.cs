﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDepartmentsTransitAccount
    {
        public int DepartmentId { get; set; }
        public int BranchId { get; set; }
        public string TransitAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual TaxDepartment1 Department { get; set; }
    }
}
