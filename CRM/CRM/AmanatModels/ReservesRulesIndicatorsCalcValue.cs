﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesRulesIndicatorsCalcValue
    {
        public int ReserveRuleId { get; set; }
        public byte ReserveIndicatorTypeId { get; set; }
        public byte OperationId { get; set; }
        public string ReserveIndicatorValue { get; set; }

        public virtual ReservesRule ReserveRule { get; set; }
    }
}
