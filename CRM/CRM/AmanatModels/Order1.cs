﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Order1
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public int? OrderTypeId { get; set; }
        public string Comment { get; set; }
        public string ApproveNo { get; set; }
        public DateTime? ApproveDate { get; set; }
        public string ApproveComment { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? DaysCount { get; set; }
        public decimal? AddSumm { get; set; }
        public decimal? DecSumm { get; set; }
        public int? Status { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual OrderType OrderType { get; set; }
    }
}
