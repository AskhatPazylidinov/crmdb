﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PreparePayOffRequestsLog
    {
        public int CardId { get; set; }
        public bool IsUnaccepted { get; set; }
        public DateTime BankDate { get; set; }
        public decimal InterestAmount { get; set; }
        public DateTime RequestTime { get; set; }
        public int UserId { get; set; }

        public virtual Card Card { get; set; }
    }
}
