﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExclusionTariff
    {
        public int OperationType { get; set; }
        public int DeviceType { get; set; }
        public string MerchantId { get; set; }
        public byte CommissionType { get; set; }
        public decimal BankCommission { get; set; }
        public decimal InterchangeCommission { get; set; }
        public decimal Cashback { get; set; }
        public string CommentBankCommission { get; set; }
        public string CommentInterchangeCommission { get; set; }
        public string CommentCashback { get; set; }
        public int? ProductId { get; set; }
    }
}
