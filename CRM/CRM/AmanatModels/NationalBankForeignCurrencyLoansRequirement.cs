﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NationalBankForeignCurrencyLoansRequirement
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
