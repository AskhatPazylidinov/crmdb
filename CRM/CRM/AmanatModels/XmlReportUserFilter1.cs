﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class XmlReportUserFilter1
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public string ReportType { get; set; }
        public int UserId { get; set; }
        public bool ForAll { get; set; }

        public virtual User User { get; set; }
    }
}
