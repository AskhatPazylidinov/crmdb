﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficersType
    {
        public OfficersType()
        {
            HistoriesOfficers = new HashSet<HistoriesOfficer>();
        }

        public int OfficerTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<HistoriesOfficer> HistoriesOfficers { get; set; }
    }
}
