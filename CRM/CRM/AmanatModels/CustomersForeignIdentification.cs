﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersForeignIdentification
    {
        public int CustomerId { get; set; }
        public string RuKpp { get; set; }
        public string KzBin { get; set; }
        public string KzRnn { get; set; }
        public string KzKnp { get; set; }
        public string KzKbe { get; set; }
        public string ByUnp { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
