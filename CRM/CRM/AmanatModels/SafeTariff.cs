﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafeTariff
    {
        public SafeTariff()
        {
            SafeGuaranteeSumms = new HashSet<SafeGuaranteeSumm>();
            SafeOverduesTariffs = new HashSet<SafeOverduesTariff>();
            SafeTariffsValues = new HashSet<SafeTariffsValue>();
        }

        public DateTime ChangeDate { get; set; }
        public int SafeTypeId { get; set; }
        public int? MinDays { get; set; }
        public int? MaxDays { get; set; }

        public virtual TariffsChangeDate1 ChangeDateNavigation { get; set; }
        public virtual SafeType SafeType { get; set; }
        public virtual ICollection<SafeGuaranteeSumm> SafeGuaranteeSumms { get; set; }
        public virtual ICollection<SafeOverduesTariff> SafeOverduesTariffs { get; set; }
        public virtual ICollection<SafeTariffsValue> SafeTariffsValues { get; set; }
    }
}
