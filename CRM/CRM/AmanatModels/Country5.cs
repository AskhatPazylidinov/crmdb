﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Country5
    {
        public Country5()
        {
            Region3s = new HashSet<Region3>();
        }

        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public string Latin2 { get; set; }
        public string Latin3 { get; set; }
        public string Digital { get; set; }
        public string Name { get; set; }
        public string PhoneCode { get; set; }
        public string NameLat { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<Region3> Region3s { get; set; }
    }
}
