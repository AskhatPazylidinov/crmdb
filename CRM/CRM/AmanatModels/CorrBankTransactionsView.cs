﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrBankTransactionsView
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public DateTime TransactionDate { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal SumV { get; set; }
        public DateTime DateV { get; set; }
        public string Comment { get; set; }
        public string DtBalanceGroup { get; set; }
        public string CtBalanceGroup { get; set; }
        public string Symbol { get; set; }
        public string Fullname { get; set; }
        public bool? IsNotSet { get; set; }
        public int? CorrBankCountryId { get; set; }
        public int? CorrBankDirectionTypeId { get; set; }
        public int? CorrBankOperationTypeId { get; set; }
    }
}
