﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SubStatusesWorkflow
    {
        public int WorkflowId { get; set; }
        public int? SourceSubStatusId { get; set; }
        public int? DestSubStatusId { get; set; }

        public virtual SubStatusesType DestSubStatus { get; set; }
        public virtual SubStatusesType SourceSubStatus { get; set; }
    }
}
