﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekGa
    {
        public int OperationId { get; set; }
        public string FullcustomerName { get; set; }
        public string PersonalAccountNo { get; set; }
        public string Address { get; set; }
        public decimal PaymentSumm { get; set; }
        public decimal FinesSumm { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
