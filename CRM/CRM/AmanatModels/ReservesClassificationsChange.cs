﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesClassificationsChange
    {
        public ReservesClassificationsChange()
        {
            ReservesClassificationsRules = new HashSet<ReservesClassificationsRule>();
        }

        public int TypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }

        public virtual ReservesClassification Type { get; set; }
        public virtual ICollection<ReservesClassificationsRule> ReservesClassificationsRules { get; set; }
    }
}
