﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesFlowOperation
    {
        public int OperationId { get; set; }
        public DateTime OperationDate { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public bool IsTransferToBranch { get; set; }
        public decimal SumV { get; set; }
        public int CorrespondentId { get; set; }
        public long Position { get; set; }
        public int? LastConfirmUserId { get; set; }

        public virtual Customer Correspondent { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual User LastConfirmUser { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
