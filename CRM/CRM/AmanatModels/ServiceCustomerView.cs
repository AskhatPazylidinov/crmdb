﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceCustomerView
    {
        public int CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public int? EnableUserId { get; set; }
        public int? DisableUserId { get; set; }
        public string AccountNo { get; set; }
        public int? CurrencyId { get; set; }
        public long? Rn { get; set; }
        public bool? IsActive { get; set; }
        public string EnableUser { get; set; }
        public string DisableUser { get; set; }
    }
}
