﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnersAccount1
    {
        public int PartnerId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? CountryCode { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Partner3 Partner { get; set; }
    }
}
