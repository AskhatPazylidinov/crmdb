﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TerminalTaxPayment
    {
        public int Id { get; set; }
        public string Inn { get; set; }
        public string CustomerName { get; set; }
        public string PaymentCode { get; set; }
        public int OfficeCode { get; set; }
        public string CarNumber { get; set; }
        public string AimagCode { get; set; }
        public decimal TaxSumm { get; set; }
        public DateTime OperationDate { get; set; }
        public long CheckNumber { get; set; }
        public int ProviderId { get; set; }
        public bool Status { get; set; }
        public int ImportFileId { get; set; }

        public virtual UtilitiesImportFile ImportFile { get; set; }
        public virtual Office2 OfficeCodeNavigation { get; set; }
    }
}
