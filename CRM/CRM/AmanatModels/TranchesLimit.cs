﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TranchesLimit
    {
        public int ProductId { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
