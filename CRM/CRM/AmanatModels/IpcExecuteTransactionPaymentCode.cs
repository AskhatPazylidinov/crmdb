﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionPaymentCode
    {
        public int Id { get; set; }
        public string BranchCode { get; set; }
    }
}
