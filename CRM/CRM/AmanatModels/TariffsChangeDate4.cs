﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate4
    {
        public TariffsChangeDate4()
        {
            Tariff3s = new HashSet<Tariff3>();
        }

        public DateTime ChangeDate { get; set; }

        public virtual ICollection<Tariff3> Tariff3s { get; set; }
    }
}
