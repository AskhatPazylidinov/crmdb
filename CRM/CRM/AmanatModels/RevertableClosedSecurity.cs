﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableClosedSecurity
    {
        public int RevertId { get; set; }
        public int SecuritiesId { get; set; }

        public virtual RevertableOperation1 Revert { get; set; }
    }
}
