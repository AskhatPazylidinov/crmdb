﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RolesAllowedViewAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int RoleId { get; set; }
        public bool IsShow { get; set; }
        public bool IsAllowCreateTransaction { get; set; }

        public virtual DisabledAccount DisabledAccount { get; set; }
        public virtual Role Role { get; set; }
    }
}
