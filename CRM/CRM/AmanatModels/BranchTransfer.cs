﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchTransfer
    {
        public string DocNo { get; set; }
        public string Ppno { get; set; }
        public DateTime Ppdate { get; set; }
        public string SecureCode { get; set; }
        public int CurrencyId { get; set; }
        public decimal TransferSum { get; set; }
        public decimal CommissionFee { get; set; }
        public decimal SenderCommissionFee { get; set; }
        public decimal ReceiverCommissionFee { get; set; }
        public string Reason { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string DocumentNo { get; set; }
        public string IssueAuthority { get; set; }
        public string RegistrationStreet { get; set; }
        public string ResidenceStreet { get; set; }
        public string ReceiverFullname { get; set; }
        public int SenderBranchId { get; set; }
        public int? SenderId { get; set; }
    }
}
