﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class LinkedCustomer
    {
        public int MainCustomerId { get; set; }
        public int LinkedCustomerId { get; set; }
        public string Remarks { get; set; }

        public virtual Customer LinkedCustomerNavigation { get; set; }
        public virtual Customer MainCustomer { get; set; }
    }
}
