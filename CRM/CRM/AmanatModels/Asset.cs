﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Asset
    {
        public Asset()
        {
            AssetsAccounts = new HashSet<AssetsAccount>();
            AssetsMovements = new HashSet<AssetsMovement>();
            AssetsOptionsValues = new HashSet<AssetsOptionsValue>();
            Balances = new HashSet<Balance>();
            Inventories = new HashSet<Inventory>();
            OperationsHistories = new HashSet<OperationsHistory>();
        }

        public int AssetId { get; set; }
        public int TypeId { get; set; }
        public int OfficeId { get; set; }
        public int EmployeeId { get; set; }
        public string InventoryNo { get; set; }
        public int MeasureId { get; set; }
        public int Quantity { get; set; }
        public decimal Balance { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? LifeTime { get; set; }
        public int? DepartmentId { get; set; }
        public int? SubGroupId { get; set; }
        public int? TaxesGroupId { get; set; }
        public int? ContractorId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Type Type { get; set; }
        public virtual User User { get; set; }
        public virtual Amortization Amortization { get; set; }
        public virtual ICollection<AssetsAccount> AssetsAccounts { get; set; }
        public virtual ICollection<AssetsMovement> AssetsMovements { get; set; }
        public virtual ICollection<AssetsOptionsValue> AssetsOptionsValues { get; set; }
        public virtual ICollection<Balance> Balances { get; set; }
        public virtual ICollection<Inventory> Inventories { get; set; }
        public virtual ICollection<OperationsHistory> OperationsHistories { get; set; }
    }
}
