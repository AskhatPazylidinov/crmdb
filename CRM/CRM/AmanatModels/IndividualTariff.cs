﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariff
    {
        public int CustomerId { get; set; }
        public byte PaymentTypeId { get; set; }
        public byte PaymentDirectionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionTypeId { get; set; }
        public bool IsAprroved { get; set; }
    }
}
