﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountNoCompany
    {
        public string CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public int CompanyId { get; set; }
    }
}
