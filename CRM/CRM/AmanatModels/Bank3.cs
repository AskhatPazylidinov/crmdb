﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Bank3
    {
        public int KyId { get; set; }
        public int FlCountrycode { get; set; }
        public string FlName { get; set; }
        public string FlAddress { get; set; }
        public string FlBic { get; set; }
    }
}
