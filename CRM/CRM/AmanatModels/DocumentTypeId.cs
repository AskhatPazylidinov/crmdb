﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentTypeId
    {
        public int DocumentId { get; set; }
        public int DocumentTypeId1 { get; set; }

        public virtual DocumentType2 Document { get; set; }
        public virtual DocumentType1 DocumentTypeId1Navigation { get; set; }
    }
}
