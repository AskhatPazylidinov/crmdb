﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardsAccountType
    {
        public CardsAccountType()
        {
            Accounts = new HashSet<Account>();
            BalanceGroupProducts = new HashSet<BalanceGroupProduct>();
        }

        public int AccountTypeId { get; set; }
        public string TypeName { get; set; }
        public byte? AccountType { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<BalanceGroupProduct> BalanceGroupProducts { get; set; }
    }
}
