﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesStatus
    {
        public int CreditId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int? UserId { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
