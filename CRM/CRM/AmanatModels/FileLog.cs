﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FileLog
    {
        public int Id { get; set; }
        public DateTime LogDate { get; set; }
        public string FileName { get; set; }
        public string CheckSum { get; set; }
        public bool Processed { get; set; }
    }
}
