﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualStornoRate
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int StartPeriod { get; set; }
        public int EndPeriod { get; set; }
        public decimal RecalcRate { get; set; }
        public byte RecalcTypeId { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
