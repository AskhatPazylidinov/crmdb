﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsStatus
    {
        public int PaysheetId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual Paysheet Paysheet { get; set; }
        public virtual User User { get; set; }
    }
}
