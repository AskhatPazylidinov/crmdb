﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDistrict
    {
        public TaxDistrict()
        {
            TaxDepartment1s = new HashSet<TaxDepartment1>();
        }

        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int RegionId { get; set; }
        public string DistrictCode { get; set; }

        public virtual Region1 Region { get; set; }
        public virtual ICollection<TaxDepartment1> TaxDepartment1s { get; set; }
    }
}
