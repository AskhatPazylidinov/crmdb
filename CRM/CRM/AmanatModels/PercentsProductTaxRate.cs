﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PercentsProductTaxRate
    {
        public int ProductId { get; set; }
        public DateTime TaxChangeDate { get; set; }
        public decimal TaxRate { get; set; }
        public bool IsResident { get; set; }

        public virtual Product2 Product { get; set; }
    }
}
