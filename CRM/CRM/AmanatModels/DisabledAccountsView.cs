﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DisabledAccountsView
    {
        public int OfficeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountName { get; set; }
        public int Allowed { get; set; }
        public bool IsAllowCreateTransaction { get; set; }
        public bool IsShow { get; set; }
        public string SrtOffice { get; set; }
        public string SrtCurrency { get; set; }
    }
}
