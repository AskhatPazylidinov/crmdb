﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrtCurrencyReserf
    {
        public int CurrencyId { get; set; }
        public DateTime ChangeDate { get; set; }
        public decimal Reserve { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
