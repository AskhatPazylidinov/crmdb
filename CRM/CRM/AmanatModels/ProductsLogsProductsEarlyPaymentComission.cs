﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsEarlyPaymentComission
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte Period { get; set; }
        public decimal? PartialComission { get; set; }
        public byte? PartialComissionType { get; set; }
        public decimal? FullPaymentComission { get; set; }
        public byte? FullPaymentComissionType { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
