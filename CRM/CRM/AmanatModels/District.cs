﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class District
    {
        public string DistrictName { get; set; }
        public string PlaceName { get; set; }
        public string RegionId { get; set; }
        public int DistrictId { get; set; }
    }
}
