﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialTaxSettingsForAccountView
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public byte TaxTypeId { get; set; }
        public string BalanceGroup { get; set; }
    }
}
