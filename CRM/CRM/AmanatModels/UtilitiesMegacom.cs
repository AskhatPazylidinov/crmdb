﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesMegacom
    {
        public int OperationId { get; set; }
        public string PersonalAccountNo { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
