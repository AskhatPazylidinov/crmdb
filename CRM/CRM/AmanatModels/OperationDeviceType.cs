﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationDeviceType
    {
        public int Id { get; set; }
        public int OperType { get; set; }
        public int DeviceType { get; set; }

        public virtual DeviceType DeviceTypeNavigation { get; set; }
        public virtual OperationType OperTypeNavigation { get; set; }
    }
}
