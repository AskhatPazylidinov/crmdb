﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialAccount1
    {
        public int Id { get; set; }
        public int OperType { get; set; }
        public int DeviceType { get; set; }
        public int OfficeId { get; set; }
        public byte BankType { get; set; }
        public int CardType { get; set; }
        public byte CardOfBank { get; set; }
        public int CurrencyId { get; set; }
        public string DeviceAccount { get; set; }
        public string TransitAccountNo { get; set; }
        public string IncomeAccountNo { get; set; }
        public byte ComissionType { get; set; }
        public decimal? Comission { get; set; }
        public byte IncomeType { get; set; }
        public string ExpenceAccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual DeviceType DeviceTypeNavigation { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual OperationType OperTypeNavigation { get; set; }
    }
}
