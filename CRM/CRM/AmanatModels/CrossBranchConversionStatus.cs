﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBranchConversionStatus
    {
        public int OperationId { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual CrossBranchConversion Operation { get; set; }
        public virtual User User { get; set; }
    }
}
