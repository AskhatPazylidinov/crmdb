﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProjectionsCurrency
    {
        public int CurrencyId { get; set; }
        public int? Priority { get; set; }
    }
}
