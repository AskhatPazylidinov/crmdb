﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TableFieldsValue
    {
        public int GaranteeId { get; set; }
        public int FieldId { get; set; }
        public int RowId { get; set; }
        public bool? ValueBoolean { get; set; }
        public int? ValueInteger { get; set; }
        public decimal? ValueDecimal { get; set; }
        public string ValueString { get; set; }
        public DateTime? ValueDateTime { get; set; }

        public virtual PropertyField Field { get; set; }
        public virtual Garantee Garantee { get; set; }
    }
}
