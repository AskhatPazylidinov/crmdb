﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Form36
    {
        public Form36()
        {
            Form36BalGroups = new HashSet<Form36BalGroup>();
        }

        public string Article { get; set; }
        public string Name { get; set; }

        public virtual OrtSetting OrtSetting { get; set; }
        public virtual ICollection<Form36BalGroup> Form36BalGroups { get; set; }
    }
}
