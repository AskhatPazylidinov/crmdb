﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ParallelLoansStatus
    {
        public int ParallelId { get; set; }
        public DateTime StatusDate { get; set; }
        public int OverdueDays { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public string CreditStatus { get; set; }
        public decimal Balance { get; set; }
        public long ParallelLoansStatusId { get; set; }
        public string RepaymentStatus { get; set; }
        public string ReasonOverdueDay { get; set; }

        public virtual ParallelLoan Parallel { get; set; }
        public virtual User User { get; set; }
    }
}
