﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesMortgagesReplacement
    {
        public int SecurityMortgageId { get; set; }
        public byte ReplacementTypeId { get; set; }
        public string ReplacementReason { get; set; }

        public virtual SecuritiesMortgage SecurityMortgage { get; set; }
    }
}
