﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Form36AccGroup
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string Article { get; set; }
        public bool IsActive { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Form36 ArticleNavigation { get; set; }
    }
}
