﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Mortgage
    {
        public Mortgage()
        {
            MortgagesAccounts = new HashSet<MortgagesAccount>();
            SecuritiesMortgages = new HashSet<SecuritiesMortgage>();
        }

        public int MortgageId { get; set; }
        public int CreditId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string RegistrationNo { get; set; }
        public decimal IndependenceEstimationAmount { get; set; }
        public string GaranteeAgreementNo { get; set; }
        public DateTime GaranteeAgreementDate { get; set; }

        public virtual History Credit { get; set; }
        public virtual ICollection<MortgagesAccount> MortgagesAccounts { get; set; }
        public virtual ICollection<SecuritiesMortgage> SecuritiesMortgages { get; set; }
    }
}
