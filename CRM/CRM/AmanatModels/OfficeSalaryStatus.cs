﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeSalaryStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
