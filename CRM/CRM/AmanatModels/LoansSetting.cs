﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LoansSetting
    {
        public int EmployeeId { get; set; }
        public byte PayrollTypeId { get; set; }
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal TransferSumm { get; set; }

        public virtual History Credit { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
