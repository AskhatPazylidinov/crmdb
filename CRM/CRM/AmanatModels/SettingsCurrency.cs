﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsCurrency
    {
        public int CurrencyId { get; set; }
        public int CibcurrencyId { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
