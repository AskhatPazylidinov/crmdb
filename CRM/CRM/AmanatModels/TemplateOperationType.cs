﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TemplateOperationType
    {
        public TemplateOperationType()
        {
            TransactionTemplateOperations = new HashSet<TransactionTemplateOperation>();
            TransactionTemplates = new HashSet<TransactionTemplate>();
        }

        public int TemplateOperationTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TransactionTemplateOperation> TransactionTemplateOperations { get; set; }
        public virtual ICollection<TransactionTemplate> TransactionTemplates { get; set; }
    }
}
