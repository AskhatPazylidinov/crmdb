﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Point
    {
        public string PointKey { get; set; }
        public string PartnerKey { get; set; }
        public string PointAddress { get; set; }
        public int? PlaceCode { get; set; }
        public string Phone { get; set; }
        public string WorkTime { get; set; }
        public string KgsSend { get; set; }
        public string KgsGet { get; set; }
        public string UsdSend { get; set; }
        public string UsdGet { get; set; }
        public string RubSend { get; set; }
        public string RubGet { get; set; }
        public string EurSenD { get; set; }
        public string EurGet { get; set; }
        public string PointDesc { get; set; }
    }
}
