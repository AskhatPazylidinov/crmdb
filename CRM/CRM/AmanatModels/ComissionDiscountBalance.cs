﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComissionDiscountBalance
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal ComissionDiscountBalance1 { get; set; }
        public decimal ComissionDiscountSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
