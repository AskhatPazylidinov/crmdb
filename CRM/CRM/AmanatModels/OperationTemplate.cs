﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationTemplate
    {
        public int TypeOperationId { get; set; }
        public int OrderId { get; set; }
        public int DebetAccountType { get; set; }
        public int CreditAccountType { get; set; }
        public byte OperationSummType { get; set; }
        public decimal Rate { get; set; }
        public string Comment { get; set; }

        public virtual TypeOperation TypeOperation { get; set; }
    }
}
