﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuredNamespace
    {
        public string NamespaceName { get; set; }
        public string GroupName { get; set; }
    }
}
