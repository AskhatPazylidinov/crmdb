﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnersAccount
    {
        public int PartnerId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? CountryCode { get; set; }
    }
}
