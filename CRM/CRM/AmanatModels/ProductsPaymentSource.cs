﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsPaymentSource
    {
        public int ProductId { get; set; }
        public int PaymentSourceId { get; set; }

        public virtual PaymentSource PaymentSource { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
