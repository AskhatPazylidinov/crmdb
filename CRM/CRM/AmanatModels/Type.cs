﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Type
    {
        public Type()
        {
            Assets = new HashSet<Asset>();
            AssetsBalanceGroups = new HashSet<AssetsBalanceGroup>();
            SpecialAccounts = new HashSet<SpecialAccount>();
            TypeOperations = new HashSet<TypeOperation>();
        }

        public int TypeId { get; set; }
        public int GroupId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsAmortization { get; set; }
        public int? LifeTime { get; set; }
        public string InventoryNoPrefix { get; set; }
        public string Description { get; set; }
        public int? TaxGroupId { get; set; }

        public virtual AssetGroup Group { get; set; }
        public virtual AssetTaxesGroup TaxGroup { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AssetsBalanceGroup> AssetsBalanceGroups { get; set; }
        public virtual ICollection<SpecialAccount> SpecialAccounts { get; set; }
        public virtual ICollection<TypeOperation> TypeOperations { get; set; }
    }
}
