﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficersLimit
    {
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Limit { get; set; }
        public int CurrencyId { get; set; }

        public virtual User User { get; set; }
    }
}
