﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PayBalance
    {
        public int Position { get; set; }
        public int PositionN { get; set; }
        public int Pnn { get; set; }
        public byte? Fldk { get; set; }
        public int? CountryCode { get; set; }
        public int? CivilCode { get; set; }
        public string OperationId { get; set; }
        public int? SectorId { get; set; }
        public string NewComment { get; set; }
        public int? AccCountryCode { get; set; }
        public int? CustomerId { get; set; }
        public byte? NewFldk { get; set; }
        public int? PartnerId { get; set; }
        public string AccountNo { get; set; }
    }
}
