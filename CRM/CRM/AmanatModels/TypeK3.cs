﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TypeK3
    {
        public TypeK3()
        {
            FormK3s = new HashSet<FormK3>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<FormK3> FormK3s { get; set; }
    }
}
