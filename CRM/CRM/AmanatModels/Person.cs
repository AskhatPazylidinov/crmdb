﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Person
    {
        public Person()
        {
            PersonsSearchIndices = new HashSet<PersonsSearchIndex>();
            PotentialBlackVisitors = new HashSet<PotentialBlackVisitor>();
            PotentialBlackVisitorsOperations = new HashSet<PotentialBlackVisitorsOperation>();
        }

        public int ItemId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string Surname { get; set; }
        public string PersonName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public int BlackListId { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportIssueDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string IdentificationNo { get; set; }
        public string Description { get; set; }
        public bool? Approved { get; set; }
        public int? ApprovedByUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ExternalId { get; set; }
        public string FullName { get; set; }
        public DateTime? ModifyTime { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ExcludeDate { get; set; }

        public virtual BlackList BlackList { get; set; }
        public virtual ICollection<PersonsSearchIndex> PersonsSearchIndices { get; set; }
        public virtual ICollection<PotentialBlackVisitor> PotentialBlackVisitors { get; set; }
        public virtual ICollection<PotentialBlackVisitorsOperation> PotentialBlackVisitorsOperations { get; set; }
    }
}
