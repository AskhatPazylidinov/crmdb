﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsCardsAccountType
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual User User { get; set; }
    }
}
