﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowedViewCompany
    {
        public int CompanyId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int UserId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual User User { get; set; }
    }
}
