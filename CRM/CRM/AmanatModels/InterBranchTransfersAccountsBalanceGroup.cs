﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchTransfersAccountsBalanceGroup
    {
        public int AccountTypeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountGroup { get; set; }

        public virtual AccountGroup AccountGroupNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
