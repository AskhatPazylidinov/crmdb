﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsPartnerCommission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CompanyId { get; set; }
        public int MinPeriod { get; set; }
        public int? MaxPeriod { get; set; }
        public decimal Rate { get; set; }
        public byte CommissionType { get; set; }

        public virtual Partner2 Company { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
