﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesWaterDepartment
    {
        public UtilitiesWaterDepartment()
        {
            UtilitiesWaterPayments = new HashSet<UtilitiesWaterPayment>();
            UtilitiesWaterProviderDepartments = new HashSet<UtilitiesWaterProviderDepartment>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public virtual ICollection<UtilitiesWaterPayment> UtilitiesWaterPayments { get; set; }
        public virtual ICollection<UtilitiesWaterProviderDepartment> UtilitiesWaterProviderDepartments { get; set; }
    }
}
