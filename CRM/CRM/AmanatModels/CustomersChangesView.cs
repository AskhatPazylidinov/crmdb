﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersChangesView
    {
        public int CustomerId { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? DocumentValidTill { get; set; }
        public bool? IsDocUnlimited { get; set; }
        public int? DocumentTypeId { get; set; }
        public string IdentificationNumber { get; set; }
        public string Okpo { get; set; }
        public string SocialRegistrationNo { get; set; }
        public string RegistrationAuthority { get; set; }
        public DateTime? DateOfRegistration { get; set; }
        public DateTime? FirstRegistrationDate { get; set; }
    }
}
