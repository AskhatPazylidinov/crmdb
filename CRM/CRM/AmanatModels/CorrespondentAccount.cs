﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrespondentAccount
    {
        public string Bik { get; set; }
        public string Name { get; set; }
        public string CorrAccountNo { get; set; }
    }
}
