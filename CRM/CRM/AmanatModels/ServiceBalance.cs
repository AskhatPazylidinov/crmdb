﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceBalance
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual BankService1 ServiceType { get; set; }
    }
}
