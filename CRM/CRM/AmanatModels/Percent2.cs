﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Percent2
    {
        public int SecuritiesId { get; set; }
        public DateTime PercentDate { get; set; }
        public decimal SumV { get; set; }
    }
}
