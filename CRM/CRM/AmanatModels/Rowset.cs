﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Rowset
    {
        public int KyId { get; set; }
        public string FlName { get; set; }
        public DateTime? FlDate { get; set; }
    }
}
