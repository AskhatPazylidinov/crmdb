﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FieldsCompareOperation
    {
        public int? Id { get; set; }
        public string Operation { get; set; }
    }
}
