﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComponentToSecuredOperation
    {
        public int ComponentId { get; set; }
        public string ProviderNamespace { get; set; }
        public string ProviderName { get; set; }
        public string OperationName { get; set; }

        public virtual Component Component { get; set; }
    }
}
