﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PayBalance1
    {
        public long Position { get; set; }
        public short PositionN { get; set; }
        public int Pnn { get; set; }
        public byte? Fldk { get; set; }
        public int? CountryCode { get; set; }
        public int? CivilCode { get; set; }
        public string OperationId { get; set; }
        public int? SectorId { get; set; }
        public string NewComment { get; set; }
        public int? AccCountryCode { get; set; }
        public int? CustomerId { get; set; }
        public byte? NewFldk { get; set; }
        public int? PartnerId { get; set; }
        public string AccountNo { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Partner3 Partner { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual RefSector1 Sector { get; set; }
        public virtual PayBalanceToEdit PayBalanceToEdit { get; set; }
    }
}
