﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDepartmentsComission
    {
        public int DepartmentId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }

        public virtual TaxDepartment1 Department { get; set; }
    }
}
