﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class History
    {
        public History()
        {
            AgileGraphics = new HashSet<AgileGraphic>();
            AllGuarantors = new HashSet<AllGuarantor>();
            BanksActions = new HashSet<BanksAction>();
            Cards = new HashSet<Card>();
            ComissionDiscountBalances = new HashSet<ComissionDiscountBalance>();
            CreditsAccounts = new HashSet<CreditsAccount>();
            CreditsDepartments = new HashSet<CreditsDepartment>();
            CreditsGarantees = new HashSet<CreditsGarantee>();
            CustomersNotifications = new HashSet<CustomersNotification>();
            DeletedRevolvingWindows = new HashSet<DeletedRevolvingWindow>();
            DepositsMortrages = new HashSet<DepositsMortrage>();
            EarlyRepayStatements = new HashSet<EarlyRepayStatement>();
            EffectiveGraphics = new HashSet<EffectiveGraphic>();
            ExternalLoansCredits = new HashSet<ExternalLoansCredit>();
            FinesBalances = new HashSet<FinesBalance>();
            FullPaymentsRequests = new HashSet<FullPaymentsRequest>();
            GracePeriods = new HashSet<GracePeriod>();
            Graphics = new HashSet<Graphic>();
            GroupCycleCredits = new HashSet<GroupCycleCredit>();
            HistoriesCancelIssueLogs = new HashSet<HistoriesCancelIssueLog>();
            HistoriesChanges = new HashSet<HistoriesChange>();
            HistoriesCustomers = new HashSet<HistoriesCustomer>();
            HistoriesEarlyPaymentComissions = new HashSet<HistoriesEarlyPaymentComission>();
            HistoriesImpairSymptoms = new HashSet<HistoriesImpairSymptom>();
            HistoriesInsurancesPolicies = new HashSet<HistoriesInsurancesPolicy>();
            HistoriesMonitorings = new HashSet<HistoriesMonitoring>();
            HistoriesMonitoringsSettings = new HashSet<HistoriesMonitoringsSetting>();
            HistoriesNotices = new HashSet<HistoriesNotice>();
            HistoriesNotifications = new HashSet<HistoriesNotification>();
            HistoriesOfficers = new HashSet<HistoriesOfficer>();
            HistoriesOfficersAllowedUpdates = new HashSet<HistoriesOfficersAllowedUpdate>();
            HistoriesOverdues = new HashSet<HistoriesOverdue>();
            HistoriesPercentsOnScheduleDates = new HashSet<HistoriesPercentsOnScheduleDate>();
            HistoriesRatings = new HashSet<HistoriesRating>();
            HistoriesReservesClassifications = new HashSet<HistoriesReservesClassification>();
            HistoriesStatuses = new HashSet<HistoriesStatus>();
            HistoriesStopCalculationsDates = new HashSet<HistoriesStopCalculationsDate>();
            HistoriesSubStatuses = new HashSet<HistoriesSubStatus>();
            HistoriesWriteOffDeferredPercents = new HashSet<HistoriesWriteOffDeferredPercent>();
            IncomesStructuresActualDates = new HashSet<IncomesStructuresActualDate>();
            IndividualFines = new HashSet<IndividualFine>();
            LimitChanges = new HashSet<LimitChange>();
            LimitCreditLimits = new HashSet<LimitCredit>();
            LoansSettings = new HashSet<LoansSetting>();
            NationalBankForeignCurrencyLoansRequirements = new HashSet<NationalBankForeignCurrencyLoansRequirement>();
            NotCalculationStatuses = new HashSet<NotCalculationStatus>();
            NotToCloseLoans = new HashSet<NotToCloseLoan>();
            OverdraftLimitsHistories = new HashSet<OverdraftLimitsHistory>();
            PaymentsFromCustomerAccountsRequests = new HashSet<PaymentsFromCustomerAccountsRequest>();
            PaysheetsEmployeesLoansTransfers = new HashSet<PaysheetsEmployeesLoansTransfer>();
            PercentBalances = new HashSet<PercentBalance>();
            PrepaymentLocks = new HashSet<PrepaymentLock>();
            PreviousLoansPayment1s = new HashSet<PreviousLoansPayment1>();
            PreviousLoansPayments = new HashSet<PreviousLoansPayment>();
            Reserves = new HashSet<Reserf>();
            ReversePaymentOrders = new HashSet<ReversePaymentOrder>();
            RevertableOperations = new HashSet<RevertableOperation>();
            RevolvingWindows = new HashSet<RevolvingWindow>();
            SecuritiesMortrages = new HashSet<SecuritiesMortrage>();
            SubsideGraphics = new HashSet<SubsideGraphic>();
            Tranches = new HashSet<Tranch>();
        }

        public int CreditId { get; set; }
        public int ProductId { get; set; }
        public DateTime RequestDate { get; set; }
        public int RequestCurrencyId { get; set; }
        public byte? RequestPeriod { get; set; }
        public decimal? RequestRate { get; set; }
        public int? PaymentSourceId { get; set; }
        public byte LanguageId { get; set; }
        public int? RequestMortrageTypeId { get; set; }
        public int? IncomeApproveTypeId { get; set; }
        public string LoanLocation { get; set; }
        public int? MarketingSourceId { get; set; }
        public decimal? RequestGrantComission { get; set; }
        public byte? RequestGrantComissionType { get; set; }
        public decimal? RequestReturnComission { get; set; }
        public byte? RequestReturnComissionType { get; set; }
        public decimal? RequestTrancheIssueComission { get; set; }
        public byte? RequestTrancheIssueComissionType { get; set; }
        public decimal? RequestTrancheSupportComission { get; set; }
        public byte? RequestTrancheSupportComissionType { get; set; }
        public decimal? RequestRateForNotUsedTranche { get; set; }
        public string GroupName { get; set; }
        public int? PartnerCompanyId { get; set; }
        public DateTime? IssueDate { get; set; }
        public string DecisionNo { get; set; }
        public string AgreementNo { get; set; }
        public int? ApprovedCurrencyId { get; set; }
        public byte? ApprovedPeriod { get; set; }
        public decimal? ApprovedRate { get; set; }
        public decimal? ApprovedGrantComission { get; set; }
        public byte? ApprovedGrantComissionType { get; set; }
        public decimal? ApprovedReturnComission { get; set; }
        public byte? ApprovedReturnComissionType { get; set; }
        public decimal? ApprovedTrancheIssueComission { get; set; }
        public byte? ApprovedTrancheIssueComissionType { get; set; }
        public decimal? ApprovedTrancheSupportComission { get; set; }
        public byte? ApprovedTrancheSupportComissionType { get; set; }
        public decimal? ApprovedRateForNotUsedTranche { get; set; }
        public int OfficeId { get; set; }
        public string CurrentObligations { get; set; }
        public string GuaranteeContractNo { get; set; }
        public string GuaranteeContractDescription { get; set; }
        public int? GuaranteeBeneficiaryId { get; set; }
        public string GuaranteeSpecialInstructions { get; set; }
        public int? GuaranteePurposeId { get; set; }
        public int? LoanPurposeTypeId { get; set; }
        public byte? CalculationTypeId { get; set; }
        public DateTime? GuaranteeEndDate { get; set; }
        public int? FinancingSourceId { get; set; }
        public int? NonPaymentRisk { get; set; }
        public byte? IssueComissionPaymentTypeId { get; set; }
        public int? CreditFraudStatus { get; set; }
        public int? InformationId { get; set; }
        public int? ParallelRelativeCredit { get; set; }
        public int? Options { get; set; }
        public int? ExternalProgramId { get; set; }
        public int? RedealType { get; set; }
        public int? CustomerAwayReasonDictionaryId { get; set; }
        public int? ExternalCashDeskId { get; set; }
        public string CurrentAssets { get; set; }
        public string MainAssets { get; set; }
        public string EquityСapital { get; set; }
        public string ProfitBeforeLoanRepayment { get; set; }
        public string NetProfitAfterLoanRepayment { get; set; }
        public string IssueAccountNo { get; set; }
        public int? ReasonId { get; set; }

        public virtual CustomerAwayReasonDictionary CustomerAwayReasonDictionary { get; set; }
        public virtual ExternalProgram ExternalProgram { get; set; }
        public virtual FinancingSource FinancingSource { get; set; }
        public virtual Customer GuaranteeBeneficiary { get; set; }
        public virtual GuaranteePurpose GuaranteePurpose { get; set; }
        public virtual IncomeApproveType IncomeApproveType { get; set; }
        public virtual Information Information { get; set; }
        public virtual Purpose LoanPurposeType { get; set; }
        public virtual MarketingSource MarketingSource { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual PaymentSource PaymentSource { get; set; }
        public virtual Product1 Product { get; set; }
        public virtual CreditRefinancingReason Reason { get; set; }
        public virtual Currency3 RequestCurrency { get; set; }
        public virtual MortrageType RequestMortrageType { get; set; }
        public virtual ComissionDiscountRate ComissionDiscountRate { get; set; }
        public virtual EbToOdbloan EbToOdbloan { get; set; }
        public virtual ExceedMaxFinesCredit ExceedMaxFinesCredit { get; set; }
        public virtual ForbiddenCredit ForbiddenCredit { get; set; }
        public virtual IndividualTariff1 IndividualTariff1 { get; set; }
        public virtual LimitCredit LimitCreditCredit { get; set; }
        public virtual Mortgage Mortgage { get; set; }
        public virtual NonStandardFine NonStandardFine { get; set; }
        public virtual PlanReserf PlanReserf { get; set; }
        public virtual RejectedHistoriesReason RejectedHistoriesReason { get; set; }
        public virtual ScheduledIssueDate ScheduledIssueDate { get; set; }
        public virtual StartCalculatedFine StartCalculatedFine { get; set; }
        public virtual TranchesClosed TranchesClosed { get; set; }
        public virtual ICollection<AgileGraphic> AgileGraphics { get; set; }
        public virtual ICollection<AllGuarantor> AllGuarantors { get; set; }
        public virtual ICollection<BanksAction> BanksActions { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
        public virtual ICollection<ComissionDiscountBalance> ComissionDiscountBalances { get; set; }
        public virtual ICollection<CreditsAccount> CreditsAccounts { get; set; }
        public virtual ICollection<CreditsDepartment> CreditsDepartments { get; set; }
        public virtual ICollection<CreditsGarantee> CreditsGarantees { get; set; }
        public virtual ICollection<CustomersNotification> CustomersNotifications { get; set; }
        public virtual ICollection<DeletedRevolvingWindow> DeletedRevolvingWindows { get; set; }
        public virtual ICollection<DepositsMortrage> DepositsMortrages { get; set; }
        public virtual ICollection<EarlyRepayStatement> EarlyRepayStatements { get; set; }
        public virtual ICollection<EffectiveGraphic> EffectiveGraphics { get; set; }
        public virtual ICollection<ExternalLoansCredit> ExternalLoansCredits { get; set; }
        public virtual ICollection<FinesBalance> FinesBalances { get; set; }
        public virtual ICollection<FullPaymentsRequest> FullPaymentsRequests { get; set; }
        public virtual ICollection<GracePeriod> GracePeriods { get; set; }
        public virtual ICollection<Graphic> Graphics { get; set; }
        public virtual ICollection<GroupCycleCredit> GroupCycleCredits { get; set; }
        public virtual ICollection<HistoriesCancelIssueLog> HistoriesCancelIssueLogs { get; set; }
        public virtual ICollection<HistoriesChange> HistoriesChanges { get; set; }
        public virtual ICollection<HistoriesCustomer> HistoriesCustomers { get; set; }
        public virtual ICollection<HistoriesEarlyPaymentComission> HistoriesEarlyPaymentComissions { get; set; }
        public virtual ICollection<HistoriesImpairSymptom> HistoriesImpairSymptoms { get; set; }
        public virtual ICollection<HistoriesInsurancesPolicy> HistoriesInsurancesPolicies { get; set; }
        public virtual ICollection<HistoriesMonitoring> HistoriesMonitorings { get; set; }
        public virtual ICollection<HistoriesMonitoringsSetting> HistoriesMonitoringsSettings { get; set; }
        public virtual ICollection<HistoriesNotice> HistoriesNotices { get; set; }
        public virtual ICollection<HistoriesNotification> HistoriesNotifications { get; set; }
        public virtual ICollection<HistoriesOfficer> HistoriesOfficers { get; set; }
        public virtual ICollection<HistoriesOfficersAllowedUpdate> HistoriesOfficersAllowedUpdates { get; set; }
        public virtual ICollection<HistoriesOverdue> HistoriesOverdues { get; set; }
        public virtual ICollection<HistoriesPercentsOnScheduleDate> HistoriesPercentsOnScheduleDates { get; set; }
        public virtual ICollection<HistoriesRating> HistoriesRatings { get; set; }
        public virtual ICollection<HistoriesReservesClassification> HistoriesReservesClassifications { get; set; }
        public virtual ICollection<HistoriesStatus> HistoriesStatuses { get; set; }
        public virtual ICollection<HistoriesStopCalculationsDate> HistoriesStopCalculationsDates { get; set; }
        public virtual ICollection<HistoriesSubStatus> HistoriesSubStatuses { get; set; }
        public virtual ICollection<HistoriesWriteOffDeferredPercent> HistoriesWriteOffDeferredPercents { get; set; }
        public virtual ICollection<IncomesStructuresActualDate> IncomesStructuresActualDates { get; set; }
        public virtual ICollection<IndividualFine> IndividualFines { get; set; }
        public virtual ICollection<LimitChange> LimitChanges { get; set; }
        public virtual ICollection<LimitCredit> LimitCreditLimits { get; set; }
        public virtual ICollection<LoansSetting> LoansSettings { get; set; }
        public virtual ICollection<NationalBankForeignCurrencyLoansRequirement> NationalBankForeignCurrencyLoansRequirements { get; set; }
        public virtual ICollection<NotCalculationStatus> NotCalculationStatuses { get; set; }
        public virtual ICollection<NotToCloseLoan> NotToCloseLoans { get; set; }
        public virtual ICollection<OverdraftLimitsHistory> OverdraftLimitsHistories { get; set; }
        public virtual ICollection<PaymentsFromCustomerAccountsRequest> PaymentsFromCustomerAccountsRequests { get; set; }
        public virtual ICollection<PaysheetsEmployeesLoansTransfer> PaysheetsEmployeesLoansTransfers { get; set; }
        public virtual ICollection<PercentBalance> PercentBalances { get; set; }
        public virtual ICollection<PrepaymentLock> PrepaymentLocks { get; set; }
        public virtual ICollection<PreviousLoansPayment1> PreviousLoansPayment1s { get; set; }
        public virtual ICollection<PreviousLoansPayment> PreviousLoansPayments { get; set; }
        public virtual ICollection<Reserf> Reserves { get; set; }
        public virtual ICollection<ReversePaymentOrder> ReversePaymentOrders { get; set; }
        public virtual ICollection<RevertableOperation> RevertableOperations { get; set; }
        public virtual ICollection<RevolvingWindow> RevolvingWindows { get; set; }
        public virtual ICollection<SecuritiesMortrage> SecuritiesMortrages { get; set; }
        public virtual ICollection<SubsideGraphic> SubsideGraphics { get; set; }
        public virtual ICollection<Tranch> Tranches { get; set; }
    }
}
