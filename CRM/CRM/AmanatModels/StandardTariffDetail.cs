﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StandardTariffDetail
    {
        public int Id { get; set; }
        public int TariffId { get; set; }
        public int? TariffCurrencyId { get; set; }
        public decimal? TariffStart { get; set; }
        public decimal? TariffEnd { get; set; }
        public decimal TariffRate { get; set; }
        public byte CustomerTypeId { get; set; }
        public bool? IsPercent { get; set; }

        public virtual StandardTariffValue StandardTariffValue { get; set; }
    }
}
