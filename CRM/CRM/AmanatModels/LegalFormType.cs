﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LegalFormType
    {
        public LegalFormType()
        {
            Customers = new HashSet<Customer>();
        }

        public int LegalFormId { get; set; }
        public string TypeName { get; set; }
        public bool HasShareholders { get; set; }
        public byte OrganizationTypeId { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
