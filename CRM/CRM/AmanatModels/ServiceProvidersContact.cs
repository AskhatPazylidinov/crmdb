﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersContact
    {
        public int ProviderId { get; set; }
        public int ContactTypeId { get; set; }

        public virtual ServiceProvider Provider { get; set; }
    }
}
