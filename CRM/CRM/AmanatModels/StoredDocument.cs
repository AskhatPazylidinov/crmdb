﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StoredDocument
    {
        public int Id { get; set; }
        public int GaranteeId { get; set; }
        public int StoredDocumentTypeId { get; set; }
        public DateTime ReceiptDate { get; set; }
        public DateTime? IssueDate { get; set; }
        public int? UserId { get; set; }
        public string Description { get; set; }

        public virtual Garantee Garantee { get; set; }
        public virtual StoredDocumentType StoredDocumentType { get; set; }
        public virtual User User { get; set; }
    }
}
