﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesCategory
    {
        public UtilitiesCategory()
        {
            ContragentTypes = new HashSet<ContragentType>();
            ServiceProviders = new HashSet<ServiceProvider>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string LogoFileName { get; set; }
        public DateTime ModifyDate { get; set; }
        public int DisplayPriority { get; set; }

        public virtual ICollection<ContragentType> ContragentTypes { get; set; }
        public virtual ICollection<ServiceProvider> ServiceProviders { get; set; }
    }
}
