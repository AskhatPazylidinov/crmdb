﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DormantAccountsSetting
    {
        public byte ProductTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StartPeriod { get; set; }
        public int EndPeriod { get; set; }
    }
}
