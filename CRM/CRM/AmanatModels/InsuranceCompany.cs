﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceCompany
    {
        public InsuranceCompany()
        {
            HistoriesInsurancesPolicies = new HashSet<HistoriesInsurancesPolicy>();
            InsuranceBranches = new HashSet<InsuranceBranch>();
            InsuranceChanges = new HashSet<InsuranceChange>();
            InsurancesCompaniesDocuments = new HashSet<InsurancesCompaniesDocument>();
            ProductsInsuranceCompanies = new HashSet<ProductsInsuranceCompany>();
        }

        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<HistoriesInsurancesPolicy> HistoriesInsurancesPolicies { get; set; }
        public virtual ICollection<InsuranceBranch> InsuranceBranches { get; set; }
        public virtual ICollection<InsuranceChange> InsuranceChanges { get; set; }
        public virtual ICollection<InsurancesCompaniesDocument> InsurancesCompaniesDocuments { get; set; }
        public virtual ICollection<ProductsInsuranceCompany> ProductsInsuranceCompanies { get; set; }
    }
}
