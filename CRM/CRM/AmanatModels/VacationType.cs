﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class VacationType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool? Period { get; set; }
        public int? ExcludeLimit { get; set; }
        public bool? IsExclude { get; set; }
    }
}
