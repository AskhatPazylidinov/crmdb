﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionsAccountsBalanceGroup
    {
        public byte ConversionAccountTypeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountGroup { get; set; }

        public virtual AccountGroup AccountGroupNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
