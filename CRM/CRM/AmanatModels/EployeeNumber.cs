﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EployeeNumber
    {
        public int EmplNumber { get; set; }
        public string AccountNo { get; set; }
        public int BranchId { get; set; }
    }
}
