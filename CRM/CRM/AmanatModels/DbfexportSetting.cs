﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DbfexportSetting
    {
        public int BankCode { get; set; }
        public int SmallBankcode { get; set; }
    }
}
