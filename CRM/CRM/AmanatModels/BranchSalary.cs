﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchSalary
    {
        public int BranchId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public long? Position { get; set; }
    }
}
