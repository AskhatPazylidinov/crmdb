﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiaTransfersReceiverChange
    {
        public int MessageId { get; set; }
        public long TransferId { get; set; }
        public int? OldReceiverCustomerId { get; set; }
        public int? NewReceiverCustomerId { get; set; }
    }
}
