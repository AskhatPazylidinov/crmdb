﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FileOperation
    {
        public byte FileType { get; set; }
        public int OperType { get; set; }

        public virtual OperationType OperTypeNavigation { get; set; }
    }
}
