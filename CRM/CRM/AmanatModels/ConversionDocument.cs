﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionDocument
    {
        public int CustomerTypeId { get; set; }
        public bool IsCrossRate { get; set; }
        public string DocumentName { get; set; }
        public bool IsInternetBanking { get; set; }
    }
}
