﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxesSetting
    {
        public byte TaxTypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal TaxRate { get; set; }
    }
}
