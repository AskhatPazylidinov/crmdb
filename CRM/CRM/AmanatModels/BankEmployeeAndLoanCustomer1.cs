﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankEmployeeAndLoanCustomer1
    {
        public int SenderCustomerId { get; set; }
        public int ReceiverCustomerId { get; set; }
        public string SenderClientType { get; set; }
        public string ReceiverClientType { get; set; }
        public int? ApprovedUserId { get; set; }
        public long TransferId { get; set; }

        public virtual MoneyTransfer Transfer { get; set; }
    }
}
