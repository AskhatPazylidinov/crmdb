﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ParticipantView
    {
        public int RowsetId { get; set; }
        public int RowId { get; set; }
        public int PrtId { get; set; }
        public int PrtType { get; set; }
        public string PageNum { get; set; }
        public string Name { get; set; }
        public string Partickind { get; set; }
        public string RegNum { get; set; }
        public string AccNum { get; set; }
    }
}
