﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Constant
    {
        public decimal? AvanceProc { get; set; }
        public decimal? BonusProc { get; set; }
        public int Id { get; set; }
        public decimal? ErSs { get; set; }
    }
}
