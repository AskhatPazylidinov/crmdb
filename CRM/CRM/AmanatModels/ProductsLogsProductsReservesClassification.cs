﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsReservesClassification
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ActualDate { get; set; }
        public int ClassificationId { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
