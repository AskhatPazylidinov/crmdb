﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerIndividualRateOperation
    {
        public long MarketRateId { get; set; }
        public long? Position { get; set; }
        public int? ConversionId { get; set; }
        public long Id { get; set; }
    }
}
