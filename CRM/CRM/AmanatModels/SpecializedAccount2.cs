﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecializedAccount2
    {
        public int OfficeId { get; set; }
        public byte AccountTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string OdbaccountNo { get; set; }
        public int AccountId { get; set; }
    }
}
