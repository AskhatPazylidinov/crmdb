﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceLogView
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public string CustomerName { get; set; }
    }
}
