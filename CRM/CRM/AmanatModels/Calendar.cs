﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Calendar
    {
        public DateTime CalendarDate { get; set; }
        public byte TypeId { get; set; }
    }
}
