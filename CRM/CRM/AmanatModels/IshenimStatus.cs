﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IshenimStatus
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime StatusDate { get; set; }
        public int StatusType { get; set; }
        public string Comment { get; set; }
        public int? UserId { get; set; }
        public long? UniqueCreditNo { get; set; }
    }
}
