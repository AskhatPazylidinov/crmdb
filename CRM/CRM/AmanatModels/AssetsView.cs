﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetsView
    {
        public int AssetId { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int OfficeId { get; set; }
        public string BranchName { get; set; }
        public string OfficeName { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string InventoryNo { get; set; }
        public int MeasureId { get; set; }
        public string MeasureName { get; set; }
        public int Quantity { get; set; }
        public decimal Balance { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int? BranchId { get; set; }
        public byte? CategoryId { get; set; }
        public int? GroupId { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? LifeTime { get; set; }
        public int? DepartmentId { get; set; }
        public int? SubGroupId { get; set; }
        public int? TaxesGroupId { get; set; }
        public int? ContractorId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public decimal AmortizationSumm { get; set; }
        public decimal? Ostatok { get; set; }
    }
}
