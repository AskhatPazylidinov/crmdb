﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCurrenciesWithdrawalLimit
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int LimitPeriod { get; set; }
        public int LimitPeriodTypeId { get; set; }
        public int MaxWithdrawalCount { get; set; }
        public decimal MaxWithdrawSumm { get; set; }
        public int MaxWithdrawSummTypeId { get; set; }
        public byte PeriodType { get; set; }

        public virtual ProductsCurrency ProductsCurrency { get; set; }
    }
}
