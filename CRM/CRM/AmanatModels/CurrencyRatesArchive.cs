﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CurrencyRatesArchive
    {
        public DateTime ArchiveDate { get; set; }
        public int ArchiveUserId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime RateDate { get; set; }
        public decimal Rate { get; set; }
        public int Nominal { get; set; }
        public int UserId { get; set; }
        public int CrossCurrencyId { get; set; }
    }
}
