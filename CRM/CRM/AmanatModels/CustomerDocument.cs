﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerDocument
    {
        public int CustomerId { get; set; }
        public DateTime IssueDate { get; set; }
        public string RegistrationNo { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? ValidTill { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
