﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionTransfer
    {
        public int Id { get; set; }
        public string SenderAccountNo { get; set; }
        public int SenderAccountCurrencyId { get; set; }
        public int SenderTransactionTypeId { get; set; }
        public string ReceiverAccountNo { get; set; }
        public int ReceiverAccountCurrencyId { get; set; }
        public int ReceiverTransactionTypeId { get; set; }
        public int? BatchNum2 { get; set; }
        public string InternalNo2 { get; set; }
        public long? Position { get; set; }

        public virtual IpcExecuteTransaction IdNavigation { get; set; }
        public virtual Account ReceiverAccount { get; set; }
        public virtual Account SenderAccount { get; set; }
    }
}
