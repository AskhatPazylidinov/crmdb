﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomFieldsModule
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public int CustomFieldId { get; set; }
        public string FieldValue { get; set; }

        public virtual CustomField CustomField { get; set; }
    }
}
