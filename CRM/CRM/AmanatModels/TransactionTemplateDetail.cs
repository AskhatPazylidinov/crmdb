﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionTemplateDetail
    {
        public int Id { get; set; }
        public int TransactionTemplateId { get; set; }
        public byte DetailsType { get; set; }
        public int Value { get; set; }
    }
}
