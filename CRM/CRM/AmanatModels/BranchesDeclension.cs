﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesDeclension
    {
        public int BranchId { get; set; }
        public byte DeclensionTypeId { get; set; }
        public string HeadOfLegalDepartment { get; set; }
        public string AccountantPosition { get; set; }
        public string AccountantName { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
