﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExceedMaxFinesCredit
    {
        public int CreditId { get; set; }
        public DateTime ExceedDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
