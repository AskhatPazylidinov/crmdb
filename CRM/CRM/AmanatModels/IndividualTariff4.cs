﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariff4
    {
        public int IndividualTariffId { get; set; }
        public DateTime ChangeDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ComissionId { get; set; }
        public byte DirectionTypeId { get; set; }
        public int CustomerId { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal ComissionSumm { get; set; }
        public byte ComissionTypeId { get; set; }
        public decimal MinComissionSumm { get; set; }
        public decimal MaxComissionSumm { get; set; }
        public bool? ReturnedOnCancel { get; set; }
        public bool IsAprroved { get; set; }
        public decimal? UnitedComissionSumm { get; set; }

        public virtual SwiftComission Comission { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
