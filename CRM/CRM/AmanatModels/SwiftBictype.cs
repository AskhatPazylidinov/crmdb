﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftBictype
    {
        public int Id { get; set; }
        public string Bictype { get; set; }
    }
}
