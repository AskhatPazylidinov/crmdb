﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesKindergartenPayment
    {
        public int OperationId { get; set; }
        public string FullCustomerName { get; set; }
        public string PaymentComment { get; set; }
        public decimal TotalSumm { get; set; }
        public string FullKinderName { get; set; }

        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
