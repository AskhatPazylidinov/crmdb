﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Emitter
    {
        public Emitter()
        {
            Emissions = new HashSet<Emission>();
        }

        public int EmitterId { get; set; }
        public string EmitterName { get; set; }
        public byte EmitterTypeId { get; set; }

        public virtual ICollection<Emission> Emissions { get; set; }
    }
}
