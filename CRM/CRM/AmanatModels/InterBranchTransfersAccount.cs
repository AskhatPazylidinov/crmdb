﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchTransfersAccount
    {
        public int AccountId { get; set; }
        public int BranchId { get; set; }
        public int AccountTypeId { get; set; }
        public byte? CustomerTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
    }
}
