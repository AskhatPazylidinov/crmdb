﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RatesBasedOnBalanceDate
    {
        public RatesBasedOnBalanceDate()
        {
            RatesBasedOnBalances = new HashSet<RatesBasedOnBalance>();
        }

        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
        public virtual ICollection<RatesBasedOnBalance> RatesBasedOnBalances { get; set; }
    }
}
