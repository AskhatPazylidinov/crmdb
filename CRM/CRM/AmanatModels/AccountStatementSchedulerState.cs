﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountStatementSchedulerState
    {
        public int Id { get; set; }
        public int SchedulerId { get; set; }
        public DateTime DateSend { get; set; }
        public bool IsSend { get; set; }
        public int Period { get; set; }
        public string Addresses { get; set; }
        public string Details { get; set; }

        public virtual AccountStatementScheduler Scheduler { get; set; }
    }
}
