﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class VacationsCalculation
    {
        public int VacationId { get; set; }
        public int Vyear { get; set; }
        public int Vmonth { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalDays { get; set; }

        public virtual Vacation Vacation { get; set; }
    }
}
