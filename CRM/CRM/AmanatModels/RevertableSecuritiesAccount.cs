﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableSecuritiesAccount
    {
        public int RevertId { get; set; }
        public int SecuritiesId { get; set; }
        public int AccountTypeId { get; set; }
        public DateTime PrevLastCalcDate { get; set; }
        public decimal PrevSumV { get; set; }

        public virtual RevertableOperation1 Revert { get; set; }
    }
}
