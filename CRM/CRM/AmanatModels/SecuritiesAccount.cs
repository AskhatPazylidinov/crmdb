﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesAccount
    {
        public int SecurityCustomerId { get; set; }
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string PercentAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public DateTime LastCalcDate { get; set; }
        public decimal AvailableSumV { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual SecuritiesCustomer SecurityCustomer { get; set; }
    }
}
