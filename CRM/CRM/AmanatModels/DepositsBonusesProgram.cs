﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsBonusesProgram
    {
        public int ProgramId { get; set; }
        public int CurrencyId { get; set; }
        public int Period { get; set; }
        public byte TypeId { get; set; }

        public virtual Product2 Program { get; set; }
    }
}
