﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MigrationAccount
    {
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
