﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesExchangeRatesHistoryView
    {
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime RateDate { get; set; }
        public decimal BuyRate { get; set; }
        public decimal SellRate { get; set; }
        public DateTime HistoryDate { get; set; }
        public int UserId { get; set; }
        public bool IsApprove { get; set; }
    }
}
