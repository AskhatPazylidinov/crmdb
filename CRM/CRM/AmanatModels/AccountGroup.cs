﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountGroup
    {
        public AccountGroup()
        {
            BranchReservedAccountsBalanceGroups = new HashSet<BranchReservedAccountsBalanceGroup>();
            ConversionsAccountsBalanceGroups = new HashSet<ConversionsAccountsBalanceGroup>();
            DepositoryAccountsBalanceGroups = new HashSet<DepositoryAccountsBalanceGroup>();
            IncomeOperationTypesBalanceGroups = new HashSet<IncomeOperationTypesBalanceGroup>();
            InterBranchTransfersAccountsBalanceGroups = new HashSet<InterBranchTransfersAccountsBalanceGroup>();
            InterBranchTransfersIncomeAccountsBalanceGroups = new HashSet<InterBranchTransfersIncomeAccountsBalanceGroup>();
            MoneyTransfersAccountsBalanceGroups = new HashSet<MoneyTransfersAccountsBalanceGroup>();
            PlainAccountsTemplates = new HashSet<PlainAccountsTemplate>();
            ReservesAccountsBalanceGroupExpenseAccountGroupNavigations = new HashSet<ReservesAccountsBalanceGroup>();
            ReservesAccountsBalanceGroupReserveAccountGroupNavigations = new HashSet<ReservesAccountsBalanceGroup>();
            SpecializedAccountsBalanceGroups = new HashSet<SpecializedAccountsBalanceGroup>();
        }

        public string Code { get; set; }
        public string RuName { get; set; }

        public virtual ICollection<BranchReservedAccountsBalanceGroup> BranchReservedAccountsBalanceGroups { get; set; }
        public virtual ICollection<ConversionsAccountsBalanceGroup> ConversionsAccountsBalanceGroups { get; set; }
        public virtual ICollection<DepositoryAccountsBalanceGroup> DepositoryAccountsBalanceGroups { get; set; }
        public virtual ICollection<IncomeOperationTypesBalanceGroup> IncomeOperationTypesBalanceGroups { get; set; }
        public virtual ICollection<InterBranchTransfersAccountsBalanceGroup> InterBranchTransfersAccountsBalanceGroups { get; set; }
        public virtual ICollection<InterBranchTransfersIncomeAccountsBalanceGroup> InterBranchTransfersIncomeAccountsBalanceGroups { get; set; }
        public virtual ICollection<MoneyTransfersAccountsBalanceGroup> MoneyTransfersAccountsBalanceGroups { get; set; }
        public virtual ICollection<PlainAccountsTemplate> PlainAccountsTemplates { get; set; }
        public virtual ICollection<ReservesAccountsBalanceGroup> ReservesAccountsBalanceGroupExpenseAccountGroupNavigations { get; set; }
        public virtual ICollection<ReservesAccountsBalanceGroup> ReservesAccountsBalanceGroupReserveAccountGroupNavigations { get; set; }
        public virtual ICollection<SpecializedAccountsBalanceGroup> SpecializedAccountsBalanceGroups { get; set; }
    }
}
