﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfersDocument
    {
        public int DocumentId { get; set; }
        public byte EventTypeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string Filename { get; set; }
    }
}
