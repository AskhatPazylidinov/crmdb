﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExchangeTransactionCorrespondentTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public long CorrPosition { get; set; }
        public short CorrPositionn { get; set; }

        public virtual Transaction2 CorrPositionNavigation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
