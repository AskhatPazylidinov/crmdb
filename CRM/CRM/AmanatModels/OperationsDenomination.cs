﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsDenomination
    {
        public int OperationId { get; set; }
        public int NoteId { get; set; }
        public int DiffAmount { get; set; }

        public virtual Denomination Note { get; set; }
        public virtual DetailedOperation Operation { get; set; }
    }
}
