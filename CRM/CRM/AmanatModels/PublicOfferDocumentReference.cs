﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PublicOfferDocumentReference
    {
        public int DocumentId { get; set; }
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int LoginId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
