﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxRequestsLog
    {
        public long LogId { get; set; }
        public int UserId { get; set; }
        public int OperationId { get; set; }
        public long? TransactionIdbyGns { get; set; }
        public string Inn { get; set; }
        public string PaymentDescription { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime EventDate { get; set; }
        public byte RequestType { get; set; }
        public int RequestResultStatus { get; set; }
        public string Comment { get; set; }
    }
}
