﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesOfficersAllowedUpdate
    {
        public int CreditId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
