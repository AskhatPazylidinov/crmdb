﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Office
    {
        public int MainOfficeId { get; set; }
        public string DeviceCode { get; set; }
        public int Iddevice { get; set; }
        public decimal Comission { get; set; }
        public string Adress { get; set; }
        public string NameDevice { get; set; }
        public string AccountNoTransit { get; set; }
        public string AccountNoIncome { get; set; }
        public decimal TotalComission { get; set; }
        public int? OfficeIdin { get; set; }
        public int? CityId { get; set; }
        public int? AdditionalDeviceType { get; set; }
        public string SerialNumber { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public virtual City1 City { get; set; }
        public virtual DeviceType IddeviceNavigation { get; set; }
    }
}
