﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StartNonComissionableSumm
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumm { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
