﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentType2
    {
        public DocumentType2()
        {
            DocumentTypeIds = new HashSet<DocumentTypeId>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DocumentTypeId> DocumentTypeIds { get; set; }
    }
}
