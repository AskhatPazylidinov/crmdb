﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsCompany
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Code { get; set; }
        public int IdinBranch { get; set; }
        public string Name { get; set; }
        public string CompanyCode { get; set; }
        public string BinCode { get; set; }
        public string AgrementCode { get; set; }
        public string AccountConditionSet { get; set; }
        public int MinBal { get; set; }
        public int NonReduceBal { get; set; }
        public string CardConditionSet { get; set; }
        public string CardRiskLevel { get; set; }
        public bool? IsView { get; set; }
        public DateTime? CloseDate { get; set; }

        public virtual User User { get; set; }
    }
}
