﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftSystem
    {
        public SwiftSystem()
        {
            Swift103Transfers = new HashSet<Swift103Transfer>();
            SwiftAccounts = new HashSet<SwiftAccount>();
        }

        public int SystemId { get; set; }
        public string SwiftSystemName { get; set; }

        public virtual ICollection<Swift103Transfer> Swift103Transfers { get; set; }
        public virtual ICollection<SwiftAccount> SwiftAccounts { get; set; }
    }
}
