﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Limit2
    {
        public int CurrencyId { get; set; }
        public decimal LimitValue { get; set; }
    }
}
