﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersLog
    {
        public long LogId { get; set; }
        public DateTime LogDate { get; set; }
        public short LogStatusId { get; set; }
        public string Description { get; set; }
        public string Ipaddress { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }

        public virtual User User { get; set; }
    }
}
