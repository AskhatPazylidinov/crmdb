﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxOkmotCode
    {
        public int TaxDepartmentId { get; set; }
        public string OkmotCode { get; set; }

        public virtual TaxDepartment1 TaxDepartment { get; set; }
    }
}
