﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Orgform
    {
        public int KyCode { get; set; }
        public string FlName { get; set; }
        public int? Id { get; set; }
    }
}
