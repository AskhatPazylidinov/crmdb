﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class WorkedHour
    {
        public int EmployeeId { get; set; }
        public DateTime WorkDate { get; set; }
        public decimal WorkedHours { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
