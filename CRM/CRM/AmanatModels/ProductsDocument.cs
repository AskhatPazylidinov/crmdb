﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsDocument
    {
        public int DocumentId { get; set; }
        public int ProductId { get; set; }
        public string FileName { get; set; }
        public byte DocumentTypeId { get; set; }
        public bool ToPdf { get; set; }

        public virtual Product Product { get; set; }
    }
}
