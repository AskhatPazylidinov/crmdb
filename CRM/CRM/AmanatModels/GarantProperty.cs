﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GarantProperty
    {
        public int? OldPropertyId { get; set; }
        public int? NewPropertyId { get; set; }
    }
}
