﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NightUserView
    {
        public int BranchId { get; set; }
        public int OfficeId { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public byte? SpecialCashierType { get; set; }
    }
}
