﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekLiftSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
        public string BankCode { get; set; }
    }
}
