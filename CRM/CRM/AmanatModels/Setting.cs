﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Setting
    {
        public string OfficerName { get; set; }
        public string OfficerPosition { get; set; }
        public string OfficerPhone { get; set; }
        public string OfficerMail { get; set; }
        public int Id { get; set; }
    }
}
