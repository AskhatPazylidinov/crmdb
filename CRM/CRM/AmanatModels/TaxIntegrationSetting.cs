﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxIntegrationSetting
    {
        public int SettingId { get; set; }
        public bool UseIntegration { get; set; }
        public byte OperatorNo { get; set; }
        public string TerminalNo { get; set; }
        public string VehicleTaxPaymentCode { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool? CheckOnlineIdentificationNumber { get; set; }
        public bool TaxPaymentDescriptionType { get; set; }
    }
}
