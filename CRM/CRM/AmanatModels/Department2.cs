﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Department2
    {
        public Department2()
        {
            Schedule1s = new HashSet<Schedule1>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? Supervisorid { get; set; }
        public int? Officeid { get; set; }
        public string Shortname { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string OpenReason { get; set; }
        public string CloseReason { get; set; }
        public int? StatusId { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int TypeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ICollection<Schedule1> Schedule1s { get; set; }
    }
}
