﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondLog
    {
        public int Id { get; set; }
        public int OperationId { get; set; }
        public string Token { get; set; }
        public int RequestType { get; set; }
        public string Message { get; set; }
    }
}
