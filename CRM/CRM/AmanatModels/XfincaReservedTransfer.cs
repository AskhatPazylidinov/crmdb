﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class XfincaReservedTransfer
    {
        public long? TransferId { get; set; }
        public int? UserId { get; set; }
        public long? Position { get; set; }
    }
}
