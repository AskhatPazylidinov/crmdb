﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneyTransfersStatus
    {
        public MoneyTransfersStatus()
        {
            MoneyTransfersTransactions = new HashSet<MoneyTransfersTransaction>();
        }

        public long OperationId { get; set; }
        public long TransferId { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public int OfficeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual MoneyTransfer Transfer { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<MoneyTransfersTransaction> MoneyTransfersTransactions { get; set; }
    }
}
