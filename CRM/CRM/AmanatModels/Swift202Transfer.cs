﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Swift202Transfer
    {
        public long TransferId { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrencyId { get; set; }
        public string RelatedReferenceInfo { get; set; }
        public int SystemId { get; set; }
        public byte TrType { get; set; }

        public virtual SwiftTransfer Transfer { get; set; }
        public virtual Currency3 TransferCurrency { get; set; }
    }
}
