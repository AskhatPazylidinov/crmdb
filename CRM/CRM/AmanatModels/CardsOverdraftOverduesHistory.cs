﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardsOverdraftOverduesHistory
    {
        public string CardAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int OverdueDays { get; set; }
        public int FileId { get; set; }

        public virtual Account C { get; set; }
        public virtual CardsOverdraftOverduesFileHistory File { get; set; }
    }
}
