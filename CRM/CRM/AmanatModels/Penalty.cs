﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Penalty
    {
        public int EmployeeId { get; set; }
        public DateTime PenaltyDate { get; set; }
        public byte TypeId { get; set; }
        public string OrderNo { get; set; }
        public string Comment { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
