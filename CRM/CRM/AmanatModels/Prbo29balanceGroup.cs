﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Prbo29balanceGroup
    {
        public int TypeId { get; set; }
        public string BalanceGroup { get; set; }
        public bool? IncludeGroupInFlowReport { get; set; }
        public byte TypeUrgency { get; set; }

        public virtual Prbo29groupsType Type { get; set; }
    }
}
