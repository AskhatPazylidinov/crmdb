﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersAllowedOperation
    {
        public int OperationId { get; set; }
        public string ProviderNamespace { get; set; }
        public string ProviderName { get; set; }
        public string OperationName { get; set; }
        public string Description { get; set; }
        public string ExtendedDescription { get; set; }
        public byte CriticalTypeId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? UserId { get; set; }
    }
}
