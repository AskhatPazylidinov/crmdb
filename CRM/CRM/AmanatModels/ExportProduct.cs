﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExportProduct
    {
        public int ExportProductId { get; set; }

        public virtual Product1 ExportProductNavigation { get; set; }
    }
}
