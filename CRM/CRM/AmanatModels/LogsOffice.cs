﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsOffice
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int MainOfficeId { get; set; }
        public string DeviceCode { get; set; }
        public int Iddevice { get; set; }
        public decimal Comission { get; set; }
        public string Adress { get; set; }
        public string NameDevice { get; set; }
        public string AccountNoTransit { get; set; }
        public string AccountNoIncome { get; set; }
        public decimal TotalComission { get; set; }
        public int? OfficeIdin { get; set; }

        public virtual User User { get; set; }
    }
}
