﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ForeignDepositsIncomeOperation
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public string SwiftAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
