﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MinimalPaymentRate
    {
        public DateTime ActualDate { get; set; }
        public decimal Rate { get; set; }
    }
}
