﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Transaction2
    {
        public Transaction2()
        {
            CorrBanksOperationsOnTemplatesTransactions = new HashSet<CorrBanksOperationsOnTemplatesTransaction>();
            ExchangeTransactionCorrespondentTransactionCorrPositionNavigations = new HashSet<ExchangeTransactionCorrespondentTransaction>();
            ExchangeTransactionCorrespondentTransactionPositionNavigations = new HashSet<ExchangeTransactionCorrespondentTransaction>();
            HistoryTransactions = new HashSet<HistoryTransaction>();
            LeasingsPayments = new HashSet<LeasingsPayment>();
            PayBalance1s = new HashSet<PayBalance1>();
            PayBalanceTemps = new HashSet<PayBalanceTemp>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
            Transaction1s = new HashSet<Transaction1>();
            TransactionsIpcExecuteTransactionPayments = new HashSet<TransactionsIpcExecuteTransactionPayment>();
            WalletOperationTransactions = new HashSet<WalletOperationTransaction>();
        }

        public long Position { get; set; }
        public short Positionn { get; set; }
        public int CurrencyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public byte OperationTypeId { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }
        public byte TransactionTypeId { get; set; }
        public byte ExtendedTypeId { get; set; }
        public string Comment { get; set; }
        public byte StatusId { get; set; }
        public bool IsFinalTransaction { get; set; }
        public byte CashSymbol { get; set; }
        public DateTime DateV { get; set; }
        public int UserId { get; set; }
        public string ResponsiblePerson { get; set; }
        public int OfficeId { get; set; }
        public int? AttorneyId { get; set; }
        public string OperCode { get; set; }
        public int? CountryId { get; set; }
        public string AddComment { get; set; }
        public bool? CreditPayment { get; set; }
        public bool? IsComplex { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Customer Attorney { get; set; }
        public virtual Account1 C { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
        public virtual CashFlowTransaction CashFlowTransaction { get; set; }
        public virtual ChequesOperation ChequesOperation { get; set; }
        public virtual ConversionsTransaction ConversionsTransaction { get; set; }
        public virtual CorrBankTransaction CorrBankTransaction { get; set; }
        public virtual CrossBankConversionsTransaction CrossBankConversionsTransaction { get; set; }
        public virtual CrossBranchConversionsTransaction CrossBranchConversionsTransaction { get; set; }
        public virtual ExchangeTransactionRate ExchangeTransactionRate { get; set; }
        public virtual FinalTurnover FinalTurnover { get; set; }
        public virtual ForeignDepositsIncomeOperation ForeignDepositsIncomeOperation { get; set; }
        public virtual IntegrationServiceTransaction IntegrationServiceTransaction { get; set; }
        public virtual InternalTaxPayment InternalTaxPayment { get; set; }
        public virtual MoneyTransfersTransaction MoneyTransfersTransaction { get; set; }
        public virtual NettingOperationsTransaction NettingOperationsTransaction { get; set; }
        public virtual NonCashOperation NonCashOperation { get; set; }
        public virtual OnlineTransaction OnlineTransaction { get; set; }
        public virtual OperationsTransaction OperationsTransaction { get; set; }
        public virtual OrdersPosition OrdersPosition { get; set; }
        public virtual PartnerOperationsTransaction PartnerOperationsTransaction { get; set; }
        public virtual PosTerminalOperation PosTerminalOperation { get; set; }
        public virtual RevertableCloseOperationsTransaction RevertableCloseOperationsTransaction { get; set; }
        public virtual RevertableOperationsTransaction RevertableOperationsTransaction { get; set; }
        public virtual SalesTaxeableTransaction SalesTaxeableTransaction { get; set; }
        public virtual SecuritiesOperationsTransaction SecuritiesOperationsTransaction { get; set; }
        public virtual SwiftInvoicesToTheBank SwiftInvoicesToTheBank { get; set; }
        public virtual SwiftTransfersTransaction SwiftTransfersTransaction { get; set; }
        public virtual TaxesOnNonResidentsIncome TaxesOnNonResidentsIncome { get; set; }
        public virtual TransactionsInternalPaymentCode TransactionsInternalPaymentCode { get; set; }
        public virtual TransfersOperationsTransaction TransfersOperationsTransaction { get; set; }
        public virtual ICollection<CorrBanksOperationsOnTemplatesTransaction> CorrBanksOperationsOnTemplatesTransactions { get; set; }
        public virtual ICollection<ExchangeTransactionCorrespondentTransaction> ExchangeTransactionCorrespondentTransactionCorrPositionNavigations { get; set; }
        public virtual ICollection<ExchangeTransactionCorrespondentTransaction> ExchangeTransactionCorrespondentTransactionPositionNavigations { get; set; }
        public virtual ICollection<HistoryTransaction> HistoryTransactions { get; set; }
        public virtual ICollection<LeasingsPayment> LeasingsPayments { get; set; }
        public virtual ICollection<PayBalance1> PayBalance1s { get; set; }
        public virtual ICollection<PayBalanceTemp> PayBalanceTemps { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
        public virtual ICollection<Transaction1> Transaction1s { get; set; }
        public virtual ICollection<TransactionsIpcExecuteTransactionPayment> TransactionsIpcExecuteTransactionPayments { get; set; }
        public virtual ICollection<WalletOperationTransaction> WalletOperationTransactions { get; set; }
    }
}
