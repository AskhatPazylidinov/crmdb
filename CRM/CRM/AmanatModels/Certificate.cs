﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Certificate
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public string Name { get; set; }
        public string Issuer { get; set; }
    }
}
