﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerAwayReasonDictionary
    {
        public CustomerAwayReasonDictionary()
        {
            CustomerAwayReasons = new HashSet<CustomerAwayReason>();
            Histories = new HashSet<History>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CustomerAwayReason> CustomerAwayReasons { get; set; }
        public virtual ICollection<History> Histories { get; set; }
    }
}
