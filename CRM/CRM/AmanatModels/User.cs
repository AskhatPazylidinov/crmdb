﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class User
    {
        public User()
        {
            Agents = new HashSet<Agent>();
            AllowedViewAccounts = new HashSet<AllowedViewAccount>();
            AllowedViewCompanies = new HashSet<AllowedViewCompany>();
            Assets = new HashSet<Asset>();
            AuthorizedOperations = new HashSet<AuthorizedOperation>();
            AutoPaymentsLogs = new HashSet<AutoPaymentsLog>();
            AverageLoansInterestRates = new HashSet<AverageLoansInterestRate>();
            BanksActions = new HashSet<BanksAction>();
            BerekeUserNameInfos = new HashSet<BerekeUserNameInfo>();
            BlockedAccounts = new HashSet<BlockedAccount>();
            BranchesCloseDays = new HashSet<BranchesCloseDay>();
            CardCloseUsers = new HashSet<Card>();
            CardOpenUsers = new HashSet<Card>();
            CardsRegistries = new HashSet<CardsRegistry>();
            ContactLoyalOperations = new HashSet<ContactLoyalOperation>();
            ConversionLimitsByRoleApprovedUsers = new HashSet<ConversionLimitsByRole>();
            ConversionLimitsByRoleUsers = new HashSet<ConversionLimitsByRole>();
            Conversions = new HashSet<Conversion>();
            ConversionsStatuses = new HashSet<ConversionsStatus>();
            CorrBanksOperationsOnTemplatesStatuses = new HashSet<CorrBanksOperationsOnTemplatesStatus>();
            CreateTransactionsInModuleUsers = new HashSet<CreateTransactionsInModuleUser>();
            CreditsGaranteesStatuses = new HashSet<CreditsGaranteesStatus>();
            CrossBankConversions = new HashSet<CrossBankConversion>();
            CrossBankConversionsStatuses = new HashSet<CrossBankConversionsStatus>();
            CrossBranchConversionStatuses = new HashSet<CrossBranchConversionStatus>();
            CurrencyRates = new HashSet<CurrencyRate>();
            CurrencyRatesHistories = new HashSet<CurrencyRatesHistory>();
            CustomersChangeApprovedUsers = new HashSet<CustomersChange>();
            CustomersChangeChangedUsers = new HashSet<CustomersChange>();
            DeletedCardsRegistries = new HashSet<DeletedCardsRegistry>();
            DeletedTransactionDeleteUsers = new HashSet<DeletedTransaction>();
            DeletedTransactionUsers = new HashSet<DeletedTransaction>();
            DeletedUtilitiesRegistryDeleteUsers = new HashSet<DeletedUtilitiesRegistry>();
            DeletedUtilitiesRegistryUsers = new HashSet<DeletedUtilitiesRegistry>();
            DepositsBroughtByOthers = new HashSet<DepositsBroughtByOther>();
            DuplicatedContragents = new HashSet<DuplicatedContragent>();
            EditApprovedParamsLimitsForUsers = new HashSet<EditApprovedParamsLimitsForUser>();
            EnrollmentFiles = new HashSet<EnrollmentFile>();
            GaranteesMonitoringsStatuses = new HashSet<GaranteesMonitoringsStatus>();
            GaranteesOfficers = new HashSet<GaranteesOfficer>();
            HeadCashiers = new HashSet<HeadCashier>();
            HistoriesCancelIssueLogs = new HashSet<HistoriesCancelIssueLog>();
            HistoriesChanges = new HashSet<HistoriesChange>();
            HistoriesMonitoringsStatuses = new HashSet<HistoriesMonitoringsStatus>();
            HistoriesNextDays = new HashSet<HistoriesNextDay>();
            HistoriesNotices = new HashSet<HistoriesNotice>();
            HistoriesNotifications = new HashSet<HistoriesNotification>();
            HistoriesOfficers = new HashSet<HistoriesOfficer>();
            HistoriesOfficersAllowedUpdates = new HashSet<HistoriesOfficersAllowedUpdate>();
            HistoriesPercentsOnScheduleDates = new HashSet<HistoriesPercentsOnScheduleDate>();
            HistoriesRatings = new HashSet<HistoriesRating>();
            HistoriesStatuses = new HashSet<HistoriesStatus>();
            HistoriesStopCalculationsDates = new HashSet<HistoriesStopCalculationsDate>();
            HistoriesSubStatuses = new HashSet<HistoriesSubStatus>();
            ImportTransactionsFiles = new HashSet<ImportTransactionsFile>();
            ImportedTaxPayments = new HashSet<ImportedTaxPayment>();
            IndividualCapitalizationDays = new HashSet<IndividualCapitalizationDay>();
            IndividualInterestRates = new HashSet<IndividualInterestRate>();
            IndividualStornoRates = new HashSet<IndividualStornoRate>();
            IndividualWithdrawalLimits = new HashSet<IndividualWithdrawalLimit>();
            InternalPersonApprovedByExcludeUsers = new HashSet<InternalPerson>();
            InternalPersonApprovedByUsers = new HashSet<InternalPerson>();
            InviteCustomer1s = new HashSet<InviteCustomer1>();
            InviteCustomers = new HashSet<InviteCustomer>();
            IssueLoanLimitsForUsers = new HashSet<IssueLoanLimitsForUser>();
            Jobs = new HashSet<Job>();
            LogsBalanceGroupProducts = new HashSet<LogsBalanceGroupProduct>();
            LogsBanks = new HashSet<LogsBank>();
            LogsCardsAccountTypes = new HashSet<LogsCardsAccountType>();
            LogsComissinEcvayers = new HashSet<LogsComissinEcvayer>();
            LogsCompanies = new HashSet<LogsCompany>();
            LogsCurrencies = new HashSet<LogsCurrency>();
            LogsDeviceTypes = new HashSet<LogsDeviceType>();
            LogsFileOperations = new HashSet<LogsFileOperation>();
            LogsOffices = new HashSet<LogsOffice>();
            LogsOperationDeviceTypes = new HashSet<LogsOperationDeviceType>();
            LogsOperationTypes = new HashSet<LogsOperationType>();
            LogsProductCardTypes = new HashSet<LogsProductCardType>();
            LogsProductChanges = new HashSet<LogsProductChange>();
            LogsProductInBranches = new HashSet<LogsProductInBranch>();
            LogsProducts = new HashSet<LogsProduct>();
            LogsSpecialAccounts = new HashSet<LogsSpecialAccount>();
            LogsTransitAccounts = new HashSet<LogsTransitAccount>();
            MarketRateApprovedUsers = new HashSet<MarketRate>();
            MarketRateUsers = new HashSet<MarketRate>();
            MinimalSumms = new HashSet<MinimalSumm>();
            MoneyTransfersStatuses = new HashSet<MoneyTransfersStatus>();
            NightCashierUsers = new HashSet<NightCashierUser>();
            NotToCloseLoans = new HashSet<NotToCloseLoan>();
            OfficersLimits = new HashSet<OfficersLimit>();
            OfficesCloseDays = new HashSet<OfficesCloseDay>();
            OfficesFlowOperations = new HashSet<OfficesFlowOperation>();
            OrdersStatuses = new HashSet<OrdersStatus>();
            OverdraftLimitApproveUsers = new HashSet<OverdraftLimit>();
            OverdraftLimitCreateUsers = new HashSet<OverdraftLimit>();
            OverdraftLimitsHistories = new HashSet<OverdraftLimitsHistory>();
            PaymentsLogs = new HashSet<PaymentsLog>();
            PaymentsOperations = new HashSet<PaymentsOperation>();
            PaysheetsStatuses = new HashSet<PaysheetsStatus>();
            PosTerminalOperations = new HashSet<PosTerminalOperation>();
            PotentialBlackVisitors = new HashSet<PotentialBlackVisitor>();
            PotentialBlackVisitorsOperations = new HashSet<PotentialBlackVisitorsOperation>();
            PrepaymentLocks = new HashSet<PrepaymentLock>();
            ProductsLogsProduct1s = new HashSet<ProductsLogsProduct1>();
            ProductsLogsProducts = new HashSet<ProductsLogsProduct>();
            ProductsLogsProductsApproveTypes = new HashSet<ProductsLogsProductsApproveType>();
            ProductsLogsProductsBalanceGroup1s = new HashSet<ProductsLogsProductsBalanceGroup1>();
            ProductsLogsProductsBalanceGroups = new HashSet<ProductsLogsProductsBalanceGroup>();
            ProductsLogsProductsCapitalizationsDates = new HashSet<ProductsLogsProductsCapitalizationsDate>();
            ProductsLogsProductsChange1s = new HashSet<ProductsLogsProductsChange1>();
            ProductsLogsProductsChanges = new HashSet<ProductsLogsProductsChange>();
            ProductsLogsProductsCurrenciesWithdrawalLimits = new HashSet<ProductsLogsProductsCurrenciesWithdrawalLimit>();
            ProductsLogsProductsCurrency1s = new HashSet<ProductsLogsProductsCurrency1>();
            ProductsLogsProductsCustomizableComissions = new HashSet<ProductsLogsProductsCustomizableComission>();
            ProductsLogsProductsEarlyPaymentComissions = new HashSet<ProductsLogsProductsEarlyPaymentComission>();
            ProductsLogsProductsFines = new HashSet<ProductsLogsProductsFine>();
            ProductsLogsProductsGuaranteePurposes = new HashSet<ProductsLogsProductsGuaranteePurpose>();
            ProductsLogsProductsInsuranceCompanies = new HashSet<ProductsLogsProductsInsuranceCompany>();
            ProductsLogsProductsInterests = new HashSet<ProductsLogsProductsInterest>();
            ProductsLogsProductsIssueComissions = new HashSet<ProductsLogsProductsIssueComission>();
            ProductsLogsProductsMonitoringsSettings = new HashSet<ProductsLogsProductsMonitoringsSetting>();
            ProductsLogsProductsMortrageTypes = new HashSet<ProductsLogsProductsMortrageType>();
            ProductsLogsProductsPaymentSources = new HashSet<ProductsLogsProductsPaymentSource>();
            ProductsLogsProductsRates = new HashSet<ProductsLogsProductsRate>();
            ProductsLogsProductsRatesOnBalances = new HashSet<ProductsLogsProductsRatesOnBalance>();
            ProductsLogsProductsReservesClassifications = new HashSet<ProductsLogsProductsReservesClassification>();
            ProductsLogsProductsReviewComissions = new HashSet<ProductsLogsProductsReviewComission>();
            ProductsLogsProductsStornoRates = new HashSet<ProductsLogsProductsStornoRate>();
            ProductsLogsTranchesLimits = new HashSet<ProductsLogsTranchesLimit>();
            ReceiverChangeLogs = new HashSet<ReceiverChangeLog>();
            ReliableCustomerApproverUsers = new HashSet<ReliableCustomer>();
            ReliableCustomerCreatorUsers = new HashSet<ReliableCustomer>();
            ReplacedAccountApprovedUsers = new HashSet<ReplacedAccount>();
            ReplacedAccountCreatedUsers = new HashSet<ReplacedAccount>();
            ReportsLogs = new HashSet<ReportsLog>();
            RequestsLog1s = new HashSet<RequestsLog1>();
            RequestsLog2s = new HashSet<RequestsLog2>();
            RestoredAccounts = new HashSet<RestoredAccount>();
            ReversePaymentOrders = new HashSet<ReversePaymentOrder>();
            RevertableCloseOperations = new HashSet<RevertableCloseOperation>();
            RevertableOperation1s = new HashSet<RevertableOperation1>();
            RevertableOperations = new HashSet<RevertableOperation>();
            SecuritiesOperationsStatuses = new HashSet<SecuritiesOperationsStatus>();
            SecuritiesStatuses = new HashSet<SecuritiesStatus>();
            SendLogs = new HashSet<SendLog>();
            Status2ApproveUsers = new HashSet<Status2>();
            Status2CreateUsers = new HashSet<Status2>();
            StorageAmounts = new HashSet<StorageAmount>();
            StorageTransfers = new HashSet<StorageTransfer>();
            StoredDocuments = new HashSet<StoredDocument>();
            SubStatusesLimitsForUsers = new HashSet<SubStatusesLimitsForUser>();
            SubStatusesRequestsLimitsForUsers = new HashSet<SubStatusesRequestsLimitsForUser>();
            SwiftTransfersOperations = new HashSet<SwiftTransfersOperation>();
            Transaction2s = new HashSet<Transaction2>();
            TransferStatuses = new HashSet<TransferStatus>();
            TransfersOperations = new HashSet<TransfersOperation>();
            UserInfos = new HashSet<UserInfo>();
            UserLimits = new HashSet<UserLimit>();
            UsersExtraOperations = new HashSet<UsersExtraOperation>();
            UsersLocks = new HashSet<UsersLock>();
            UsersLogs = new HashSet<UsersLog>();
            UsersPasswords = new HashSet<UsersPassword>();
            UsersRoles = new HashSet<UsersRole>();
            UsersRolesChanges = new HashSet<UsersRolesChange>();
            UtilitiesEpayRequestsLogs = new HashSet<UtilitiesEpayRequestsLog>();
            UtilitiesRegistries = new HashSet<UtilitiesRegistry>();
            UtilitiesTransactions = new HashSet<UtilitiesTransaction>();
            UtilitiesUmaiRequestsLogs = new HashSet<UtilitiesUmaiRequestsLog>();
            Verifications = new HashSet<Verification>();
            XmlReportUserFilter1s = new HashSet<XmlReportUserFilter1>();
        }

        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public string UserName { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public int? DefaultModuleId { get; set; }
        public string WorkDir { get; set; }
        public DateTime PasswordExpiryDate { get; set; }
        public bool HasToChangePasswordAfterLogin { get; set; }
        public bool SignIn { get; set; }
        public string DomainUsername { get; set; }

        public virtual ProgramModule DefaultModule { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual UsersCashierMachine UsersCashierMachine { get; set; }
        public virtual UsersCustomer UsersCustomer { get; set; }
        public virtual UtilitiesDoshcardUserMapping UtilitiesDoshcardUserMapping { get; set; }
        public virtual ICollection<Agent> Agents { get; set; }
        public virtual ICollection<AllowedViewAccount> AllowedViewAccounts { get; set; }
        public virtual ICollection<AllowedViewCompany> AllowedViewCompanies { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<AuthorizedOperation> AuthorizedOperations { get; set; }
        public virtual ICollection<AutoPaymentsLog> AutoPaymentsLogs { get; set; }
        public virtual ICollection<AverageLoansInterestRate> AverageLoansInterestRates { get; set; }
        public virtual ICollection<BanksAction> BanksActions { get; set; }
        public virtual ICollection<BerekeUserNameInfo> BerekeUserNameInfos { get; set; }
        public virtual ICollection<BlockedAccount> BlockedAccounts { get; set; }
        public virtual ICollection<BranchesCloseDay> BranchesCloseDays { get; set; }
        public virtual ICollection<Card> CardCloseUsers { get; set; }
        public virtual ICollection<Card> CardOpenUsers { get; set; }
        public virtual ICollection<CardsRegistry> CardsRegistries { get; set; }
        public virtual ICollection<ContactLoyalOperation> ContactLoyalOperations { get; set; }
        public virtual ICollection<ConversionLimitsByRole> ConversionLimitsByRoleApprovedUsers { get; set; }
        public virtual ICollection<ConversionLimitsByRole> ConversionLimitsByRoleUsers { get; set; }
        public virtual ICollection<Conversion> Conversions { get; set; }
        public virtual ICollection<ConversionsStatus> ConversionsStatuses { get; set; }
        public virtual ICollection<CorrBanksOperationsOnTemplatesStatus> CorrBanksOperationsOnTemplatesStatuses { get; set; }
        public virtual ICollection<CreateTransactionsInModuleUser> CreateTransactionsInModuleUsers { get; set; }
        public virtual ICollection<CreditsGaranteesStatus> CreditsGaranteesStatuses { get; set; }
        public virtual ICollection<CrossBankConversion> CrossBankConversions { get; set; }
        public virtual ICollection<CrossBankConversionsStatus> CrossBankConversionsStatuses { get; set; }
        public virtual ICollection<CrossBranchConversionStatus> CrossBranchConversionStatuses { get; set; }
        public virtual ICollection<CurrencyRate> CurrencyRates { get; set; }
        public virtual ICollection<CurrencyRatesHistory> CurrencyRatesHistories { get; set; }
        public virtual ICollection<CustomersChange> CustomersChangeApprovedUsers { get; set; }
        public virtual ICollection<CustomersChange> CustomersChangeChangedUsers { get; set; }
        public virtual ICollection<DeletedCardsRegistry> DeletedCardsRegistries { get; set; }
        public virtual ICollection<DeletedTransaction> DeletedTransactionDeleteUsers { get; set; }
        public virtual ICollection<DeletedTransaction> DeletedTransactionUsers { get; set; }
        public virtual ICollection<DeletedUtilitiesRegistry> DeletedUtilitiesRegistryDeleteUsers { get; set; }
        public virtual ICollection<DeletedUtilitiesRegistry> DeletedUtilitiesRegistryUsers { get; set; }
        public virtual ICollection<DepositsBroughtByOther> DepositsBroughtByOthers { get; set; }
        public virtual ICollection<DuplicatedContragent> DuplicatedContragents { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsForUser> EditApprovedParamsLimitsForUsers { get; set; }
        public virtual ICollection<EnrollmentFile> EnrollmentFiles { get; set; }
        public virtual ICollection<GaranteesMonitoringsStatus> GaranteesMonitoringsStatuses { get; set; }
        public virtual ICollection<GaranteesOfficer> GaranteesOfficers { get; set; }
        public virtual ICollection<HeadCashier> HeadCashiers { get; set; }
        public virtual ICollection<HistoriesCancelIssueLog> HistoriesCancelIssueLogs { get; set; }
        public virtual ICollection<HistoriesChange> HistoriesChanges { get; set; }
        public virtual ICollection<HistoriesMonitoringsStatus> HistoriesMonitoringsStatuses { get; set; }
        public virtual ICollection<HistoriesNextDay> HistoriesNextDays { get; set; }
        public virtual ICollection<HistoriesNotice> HistoriesNotices { get; set; }
        public virtual ICollection<HistoriesNotification> HistoriesNotifications { get; set; }
        public virtual ICollection<HistoriesOfficer> HistoriesOfficers { get; set; }
        public virtual ICollection<HistoriesOfficersAllowedUpdate> HistoriesOfficersAllowedUpdates { get; set; }
        public virtual ICollection<HistoriesPercentsOnScheduleDate> HistoriesPercentsOnScheduleDates { get; set; }
        public virtual ICollection<HistoriesRating> HistoriesRatings { get; set; }
        public virtual ICollection<HistoriesStatus> HistoriesStatuses { get; set; }
        public virtual ICollection<HistoriesStopCalculationsDate> HistoriesStopCalculationsDates { get; set; }
        public virtual ICollection<HistoriesSubStatus> HistoriesSubStatuses { get; set; }
        public virtual ICollection<ImportTransactionsFile> ImportTransactionsFiles { get; set; }
        public virtual ICollection<ImportedTaxPayment> ImportedTaxPayments { get; set; }
        public virtual ICollection<IndividualCapitalizationDay> IndividualCapitalizationDays { get; set; }
        public virtual ICollection<IndividualInterestRate> IndividualInterestRates { get; set; }
        public virtual ICollection<IndividualStornoRate> IndividualStornoRates { get; set; }
        public virtual ICollection<IndividualWithdrawalLimit> IndividualWithdrawalLimits { get; set; }
        public virtual ICollection<InternalPerson> InternalPersonApprovedByExcludeUsers { get; set; }
        public virtual ICollection<InternalPerson> InternalPersonApprovedByUsers { get; set; }
        public virtual ICollection<InviteCustomer1> InviteCustomer1s { get; set; }
        public virtual ICollection<InviteCustomer> InviteCustomers { get; set; }
        public virtual ICollection<IssueLoanLimitsForUser> IssueLoanLimitsForUsers { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
        public virtual ICollection<LogsBalanceGroupProduct> LogsBalanceGroupProducts { get; set; }
        public virtual ICollection<LogsBank> LogsBanks { get; set; }
        public virtual ICollection<LogsCardsAccountType> LogsCardsAccountTypes { get; set; }
        public virtual ICollection<LogsComissinEcvayer> LogsComissinEcvayers { get; set; }
        public virtual ICollection<LogsCompany> LogsCompanies { get; set; }
        public virtual ICollection<LogsCurrency> LogsCurrencies { get; set; }
        public virtual ICollection<LogsDeviceType> LogsDeviceTypes { get; set; }
        public virtual ICollection<LogsFileOperation> LogsFileOperations { get; set; }
        public virtual ICollection<LogsOffice> LogsOffices { get; set; }
        public virtual ICollection<LogsOperationDeviceType> LogsOperationDeviceTypes { get; set; }
        public virtual ICollection<LogsOperationType> LogsOperationTypes { get; set; }
        public virtual ICollection<LogsProductCardType> LogsProductCardTypes { get; set; }
        public virtual ICollection<LogsProductChange> LogsProductChanges { get; set; }
        public virtual ICollection<LogsProductInBranch> LogsProductInBranches { get; set; }
        public virtual ICollection<LogsProduct> LogsProducts { get; set; }
        public virtual ICollection<LogsSpecialAccount> LogsSpecialAccounts { get; set; }
        public virtual ICollection<LogsTransitAccount> LogsTransitAccounts { get; set; }
        public virtual ICollection<MarketRate> MarketRateApprovedUsers { get; set; }
        public virtual ICollection<MarketRate> MarketRateUsers { get; set; }
        public virtual ICollection<MinimalSumm> MinimalSumms { get; set; }
        public virtual ICollection<MoneyTransfersStatus> MoneyTransfersStatuses { get; set; }
        public virtual ICollection<NightCashierUser> NightCashierUsers { get; set; }
        public virtual ICollection<NotToCloseLoan> NotToCloseLoans { get; set; }
        public virtual ICollection<OfficersLimit> OfficersLimits { get; set; }
        public virtual ICollection<OfficesCloseDay> OfficesCloseDays { get; set; }
        public virtual ICollection<OfficesFlowOperation> OfficesFlowOperations { get; set; }
        public virtual ICollection<OrdersStatus> OrdersStatuses { get; set; }
        public virtual ICollection<OverdraftLimit> OverdraftLimitApproveUsers { get; set; }
        public virtual ICollection<OverdraftLimit> OverdraftLimitCreateUsers { get; set; }
        public virtual ICollection<OverdraftLimitsHistory> OverdraftLimitsHistories { get; set; }
        public virtual ICollection<PaymentsLog> PaymentsLogs { get; set; }
        public virtual ICollection<PaymentsOperation> PaymentsOperations { get; set; }
        public virtual ICollection<PaysheetsStatus> PaysheetsStatuses { get; set; }
        public virtual ICollection<PosTerminalOperation> PosTerminalOperations { get; set; }
        public virtual ICollection<PotentialBlackVisitor> PotentialBlackVisitors { get; set; }
        public virtual ICollection<PotentialBlackVisitorsOperation> PotentialBlackVisitorsOperations { get; set; }
        public virtual ICollection<PrepaymentLock> PrepaymentLocks { get; set; }
        public virtual ICollection<ProductsLogsProduct1> ProductsLogsProduct1s { get; set; }
        public virtual ICollection<ProductsLogsProduct> ProductsLogsProducts { get; set; }
        public virtual ICollection<ProductsLogsProductsApproveType> ProductsLogsProductsApproveTypes { get; set; }
        public virtual ICollection<ProductsLogsProductsBalanceGroup1> ProductsLogsProductsBalanceGroup1s { get; set; }
        public virtual ICollection<ProductsLogsProductsBalanceGroup> ProductsLogsProductsBalanceGroups { get; set; }
        public virtual ICollection<ProductsLogsProductsCapitalizationsDate> ProductsLogsProductsCapitalizationsDates { get; set; }
        public virtual ICollection<ProductsLogsProductsChange1> ProductsLogsProductsChange1s { get; set; }
        public virtual ICollection<ProductsLogsProductsChange> ProductsLogsProductsChanges { get; set; }
        public virtual ICollection<ProductsLogsProductsCurrenciesWithdrawalLimit> ProductsLogsProductsCurrenciesWithdrawalLimits { get; set; }
        public virtual ICollection<ProductsLogsProductsCurrency1> ProductsLogsProductsCurrency1s { get; set; }
        public virtual ICollection<ProductsLogsProductsCustomizableComission> ProductsLogsProductsCustomizableComissions { get; set; }
        public virtual ICollection<ProductsLogsProductsEarlyPaymentComission> ProductsLogsProductsEarlyPaymentComissions { get; set; }
        public virtual ICollection<ProductsLogsProductsFine> ProductsLogsProductsFines { get; set; }
        public virtual ICollection<ProductsLogsProductsGuaranteePurpose> ProductsLogsProductsGuaranteePurposes { get; set; }
        public virtual ICollection<ProductsLogsProductsInsuranceCompany> ProductsLogsProductsInsuranceCompanies { get; set; }
        public virtual ICollection<ProductsLogsProductsInterest> ProductsLogsProductsInterests { get; set; }
        public virtual ICollection<ProductsLogsProductsIssueComission> ProductsLogsProductsIssueComissions { get; set; }
        public virtual ICollection<ProductsLogsProductsMonitoringsSetting> ProductsLogsProductsMonitoringsSettings { get; set; }
        public virtual ICollection<ProductsLogsProductsMortrageType> ProductsLogsProductsMortrageTypes { get; set; }
        public virtual ICollection<ProductsLogsProductsPaymentSource> ProductsLogsProductsPaymentSources { get; set; }
        public virtual ICollection<ProductsLogsProductsRate> ProductsLogsProductsRates { get; set; }
        public virtual ICollection<ProductsLogsProductsRatesOnBalance> ProductsLogsProductsRatesOnBalances { get; set; }
        public virtual ICollection<ProductsLogsProductsReservesClassification> ProductsLogsProductsReservesClassifications { get; set; }
        public virtual ICollection<ProductsLogsProductsReviewComission> ProductsLogsProductsReviewComissions { get; set; }
        public virtual ICollection<ProductsLogsProductsStornoRate> ProductsLogsProductsStornoRates { get; set; }
        public virtual ICollection<ProductsLogsTranchesLimit> ProductsLogsTranchesLimits { get; set; }
        public virtual ICollection<ReceiverChangeLog> ReceiverChangeLogs { get; set; }
        public virtual ICollection<ReliableCustomer> ReliableCustomerApproverUsers { get; set; }
        public virtual ICollection<ReliableCustomer> ReliableCustomerCreatorUsers { get; set; }
        public virtual ICollection<ReplacedAccount> ReplacedAccountApprovedUsers { get; set; }
        public virtual ICollection<ReplacedAccount> ReplacedAccountCreatedUsers { get; set; }
        public virtual ICollection<ReportsLog> ReportsLogs { get; set; }
        public virtual ICollection<RequestsLog1> RequestsLog1s { get; set; }
        public virtual ICollection<RequestsLog2> RequestsLog2s { get; set; }
        public virtual ICollection<RestoredAccount> RestoredAccounts { get; set; }
        public virtual ICollection<ReversePaymentOrder> ReversePaymentOrders { get; set; }
        public virtual ICollection<RevertableCloseOperation> RevertableCloseOperations { get; set; }
        public virtual ICollection<RevertableOperation1> RevertableOperation1s { get; set; }
        public virtual ICollection<RevertableOperation> RevertableOperations { get; set; }
        public virtual ICollection<SecuritiesOperationsStatus> SecuritiesOperationsStatuses { get; set; }
        public virtual ICollection<SecuritiesStatus> SecuritiesStatuses { get; set; }
        public virtual ICollection<SendLog> SendLogs { get; set; }
        public virtual ICollection<Status2> Status2ApproveUsers { get; set; }
        public virtual ICollection<Status2> Status2CreateUsers { get; set; }
        public virtual ICollection<StorageAmount> StorageAmounts { get; set; }
        public virtual ICollection<StorageTransfer> StorageTransfers { get; set; }
        public virtual ICollection<StoredDocument> StoredDocuments { get; set; }
        public virtual ICollection<SubStatusesLimitsForUser> SubStatusesLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsForUser> SubStatusesRequestsLimitsForUsers { get; set; }
        public virtual ICollection<SwiftTransfersOperation> SwiftTransfersOperations { get; set; }
        public virtual ICollection<Transaction2> Transaction2s { get; set; }
        public virtual ICollection<TransferStatus> TransferStatuses { get; set; }
        public virtual ICollection<TransfersOperation> TransfersOperations { get; set; }
        public virtual ICollection<UserInfo> UserInfos { get; set; }
        public virtual ICollection<UserLimit> UserLimits { get; set; }
        public virtual ICollection<UsersExtraOperation> UsersExtraOperations { get; set; }
        public virtual ICollection<UsersLock> UsersLocks { get; set; }
        public virtual ICollection<UsersLog> UsersLogs { get; set; }
        public virtual ICollection<UsersPassword> UsersPasswords { get; set; }
        public virtual ICollection<UsersRole> UsersRoles { get; set; }
        public virtual ICollection<UsersRolesChange> UsersRolesChanges { get; set; }
        public virtual ICollection<UtilitiesEpayRequestsLog> UtilitiesEpayRequestsLogs { get; set; }
        public virtual ICollection<UtilitiesRegistry> UtilitiesRegistries { get; set; }
        public virtual ICollection<UtilitiesTransaction> UtilitiesTransactions { get; set; }
        public virtual ICollection<UtilitiesUmaiRequestsLog> UtilitiesUmaiRequestsLogs { get; set; }
        public virtual ICollection<Verification> Verifications { get; set; }
        public virtual ICollection<XmlReportUserFilter1> XmlReportUserFilter1s { get; set; }
    }
}
