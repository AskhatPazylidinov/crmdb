﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsCurrenciesWithdrawalLimit
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int LimitPeriod { get; set; }
        public int LimitPeriodTypeId { get; set; }
        public int MaxWithdrawalCount { get; set; }
        public decimal MaxWithdrawSumm { get; set; }
        public int MaxWithdrawSummTypeId { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
