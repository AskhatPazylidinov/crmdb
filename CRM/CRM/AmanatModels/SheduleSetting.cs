﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SheduleSetting
    {
        public byte PaymentTypeId { get; set; }
        public int Period { get; set; }
        public int GraseDay { get; set; }
    }
}
