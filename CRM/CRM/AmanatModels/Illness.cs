﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Illness
    {
        public int EmployeeId { get; set; }
        public byte TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DocumentNo { get; set; }
        public string IssueAuthority { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
