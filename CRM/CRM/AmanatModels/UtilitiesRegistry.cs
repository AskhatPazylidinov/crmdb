﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesRegistry
    {
        public UtilitiesRegistry()
        {
            OperationsUtilities = new HashSet<OperationsUtility>();
        }

        public int OperationId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public int ProviderId { get; set; }
        public decimal PaymentSum { get; set; }
        public decimal CommissionSum { get; set; }
        public string Description { get; set; }
        public byte DirectionTypeId { get; set; }
        public DateTime OperationDate { get; set; }
        public bool IsOnlineBanking { get; set; }
        public string CustomerName { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
        public virtual User User { get; set; }
        public virtual ImportedUtilitiesPayment ImportedUtilitiesPayment { get; set; }
        public virtual UtilitiesAlaTv UtilitiesAlaTv { get; set; }
        public virtual UtilitiesBishkekGa UtilitiesBishkekGa { get; set; }
        public virtual UtilitiesBishkekLift UtilitiesBishkekLift { get; set; }
        public virtual UtilitiesBishkekPhone UtilitiesBishkekPhone { get; set; }
        public virtual UtilitiesBishkekTeploEnergo UtilitiesBishkekTeploEnergo { get; set; }
        public virtual UtilitiesBishkekTeploset UtilitiesBishkekTeploset { get; set; }
        public virtual UtilitiesContragentPayment UtilitiesContragentPayment { get; set; }
        public virtual UtilitiesElectroPayment UtilitiesElectroPayment { get; set; }
        public virtual UtilitiesElsomPayment UtilitiesElsomPayment { get; set; }
        public virtual UtilitiesEpayPayment UtilitiesEpayPayment { get; set; }
        public virtual UtilitiesGeopayPayment UtilitiesGeopayPayment { get; set; }
        public virtual UtilitiesKindergartenPayment UtilitiesKindergartenPayment { get; set; }
        public virtual UtilitiesLawCourtPayment UtilitiesLawCourtPayment { get; set; }
        public virtual UtilitiesMegacom UtilitiesMegacom { get; set; }
        public virtual UtilitiesMobilnikPayment UtilitiesMobilnikPayment { get; set; }
        public virtual UtilitiesNotaryPayment UtilitiesNotaryPayment { get; set; }
        public virtual UtilitiesOshTeploset UtilitiesOshTeploset { get; set; }
        public virtual UtilitiesQuickPayPayment UtilitiesQuickPayPayment { get; set; }
        public virtual UtilitiesUmaiPayment UtilitiesUmaiPayment { get; set; }
        public virtual UtilitiesWaterPayment UtilitiesWaterPayment { get; set; }
        public virtual ICollection<OperationsUtility> OperationsUtilities { get; set; }
    }
}
