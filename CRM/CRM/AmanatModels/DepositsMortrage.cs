﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsMortrage
    {
        public int CreditId { get; set; }
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? MortrageSumm { get; set; }

        public virtual History Credit { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
