﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesDoshcardResponseLog
    {
        public long LogId { get; set; }
        public long? PayId { get; set; }
        public DateTime OperationDate { get; set; }
        public bool OperationResult { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
