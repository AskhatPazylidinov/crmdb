﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GroupCycle
    {
        public GroupCycle()
        {
            GroupCycleCredits = new HashSet<GroupCycleCredit>();
        }

        public int Id { get; set; }
        public int CreditGroupCode { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsClose { get; set; }

        public virtual CreditGroup CreditGroupCodeNavigation { get; set; }
        public virtual ICollection<GroupCycleCredit> GroupCycleCredits { get; set; }
    }
}
