﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PlainAccountsTemplate
    {
        public int Id { get; set; }
        public string BalanceGroup { get; set; }
        public byte CurrenciesTypeId { get; set; }
        public string AccountName { get; set; }
        public string AccountGroup { get; set; }

        public virtual AccountGroup AccountGroupNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
