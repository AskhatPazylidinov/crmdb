﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SchedulePaymentRepeat
    {
        public int PaymentId { get; set; }
        public int Repeat { get; set; }

        public virtual ScheduledPayment Payment { get; set; }
    }
}
