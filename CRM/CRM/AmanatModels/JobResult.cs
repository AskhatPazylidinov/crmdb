﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class JobResult
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int FileId { get; set; }
        public string FileName { get; set; }

        public virtual Job Job { get; set; }
    }
}
