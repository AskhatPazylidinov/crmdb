﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountsSourceCurrency
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int SourceCurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Currency3 SourceCurrency { get; set; }
    }
}
