﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersContragentsBranch
    {
        public int ProviderId { get; set; }
        public int ContragentTypeId { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ServiceProvidersContragent ServiceProvidersContragent { get; set; }
    }
}
