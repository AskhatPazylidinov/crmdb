﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountsChange
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string OldAccountNo { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
