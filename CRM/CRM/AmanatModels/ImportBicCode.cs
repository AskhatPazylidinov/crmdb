﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportBicCode
    {
        public int ImportBicCodeId { get; set; }
        public int ImportBicCodeFileId { get; set; }
        public string Tag { get; set; }
        public string ModificationFlag { get; set; }
        public string BicCode { get; set; }
        public string BranchCode { get; set; }
        public string InstitutionName { get; set; }
        public string BranchInformation { get; set; }
        public string CityHeading { get; set; }
        public string SubtypeIndication { get; set; }
        public string ValueAddedServices { get; set; }
        public string ExtraInfo { get; set; }
        public string PhysicalAddress1 { get; set; }
        public string PhysicalAddress2 { get; set; }
        public string PhysicalAddress3 { get; set; }
        public string PhysicalAddress4 { get; set; }
        public string Location { get; set; }
        public string CountryName { get; set; }
        public string PobNumber { get; set; }
        public string PobLocation { get; set; }
        public string PobCountryName { get; set; }
        public string CountryCode { get; set; }
        public string AccountNo { get; set; }

        public virtual ImportBicCodeFile ImportBicCodeFile { get; set; }
    }
}
