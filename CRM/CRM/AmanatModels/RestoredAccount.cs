﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RestoredAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime RestoreDate { get; set; }
        public int? UserId { get; set; }
        public string Comment { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
        public virtual User User { get; set; }
    }
}
