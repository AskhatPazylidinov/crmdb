﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Graphic
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int GraphicId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal MainSumm { get; set; }
        public decimal PercentsSumm { get; set; }
        public decimal DeferredPercentsSumm { get; set; }
        public decimal TotalSumm { get; set; }
        public decimal BaseDeptToPay { get; set; }
        public int DaysInCalculation { get; set; }
        public decimal SalesTaxOnPercentsSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
