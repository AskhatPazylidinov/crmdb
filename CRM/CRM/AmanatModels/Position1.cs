﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Position1
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int? DepId { get; set; }
        public int? OfficeId { get; set; }
        public int? SchedId { get; set; }
        public string Comment { get; set; }
        public string DepFullName { get; set; }
        public DateTime? OrderDate { get; set; }
        public string OrderText { get; set; }
        public string OrderNo { get; set; }
        public int? StatusId { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? Srochniy { get; set; }
        public string Pdffile { get; set; }
        public bool? Trial { get; set; }
        public DateTime? TrialEndDate { get; set; }
        public bool? Temp { get; set; }
        public DateTime? TempEndDate { get; set; }
        public decimal? TempSalaryPercent { get; set; }
        public decimal? TempSalarySumm { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? StatusUserId { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual Schedule1 Sched { get; set; }
    }
}
