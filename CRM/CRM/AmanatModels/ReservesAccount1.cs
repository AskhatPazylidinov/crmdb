﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesAccount1
    {
        public int BranchId { get; set; }
        public bool IsFinancialCompany { get; set; }
        public byte ShortReserveTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string ReserveAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public int AccountId { get; set; }
    }
}
