﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StorageTransfer
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal Amount { get; set; }
        public int CashierId { get; set; }
        public bool IsBagIncome { get; set; }
        public DateTime TransferDate { get; set; }
        public int IsNightWeekendCash { get; set; }
        public int OfficeId { get; set; }

        public virtual User Cashier { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
