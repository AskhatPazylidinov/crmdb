﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesDeclension
    {
        public int OfficeId { get; set; }
        public byte DeclensionTypeId { get; set; }
        public string ManagerName { get; set; }
        public string ManagerPosition { get; set; }
        public string AdminDepartmentPosition { get; set; }
        public string AdminDepartmentManager { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
