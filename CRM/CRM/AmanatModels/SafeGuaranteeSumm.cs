﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafeGuaranteeSumm
    {
        public DateTime ChangeDate { get; set; }
        public int SafeTypeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public decimal GuaranteeSumm { get; set; }

        public virtual SafeTariff SafeTariff { get; set; }
    }
}
