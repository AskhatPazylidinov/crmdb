﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GuaranteeIssueComission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int MortgageTypeId { get; set; }
        public byte MinPeriod { get; set; }
        public byte? MaxPeriod { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }

        public virtual MortrageType MortgageType { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
