﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceChange
    {
        public InsuranceChange()
        {
            InsuranceGraphics = new HashSet<InsuranceGraphic>();
            InsuranceInterests = new HashSet<InsuranceInterest>();
        }

        public int InsuranceId { get; set; }
        public DateTime ChangeDate { get; set; }
        public decimal SalesTax { get; set; }

        public virtual InsuranceCompany Insurance { get; set; }
        public virtual ICollection<InsuranceGraphic> InsuranceGraphics { get; set; }
        public virtual ICollection<InsuranceInterest> InsuranceInterests { get; set; }
    }
}
