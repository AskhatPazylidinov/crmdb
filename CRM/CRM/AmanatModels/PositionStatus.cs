﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PositionStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
