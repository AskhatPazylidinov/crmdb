﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersBranch
    {
        public int ProviderId { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
