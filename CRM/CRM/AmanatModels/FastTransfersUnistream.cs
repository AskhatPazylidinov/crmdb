﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FastTransfersUnistream
    {
        public int Id { get; set; }
        public int FastTransferId { get; set; }
        public int? UnistreamId { get; set; }
        public int? ReceiverPos { get; set; }
        public int? SenderPos { get; set; }
        public string PartnerLastname { get; set; }
        public string PartnerFirstname { get; set; }
        public string PartnerMiddlename { get; set; }
        public string UnistreamUsername { get; set; }
        public string UnistreamPassword { get; set; }
        public string UnistreamAppkey { get; set; }
        public int? PayCurrencyId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public string OldPartnerLastname { get; set; }
        public string OldPartnerFirstname { get; set; }
        public string OldPartnerMiddlename { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime? OldBirthDate { get; set; }
        public string OldPhone { get; set; }
        public string OldAddress { get; set; }
    }
}
