﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class View1
    {
        public string Bic { get; set; }
        public string Adres { get; set; }
        public string InstitutionName { get; set; }
        public string BranchInformation { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
