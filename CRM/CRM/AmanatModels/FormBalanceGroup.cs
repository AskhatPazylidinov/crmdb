﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FormBalanceGroup
    {
        public int? Id { get; set; }
        public string FormNum { get; set; }
        public string BalanceGroup { get; set; }

        public virtual Form Form { get; set; }
    }
}
