﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CodeWordChangeLog
    {
        public int LogId { get; set; }
        public int CardId { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int UserId { get; set; }
        public DateTime ChangeDate { get; set; }

        public virtual Card Card { get; set; }
    }
}
