﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetSubGroup
    {
        public int SubGroupId { get; set; }
        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual AssetGroup Group { get; set; }
    }
}
