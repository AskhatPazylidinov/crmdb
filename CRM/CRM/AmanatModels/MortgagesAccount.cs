﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MortgagesAccount
    {
        public int MortgageId { get; set; }
        public byte TypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Mortgage Mortgage { get; set; }
    }
}
