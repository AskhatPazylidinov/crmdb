﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalReserf
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int TypeId { get; set; }
        public int? ReserveTypeId { get; set; }
        public string ReserveReason { get; set; }
        public decimal? ReserveRate { get; set; }
        public int OverdueDays { get; set; }
        public int? RestructuringCount { get; set; }
        public int? ParallelOverdueDays { get; set; }
        public int? ParallelLinkedOverdueDays { get; set; }
        public int? ParallelOverdueDaysInOtherFcu { get; set; }
        public int? ParallelLinkedOverdueDaysInOtherFcu { get; set; }
        public bool? DepositMortgage { get; set; }
        public decimal? IncomeStructurePercent { get; set; }
        public int? ImpairSymptom { get; set; }
        public int? CustomerId { get; set; }
        public bool? NoGuarantee { get; set; }
        public int? GaranteesImpairSymptom { get; set; }
        public bool? Dissolution { get; set; }
        public bool? NoGuarantor { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
