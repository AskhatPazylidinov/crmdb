﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class WorkInterval
    {
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public string StartOrderNo { get; set; }
        public DateTime? EndDate { get; set; }
        public string EndOrderNo { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
