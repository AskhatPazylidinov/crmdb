﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserLimit
    {
        public int LimitId { get; set; }
        public int UserId { get; set; }
        public int ModuleId { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinSum { get; set; }
        public decimal? MaxSum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool AllCurrency { get; set; }
        public int? ConfirmUserId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProgramModule Module { get; set; }
        public virtual User User { get; set; }
    }
}
