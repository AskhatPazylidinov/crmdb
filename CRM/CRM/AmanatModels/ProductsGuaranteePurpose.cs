﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsGuaranteePurpose
    {
        public int ProductId { get; set; }
        public int PurposeTypeId { get; set; }

        public virtual Product1 Product { get; set; }
        public virtual GuaranteePurpose PurposeType { get; set; }
    }
}
