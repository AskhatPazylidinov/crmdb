﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsActivityType
    {
        public int ActivityTypeId { get; set; }
        public int CibactivityTypeId { get; set; }

        public virtual ActivityType ActivityType { get; set; }
    }
}
