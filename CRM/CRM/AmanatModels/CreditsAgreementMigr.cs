﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsAgreementMigr
    {
        public string AgreementNo { get; set; }
        public int UniqueId { get; set; }
    }
}
