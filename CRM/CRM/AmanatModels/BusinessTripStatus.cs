﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BusinessTripStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
