﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvider
    {
        public ServiceProvider()
        {
            DeletedUtilitiesRegistries = new HashSet<DeletedUtilitiesRegistry>();
            InfoSystemTransitAccountsForCommissions = new HashSet<InfoSystemTransitAccountsForCommission>();
            ProvidersComissionsPartitions = new HashSet<ProvidersComissionsPartition>();
            ProvidersFixedCommissionsPartitions = new HashSet<ProvidersFixedCommissionsPartition>();
            ProvidersIncomeAccounts = new HashSet<ProvidersIncomeAccount>();
            ProvidersTransitAccounts = new HashSet<ProvidersTransitAccount>();
            ServiceProvidersBranches = new HashSet<ServiceProvidersBranch>();
            ServiceProvidersComissions = new HashSet<ServiceProvidersComission>();
            ServiceProvidersContacts = new HashSet<ServiceProvidersContact>();
            ServiceProvidersContragents = new HashSet<ServiceProvidersContragent>();
            ServiceProvidersInternalAccounts = new HashSet<ServiceProvidersInternalAccount>();
            UtilitiesBaseElectroDictionaryFilenames = new HashSet<UtilitiesBaseElectroDictionaryFilename>();
            UtilitiesBaseElectroProviderDepartments = new HashSet<UtilitiesBaseElectroProviderDepartment>();
            UtilitiesImportFiles = new HashSet<UtilitiesImportFile>();
            UtilitiesRegistries = new HashSet<UtilitiesRegistry>();
            UtilitiesTransactions = new HashSet<UtilitiesTransaction>();
            UtilitiesWaterProviderDepartments = new HashSet<UtilitiesWaterProviderDepartment>();
        }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public int CustomerId { get; set; }
        public string PaymentCode { get; set; }
        public string ExternalBikcode { get; set; }
        public string ExternalAccountNo { get; set; }
        public byte ProviderTypeId { get; set; }
        public byte ComissionPaymentTypeId { get; set; }
        public string ProcessorAssemblyName { get; set; }
        public string LogoFileName { get; set; }
        public int? CategoryId { get; set; }
        public byte? IncomeCashSymbol { get; set; }
        public byte? WithdrawCashSymbol { get; set; }

        public virtual UtilitiesCategory Category { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ServiceProvidersExternalSetting ServiceProvidersExternalSetting { get; set; }
        public virtual ICollection<DeletedUtilitiesRegistry> DeletedUtilitiesRegistries { get; set; }
        public virtual ICollection<InfoSystemTransitAccountsForCommission> InfoSystemTransitAccountsForCommissions { get; set; }
        public virtual ICollection<ProvidersComissionsPartition> ProvidersComissionsPartitions { get; set; }
        public virtual ICollection<ProvidersFixedCommissionsPartition> ProvidersFixedCommissionsPartitions { get; set; }
        public virtual ICollection<ProvidersIncomeAccount> ProvidersIncomeAccounts { get; set; }
        public virtual ICollection<ProvidersTransitAccount> ProvidersTransitAccounts { get; set; }
        public virtual ICollection<ServiceProvidersBranch> ServiceProvidersBranches { get; set; }
        public virtual ICollection<ServiceProvidersComission> ServiceProvidersComissions { get; set; }
        public virtual ICollection<ServiceProvidersContact> ServiceProvidersContacts { get; set; }
        public virtual ICollection<ServiceProvidersContragent> ServiceProvidersContragents { get; set; }
        public virtual ICollection<ServiceProvidersInternalAccount> ServiceProvidersInternalAccounts { get; set; }
        public virtual ICollection<UtilitiesBaseElectroDictionaryFilename> UtilitiesBaseElectroDictionaryFilenames { get; set; }
        public virtual ICollection<UtilitiesBaseElectroProviderDepartment> UtilitiesBaseElectroProviderDepartments { get; set; }
        public virtual ICollection<UtilitiesImportFile> UtilitiesImportFiles { get; set; }
        public virtual ICollection<UtilitiesRegistry> UtilitiesRegistries { get; set; }
        public virtual ICollection<UtilitiesTransaction> UtilitiesTransactions { get; set; }
        public virtual ICollection<UtilitiesWaterProviderDepartment> UtilitiesWaterProviderDepartments { get; set; }
    }
}
