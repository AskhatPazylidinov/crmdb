﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationTypeComment
    {
        public int OperationTypeId { get; set; }
        public byte FileType { get; set; }
        public string Comment { get; set; }
        public string ComissionComment { get; set; }
        public string ExchangeComissionComment { get; set; }
        public bool CardTypeName { get; set; }
        public bool IssuerType { get; set; }
        public bool Destination { get; set; }
        public bool AccountNo { get; set; }
        public bool CardNo { get; set; }

        public virtual OperationType OperationType { get; set; }
    }
}
