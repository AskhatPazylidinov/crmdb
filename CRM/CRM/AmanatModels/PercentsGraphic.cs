﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PercentsGraphic
    {
        public int SecuritiesId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal PercentPaySumm { get; set; }
    }
}
