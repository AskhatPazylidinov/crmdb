﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekTeplosetSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
    }
}
