﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Institution
    {
        public Institution()
        {
            Educations = new HashSet<Education>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Country { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Education> Educations { get; set; }
    }
}
