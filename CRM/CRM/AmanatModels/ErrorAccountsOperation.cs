﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ErrorAccountsOperation
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public string ErrorMessage { get; set; }
    }
}
