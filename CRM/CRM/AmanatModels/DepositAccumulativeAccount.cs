﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositAccumulativeAccount
    {
        public string AccountNo { get; set; }
        public string BalanceGroup { get; set; }
        public int? ClientTypeId { get; set; }
        public int? BranchId { get; set; }
        public int? CurrencyId { get; set; }
        public int Id { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
