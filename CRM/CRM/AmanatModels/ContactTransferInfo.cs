﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ContactTransferInfo
    {
        public int Id { get; set; }
        public string ContactIdentity { get; set; }
        public DateTime TransactionDate { get; set; }
        public string PickupPointCode { get; set; }
        public long TransferId { get; set; }
        public string CardNumber { get; set; }

        public virtual MoneyTransfer Transfer { get; set; }
    }
}
