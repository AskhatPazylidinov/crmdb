﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesLoansCoveringPercentage
    {
        public int AuditResultTypeId { get; set; }
        public int CoveringPercentage { get; set; }
    }
}
