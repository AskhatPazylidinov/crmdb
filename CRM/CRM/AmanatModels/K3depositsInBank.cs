﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class K3depositsInBank
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string CorrBankCode { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual SwiftCorrBank CorrBankCodeNavigation { get; set; }
    }
}
