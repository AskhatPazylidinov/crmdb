﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Office1
    {
        public Office1()
        {
            Account1s = new HashSet<Account1>();
            AccountGroupBalances = new HashSet<AccountGroupBalance>();
            AllowedDepositsWithdrawOffices = new HashSet<AllowedDepositsWithdrawOffice>();
            Assets = new HashSet<Asset>();
            CardsRegistries = new HashSet<CardsRegistry>();
            CashOfficesLimits = new HashSet<CashOfficesLimit>();
            CreditGroups = new HashSet<CreditGroup>();
            DeletedTransactions = new HashSet<DeletedTransaction>();
            DeletedUtilitiesRegistries = new HashSet<DeletedUtilitiesRegistry>();
            Department2s = new HashSet<Department2>();
            EditApprovedParamsLimitsForUsers = new HashSet<EditApprovedParamsLimitsForUser>();
            EditApprovedParamsLimitsOnRoles = new HashSet<EditApprovedParamsLimitsOnRole>();
            EmployeesOffices = new HashSet<EmployeesOffice>();
            Garantees = new HashSet<Garantee>();
            Histories = new HashSet<History>();
            ImportTransactionsFiles = new HashSet<ImportTransactionsFile>();
            ImportedTaxPayments = new HashSet<ImportedTaxPayment>();
            IncomeAccounts = new HashSet<IncomeAccount>();
            InsurancesIncomesAccounts = new HashSet<InsurancesIncomesAccount>();
            InterBranchTransfersIncomeAccounts = new HashSet<InterBranchTransfersIncomeAccount>();
            IssueLoanLimitsForUsers = new HashSet<IssueLoanLimitsForUser>();
            IssueLoanLimitsOnRoles = new HashSet<IssueLoanLimitsOnRole>();
            Limits = new HashSet<Limit>();
            MarketRates = new HashSet<MarketRate>();
            MoneyTransfersAccounts = new HashSet<MoneyTransfersAccount>();
            MoneyTransfersStatuses = new HashSet<MoneyTransfersStatus>();
            NettingOperations = new HashSet<NettingOperation>();
            NightCashierOffices = new HashSet<NightCashierOffice>();
            NightCashiersOperationDates = new HashSet<NightCashiersOperationDate>();
            OfficesAllowances = new HashSet<OfficesAllowance>();
            OfficesCloseDays = new HashSet<OfficesCloseDay>();
            OfficesDeclensions = new HashSet<OfficesDeclension>();
            OfficesExchangeRates = new HashSet<OfficesExchangeRate>();
            OfficesExchangeRatesHistories = new HashSet<OfficesExchangeRatesHistory>();
            OfficesFlowOperations = new HashSet<OfficesFlowOperation>();
            OfficesMinimalTaxesBases = new HashSet<OfficesMinimalTaxesBasis>();
            OfficesPlannedIncomes = new HashSet<OfficesPlannedIncome>();
            OfficesPlannedIssuings = new HashSet<OfficesPlannedIssuing>();
            OnlineOperatorsWithdrawTariffs = new HashSet<OnlineOperatorsWithdrawTariff>();
            Operation2s = new HashSet<Operation2>();
            OperationalAccountGroupBalances = new HashSet<OperationalAccountGroupBalance>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
            PaymentsOperations = new HashSet<PaymentsOperation>();
            Paysheets = new HashSet<Paysheet>();
            PlanningInFlows = new HashSet<PlanningInFlow>();
            ProvidersIncomeAccounts = new HashSet<ProvidersIncomeAccount>();
            ReceiverChangeLogs = new HashSet<ReceiverChangeLog>();
            ReplenishmentTariffs = new HashSet<ReplenishmentTariff>();
            Schedules = new HashSet<Schedule>();
            ServiceProvidersComissions = new HashSet<ServiceProvidersComission>();
            ServiceProvidersContragentsComissions = new HashSet<ServiceProvidersContragentsComission>();
            SocialFondDepartmentsIncomeAccounts = new HashSet<SocialFondDepartmentsIncomeAccount>();
            SpecialAccount1s = new HashSet<SpecialAccount1>();
            SpecialAccounts = new HashSet<SpecialAccount>();
            SpecialCashierTypeAccounts = new HashSet<SpecialCashierTypeAccount>();
            SpecializedAccount1s = new HashSet<SpecializedAccount1>();
            SpecializedAccounts = new HashSet<SpecializedAccount>();
            StandardWithdrawTariffs = new HashSet<StandardWithdrawTariff>();
            StorageAmounts = new HashSet<StorageAmount>();
            SubStatusesLimitsForUsers = new HashSet<SubStatusesLimitsForUser>();
            SubStatusesLimitsOnRoles = new HashSet<SubStatusesLimitsOnRole>();
            SubStatusesRequestsLimitsForUsers = new HashSet<SubStatusesRequestsLimitsForUser>();
            SubStatusesRequestsLimitsOnRoles = new HashSet<SubStatusesRequestsLimitsOnRole>();
            SwiftAccounts = new HashSet<SwiftAccount>();
            SwiftTransfersOperations = new HashSet<SwiftTransfersOperation>();
            Tariffs = new HashSet<Tariff>();
            TaxDepartmentsIncomeAccounts = new HashSet<TaxDepartmentsIncomeAccount>();
            TaxesOnNonResidentsIncomes = new HashSet<TaxesOnNonResidentsIncome>();
            Transaction2s = new HashSet<Transaction2>();
            TransactionTemplateOperations = new HashSet<TransactionTemplateOperation>();
            TransactionTemplates = new HashSet<TransactionTemplate>();
            TransferOfficeNewOffices = new HashSet<TransferOffice>();
            TransferStatuses = new HashSet<TransferStatus>();
            TransfersOperations = new HashSet<TransfersOperation>();
            Users = new HashSet<User>();
            UtilitiesImportFiles = new HashSet<UtilitiesImportFile>();
            UtilitiesRegistries = new HashSet<UtilitiesRegistry>();
            UtilitiesTransactions = new HashSet<UtilitiesTransaction>();
        }

        public int Id { get; set; }
        public int BranchId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string ManagerName { get; set; }
        public string ManagerPosition { get; set; }
        public string AttorneyNo { get; set; }
        public DateTime AttorneyIssueDate { get; set; }
        public bool IsMainOffice { get; set; }
        public string AdminDepartmentPosition { get; set; }
        public string AdminDepartmentManager { get; set; }
        public string ShortName { get; set; }
        public int CityId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual DepositsAgreementNo DepositsAgreementNo { get; set; }
        public virtual OfficeId OfficeId { get; set; }
        public virtual OfficesContactPointCode OfficesContactPointCode { get; set; }
        public virtual OfficesUnistreamPointCode OfficesUnistreamPointCode { get; set; }
        public virtual RecipientsExceedingLimit RecipientsExceedingLimit { get; set; }
        public virtual SettingsOfficesRegion SettingsOfficesRegion { get; set; }
        public virtual TransferOffice TransferOfficeOldOffice { get; set; }
        public virtual ICollection<Account1> Account1s { get; set; }
        public virtual ICollection<AccountGroupBalance> AccountGroupBalances { get; set; }
        public virtual ICollection<AllowedDepositsWithdrawOffice> AllowedDepositsWithdrawOffices { get; set; }
        public virtual ICollection<Asset> Assets { get; set; }
        public virtual ICollection<CardsRegistry> CardsRegistries { get; set; }
        public virtual ICollection<CashOfficesLimit> CashOfficesLimits { get; set; }
        public virtual ICollection<CreditGroup> CreditGroups { get; set; }
        public virtual ICollection<DeletedTransaction> DeletedTransactions { get; set; }
        public virtual ICollection<DeletedUtilitiesRegistry> DeletedUtilitiesRegistries { get; set; }
        public virtual ICollection<Department2> Department2s { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsForUser> EditApprovedParamsLimitsForUsers { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsOnRole> EditApprovedParamsLimitsOnRoles { get; set; }
        public virtual ICollection<EmployeesOffice> EmployeesOffices { get; set; }
        public virtual ICollection<Garantee> Garantees { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<ImportTransactionsFile> ImportTransactionsFiles { get; set; }
        public virtual ICollection<ImportedTaxPayment> ImportedTaxPayments { get; set; }
        public virtual ICollection<IncomeAccount> IncomeAccounts { get; set; }
        public virtual ICollection<InsurancesIncomesAccount> InsurancesIncomesAccounts { get; set; }
        public virtual ICollection<InterBranchTransfersIncomeAccount> InterBranchTransfersIncomeAccounts { get; set; }
        public virtual ICollection<IssueLoanLimitsForUser> IssueLoanLimitsForUsers { get; set; }
        public virtual ICollection<IssueLoanLimitsOnRole> IssueLoanLimitsOnRoles { get; set; }
        public virtual ICollection<Limit> Limits { get; set; }
        public virtual ICollection<MarketRate> MarketRates { get; set; }
        public virtual ICollection<MoneyTransfersAccount> MoneyTransfersAccounts { get; set; }
        public virtual ICollection<MoneyTransfersStatus> MoneyTransfersStatuses { get; set; }
        public virtual ICollection<NettingOperation> NettingOperations { get; set; }
        public virtual ICollection<NightCashierOffice> NightCashierOffices { get; set; }
        public virtual ICollection<NightCashiersOperationDate> NightCashiersOperationDates { get; set; }
        public virtual ICollection<OfficesAllowance> OfficesAllowances { get; set; }
        public virtual ICollection<OfficesCloseDay> OfficesCloseDays { get; set; }
        public virtual ICollection<OfficesDeclension> OfficesDeclensions { get; set; }
        public virtual ICollection<OfficesExchangeRate> OfficesExchangeRates { get; set; }
        public virtual ICollection<OfficesExchangeRatesHistory> OfficesExchangeRatesHistories { get; set; }
        public virtual ICollection<OfficesFlowOperation> OfficesFlowOperations { get; set; }
        public virtual ICollection<OfficesMinimalTaxesBasis> OfficesMinimalTaxesBases { get; set; }
        public virtual ICollection<OfficesPlannedIncome> OfficesPlannedIncomes { get; set; }
        public virtual ICollection<OfficesPlannedIssuing> OfficesPlannedIssuings { get; set; }
        public virtual ICollection<OnlineOperatorsWithdrawTariff> OnlineOperatorsWithdrawTariffs { get; set; }
        public virtual ICollection<Operation2> Operation2s { get; set; }
        public virtual ICollection<OperationalAccountGroupBalance> OperationalAccountGroupBalances { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
        public virtual ICollection<PaymentsOperation> PaymentsOperations { get; set; }
        public virtual ICollection<Paysheet> Paysheets { get; set; }
        public virtual ICollection<PlanningInFlow> PlanningInFlows { get; set; }
        public virtual ICollection<ProvidersIncomeAccount> ProvidersIncomeAccounts { get; set; }
        public virtual ICollection<ReceiverChangeLog> ReceiverChangeLogs { get; set; }
        public virtual ICollection<ReplenishmentTariff> ReplenishmentTariffs { get; set; }
        public virtual ICollection<Schedule> Schedules { get; set; }
        public virtual ICollection<ServiceProvidersComission> ServiceProvidersComissions { get; set; }
        public virtual ICollection<ServiceProvidersContragentsComission> ServiceProvidersContragentsComissions { get; set; }
        public virtual ICollection<SocialFondDepartmentsIncomeAccount> SocialFondDepartmentsIncomeAccounts { get; set; }
        public virtual ICollection<SpecialAccount1> SpecialAccount1s { get; set; }
        public virtual ICollection<SpecialAccount> SpecialAccounts { get; set; }
        public virtual ICollection<SpecialCashierTypeAccount> SpecialCashierTypeAccounts { get; set; }
        public virtual ICollection<SpecializedAccount1> SpecializedAccount1s { get; set; }
        public virtual ICollection<SpecializedAccount> SpecializedAccounts { get; set; }
        public virtual ICollection<StandardWithdrawTariff> StandardWithdrawTariffs { get; set; }
        public virtual ICollection<StorageAmount> StorageAmounts { get; set; }
        public virtual ICollection<SubStatusesLimitsForUser> SubStatusesLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesLimitsOnRole> SubStatusesLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsForUser> SubStatusesRequestsLimitsForUsers { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsOnRole> SubStatusesRequestsLimitsOnRoles { get; set; }
        public virtual ICollection<SwiftAccount> SwiftAccounts { get; set; }
        public virtual ICollection<SwiftTransfersOperation> SwiftTransfersOperations { get; set; }
        public virtual ICollection<Tariff> Tariffs { get; set; }
        public virtual ICollection<TaxDepartmentsIncomeAccount> TaxDepartmentsIncomeAccounts { get; set; }
        public virtual ICollection<TaxesOnNonResidentsIncome> TaxesOnNonResidentsIncomes { get; set; }
        public virtual ICollection<Transaction2> Transaction2s { get; set; }
        public virtual ICollection<TransactionTemplateOperation> TransactionTemplateOperations { get; set; }
        public virtual ICollection<TransactionTemplate> TransactionTemplates { get; set; }
        public virtual ICollection<TransferOffice> TransferOfficeNewOffices { get; set; }
        public virtual ICollection<TransferStatus> TransferStatuses { get; set; }
        public virtual ICollection<TransfersOperation> TransfersOperations { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UtilitiesImportFile> UtilitiesImportFiles { get; set; }
        public virtual ICollection<UtilitiesRegistry> UtilitiesRegistries { get; set; }
        public virtual ICollection<UtilitiesTransaction> UtilitiesTransactions { get; set; }
    }
}
