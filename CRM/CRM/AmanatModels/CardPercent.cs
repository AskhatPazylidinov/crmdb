﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardPercent
    {
        public string AccountNo { get; set; }
        public decimal SumV { get; set; }
    }
}
