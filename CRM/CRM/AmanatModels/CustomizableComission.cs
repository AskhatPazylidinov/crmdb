﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomizableComission
    {
        public CustomizableComission()
        {
            CustomizableComissionsAccounts = new HashSet<CustomizableComissionsAccount>();
            CustomizableComissionsSubjects = new HashSet<CustomizableComissionsSubject>();
            ProductsCustomizableComissions = new HashSet<ProductsCustomizableComission>();
        }

        public int ComissionId { get; set; }
        public string ComissionName { get; set; }

        public virtual ICollection<CustomizableComissionsAccount> CustomizableComissionsAccounts { get; set; }
        public virtual ICollection<CustomizableComissionsSubject> CustomizableComissionsSubjects { get; set; }
        public virtual ICollection<ProductsCustomizableComission> ProductsCustomizableComissions { get; set; }
    }
}
