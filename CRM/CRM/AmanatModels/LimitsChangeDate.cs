﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitsChangeDate
    {
        public LimitsChangeDate()
        {
            Limit1s = new HashSet<Limit1>();
        }

        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }

        public virtual ICollection<Limit1> Limit1s { get; set; }
    }
}
