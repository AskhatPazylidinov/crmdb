﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EducationType3
    {
        public int EducationType { get; set; }
        public int EducationTypeKib { get; set; }

        public virtual EducationType2 EducationTypeKibNavigation { get; set; }
        public virtual EducationType EducationTypeNavigation { get; set; }
    }
}
