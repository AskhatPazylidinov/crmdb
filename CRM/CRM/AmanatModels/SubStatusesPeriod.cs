﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SubStatusesPeriod
    {
        public int ProductId { get; set; }
        public int SubStatusId { get; set; }
        public int Period { get; set; }

        public virtual Product1 Product { get; set; }
        public virtual SubStatusesType SubStatus { get; set; }
    }
}
