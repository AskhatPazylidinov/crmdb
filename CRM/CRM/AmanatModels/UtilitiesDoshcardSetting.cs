﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesDoshcardSetting
    {
        public int Id { get; set; }
        public string DoshcardAddress { get; set; }
        public bool? UseProxy { get; set; }
        public string ProxyAddress { get; set; }
        public int? ProxyPort { get; set; }
        public bool? UseProxyAuthentication { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }
    }
}
