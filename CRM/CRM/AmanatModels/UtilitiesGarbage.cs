﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesGarbage
    {
        public int OperationId { get; set; }
        public string PersonalAccountNo { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public decimal TotalSumm { get; set; }
    }
}
