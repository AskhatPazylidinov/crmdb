﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCurrency
    {
        public ProductsCurrency()
        {
            ProductsCurrenciesReplenishmentLimits = new HashSet<ProductsCurrenciesReplenishmentLimit>();
            ProductsCurrenciesWithdrawTariffs = new HashSet<ProductsCurrenciesWithdrawTariff>();
            ProductsCurrenciesWithdrawalLimits = new HashSet<ProductsCurrenciesWithdrawalLimit>();
            ProductsRatesOnBalances = new HashSet<ProductsRatesOnBalance>();
        }

        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal? MinDepositSumm { get; set; }
        public decimal? MaxDepositSumm { get; set; }
        public decimal MinSummOnAccount { get; set; }
        public bool? IsActive { get; set; }
        public decimal? MinOpenSumm { get; set; }
        public decimal? MaxOpenSumm { get; set; }
        public decimal? IndividualRateMaximumDifference { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProductsChange1 ProductsChange1 { get; set; }
        public virtual ICollection<ProductsCurrenciesReplenishmentLimit> ProductsCurrenciesReplenishmentLimits { get; set; }
        public virtual ICollection<ProductsCurrenciesWithdrawTariff> ProductsCurrenciesWithdrawTariffs { get; set; }
        public virtual ICollection<ProductsCurrenciesWithdrawalLimit> ProductsCurrenciesWithdrawalLimits { get; set; }
        public virtual ICollection<ProductsRatesOnBalance> ProductsRatesOnBalances { get; set; }
    }
}
