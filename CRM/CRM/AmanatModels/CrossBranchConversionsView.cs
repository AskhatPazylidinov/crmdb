﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBranchConversionsView
    {
        public int OperationId { get; set; }
        public DateTime ConversionDate { get; set; }
        public int BranchId { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal BuySumm { get; set; }
        public int BuyCurrencyId { get; set; }
        public decimal SellSumm { get; set; }
        public int SellCurrencyId { get; set; }
        public int? ActualStatusId { get; set; }
        public int? UserId { get; set; }
        public DateTime? OperationDate { get; set; }
    }
}
