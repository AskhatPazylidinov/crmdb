﻿using CRM.CrmModels;
using CRM.ScriptOptions;
using Microsoft.EntityFrameworkCore;
namespace CRM.AmanatModels
{
    public partial class CrmDbContext : DbContext
    {
        public CrmDbContext() { }

        public CrmDbContext(DbContextOptions<CrmDbContext> options)
            : base(options) { }

        public virtual DbSet<UpdateDatabaseSchedule> UpdateDatabaseSchedule { get; set; }
        public virtual DbSet<HistoryStopCalculated> HistoriesStopCalculationsDates { get; set; }

        public virtual DbSet<Documents> Documents { get; set; }

        public virtual DbSet<Guarantor> Guarantors { get; set; }

        public virtual DbSet<AddressAttribute> AddressAttributes { get; set; }

        public virtual DbSet<ClientAttribute> ClientAttributes { get; set; }

        public virtual DbSet<EmailAttribute> EmailAttributes { get; set; }

        public virtual DbSet<LinkGuarantorAttribute> LinkGuarantorAttributes { get; set; }

        public virtual DbSet<LinkPledgerAttribute> LinkPledgerAttributes { get; set; }

        public virtual DbSet<LinkRelativeAttribute> LinkRelativeAttributes { get; set; }

        public virtual DbSet<LinkType> LinkTypes { get; set; }

        public virtual DbSet<LoanAttribute> LoanAttributes { get; set; }

        public virtual DbSet<PaymentsAttribute> PaymentsAttributes { get; set; }

        public virtual DbSet<PhoneAttribute> PhoneAttributes { get; set; }

        public virtual DbSet<SheduleOfPlanesRepayment> SheduleOfPlanesRepayments { get; set; }

        public virtual DbSet<GarantsFieldsValue> GarantsFieldsValue { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(Options.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");
            
            modelBuilder.Entity<UpdateDatabaseSchedule>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.DateTime
                });

                entity.Property(e => e.DateTime).HasColumnName("DateTime");

                entity.Property(e => e.Message).HasColumnName("Message");
            });

            modelBuilder.Entity<HistoryStopCalculated>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CreditId,
                    e.StopType,
                    e.StartDate
                });

                entity.Property(e => e.CreditId).HasColumnName("CreditId");

                entity.Property(e => e.StartDate).HasColumnName("StartDate");

                entity.Property(e => e.EndDate).HasColumnName("EndDate");

                entity.Property(e => e.Description).HasColumnName("Description");

                entity.Property(e => e.StopType).HasMaxLength(4000);
            });

            modelBuilder.Entity<GarantsFieldsValue>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.FieldId,
                    e.GaranteeId
                });

                entity.Property(e => e.GaranteeId).HasColumnName("GaranteeId");

                entity.Property(e => e.FieldId).HasColumnName("FieldId");

                entity.Property(e => e.ValueBoolean).HasColumnName("ValueBoolean");

                entity.Property(e => e.ValueInteger).HasColumnName("ValueInteger");

                entity.Property(e => e.ValueDecimal).HasColumnName("ValueDecimal");

                entity.Property(e => e.ValueString).HasMaxLength(4000);
            });

            modelBuilder.Entity<Guarantor>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CreditID,
                    e.CustomerID
                });

                entity.Property(e => e.CreditID).HasColumnName("CreditID");

                entity.Property(e => e.CustomerID).HasColumnName("CustomerID");

                entity.Property(e => e.StartDate).HasColumnName("StartDate");

                entity.Property(e => e.EndDate).HasColumnName("EndDate");
            });

            modelBuilder.Entity<AddressAttribute>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.RegistrationCityName).HasMaxLength(150);

                entity.Property(e => e.RegistrationCountryId).HasColumnName("RegistrationCountryID");

                entity.Property(e => e.RegistrationFlat).HasMaxLength(10);

                entity.Property(e => e.RegistrationHouse).HasMaxLength(10);

                entity.Property(e => e.RegistrationPostalCode).HasMaxLength(50);

                entity.Property(e => e.RegistrationStreet).HasMaxLength(300);

                entity.Property(e => e.ResidenceCityName).HasMaxLength(150);

                entity.Property(e => e.ResidenceCountryId).HasColumnName("ResidenceCountryID");

                entity.Property(e => e.ResidenceFlat).HasMaxLength(10);

                entity.Property(e => e.ResidenceHouse).HasMaxLength(10);

                entity.Property(e => e.ResidencePostalCode).HasMaxLength(50);

                entity.Property(e => e.ResidenceStreet).HasMaxLength(300);
            });

            modelBuilder.Entity<ClientAttribute>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("datetime");

                entity.Property(e => e.DocumentNo).HasMaxLength(20);

                entity.Property(e => e.DocumentSeries).HasMaxLength(20);

                entity.Property(e => e.IdentificationNumber).HasMaxLength(20);

                entity.Property(e => e.IssueAuthority).HasMaxLength(150);

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.Otchestvo).HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);

                entity.Property(e => e.WorkName).HasMaxLength(130);

                entity.Property(e => e.WorkPosition).HasMaxLength(50);
            });

            modelBuilder.Entity<EmailAttribute>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Email).HasMaxLength(30);
            });

            modelBuilder.Entity<Documents>(entity =>
            {
                entity.HasKey(e => e.ID);

                entity.Property(e => e.CustomerID).HasColumnName("CustomerID");

                entity.Property(e => e.FileName).HasMaxLength(500);

                entity.Property(e => e.DATA).HasColumnName("DATA");

                entity.Property(e => e.Type).HasColumnName("Type");
            });

            modelBuilder.Entity<LinkGuarantorAttribute>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CustomerId,
                    e.CreditId,
                    e.GaranteeID,
                    e.GarantTypeId
                });

                entity.Property(e => e.CreditId).HasColumnName("CreditID");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.GaranteeID).HasColumnName("GaranteeID");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DocumentNo).HasMaxLength(50);

                entity.Property(e => e.DocumentSeries).HasMaxLength(50);

                entity.Property(e => e.GarantTypeId).HasColumnName("GarantTypeID");

                entity.Property(e => e.GarantTypeName).HasMaxLength(50);

                entity.Property(e => e.IdentificationNumber).HasMaxLength(50);

                entity.Property(e => e.IssueAuthority).HasMaxLength(50);

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.Otchestvo).HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);
            });

            modelBuilder.Entity<LinkPledgerAttribute>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CustomerId,
                    e.GaranteeId
                });

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DocumentNo).HasMaxLength(50);

                entity.Property(e => e.DocumentSeries).HasMaxLength(50);

                entity.Property(e => e.GarantTypeId).HasColumnName("GarantTypeID");

                entity.Property(e => e.GarantTypeName).HasMaxLength(50);

                entity.Property(e => e.GaranteeId).HasColumnName("GaranteeID");

                entity.Property(e => e.IdentificationNumber).HasMaxLength(50);

                entity.Property(e => e.IssueAuthority).HasMaxLength(50);

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.Otchestvo).HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);
            });

            modelBuilder.Entity<LinkRelativeAttribute>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CustomerId,
                    e.RelativeId
                });

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CustomerName).HasMaxLength(50);

                entity.Property(e => e.Otchestvo).HasMaxLength(50);

                entity.Property(e => e.RelativeId).HasColumnName("RelativeID");

                entity.Property(e => e.RelativeName).HasMaxLength(50);

                entity.Property(e => e.RelativeTypeId).HasColumnName("RelativeTypeID");

                entity.Property(e => e.Surname).HasMaxLength(50);
            });

            modelBuilder.Entity<LinkType>(entity =>
            {
                entity.HasKey(e => e.RelativeTypeId);

                entity.Property(e => e.RelativeName).HasMaxLength(50);

                entity.Property(e => e.RelativeTypeId).HasColumnName("RelativeTypeID");
            });

            modelBuilder.Entity<LoanAttribute>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.Id,
                    e.TranchIdHistoryOverdues
                });

                entity.Property(e => e.Id).HasColumnName("Id");

                entity.Property(e => e.AgreementNo).HasMaxLength(40);

                entity.Property(e => e.ApprovedRate).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ApprovedSumm).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BrancheName).HasMaxLength(500);

                entity.Property(e => e.ChangeDate).HasColumnType("date");

                entity.Property(e => e.CreditId).HasColumnName("CreditID");

                entity.Property(e => e.TranchIdHistoryOverdues).HasColumnName("TranchIdHistoryOverdues");

                entity.Property(e => e.CurrentBody).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.HaveToPay).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.MainOverdueSumm).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MonthlyPay).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Peny).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PercentBalanceOverdueSumm).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Rvd).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.SumGosSubsid).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<PaymentsAttribute>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CreditId,
                    e.TransactionDate,
                    e.SumN,
                    e.Comment
                });

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.CreditId).HasColumnName("CreditID");

                entity.Property(e => e.PayBody).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PayInterests).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.SumN).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TransactionDate).HasColumnType("date");
            });

            modelBuilder.Entity<PhoneAttribute>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.Property(e => e.ContactPhone1).HasMaxLength(50);

                entity.Property(e => e.ContactPhone2).HasMaxLength(50);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");
            });

            modelBuilder.Entity<SheduleOfPlanesRepayment>(entity =>
            {
                entity.HasKey(e => new
                {
                    e.CreditId,
                    e.PayDate,
                    e.MainSumm,
                    e.PercentsSumm
                });

                entity.ToTable("SheduleOfPlanesRepayment");

                entity.Property(e => e.AgreementNo).HasMaxLength(40);

                entity.Property(e => e.CreditId).HasColumnName("CreditID");

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.MainSumm).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PayDate).HasColumnType("date");

                entity.Property(e => e.PercentsSumm).HasColumnType("decimal(18, 0)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}