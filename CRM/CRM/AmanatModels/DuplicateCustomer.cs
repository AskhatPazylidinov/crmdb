﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DuplicateCustomer
    {
        public int CustomerId { get; set; }
        public int DuplicateCustomerId { get; set; }
        public bool IsIdentificationNumberMatch { get; set; }
        public bool IsCustomerNameMatch { get; set; }
        public bool IsDocumentMatch { get; set; }
        public bool IsDateOfBirthMatch { get; set; }
        public int TotalMatch { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
