﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MonthlyPayment
    {
        public int Id { get; set; }
        public int ContrAgentId { get; set; }
        public DateTime SchaduleDate { get; set; }
        public decimal? Summa { get; set; }
        public int? TransactionId { get; set; }
        public decimal? PlusMinus { get; set; }
        public bool? Checked { get; set; }
        public string Comment { get; set; }
    }
}
