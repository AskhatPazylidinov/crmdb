﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComplianceChangeDate
    {
        public DateTime ChangeDate { get; set; }
    }
}
