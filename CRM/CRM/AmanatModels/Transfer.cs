﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Transfer
    {
        public Transfer()
        {
            TransferStatuses = new HashSet<TransferStatus>();
        }

        public int TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public decimal TransferSum { get; set; }
        public int TransferCurrencyId { get; set; }
        public decimal BankComission { get; set; }
        public string PaymentComment { get; set; }
        public int SenderCustomerId { get; set; }
        public string SenderAccountNo { get; set; }
        public int ReceiverCustomerId { get; set; }
        public string ReceiverCardNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public byte ReceiverBankType { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Customer ReceiverCustomer { get; set; }
        public virtual Customer SenderCustomer { get; set; }
        public virtual Currency3 TransferCurrency { get; set; }
        public virtual ICollection<TransferStatus> TransferStatuses { get; set; }
    }
}
