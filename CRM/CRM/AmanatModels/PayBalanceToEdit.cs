﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PayBalanceToEdit
    {
        public long Position { get; set; }
        public short PositionN { get; set; }
        public int Pnn { get; set; }
        public byte? Fldk { get; set; }
        public int? CountryCode { get; set; }
        public int? CivilCode { get; set; }
        public string OperationId { get; set; }
        public int? SectorId { get; set; }
        public string NewComment { get; set; }
        public int? AccCountryCode { get; set; }
        public int? CustomerId { get; set; }
        public byte? NewFldk { get; set; }
        public int? PartnerId { get; set; }
        public int BranchId { get; set; }
        public int OfficeId { get; set; }
        public int? PartnerCode { get; set; }
        public DateTime TransactionDate { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string DebetAccountGroup { get; set; }
        public string CreditAccountId { get; set; }
        public string CreditAccountGroup { get; set; }
        public decimal SumV { get; set; }
        public int CurrencyId { get; set; }
        public decimal SumN { get; set; }
        public string Comment { get; set; }
        public byte TransactionTypeId { get; set; }
        public int? CustomerTypeId { get; set; }
        public string CustomerName { get; set; }
        public string LegalForm { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual Account1 C { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual PayBalance1 P { get; set; }
        public virtual Partner3 Partner { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual RefSector1 Sector { get; set; }
    }
}
