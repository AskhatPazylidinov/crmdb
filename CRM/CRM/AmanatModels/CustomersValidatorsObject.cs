﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersValidatorsObject
    {
        public string ValidatorName { get; set; }
        public string ItemToValidate { get; set; }
        public bool IsGroup { get; set; }
        public string DependencyValue { get; set; }

        public virtual CustomersValidator ValidatorNameNavigation { get; set; }
    }
}
