﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceBranch
    {
        public int InsuranceId { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual InsuranceCompany Insurance { get; set; }
    }
}
