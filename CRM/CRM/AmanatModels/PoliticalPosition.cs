﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PoliticalPosition
    {
        public int TypeId { get; set; }
        public string Typename { get; set; }
    }
}
