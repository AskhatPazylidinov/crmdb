﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesMinimalTaxesBasis
    {
        public int BranchId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal MinIncomeTaxBase { get; set; }
        public decimal MinSocialTaxBase { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
