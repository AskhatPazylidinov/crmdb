﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashFlowType
    {
        public CashFlowType()
        {
            CashFlowOperationTypes = new HashSet<CashFlowOperationType>();
        }

        public int TypeId { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }

        public virtual ICollection<CashFlowOperationType> CashFlowOperationTypes { get; set; }
    }
}
