﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashIncomeTypeClassification
    {
        public DateTime ActualDate { get; set; }
        public byte CashTypeId { get; set; }
        public int ElapsedDays { get; set; }
    }
}
