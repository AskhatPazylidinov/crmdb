﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AuthActionCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public byte Status { get; set; }
    }
}
