﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployeesTaxis
    {
        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public int TaxTypeId { get; set; }
        public decimal TaxSumm { get; set; }

        public virtual PaysheetsEmployee PaysheetsEmployee { get; set; }
    }
}
