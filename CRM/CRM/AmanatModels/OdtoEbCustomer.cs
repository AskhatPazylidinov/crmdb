﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OdtoEbCustomer
    {
        public long KodKl { get; set; }
        public int CustomerId { get; set; }
    }
}
