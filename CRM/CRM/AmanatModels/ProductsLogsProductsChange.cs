﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsChange
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }
        public string Description { get; set; }
        public bool HasReviewComission { get; set; }
        public bool? HasSalesTaxOnIssueComission { get; set; }
        public bool HasGrantComission { get; set; }
        public byte? PeriodsCount { get; set; }
        public bool AllowsEarlyPartialPayment { get; set; }
        public bool AllowsEarlyFullPayment { get; set; }
        public decimal? MinimumEarlyAmount { get; set; }
        public bool HasTrancheIssueComission { get; set; }
        public bool HasTrancheSupportComission { get; set; }
        public decimal? TrancheIssueComission { get; set; }
        public byte? TrancheIssueComissionType { get; set; }
        public decimal? TrancheSupportComission { get; set; }
        public byte? TrancheSupportComissionType { get; set; }
        public decimal? RateForNotUsedTranche { get; set; }
        public byte DaysToStartFines { get; set; }
        public bool HasFineForLatePercentsPayment { get; set; }
        public bool HasFineForLateMainSummPayment { get; set; }
        public decimal? ReturnComission { get; set; }
        public byte? ReturnComissionType { get; set; }
        public byte? EarlyDaysDiscount { get; set; }
        public decimal? MinimumIssueComission { get; set; }
        public decimal? MaximumIssueComission { get; set; }
        public bool HasFineForLateGuaranteePayment { get; set; }
        public decimal? FineForLateGuaranteePayment { get; set; }
        public byte? FineForLateGuaranteePaymentType { get; set; }
        public byte? FinesDaysToWithdrawGuaranteePayment { get; set; }
        public decimal? MaxFinesSumm { get; set; }
        public int? RateIntervalSummTypeId { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public bool? HasSalesTaxOnIssueTranchComission { get; set; }

        public virtual User LogUser { get; set; }
    }
}
