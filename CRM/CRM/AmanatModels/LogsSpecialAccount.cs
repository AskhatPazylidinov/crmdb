﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsSpecialAccount
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Id { get; set; }
        public int OperType { get; set; }
        public int DeviceType { get; set; }
        public int OfficeId { get; set; }
        public byte BankType { get; set; }
        public int CardType { get; set; }
        public byte CardOfBank { get; set; }
        public int CurrencyId { get; set; }
        public string DeviceAccount { get; set; }
        public string TransitAccountNo { get; set; }
        public string IncomeAccountNo { get; set; }
        public byte ComissionType { get; set; }
        public decimal? Comission { get; set; }
        public byte IncomeType { get; set; }

        public virtual User User { get; set; }
    }
}
