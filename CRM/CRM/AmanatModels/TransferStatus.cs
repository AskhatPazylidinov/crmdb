﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferStatus
    {
        public long OperationId { get; set; }
        public int TransferId { get; set; }
        public byte StatusId { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public long Position { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Transfer Transfer { get; set; }
        public virtual User User { get; set; }
    }
}
