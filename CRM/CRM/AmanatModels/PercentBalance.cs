﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PercentBalance
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal PercentBalance1 { get; set; }
        public decimal OffSystemBalance { get; set; }

        public virtual History Credit { get; set; }
    }
}
