﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankCurrency
    {
        public int BankId { get; set; }
        public int CountryId { get; set; }
        public string CurrencyCode { get; set; }

        public virtual RiaCurrency CurrencyCodeNavigation { get; set; }
    }
}
