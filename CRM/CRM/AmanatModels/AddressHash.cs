﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AddressHash
    {
        public int CustomerId { get; set; }
        public short Count1 { get; set; }
        public short Count2 { get; set; }
        public short Count3 { get; set; }
        public short Count4 { get; set; }
        public short Count5 { get; set; }
        public short Count6 { get; set; }
        public short Count7 { get; set; }
        public short Count8 { get; set; }
        public short Count9 { get; set; }
        public short Count10 { get; set; }
        public short Count11 { get; set; }
        public short Count12 { get; set; }
        public short Count13 { get; set; }
        public short Count14 { get; set; }
        public short Count15 { get; set; }
        public short Count16 { get; set; }
        public short Count17 { get; set; }
        public short Count18 { get; set; }
        public short Count19 { get; set; }
        public short Count20 { get; set; }
        public short Count21 { get; set; }
        public short Count22 { get; set; }
        public short Count23 { get; set; }
        public short Count24 { get; set; }
        public short Count25 { get; set; }
        public short Count26 { get; set; }
        public short Count27 { get; set; }
        public short Count28 { get; set; }
        public short Count29 { get; set; }
        public short Count30 { get; set; }
        public short Count31 { get; set; }
        public short Count32 { get; set; }
        public short Count33 { get; set; }
    }
}
