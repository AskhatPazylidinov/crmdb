﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Reason
    {
        public Reason()
        {
            InternalPersonFraudCode1Navigations = new HashSet<InternalPerson>();
            InternalPersonFraudCode2Navigations = new HashSet<InternalPerson>();
            InternalPersonFraudCode3Navigations = new HashSet<InternalPerson>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; }

        public virtual ICollection<InternalPerson> InternalPersonFraudCode1Navigations { get; set; }
        public virtual ICollection<InternalPerson> InternalPersonFraudCode2Navigations { get; set; }
        public virtual ICollection<InternalPerson> InternalPersonFraudCode3Navigations { get; set; }
    }
}
