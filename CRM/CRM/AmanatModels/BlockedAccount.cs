﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BlockedAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public byte LockTypeId { get; set; }
        public DateTime? EndDate { get; set; }
        public string LockMessage { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal? MinBalance { get; set; }
        public bool IsApprove { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual User User { get; set; }
    }
}
