﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RefOper
    {
        public string Code { get; set; }
        public string DebetCode { get; set; }
        public string CreditCode { get; set; }
        public string Name { get; set; }
    }
}
