﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SystemNumeration
    {
        public DateTime BankDate { get; set; }
        public int DailyCounter { get; set; }
        public int MonthlyCounter { get; set; }
    }
}
