﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RejectedHistoriesReason
    {
        public int CreditId { get; set; }
        public string RejectReason { get; set; }
        public int RejectedType { get; set; }
        public DateTime NoticeDate { get; set; }
        public byte NoticeTypeId { get; set; }

        public virtual History Credit { get; set; }
    }
}
