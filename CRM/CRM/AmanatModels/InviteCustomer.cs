﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InviteCustomer
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int InviteEmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public int UserId { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Account Account { get; set; }
        public virtual Customer InviteEmployee { get; set; }
        public virtual User User { get; set; }
    }
}
