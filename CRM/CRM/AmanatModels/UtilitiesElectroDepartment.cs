﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElectroDepartment
    {
        public UtilitiesElectroDepartment()
        {
            UtilitiesBaseElectroProviderDepartments = new HashSet<UtilitiesBaseElectroProviderDepartment>();
            UtilitiesElectroPayments = new HashSet<UtilitiesElectroPayment>();
            UtilitiesElectroPaymentsExternalAccounts = new HashSet<UtilitiesElectroPaymentsExternalAccount>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public bool? IsActive { get; set; }
        public string DbfprefixName { get; set; }
        public int ExternalDepartmentId { get; set; }

        public virtual ICollection<UtilitiesBaseElectroProviderDepartment> UtilitiesBaseElectroProviderDepartments { get; set; }
        public virtual ICollection<UtilitiesElectroPayment> UtilitiesElectroPayments { get; set; }
        public virtual ICollection<UtilitiesElectroPaymentsExternalAccount> UtilitiesElectroPaymentsExternalAccounts { get; set; }
    }
}
