﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersChangeType
    {
        public int CustomerType { get; set; }
        public string TypeName { get; set; }
        public bool IsNeedToApprove { get; set; }
        public bool IsNeedToLog { get; set; }
    }
}
