﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InputPaymentSenderInfo
    {
        public long PaymentId { get; set; }
        public string IdentificationNumber { get; set; }
        public string Okpo { get; set; }
        public string SocialFundNo { get; set; }
        public string Refnum { get; set; }
        public bool? IsReciverOk { get; set; }

        public virtual Payment1 Payment { get; set; }
    }
}
