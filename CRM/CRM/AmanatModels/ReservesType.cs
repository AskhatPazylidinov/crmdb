﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesType
    {
        public ReservesType()
        {
            PlanReserves = new HashSet<PlanReserf>();
            Reserves = new HashSet<Reserf>();
            ReservesClassificationsRules = new HashSet<ReservesClassificationsRule>();
            ReservesClassificationsSettings = new HashSet<ReservesClassificationsSetting>();
        }

        public int ReserveTypeId { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public byte ReserveShortTypeId { get; set; }

        public virtual ICollection<PlanReserf> PlanReserves { get; set; }
        public virtual ICollection<Reserf> Reserves { get; set; }
        public virtual ICollection<ReservesClassificationsRule> ReservesClassificationsRules { get; set; }
        public virtual ICollection<ReservesClassificationsSetting> ReservesClassificationsSettings { get; set; }
    }
}
