﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FieldsValue
    {
        public int Id { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
    }
}
