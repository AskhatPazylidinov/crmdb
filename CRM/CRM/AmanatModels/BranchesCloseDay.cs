﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesCloseDay
    {
        public DateTime CloseDate { get; set; }
        public int BranchId { get; set; }
        public int ModuleId { get; set; }
        public byte CloseTypeId { get; set; }
        public int? UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public string Description { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual ProgramModule Module { get; set; }
        public virtual User User { get; set; }
    }
}
