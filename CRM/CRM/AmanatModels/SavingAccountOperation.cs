﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SavingAccountOperation
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal CapitalSum { get; set; }
        public int CustomerId { get; set; }
        public DateTime OperationDate { get; set; }
        public bool? IsSavingsClose { get; set; }
        public long Position { get; set; }
    }
}
