﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesCoverRate
    {
        public int LiquidAssetTypeId { get; set; }
        public bool IsLoanAndAssetSameCurrency { get; set; }
        public decimal CoverRate { get; set; }
    }
}
