﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesWaterPayment
    {
        public int OperationId { get; set; }
        public int DepartmentId { get; set; }
        public string FullcustomerName { get; set; }
        public string PersonalAccountNo { get; set; }
        public string Address { get; set; }
        public int BuildingTypeId { get; set; }
        public decimal TotalSumm { get; set; }

        public virtual UtilitiesWaterBuildingType BuildingType { get; set; }
        public virtual UtilitiesWaterDepartment Department { get; set; }
        public virtual UtilitiesRegistry Operation { get; set; }
    }
}
