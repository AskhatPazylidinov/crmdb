﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersCommission
    {
        public int Id { get; set; }
        public byte BankType { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinSumValue { get; set; }
        public decimal? MaxSumValue { get; set; }
        public byte CommissionType { get; set; }
        public decimal CommissionValue { get; set; }
    }
}
