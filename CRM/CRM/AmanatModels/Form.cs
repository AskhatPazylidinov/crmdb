﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Form
    {
        public string FormNum { get; set; }
        public int? Class { get; set; }
        public int Id { get; set; }
        public string ArticleName { get; set; }
    }
}
