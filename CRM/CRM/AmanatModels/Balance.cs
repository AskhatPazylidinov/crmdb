﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Balance
    {
        public int AssetId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal Balance1 { get; set; }
        public int Quantity { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
