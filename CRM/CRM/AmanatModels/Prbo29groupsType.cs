﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Prbo29groupsType
    {
        public Prbo29groupsType()
        {
            Prbo29balanceGroups = new HashSet<Prbo29balanceGroup>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Prbo29balanceGroup> Prbo29balanceGroups { get; set; }
    }
}
