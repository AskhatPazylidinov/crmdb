﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsCurrency
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int ProductId { get; set; }
        public int CurencyId { get; set; }

        public virtual User User { get; set; }
    }
}
