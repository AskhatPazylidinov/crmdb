﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PersonsTemp
    {
        public int ItemId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string Surname { get; set; }
        public string PersonName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public int BlackListId { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportIssueDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string IdentificationNo { get; set; }
        public string Description { get; set; }
        public string ExternalId { get; set; }
    }
}
