﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class StockItemSearchView
    {
        public int TypeId { get; set; }
        public int MeasureId { get; set; }
        public string Name { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        public int OfficeId { get; set; }
    }
}
