﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftCorrBankAccount
    {
        public SwiftCorrBankAccount()
        {
            CorrAccountsLimits = new HashSet<CorrAccountsLimit>();
            CorrAccountsReserveAccounts = new HashSet<CorrAccountsReserveAccount>();
        }

        public string BankCode { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? CountryId { get; set; }
        public string Nostro { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual SwiftCorrBank BankCodeNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual ICollection<CorrAccountsLimit> CorrAccountsLimits { get; set; }
        public virtual ICollection<CorrAccountsReserveAccount> CorrAccountsReserveAccounts { get; set; }
    }
}
