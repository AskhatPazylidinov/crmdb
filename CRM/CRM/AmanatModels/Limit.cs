﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Limit
    {
        public int LimitId { get; set; }
        public DateTime LimitDate { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal LimitSum { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
