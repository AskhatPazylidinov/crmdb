﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesOfficer
    {
        public int CreditId { get; set; }
        public int OfficerTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? UserId { get; set; }
        public int? RotationReasonId { get; set; }
        public int? RotatedByUserId { get; set; }

        public virtual History Credit { get; set; }
        public virtual OfficersType OfficerType { get; set; }
        public virtual RotationReason RotationReason { get; set; }
        public virtual User User { get; set; }
    }
}
