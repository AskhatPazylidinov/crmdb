﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Extension
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ExtensionDate { get; set; }
        public int? UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
