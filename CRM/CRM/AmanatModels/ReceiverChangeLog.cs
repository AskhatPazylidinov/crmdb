﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReceiverChangeLog
    {
        public long LogId { get; set; }
        public long TransferId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime LogDate { get; set; }
        public int LogUserId { get; set; }
        public int LogOfficeId { get; set; }
        public int PrevReceiverCustomerId { get; set; }
        public int NewReceiverCustomerId { get; set; }
        public string Comment { get; set; }

        public virtual Office1 LogOffice { get; set; }
        public virtual User LogUser { get; set; }
        public virtual Customer NewReceiverCustomer { get; set; }
        public virtual Customer PrevReceiverCustomer { get; set; }
        public virtual InterTransfer Transfer { get; set; }
    }
}
