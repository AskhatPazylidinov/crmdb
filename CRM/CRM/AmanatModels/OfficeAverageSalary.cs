﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeAverageSalary
    {
        public int OfficeId { get; set; }
        public DateTime StartDate { get; set; }
        public decimal AverageSalary { get; set; }
    }
}
