﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Service
    {
        public Service()
        {
            BankServices = new HashSet<BankService>();
        }

        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public int? ParentId { get; set; }
        public string Caption { get; set; }
        public string Comment { get; set; }
        public string CaptionEng { get; set; }
        public string CommentEng { get; set; }
        public int CanIn { get; set; }
        public int CanPay { get; set; }

        public virtual ICollection<BankService> BankServices { get; set; }
    }
}
