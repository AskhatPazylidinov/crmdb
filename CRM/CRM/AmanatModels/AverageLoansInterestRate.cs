﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AverageLoansInterestRate
    {
        public DateTime RateDate { get; set; }
        public decimal Rate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
