﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankTransactionsView
    {
        public int OperationId { get; set; }
        public byte ValueDateId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }
        public DateTime TransactionDate { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string DtBalanceGroup { get; set; }
        public string CreditAccountId { get; set; }
        public string CtBalanceGroup { get; set; }
        public decimal TransactionSumm { get; set; }
        public int CurrencyId { get; set; }
    }
}
