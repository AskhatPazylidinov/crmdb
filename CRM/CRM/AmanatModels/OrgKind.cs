﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrgKind
    {
        public int KyCode { get; set; }
        public string FlName { get; set; }
    }
}
