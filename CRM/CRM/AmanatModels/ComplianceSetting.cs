﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComplianceSetting
    {
        public DateTime ChangeDate { get; set; }
        public byte SenderCustomerTypeId { get; set; }
        public byte ReceiverCustomerTypeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
    }
}
