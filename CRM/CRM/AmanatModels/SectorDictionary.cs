﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SectorDictionary
    {
        public int Id { get; set; }
        public string ItemValue { get; set; }
        public string Description { get; set; }
    }
}
