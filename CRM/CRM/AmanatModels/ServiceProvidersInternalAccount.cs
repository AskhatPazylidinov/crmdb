﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersInternalAccount
    {
        public int ProviderId { get; set; }
        public int BranchId { get; set; }
        public string InternalAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
