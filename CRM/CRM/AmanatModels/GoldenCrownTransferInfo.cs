﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GoldenCrownTransferInfo
    {
        public int Id { get; set; }
        public string GoldenCrownUin { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public long MoneyTransferId { get; set; }

        public virtual MoneyTransfer MoneyTransfer { get; set; }
    }
}
