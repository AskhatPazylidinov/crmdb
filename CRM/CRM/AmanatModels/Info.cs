﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Info
    {
        public string BankCode { get; set; }
        public int MainOfficeId { get; set; }
        public int FileNumber { get; set; }
        public int? LastCustomerId { get; set; }
        public string OnlineDeviceCode { get; set; }
    }
}
