﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionView
    {
        public int ConversionId { get; set; }
        public int CustomerId { get; set; }
        public DateTime ConversionDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public decimal? CrossRate { get; set; }
        public string CustomerName { get; set; }
        public int ActualStatusId { get; set; }
        public string ActualUserName { get; set; }
        public string SellAccountNo { get; set; }
        public int SellCurrencyId { get; set; }
        public decimal? SellRate { get; set; }
        public decimal SellSumm { get; set; }
        public string BuyAccountNo { get; set; }
        public int BuyCurrencyId { get; set; }
        public decimal? BuyRate { get; set; }
        public decimal BuySumm { get; set; }
        public int BranchId { get; set; }
    }
}
