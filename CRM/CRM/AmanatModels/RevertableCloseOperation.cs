﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableCloseOperation
    {
        public RevertableCloseOperation()
        {
            RevertableCloseOperationsAccounts = new HashSet<RevertableCloseOperationsAccount>();
            RevertableCloseOperationsTransactions = new HashSet<RevertableCloseOperationsTransaction>();
        }

        public long RevertId { get; set; }
        public DateTime BankDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public long? Position { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<RevertableCloseOperationsAccount> RevertableCloseOperationsAccounts { get; set; }
        public virtual ICollection<RevertableCloseOperationsTransaction> RevertableCloseOperationsTransactions { get; set; }
    }
}
