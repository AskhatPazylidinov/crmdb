﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsDocument
    {
        public int DocumentNo { get; set; }
        public DateTime GenerateDateTime { get; set; }
    }
}
