﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetsAccount
    {
        public int AssetId { get; set; }
        public int TypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Asset Asset { get; set; }
    }
}
