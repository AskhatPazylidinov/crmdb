﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeeWorkedDay
    {
        public int EmployeeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public decimal? TotalWorkedHours { get; set; }
        public decimal? OverTimeHours { get; set; }
        public int? OfficeId { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
