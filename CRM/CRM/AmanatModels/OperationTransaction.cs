﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationTransaction
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int OrderId { get; set; }
        public string DebetAccountNo { get; set; }
        public int? DebetCurrencyId { get; set; }
        public string CreditAccountNo { get; set; }
        public int? CreditCurrencyId { get; set; }
        public decimal? OperationSumm { get; set; }
        public string Comment { get; set; }
        public int? DebetOfficeId { get; set; }
        public int? CreditOfficeId { get; set; }
    }
}
