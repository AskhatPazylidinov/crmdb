﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrBankTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int CountryId { get; set; }
        public int DirectionTypeId { get; set; }
        public int OperationTypeId { get; set; }

        public virtual Country2 Country { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
