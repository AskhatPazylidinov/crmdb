﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionsAccount
    {
        public int ConversionId { get; set; }
        public bool IsCustomerBuy { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal? Rate { get; set; }
        public decimal Summ { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Conversion Conversion { get; set; }
    }
}
