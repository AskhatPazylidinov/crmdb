﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeGroupIncExpTransaction
    {
        public int GroupId { get; set; }
        public long Position { get; set; }
        public int StatusId { get; set; }

        public virtual OfficeGroupIncExp Group { get; set; }
    }
}
