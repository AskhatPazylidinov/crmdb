﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankCloseDay
    {
        public DateTime CloseDate { get; set; }
        public int CloseTypeId { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
    }
}
