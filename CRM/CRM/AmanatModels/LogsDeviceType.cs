﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsDeviceType
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Code { get; set; }
        public string NameDevice { get; set; }

        public virtual User User { get; set; }
    }
}
