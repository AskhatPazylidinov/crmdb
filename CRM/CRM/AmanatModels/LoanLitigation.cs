﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LoanLitigation
    {
        public LoanLitigation()
        {
            LoanLitigationEvents = new HashSet<LoanLitigationEvent>();
        }

        public int Id { get; set; }
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte Status { get; set; }
        public DateTime? BeforePlanDate { get; set; }
        public DateTime? BeforeFactDate { get; set; }
        public int? ProceedingsState { get; set; }
        public decimal? ProceedingsClaimAmountBase { get; set; }
        public decimal? ProceedingsClaimAmountPercent { get; set; }
        public decimal? ProceedingsClaimAmountFine { get; set; }
        public decimal? ProceedingsJudgmentAmountCollected { get; set; }
        public decimal? ProceedingsJudgmentStateFee { get; set; }
        public DateTime? ProceedingsJudgmentDate { get; set; }
        public DateTime? EnforcementExecuteDate { get; set; }
        public DateTime? EnforcementGuaranteesTerminationDate { get; set; }
        public int? RealizationState { get; set; }
        public int? TerminationState { get; set; }
        public string WriteOffDecisionNo { get; set; }
        public DateTime? WriteOffDate { get; set; }
        public DateTime? LawsuitFillingDate { get; set; }

        public virtual ICollection<LoanLitigationEvent> LoanLitigationEvents { get; set; }
    }
}
