﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsMonitoringsSetting
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public int TypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int Interval { get; set; }
        public int? MaxMonitorings { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
