﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplaceCustomersLog
    {
        public int Id { get; set; }
        public int OldCustomerId { get; set; }
        public int NewCustomerId { get; set; }
        public string OldCustomerName { get; set; }
        public int UserId { get; set; }
        public int ApproveUserId { get; set; }
        public DateTime OperationDate { get; set; }
        public DateTime CreateDateForMerge { get; set; }
        public int? ErrNumber { get; set; }
        public int? ErrLine { get; set; }
        public string ErrMessage { get; set; }
    }
}
