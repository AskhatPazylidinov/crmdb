﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PositionType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
