﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersContragentsComission
    {
        public int ProviderId { get; set; }
        public int ContragentTypeId { get; set; }
        public int ContactTypeId { get; set; }
        public int OfficeId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionTypeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ServiceProvidersContragent ServiceProvidersContragent { get; set; }
    }
}
