﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncomesStructure
    {
        public int CreditId { get; set; }
        public DateTime ActualDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal TotalPercents { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual IncomesStructuresActualDate IncomesStructuresActualDate { get; set; }
    }
}
