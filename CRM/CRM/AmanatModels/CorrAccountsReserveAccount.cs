﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrAccountsReserveAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string ReserveAccountNo { get; set; }
        public int ReserveCurrencyId { get; set; }

        public virtual Account1 Reserve { get; set; }
        public virtual SwiftCorrBankAccount SwiftCorrBankAccount { get; set; }
    }
}
