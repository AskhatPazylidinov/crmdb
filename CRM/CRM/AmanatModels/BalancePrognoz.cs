﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BalancePrognoz
    {
        public int OfficeId { get; set; }
        public string BalanceGroup { get; set; }
        public DateTime Date { get; set; }
        public decimal? Balance { get; set; }
    }
}
