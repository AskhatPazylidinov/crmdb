﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountStatementScheduler
    {
        public int Id { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int Period { get; set; }
        public string Addresses { get; set; }
        public bool IsActive { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
