﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Security
    {
        public Security()
        {
            SecuritiesCustomers = new HashSet<SecuritiesCustomer>();
            SecuritiesInterestRates = new HashSet<SecuritiesInterestRate>();
            SecuritiesMortgages = new HashSet<SecuritiesMortgage>();
            SecuritiesStatuses = new HashSet<SecuritiesStatus>();
        }

        public int SecurityId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte CalculationTypeId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string RegistrationNo { get; set; }
        public decimal CostSumV { get; set; }
        public byte WithdrawPercentsPeriod { get; set; }
        public DateTime KuapresolutionDate { get; set; }
        public string KuapresolutionNo { get; set; }
        public int SecuritiesQuantity { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ICollection<SecuritiesCustomer> SecuritiesCustomers { get; set; }
        public virtual ICollection<SecuritiesInterestRate> SecuritiesInterestRates { get; set; }
        public virtual ICollection<SecuritiesMortgage> SecuritiesMortgages { get; set; }
        public virtual ICollection<SecuritiesStatus> SecuritiesStatuses { get; set; }
    }
}
