﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IssueLoanLimitsForUser
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int CurrencyId { get; set; }
        public int OfficeId { get; set; }
        public decimal SumFrom { get; set; }
        public decimal? SumTo { get; set; }
        public int UserId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual Product1 Product { get; set; }
        public virtual User User { get; set; }
    }
}
