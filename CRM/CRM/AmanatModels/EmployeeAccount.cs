﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeeAccount
    {
        public int TypeId { get; set; }
        public int EmployeeId { get; set; }
        public string AccountNo { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? TransferSumm { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
