﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OverdraftLimit
    {
        public int Id { get; set; }
        public int CardId { get; set; }
        public decimal LimitValue { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUserId { get; set; }
        public DateTime? ApproveDate { get; set; }
        public int? ApproveUserId { get; set; }

        public virtual User ApproveUser { get; set; }
        public virtual Card Card { get; set; }
        public virtual User CreateUser { get; set; }
    }
}
