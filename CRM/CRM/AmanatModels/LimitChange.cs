﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitChange
    {
        public int CreditId { get; set; }
        public int LimitId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal LimitSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
