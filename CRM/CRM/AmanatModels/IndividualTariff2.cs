﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariff2
    {
        public int IndividualTariffId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ComissionId { get; set; }
        public int CustomerId { get; set; }
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal TariffStart { get; set; }
        public decimal? TariffEnd { get; set; }
        public decimal ComissionSumm { get; set; }
        public byte ComissionTypeId { get; set; }
        public bool IsApproved { get; set; }

        public virtual IndividualTariffsChangeDate ChangeDateNavigation { get; set; }
        public virtual StandardTariff Comission { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
