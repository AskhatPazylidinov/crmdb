﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariff1
    {
        public int CreditId { get; set; }
        public decimal? RequestGrantComission { get; set; }
        public byte? RequestGrantComissionType { get; set; }
        public decimal? RequestRate { get; set; }

        public virtual History Credit { get; set; }
    }
}
