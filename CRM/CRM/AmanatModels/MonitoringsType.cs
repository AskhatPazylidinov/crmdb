﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MonitoringsType
    {
        public MonitoringsType()
        {
            HistoriesMonitorings = new HashSet<HistoriesMonitoring>();
            HistoriesMonitoringsSettings = new HashSet<HistoriesMonitoringsSetting>();
            ProductsMonitoringsSettings = new HashSet<ProductsMonitoringsSetting>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<HistoriesMonitoring> HistoriesMonitorings { get; set; }
        public virtual ICollection<HistoriesMonitoringsSetting> HistoriesMonitoringsSettings { get; set; }
        public virtual ICollection<ProductsMonitoringsSetting> ProductsMonitoringsSettings { get; set; }
    }
}
