﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditGroup
    {
        public CreditGroup()
        {
            GroupCycles = new HashSet<GroupCycle>();
        }

        public int Code { get; set; }
        public int OfficeId { get; set; }
        public string Name { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ICollection<GroupCycle> GroupCycles { get; set; }
    }
}
