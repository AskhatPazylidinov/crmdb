﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchTransfersLimitsChangeDate
    {
        public InterBranchTransfersLimitsChangeDate()
        {
            InterBranchTransfersLimits = new HashSet<InterBranchTransfersLimit>();
        }

        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }

        public virtual ICollection<InterBranchTransfersLimit> InterBranchTransfersLimits { get; set; }
    }
}
