﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferAccount1
    {
        public string OldAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string NewAccountNo { get; set; }
        public DateTime MigrateDate { get; set; }
        public int Id { get; set; }
        public int? AccountChangeType { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Account1 Account1Navigation { get; set; }
    }
}
