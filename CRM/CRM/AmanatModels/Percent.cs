﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Percent
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime PercentDate { get; set; }
        public decimal SumV { get; set; }
        public decimal FinesOnMainSumm { get; set; }
        public decimal FinesOnPercents { get; set; }
    }
}
