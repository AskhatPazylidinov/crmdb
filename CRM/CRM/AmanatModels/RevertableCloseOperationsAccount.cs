﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableCloseOperationsAccount
    {
        public long RevertId { get; set; }
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime PrevLastCalcDate { get; set; }
        public decimal PrevSumV { get; set; }
        public string CloseReason { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
        public virtual RevertableCloseOperation Revert { get; set; }
    }
}
