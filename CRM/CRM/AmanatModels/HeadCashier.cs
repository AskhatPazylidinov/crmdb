﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HeadCashier
    {
        public int CashierId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual User Cashier { get; set; }
    }
}
