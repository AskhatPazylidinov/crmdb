﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OriginOfFundsType
    {
        public OriginOfFundsType()
        {
            Customers = new HashSet<Customer>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }
        public bool IsForPrivateCustomers { get; set; }
        public bool IsForLegalCustomers { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
