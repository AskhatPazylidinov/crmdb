﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesMonitoringsStatus
    {
        public int ActionId { get; set; }
        public int MonitoringId { get; set; }
        public int StatusId { get; set; }
        public DateTime StatusDate { get; set; }
        public int PerformUserId { get; set; }
        public DateTime OperationDate { get; set; }
        public string Comment { get; set; }

        public virtual HistoriesMonitoring Monitoring { get; set; }
        public virtual User PerformUser { get; set; }
    }
}
