﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeBonusStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
