﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EnrollmentTransaction
    {
        public long OperationId { get; set; }
        public long? Position { get; set; }
        public string ErrorMessage { get; set; }

        public virtual EnrollmentOperation Operation { get; set; }
    }
}
