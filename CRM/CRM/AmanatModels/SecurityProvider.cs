﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecurityProvider
    {
        public SecurityProvider()
        {
            SecuredOperations = new HashSet<SecuredOperation>();
        }

        public string ProviderNamespace { get; set; }
        public string ProviderName { get; set; }
        public string Description { get; set; }
        public string ExtendedDescription { get; set; }

        public virtual ICollection<SecuredOperation> SecuredOperations { get; set; }
    }
}
