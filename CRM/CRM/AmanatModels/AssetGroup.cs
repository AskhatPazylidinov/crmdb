﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetGroup
    {
        public AssetGroup()
        {
            AssetSubGroups = new HashSet<AssetSubGroup>();
            TypeOperations = new HashSet<TypeOperation>();
            Types = new HashSet<Type>();
        }

        public int GroupId { get; set; }
        public byte CategoryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AssetSubGroup> AssetSubGroups { get; set; }
        public virtual ICollection<TypeOperation> TypeOperations { get; set; }
        public virtual ICollection<Type> Types { get; set; }
    }
}
