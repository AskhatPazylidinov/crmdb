﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Tariff2
    {
        public int TariffId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public int? SenderBranchId { get; set; }
        public int? ReceiverBranchId { get; set; }
        public int? SenderAccountTypeId { get; set; }
        public int? ReceiverAccountTypeId { get; set; }
        public decimal TotalComission { get; set; }
        public int ComissionTypeId { get; set; }
        public decimal SenderComission { get; set; }

        public virtual TariffsChangeDate3 ChangeDateNavigation { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Branch ReceiverBranch { get; set; }
        public virtual Branch SenderBranch { get; set; }
    }
}
