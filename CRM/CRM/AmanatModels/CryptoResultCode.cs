﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CryptoResultCode
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
