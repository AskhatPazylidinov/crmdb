﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositBonusesComission
    {
        public byte TypeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinAmount { get; set; }
        public decimal Comission { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
