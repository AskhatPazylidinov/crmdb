﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Partner2
    {
        public Partner2()
        {
            PartnersContracts = new HashSet<PartnersContract>();
            ProductsPartnerCommissions = new HashSet<ProductsPartnerCommission>();
        }

        public int CompanyId { get; set; }
        public byte TypeId { get; set; }
        public int? BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Customer Company { get; set; }
        public virtual ICollection<PartnersContract> PartnersContracts { get; set; }
        public virtual ICollection<ProductsPartnerCommission> ProductsPartnerCommissions { get; set; }
    }
}
