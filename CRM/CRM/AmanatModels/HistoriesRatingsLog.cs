﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesRatingsLog
    {
        public int CreditId { get; set; }
        public int CustomerId { get; set; }
        public string TypeChange { get; set; }
        public int OldValue { get; set; }
        public int NewValue { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ChangeUserId { get; set; }
    }
}
