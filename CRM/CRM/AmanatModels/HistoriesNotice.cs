﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesNotice
    {
        public int CreditId { get; set; }
        public DateTime ActionDate { get; set; }
        public int? UserId { get; set; }
        public DateTime? OperationDate { get; set; }
        public string RegNumber { get; set; }
        public string Comment { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
