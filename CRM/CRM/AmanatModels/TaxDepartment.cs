﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxDepartment
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Bik { get; set; }
        public string AccountNo { get; set; }
    }
}
