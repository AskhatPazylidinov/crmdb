﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransitAccount
    {
        public int OfficeId { get; set; }
        public byte TransitType { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
    }
}
