﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxPayment1
    {
        public int OperationId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }
        public string IdentificationNo { get; set; }
        public string Okpocode { get; set; }
        public string VehicleNumber { get; set; }

        public virtual TaxDepartment1 Department { get; set; }
        public virtual TaxService Service { get; set; }
        public virtual ImportedTaxPayment ImportedTaxPayment { get; set; }
        public virtual InternalTaxPayment InternalTaxPayment { get; set; }
    }
}
