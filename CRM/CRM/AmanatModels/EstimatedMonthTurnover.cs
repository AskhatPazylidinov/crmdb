﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EstimatedMonthTurnover
    {
        public int CustomerId { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinTurnoverSumm { get; set; }
        public decimal MaxTurnoverSumm { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
