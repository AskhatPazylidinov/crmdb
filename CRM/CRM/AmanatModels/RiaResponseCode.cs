﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiaResponseCode
    {
        public int CodeId { get; set; }
        public string Description { get; set; }
    }
}
