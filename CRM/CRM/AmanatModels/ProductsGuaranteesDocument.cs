﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsGuaranteesDocument
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int GuaranteePurposeId { get; set; }
        public byte DocumentTypeId { get; set; }
        public string Filename { get; set; }
        public bool? ToPdf { get; set; }

        public virtual GuaranteePurpose GuaranteePurpose { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
