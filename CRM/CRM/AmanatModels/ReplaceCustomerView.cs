﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplaceCustomerView
    {
        public string SelectorId { get; set; }
        public int OldCustomerId { get; set; }
        public int NewCustomerId { get; set; }
        public string OldCustomerName { get; set; }
        public string NewCustomerName { get; set; }
        public string OldIdentificationNumber { get; set; }
        public string NewIdentificationNumber { get; set; }
        public string Fullname { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? HasChange { get; set; }
    }
}
