﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? CityId { get; set; }
        public string Bik { get; set; }
        public string HeadOfLegalDepartment { get; set; }
        public string ShortName { get; set; }
        public int DefaultCustomerId { get; set; }
        public string AccountantPosition { get; set; }
        public string AccountantName { get; set; }
        public int BranchId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public bool? IsHead { get; set; }
    }
}
