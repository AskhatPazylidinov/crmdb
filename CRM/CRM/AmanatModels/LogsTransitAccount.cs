﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsTransitAccount
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int OfficeId { get; set; }
        public byte TransitType { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual User User { get; set; }
    }
}
