﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployeesExtraTransfer
    {
        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public byte ExtraPaymentTypeId { get; set; }
        public decimal TransferSumm { get; set; }

        public virtual PaysheetsEmployee PaysheetsEmployee { get; set; }
    }
}
