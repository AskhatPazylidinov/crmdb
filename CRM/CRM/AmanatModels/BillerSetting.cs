﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BillerSetting
    {
        public int ServiceOperationId { get; set; }
        public string BillerRef { get; set; }
        public string PayinstrRef { get; set; }
        public string MerchantId { get; set; }
    }
}
