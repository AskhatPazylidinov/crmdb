﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Partickind
    {
        public int KyCode { get; set; }
        public string FlName { get; set; }
        public string FlOper { get; set; }
    }
}
