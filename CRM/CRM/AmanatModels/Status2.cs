﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Status2
    {
        public int CardId { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateUserId { get; set; }
        public DateTime? ApproveDate { get; set; }
        public int? ApproveUserId { get; set; }

        public virtual User ApproveUser { get; set; }
        public virtual Card Card { get; set; }
        public virtual User CreateUser { get; set; }
    }
}
