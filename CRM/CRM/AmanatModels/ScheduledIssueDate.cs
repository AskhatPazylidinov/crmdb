﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ScheduledIssueDate
    {
        public int CreditId { get; set; }
        public DateTime SheduledDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
