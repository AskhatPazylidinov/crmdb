﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualCapitalizationDay
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public byte CapitalizeMonth { get; set; }
        public byte CapitalizeDay { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
