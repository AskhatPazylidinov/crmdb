﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SubStatusesReason
    {
        public SubStatusesReason()
        {
            HistoriesSubStatuses = new HashSet<HistoriesSubStatus>();
        }

        public int Id { get; set; }
        public int SubStatusId { get; set; }
        public string Name { get; set; }

        public virtual SubStatusesType SubStatus { get; set; }
        public virtual ICollection<HistoriesSubStatus> HistoriesSubStatuses { get; set; }
    }
}
