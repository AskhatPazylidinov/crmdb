﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ContactLoyalOperation
    {
        public string PaymentCode { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual User User { get; set; }
    }
}
