﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StorageAmount
    {
        public int UserId { get; set; }
        public DateTime StorageDate { get; set; }
        public int NoteId { get; set; }
        public int Amount { get; set; }
        public int OfficeId { get; set; }

        public virtual Denomination Note { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
    }
}
