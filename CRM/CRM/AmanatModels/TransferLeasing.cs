﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferLeasing
    {
        public int OldLeaseId { get; set; }
        public int NewLeaseId { get; set; }
        public DateTime TransferDate { get; set; }

        public virtual SafesLeasing NewLease { get; set; }
        public virtual SafesLeasing OldLease { get; set; }
    }
}
