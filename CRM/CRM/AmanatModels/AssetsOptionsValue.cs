﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetsOptionsValue
    {
        public int AssetId { get; set; }
        public int OptionId { get; set; }
        public string Value { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
