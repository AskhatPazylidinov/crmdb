﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalLoansGraphic
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int GraphicId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal MainSumm { get; set; }
        public decimal PercentsSumm { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
