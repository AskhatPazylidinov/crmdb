﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployeesAliment
    {
        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public int AlimentId { get; set; }
        public decimal TransferSumm { get; set; }

        public virtual Aliment Aliment { get; set; }
        public virtual PaysheetsEmployee PaysheetsEmployee { get; set; }
    }
}
