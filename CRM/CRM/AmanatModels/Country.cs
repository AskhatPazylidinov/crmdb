﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Country
    {
        public string Code { get; set; }
        public string Code3 { get; set; }
        public string InternationalCountryName { get; set; }
        public string FullCountryName { get; set; }
        public string ShortCountryName { get; set; }
    }
}
