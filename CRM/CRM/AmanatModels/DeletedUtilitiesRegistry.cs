﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeletedUtilitiesRegistry
    {
        public int OperationId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int UserId { get; set; }
        public int OfficeId { get; set; }
        public int ProviderId { get; set; }
        public decimal PaymentSum { get; set; }
        public decimal CommissionSum { get; set; }
        public string Description { get; set; }
        public byte DirectionTypeId { get; set; }
        public DateTime OperationDate { get; set; }
        public int DeleteUserId { get; set; }
        public DateTime DeleteDate { get; set; }
        public bool IsOnlineBanking { get; set; }
        public string CustomerName { get; set; }
        public bool? IsManualDelete { get; set; }

        public virtual User DeleteUser { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
        public virtual User User { get; set; }
    }
}
