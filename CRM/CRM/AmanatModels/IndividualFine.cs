﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualFine
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool HasFineForLatePercentsPayment { get; set; }
        public bool HasFineForLateMainSummPayment { get; set; }
        public decimal? FineForLatePercentsPayment { get; set; }
        public byte? FineForLatePercentsPaymentType { get; set; }
        public byte? FineForLatePercentsCalculationType { get; set; }
        public decimal? FineForLateMainSummPayment { get; set; }
        public byte? FineForLateMainSummPaymentType { get; set; }
        public byte? FineForLateMainSummCalculationType { get; set; }
        public byte? FineForLateMainSummBaseType { get; set; }
        public byte? FineForLatePercentsBaseType { get; set; }

        public virtual History Credit { get; set; }
    }
}
