﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductBonusesOnCooperationDate
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public int StartCooperationPeriod { get; set; }
        public int? EndCooperationPeriod { get; set; }
        public decimal BonusRate { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProductsChange1 ProductsChange1 { get; set; }
    }
}
