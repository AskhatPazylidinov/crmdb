﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReliableCustomer
    {
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Reason { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int? ApproverUserId { get; set; }
        public DateTime? ApproveDateTime { get; set; }

        public virtual User ApproverUser { get; set; }
        public virtual User CreatorUser { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
