﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesOperation
    {
        public SecuritiesOperation()
        {
            SecuritiesOperationsStatuses = new HashSet<SecuritiesOperationsStatus>();
            SecuritiesOperationsTransactions = new HashSet<SecuritiesOperationsTransaction>();
        }

        public long OperationId { get; set; }
        public int SecuritiesId { get; set; }
        public DateTime OperationDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public byte OperationTypeId { get; set; }
        public decimal SummPerItem { get; set; }
        public int TotalAmount { get; set; }
        public decimal CouponIncomeToTransfer { get; set; }

        public virtual ICollection<SecuritiesOperationsStatus> SecuritiesOperationsStatuses { get; set; }
        public virtual ICollection<SecuritiesOperationsTransaction> SecuritiesOperationsTransactions { get; set; }
    }
}
