﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DictionaryInfo
    {
        public int Id { get; set; }
        public DateTime UpdateDate { get; set; }
        public int Version { get; set; }
        public string BookId { get; set; }
        public int Part { get; set; }
        public int Total { get; set; }
        public bool IsComplete { get; set; }
    }
}
