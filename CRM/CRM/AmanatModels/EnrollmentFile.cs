﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EnrollmentFile
    {
        public EnrollmentFile()
        {
            EnrollmentOperations = new HashSet<EnrollmentOperation>();
        }

        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime LoadDateTime { get; set; }
        public int UserId { get; set; }
        public byte StatusId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<EnrollmentOperation> EnrollmentOperations { get; set; }
    }
}
