﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Percent1
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime PercentDate { get; set; }
        public decimal SumV { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
