﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AvanceStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
