﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Aimag
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public int OfficeCode { get; set; }

        public virtual Office2 OfficeCodeNavigation { get; set; }
    }
}
