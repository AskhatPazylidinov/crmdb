﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowedFullname
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
    }
}
