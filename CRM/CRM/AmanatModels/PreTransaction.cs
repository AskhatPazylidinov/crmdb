﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PreTransaction
    {
        public long Position { get; set; }
        public int DtCurrencyId { get; set; }
        public string DtConversionAccountNo { get; set; }
        public decimal DtSumV { get; set; }
        public int CtCurrencyId { get; set; }
        public string CtConversionAccountNo { get; set; }
        public decimal CtSumV { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public byte CashSymbol { get; set; }
        public bool IsFinalTransaction { get; set; }
        public int? AttorneyId { get; set; }
        public string OperCode { get; set; }
        public int? CountryId { get; set; }
        public string ResponsiblePerson { get; set; }
        public bool? IsComplex { get; set; }
        public long? TransactionId { get; set; }
        public DateTime DateV { get; set; }
    }
}
