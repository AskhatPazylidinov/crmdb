﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LeasingsPayment
    {
        public int PaymentId { get; set; }
        public int LeaseId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal PaymentSumm { get; set; }
        public int PaymentTypeId { get; set; }
        public byte DirectionTypeId { get; set; }
        public int? PayDays { get; set; }
        public long? Position { get; set; }
        public short? Positionn { get; set; }

        public virtual SafesLeasing Lease { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
