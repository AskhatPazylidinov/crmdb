﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersWithdrawTariff
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public decimal StartSumm { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public int ComissionCalculationTypeId { get; set; }
        public decimal? MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
