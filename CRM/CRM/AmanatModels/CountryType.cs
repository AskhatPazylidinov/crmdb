﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CountryType
    {
        public CountryType()
        {
            Country2s = new HashSet<Country2>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<Country2> Country2s { get; set; }
    }
}
