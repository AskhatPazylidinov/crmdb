﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsLog
    {
        public long Id { get; set; }
        public long? PaymentId { get; set; }
        public string AccountNo { get; set; }
        public string CardNo { get; set; }
        public bool IsWithdraw { get; set; }
        public bool IsThroughCash { get; set; }
        public decimal Amount { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public string AuthActionCode { get; set; }

        public virtual User User { get; set; }
    }
}
