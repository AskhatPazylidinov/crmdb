﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Account
    {
        public Account()
        {
            CardsOverdraftOverduesHistories = new HashSet<CardsOverdraftOverduesHistory>();
            InviteCustomers = new HashSet<InviteCustomer>();
            IpcExecuteTransactionPayments = new HashSet<IpcExecuteTransactionPayment>();
            IpcExecuteTransactionTransferReceiverAccounts = new HashSet<IpcExecuteTransactionTransfer>();
            IpcExecuteTransactionTransferSenderAccounts = new HashSet<IpcExecuteTransactionTransfer>();
            Payments = new HashSet<Payment>();
        }

        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public int AccountType { get; set; }
        public int CardId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual CardsAccountType AccountTypeNavigation { get; set; }
        public virtual Card Card { get; set; }
        public virtual ICollection<CardsOverdraftOverduesHistory> CardsOverdraftOverduesHistories { get; set; }
        public virtual ICollection<InviteCustomer> InviteCustomers { get; set; }
        public virtual ICollection<IpcExecuteTransactionPayment> IpcExecuteTransactionPayments { get; set; }
        public virtual ICollection<IpcExecuteTransactionTransfer> IpcExecuteTransactionTransferReceiverAccounts { get; set; }
        public virtual ICollection<IpcExecuteTransactionTransfer> IpcExecuteTransactionTransferSenderAccounts { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
