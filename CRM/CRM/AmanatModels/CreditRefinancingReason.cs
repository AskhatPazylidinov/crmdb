﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditRefinancingReason
    {
        public CreditRefinancingReason()
        {
            Histories = new HashSet<History>();
        }

        public int ReasonId { get; set; }
        public string Reason { get; set; }

        public virtual ICollection<History> Histories { get; set; }
    }
}
