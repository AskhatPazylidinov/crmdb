﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OrganizationCorrAccountsView
    {
        public string CorrAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int? BranchId { get; set; }
        public int CorrAccountTypeId { get; set; }
        public string AccountName { get; set; }
    }
}
