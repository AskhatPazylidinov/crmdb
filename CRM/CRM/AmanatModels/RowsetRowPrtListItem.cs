﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RowsetRowPrtListItem
    {
        public int KyRowsetId { get; set; }
        public int KyRowId { get; set; }
        public int KyId { get; set; }
        public int KyPrttype { get; set; }
        public string FlAccountNum { get; set; }
        public string FlAccountBank { get; set; }
        public string FlAccountBic { get; set; }
        public int? FlAccountCountrycode { get; set; }
        public int? FlAccountState { get; set; }
        public string FlAccountAddress { get; set; }
        public double? FlSharesQty { get; set; }
        public int? FlParticipantPartickind { get; set; }
        public string FlParticipantRegnum { get; set; }
        public string FlParticipantPagenumref { get; set; }
        public string FlParticipantLegaladdressPostcode { get; set; }
        public string FlParticipantLegaladdressTowncode { get; set; }
        public string FlParticipantLegaladdressRegion { get; set; }
        public string FlParticipantLegaladdressArea { get; set; }
        public string FlParticipantLegaladdressTown { get; set; }
        public string FlParticipantLegaladdressStreet { get; set; }
        public string FlParticipantLegaladdressHouse { get; set; }
        public string FlParticipantLegaladdressCorp { get; set; }
        public string FlParticipantLegaladdressBuilding { get; set; }
        public string FlParticipantLegaladdressRoom { get; set; }
        public string FlParticipantNaturaladdressPostcode { get; set; }
        public string FlParticipantNaturaladdressTowncode { get; set; }
        public string FlParticipantNaturaladdressRegion { get; set; }
        public string FlParticipantNaturaladdressArea { get; set; }
        public string FlParticipantNaturaladdressTown { get; set; }
        public string FlParticipantNaturaladdressStreet { get; set; }
        public string FlParticipantNaturaladdressHouse { get; set; }
        public string FlParticipantNaturaladdressCorp { get; set; }
        public string FlParticipantNaturaladdressBuilding { get; set; }
        public string FlParticipantNaturaladdressRoom { get; set; }
        public int? FlParticipantJurnameOrgformcode { get; set; }
        public string FlParticipantJurnameOrgname { get; set; }
        public string FlParticipantJurnameBranch { get; set; }
        public string FlParticipantOkpo { get; set; }
        public int? FlParticipantForeignerResident { get; set; }
        public string FlParticipantForeignerRegnum { get; set; }
        public string FlParticipantForeignerOrgan { get; set; }
        public string FlParticipantAddinfoActivity { get; set; }
        public string FlParticipantAddinfoActivities { get; set; }
        public string FlParticipantDisponentName { get; set; }
        public int? FlParticipantDisponentDocCode { get; set; }
        public string FlParticipantDisponentDocSeries { get; set; }
        public string FlParticipantDisponentDocNum { get; set; }
        public DateTime? FlParticipantDisponentDocIssuedate { get; set; }
        public string FlParticipantDisponentDocOrgan { get; set; }
        public string FlParticipantPhysnameLastname { get; set; }
        public string FlParticipantPhysnameFirstname { get; set; }
        public string FlParticipantPhysnameMiddlename { get; set; }
        public string FlParticipantNationcode { get; set; }
        public int? FlParticipantOwnerFlag { get; set; }
        public string FlParticipantOwnerOkpo { get; set; }
        public string FlParticipantOwnerActivities { get; set; }
        public int? FlParticipantDocCode { get; set; }
        public string FlParticipantDocSeries { get; set; }
        public string FlParticipantDocNum { get; set; }
        public DateTime? FlParticipantDocIssuedate { get; set; }
        public string FlParticipantDocOrgan { get; set; }
        public DateTime? FlParticipantDocBirthdate { get; set; }
        public string FlParticipantDocBirthplace { get; set; }
    }
}
