﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FilesCounter
    {
        public DateTime BankDay { get; set; }
        public int FilesCount { get; set; }
    }
}
