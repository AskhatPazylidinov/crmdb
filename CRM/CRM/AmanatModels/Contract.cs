﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Contract
    {
        public Contract()
        {
            AuthorizedOperations = new HashSet<AuthorizedOperation>();
            Swift103Transfers = new HashSet<Swift103Transfer>();
        }

        public int ContractId { get; set; }
        public int CustomerId { get; set; }
        public int ContragentCustomerId { get; set; }
        public string ContractNo { get; set; }
        public DateTime StartDate { get; set; }
        public byte SummTypeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal? ContractSumm { get; set; }
        public DateTime? EndDate { get; set; }
        public string ContractTimeComment { get; set; }
        public string ContractPurposeComment { get; set; }

        public virtual Customer ContragentCustomer { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<AuthorizedOperation> AuthorizedOperations { get; set; }
        public virtual ICollection<Swift103Transfer> Swift103Transfers { get; set; }
    }
}
