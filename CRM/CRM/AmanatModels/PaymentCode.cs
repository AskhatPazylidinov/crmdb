﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string TreasuryCode { get; set; }
    }
}
