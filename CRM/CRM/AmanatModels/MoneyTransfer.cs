﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneyTransfer
    {
        public MoneyTransfer()
        {
            BerekeTransferInfos = new HashSet<BerekeTransferInfo>();
            GoldenCrownTransferInfos = new HashSet<GoldenCrownTransferInfo>();
            MoneyTransfersStatuses = new HashSet<MoneyTransfersStatus>();
            UnistreamTransferInfos = new HashSet<UnistreamTransferInfo>();
        }

        public long TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public int MoneySystemId { get; set; }
        public string PaymentCode { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrencyId { get; set; }
        public decimal TotalComission { get; set; }
        public decimal BankComission { get; set; }
        public decimal? ComissionReturnedOnCancel { get; set; }
        public string PaymentComment { get; set; }
        public int? SenderCustomerId { get; set; }
        public string SenderCustomerName { get; set; }
        public int? ReceiverCustomerId { get; set; }
        public string ReceiverCustomerName { get; set; }
        public int ContragentCountryId { get; set; }
        public byte DirectionTypeId { get; set; }
        public string ContrAccountNo { get; set; }
        public decimal SumOnContrAccount { get; set; }
        public string Locality { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Country2 ContragentCountry { get; set; }
        public virtual MoneySystem MoneySystem { get; set; }
        public virtual Customer ReceiverCustomer { get; set; }
        public virtual Customer SenderCustomer { get; set; }
        public virtual Currency3 TransferCurrency { get; set; }
        public virtual BankEmployeeAndLoanCustomer1 BankEmployeeAndLoanCustomer1 { get; set; }
        public virtual ContactTransferInfo ContactTransferInfo { get; set; }
        public virtual MoneyTransferMarketingSource MoneyTransferMarketingSource { get; set; }
        public virtual ICollection<BerekeTransferInfo> BerekeTransferInfos { get; set; }
        public virtual ICollection<GoldenCrownTransferInfo> GoldenCrownTransferInfos { get; set; }
        public virtual ICollection<MoneyTransfersStatus> MoneyTransfersStatuses { get; set; }
        public virtual ICollection<UnistreamTransferInfo> UnistreamTransferInfos { get; set; }
    }
}
