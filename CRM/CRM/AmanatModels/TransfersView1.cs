﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransfersView1
    {
        public long TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public decimal TransferSumm { get; set; }
        public int TransferCurrency { get; set; }
        public int SenderCustomerId { get; set; }
        public string SenderCustomerName { get; set; }
        public int ReceiverCustomerId { get; set; }
        public string ReceiverCustomerName { get; set; }
        public int? TransferStatus { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? OfficeId { get; set; }
    }
}
