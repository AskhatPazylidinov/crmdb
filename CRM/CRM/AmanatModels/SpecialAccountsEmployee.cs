﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecialAccountsEmployee
    {
        public int BranchId { get; set; }
        public string BalanceGroup { get; set; }
        public bool IsEmployee { get; set; }
        public string AccountNo { get; set; }
    }
}
