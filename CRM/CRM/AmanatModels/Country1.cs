﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Country1
    {
        public Country1()
        {
            Bank1s = new HashSet<Bank1>();
            Cities = new HashSet<City>();
            DocumentTypes = new HashSet<DocumentType>();
        }

        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SendCurrency { get; set; }
        public string RecCurrency { get; set; }
        public int IsFatf { get; set; }
        public string PpCode { get; set; }

        public virtual ICollection<Bank1> Bank1s { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<DocumentType> DocumentTypes { get; set; }
    }
}
