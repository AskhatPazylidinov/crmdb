﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Paysheet
    {
        public Paysheet()
        {
            PaysheetsEmployees = new HashSet<PaysheetsEmployee>();
            PaysheetsStatuses = new HashSet<PaysheetsStatus>();
        }

        public int PaysheetId { get; set; }
        public DateTime PaysheetDate { get; set; }
        public int BranchId { get; set; }
        public byte PartitionTypeId { get; set; }
        public int? OfficeId { get; set; }
        public byte PaysheetTypeId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual ICollection<PaysheetsEmployee> PaysheetsEmployees { get; set; }
        public virtual ICollection<PaysheetsStatus> PaysheetsStatuses { get; set; }
    }
}
