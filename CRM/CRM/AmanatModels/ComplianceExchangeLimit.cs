﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComplianceExchangeLimit
    {
        public DateTime ActualDate { get; set; }
        public decimal Limit { get; set; }
        public int CurrencyId { get; set; }
        public string Description { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
