﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsOperationDeviceType
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Id { get; set; }
        public int OperType { get; set; }
        public int DeviceType { get; set; }

        public virtual User User { get; set; }
    }
}
