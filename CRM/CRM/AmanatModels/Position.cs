﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Position
    {
        public int EmployeeId { get; set; }
        public DateTime ActualDate { get; set; }
        public string DepartmentName { get; set; }
        public string Position1 { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
