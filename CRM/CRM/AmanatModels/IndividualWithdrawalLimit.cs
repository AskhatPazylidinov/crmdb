﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualWithdrawalLimit
    {
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }
        public int LimitPeriod { get; set; }
        public int LimitPeriodTypeId { get; set; }
        public int WithdrawalCount { get; set; }
        public decimal LimitSumm { get; set; }
        public int LimitSummTypeId { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual IndividualWithdrawalLimitsDate IndividualWithdrawalLimitsDate { get; set; }
    }
}
