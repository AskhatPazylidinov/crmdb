﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesChange
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime ChangeDate { get; set; }
        public byte ChangeTypeId { get; set; }
        public decimal OldInterestRate { get; set; }
        public DateTime OldEndDate { get; set; }
        public string Comment { get; set; }
        public int? UserId { get; set; }
        public bool? ReservesIgnore { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
