﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BalanceGroupsBalance
    {
        public DateTime BalanceDate { get; set; }
        public string BalanceGroup { get; set; }
        public int BranchId { get; set; }
        public int CurrencyId { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }
        public decimal DtSumV { get; set; }
        public decimal DtSumN { get; set; }
        public decimal CtSumV { get; set; }
        public decimal CtSumN { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
