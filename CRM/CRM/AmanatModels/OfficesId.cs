﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesId
    {
        public int OfficeId { get; set; }
        public int OldOfficeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Office1 OldOffice { get; set; }
    }
}
