﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperation1
    {
        public RevertableOperation1()
        {
            RevertableClosedSecurities = new HashSet<RevertableClosedSecurity>();
            RevertableOperationsTransaction1s = new HashSet<RevertableOperationsTransaction1>();
            RevertableSecuritiesAccounts = new HashSet<RevertableSecuritiesAccount>();
        }

        public int RevertId { get; set; }
        public DateTime RevertDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<RevertableClosedSecurity> RevertableClosedSecurities { get; set; }
        public virtual ICollection<RevertableOperationsTransaction1> RevertableOperationsTransaction1s { get; set; }
        public virtual ICollection<RevertableSecuritiesAccount> RevertableSecuritiesAccounts { get; set; }
    }
}
