﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Option
    {
        public int OptionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? TypeId { get; set; }
    }
}
