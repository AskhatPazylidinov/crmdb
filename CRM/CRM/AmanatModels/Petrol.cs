﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Petrol
    {
        public int EmployeeId { get; set; }
        public DateTime CheckDate { get; set; }
        public decimal TotalAmount { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
