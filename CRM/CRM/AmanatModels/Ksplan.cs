﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Ksplan
    {
        public DateTime CreateDate { get; set; }
        public short Symbol { get; set; }
        public string Name { get; set; }
        public byte? IsInput { get; set; }
        public byte? ClientType { get; set; }
        public int? CashOperation { get; set; }
    }
}
