﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesAttorneysDeclension
    {
        public int AttorneyId { get; set; }
        public byte DeclensionTypeId { get; set; }
        public string AttorneyPosition { get; set; }
        public string AttorneyName { get; set; }

        public virtual OfficesAttorney Attorney { get; set; }
    }
}
