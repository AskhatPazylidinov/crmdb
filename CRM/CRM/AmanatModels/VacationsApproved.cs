﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class VacationsApproved
    {
        public VacationsApproved()
        {
            Recalls = new HashSet<Recall>();
        }

        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public int? TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? StatusId { get; set; }
        public int? DaysCount { get; set; }
        public decimal? AvgIncome { get; set; }
        public int? IncomeId { get; set; }
        public string OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Pdffile { get; set; }
        public decimal? PaymentSum { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? StatusUserId { get; set; }
        public string ResponisibleEmployee { get; set; }
        public int? ResponisibleEmployeeId { get; set; }
        public DateTime? PstartDate { get; set; }
        public DateTime? PendDate { get; set; }
        public int? RecallId { get; set; }
        public int? RetirePositionId { get; set; }
        public decimal? AddSalaryPercent { get; set; }
        public decimal? AddSalarySumm { get; set; }
        public int? IncomeTypeId { get; set; }

        public virtual Employee1 Employee { get; set; }
        public virtual ICollection<Recall> Recalls { get; set; }
    }
}
