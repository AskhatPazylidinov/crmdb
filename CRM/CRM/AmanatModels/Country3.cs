﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Country3
    {
        public Country3()
        {
            Bank2s = new HashSet<Bank2>();
            RiaTransfers = new HashSet<RiaTransfer>();
        }

        public int RiaCountryId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool OpenPayment { get; set; }
        public bool OpenNetwork { get; set; }
        public int? CountryId { get; set; }

        public virtual ICollection<Bank2> Bank2s { get; set; }
        public virtual ICollection<RiaTransfer> RiaTransfers { get; set; }
    }
}
