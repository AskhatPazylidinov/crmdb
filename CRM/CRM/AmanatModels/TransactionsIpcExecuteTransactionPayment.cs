﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsIpcExecuteTransactionPayment
    {
        public int IpcExecuteTransactionPaymentId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }
        public bool IsWithdrawal { get; set; }

        public virtual IpcExecuteTransactionPayment IpcExecuteTransactionPayment { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
