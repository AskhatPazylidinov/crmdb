﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Product2
    {
        public Product2()
        {
            DepositAccounts = new HashSet<DepositAccount>();
            DepositsBonusesPrograms = new HashSet<DepositsBonusesProgram>();
            K3depositAlloweds = new HashSet<K3depositAllowed>();
            PercentsProductTaxRates = new HashSet<PercentsProductTaxRate>();
            PlanningInFlows = new HashSet<PlanningInFlow>();
            ProductsBalanceGroup1s = new HashSet<ProductsBalanceGroup1>();
            ProductsChange1s = new HashSet<ProductsChange1>();
            ProductsDocument2s = new HashSet<ProductsDocument2>();
            ProductsPeriodsCalculationTypes = new HashSet<ProductsPeriodsCalculationType>();
        }

        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime OpenDate { get; set; }
        public string CloseOrderNo { get; set; }
        public DateTime? CloseDate { get; set; }
        public byte ProductTypeId { get; set; }
        public byte OrganizationTypeId { get; set; }
        public byte OpeningTypeFirstCurrentAccount { get; set; }
        public byte? WalletIntegration { get; set; }
        public byte ProductPeriodTypeId { get; set; }
        public bool BindingCurrentAccount { get; set; }
        public byte AccrualInterest { get; set; }
        public bool IsGraphic { get; set; }
        public bool? IsNotTaxeablePercents { get; set; }
        public byte AllowOpenFromInternetBanking { get; set; }
        public string CapitalizationAgreementMessage { get; set; }
        public string ConfirmationAgreementMessage { get; set; }
        public string ImageName { get; set; }

        public virtual ProductsGroupCode1 ProductsGroupCode1 { get; set; }
        public virtual ICollection<DepositAccount> DepositAccounts { get; set; }
        public virtual ICollection<DepositsBonusesProgram> DepositsBonusesPrograms { get; set; }
        public virtual ICollection<K3depositAllowed> K3depositAlloweds { get; set; }
        public virtual ICollection<PercentsProductTaxRate> PercentsProductTaxRates { get; set; }
        public virtual ICollection<PlanningInFlow> PlanningInFlows { get; set; }
        public virtual ICollection<ProductsBalanceGroup1> ProductsBalanceGroup1s { get; set; }
        public virtual ICollection<ProductsChange1> ProductsChange1s { get; set; }
        public virtual ICollection<ProductsDocument2> ProductsDocument2s { get; set; }
        public virtual ICollection<ProductsPeriodsCalculationType> ProductsPeriodsCalculationTypes { get; set; }
    }
}
