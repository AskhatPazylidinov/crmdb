﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProduct
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsForIndividuals { get; set; }
        public bool IsForCompanies { get; set; }
        public bool IsForResidents { get; set; }
        public bool IsForNonResidents { get; set; }
        public bool IsOwnMoneyRequired { get; set; }
        public byte? OwnMoneyAmount { get; set; }
        public byte CreditTypeId { get; set; }
        public byte? MinPeopleInGroup { get; set; }
        public byte? MaxPeopleInGroup { get; set; }
        public bool IsTranchesAllowed { get; set; }
        public byte? MaxTranches { get; set; }
        public byte? MaxTranchesAtOneTime { get; set; }
        public string AdditionalInformation { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime OpenOrderDate { get; set; }
        public string CloseOrderNo { get; set; }
        public DateTime? CloseOrderDate { get; set; }
        public int? PurposeTypeId { get; set; }
        public decimal? DiscountForPositiveCredit { get; set; }
        public decimal? DiscountInterestLimit { get; set; }
        public string RemarksForUser { get; set; }
        public byte? MaxTranchePeriod { get; set; }
        public bool HasPartners { get; set; }
        public byte? PartnerTypeId { get; set; }
        public bool? IsTrancheExtendable { get; set; }
        public byte? MaxDaysForTrancheCancellation { get; set; }
        public bool IsMortgageProduct { get; set; }
        public bool IsBusinessProduct { get; set; }
        public bool? IsInBonusSystem { get; set; }
        public byte ProductTypeId { get; set; }
        public byte IssueComissionPaymentTypeId { get; set; }
        public byte CalculationType { get; set; }
        public bool? UseOffBalanceAccounts { get; set; }
        public byte PercentsBaseTypeId { get; set; }
        public byte? PercentsCalculationSchemeTypeId { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public bool? IsTranchInsurance { get; set; }
        public byte? LoanPaymentOrderTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
