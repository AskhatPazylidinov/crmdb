﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Payment
    {
        public int Id { get; set; }
        public int PaymentType { get; set; }
        public long PaymentId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public bool IsWithdraw { get; set; }
        public decimal Amount { get; set; }
        public byte PayedStatus { get; set; }
        public bool IsThroughCash { get; set; }
        public string CardNo { get; set; }
        public long? Position { get; set; }
        public short? Positionn { get; set; }
        public long? OperationId { get; set; }

        public virtual Account Account { get; set; }
    }
}
