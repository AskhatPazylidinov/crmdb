﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Salary
    {
        public int EmployeeId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal Salary1 { get; set; }
        public int CurrencyId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
