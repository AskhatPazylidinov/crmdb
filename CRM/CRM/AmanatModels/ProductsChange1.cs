﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsChange1
    {
        public ProductsChange1()
        {
            ProductBonusesOnCooperationDates = new HashSet<ProductBonusesOnCooperationDate>();
            ProductsCapitalizationsDates = new HashSet<ProductsCapitalizationsDate>();
            ProductsCurrencies = new HashSet<ProductsCurrency>();
        }

        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool IsInvestable { get; set; }
        public bool IsWithdrawable { get; set; }
        public bool IsPercentWithdrawable { get; set; }
        public byte WithdrawPercentsPeriod { get; set; }
        public int InvestPeriodsCount { get; set; }
        public byte StartWithdrawMonths { get; set; }
        public int ProlongationsCount { get; set; }
        public byte? CapitalizationDateTypeId { get; set; }
        public byte? CapitalizationSummTypeId { get; set; }
        public byte? CapitalizationPercentsPeriod { get; set; }
        public int? GraphicPeriodDays { get; set; }
        public bool CanOpenOnlyAllowedCurrencies { get; set; }
        public int InitialInvestablePeriodsCount { get; set; }
        public byte PercentsBehaviorByProlongation { get; set; }
        public byte? WithdrawalTypeId { get; set; }

        public virtual Product2 Product { get; set; }
        public virtual ICollection<ProductBonusesOnCooperationDate> ProductBonusesOnCooperationDates { get; set; }
        public virtual ICollection<ProductsCapitalizationsDate> ProductsCapitalizationsDates { get; set; }
        public virtual ICollection<ProductsCurrency> ProductsCurrencies { get; set; }
    }
}
