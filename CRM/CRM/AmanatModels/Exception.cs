﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Exception
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string ClassName { get; set; }
        public DateTime ExceptionDate { get; set; }
        public int ExceptionCode { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
