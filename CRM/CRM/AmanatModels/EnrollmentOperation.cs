﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EnrollmentOperation
    {
        public long Id { get; set; }
        public int FileId { get; set; }
        public string AccountNo { get; set; }
        public decimal Amount { get; set; }
        public int RowNumber { get; set; }

        public virtual EnrollmentFile File { get; set; }
        public virtual EnrollmentIpcExecuteTransaction EnrollmentIpcExecuteTransaction { get; set; }
        public virtual EnrollmentTransaction EnrollmentTransaction { get; set; }
    }
}
