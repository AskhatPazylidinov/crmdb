﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IllnessType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
