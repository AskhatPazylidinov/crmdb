﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalScoreView
    {
        public int CustomerId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? NewScoreTypeId { get; set; }
        public int? CurrentScoreTypeId { get; set; }
        public int? NewScore { get; set; }
        public int? CurrentScore { get; set; }
        public bool? IsExclude { get; set; }
        public string Comment { get; set; }
        public int? ScoreTypeId { get; set; }
        public int? ScoreValue { get; set; }
        public string ScoreTypeName { get; set; }
    }
}
