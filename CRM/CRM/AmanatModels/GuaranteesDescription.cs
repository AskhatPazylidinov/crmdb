﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GuaranteesDescription
    {
        public int GaranteeId { get; set; }
        public string TypeName { get; set; }
        public string Descriptions { get; set; }
    }
}
