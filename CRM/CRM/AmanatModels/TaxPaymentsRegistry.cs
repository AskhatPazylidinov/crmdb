﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxPaymentsRegistry
    {
        public int OperationId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int DepartmentId { get; set; }
        public int ServiceId { get; set; }
        public string IdentificationNo { get; set; }
        public string Okpocode { get; set; }
        public string VehicleNumber { get; set; }
        public decimal PaymentSum { get; set; }
        public decimal CommissionSum { get; set; }
        public string DepartmentName { get; set; }
        public int DistrictId { get; set; }
        public int RegionId { get; set; }
        public int BranchId { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public string PaymentCode { get; set; }
        public long? TransactionIdbyGns { get; set; }
    }
}
