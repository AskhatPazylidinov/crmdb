﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesAccountsBalanceGroup1
    {
        public int ReserveAccountTypeId { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
