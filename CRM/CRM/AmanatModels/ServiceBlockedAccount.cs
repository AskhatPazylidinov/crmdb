﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceBlockedAccount
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal SumN { get; set; }
        public int ServiceTypeId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual BankService1 ServiceType { get; set; }
    }
}
