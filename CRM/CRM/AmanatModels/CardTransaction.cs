﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardTransaction
    {
        public int UserId { get; set; }
        public long Position { get; set; }
        public int? SpecialCashierType { get; set; }
    }
}
