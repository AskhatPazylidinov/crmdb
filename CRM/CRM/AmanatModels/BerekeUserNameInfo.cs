﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BerekeUserNameInfo
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }

        public virtual User User { get; set; }
    }
}
