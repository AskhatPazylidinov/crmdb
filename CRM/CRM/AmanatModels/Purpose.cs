﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Purpose
    {
        public Purpose()
        {
            Histories = new HashSet<History>();
            HistoriesObjects = new HashSet<HistoriesObject>();
            ProductPurposes = new HashSet<ProductPurpose>();
            ProductsPurposesBalanceGroups = new HashSet<ProductsPurposesBalanceGroup>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual SettingsCreditsPurpose SettingsCreditsPurpose { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<HistoriesObject> HistoriesObjects { get; set; }
        public virtual ICollection<ProductPurpose> ProductPurposes { get; set; }
        public virtual ICollection<ProductsPurposesBalanceGroup> ProductsPurposesBalanceGroups { get; set; }
    }
}
