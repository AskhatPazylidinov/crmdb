﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Version
    {
        public string CustomSchemaName { get; set; }
        public string ScriptPrefix { get; set; }
        public int? ScriptLastNumber { get; set; }
    }
}
