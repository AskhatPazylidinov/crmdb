﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxPayment
    {
        public int Id { get; set; }
        public DateTime PaymentDate { get; set; }
        public string TerminalId { get; set; }
        public string Rok { get; set; }
        public string ReciverBik { get; set; }
        public string Fio { get; set; }
        public string Inn { get; set; }
        public string DestinationCode { get; set; }
        public string CardAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal TransferSum { get; set; }
        public string AuthStan { get; set; }
        public DateTime AuthTime { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
    }
}
