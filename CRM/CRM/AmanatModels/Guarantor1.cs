﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Guarantor1
    {
        public int CreditId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal GuaranteeAmount { get; set; }
    }
}
