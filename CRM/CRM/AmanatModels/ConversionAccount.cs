﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionAccount
    {
        public int CurrencyId { get; set; }
        public byte AccountTypeId { get; set; }
        public string AccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
