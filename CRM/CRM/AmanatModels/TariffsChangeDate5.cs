﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate5
    {
        public TariffsChangeDate5()
        {
            Tariff4s = new HashSet<Tariff4>();
        }

        public DateTime ChangeDate { get; set; }

        public virtual ICollection<Tariff4> Tariff4s { get; set; }
    }
}
