﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElsomWalletPaymentResponse
    {
        public int OperationId { get; set; }
        public string TransactionNo { get; set; }
        public decimal Amount { get; set; }
    }
}
