﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchesOrdersNo
    {
        public int BranchId { get; set; }
        public int CurrentNo { get; set; }

        public virtual Branch Branch { get; set; }
    }
}
