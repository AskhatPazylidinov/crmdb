﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekTeplosetDictionaryFilename
    {
        public int FileId { get; set; }
        public string DictionaryFilename { get; set; }
    }
}
