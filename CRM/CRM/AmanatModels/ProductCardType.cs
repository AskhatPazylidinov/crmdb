﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductCardType
    {
        public int ProductId { get; set; }
        public int CardType { get; set; }

        public virtual CardType CardTypeNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
