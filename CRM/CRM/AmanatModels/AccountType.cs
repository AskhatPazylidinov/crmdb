﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BalanceGroup { get; set; }
        public int? Direction { get; set; }
    }
}
