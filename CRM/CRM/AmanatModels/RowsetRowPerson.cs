﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RowsetRowPerson
    {
        public int KyRowsetId { get; set; }
        public int KyRowId { get; set; }
        public int? FlOrgkind { get; set; }
        public string FlRegnum { get; set; }
        public string FlBankbic { get; set; }
        public string FlOkpo { get; set; }
        public int? FlOrgformcode { get; set; }
        public string FlPersonname { get; set; }
        public string FlBranch { get; set; }
        public string FlLegaladdressPostcode { get; set; }
        public string FlLegaladdressTowncode { get; set; }
        public string FlLegaladdressRegion { get; set; }
        public string FlLegaladdressArea { get; set; }
        public string FlLegaladdressTown { get; set; }
        public string FlLegaladdressStreet { get; set; }
        public string FlLegaladdressHouse { get; set; }
        public string FlLegaladdressCorp { get; set; }
        public string FlLegaladdressBuilding { get; set; }
        public string FlLegaladdressRoom { get; set; }
        public string FlNaturaladdressPostcode { get; set; }
        public string FlNaturaladdressTowncode { get; set; }
        public string FlNaturaladdressRegion { get; set; }
        public string FlNaturaladdressArea { get; set; }
        public string FlNaturaladdressTown { get; set; }
        public string FlNaturaladdressStreet { get; set; }
        public string FlNaturaladdressHouse { get; set; }
        public string FlNaturaladdressCorp { get; set; }
        public string FlNaturaladdressBuilding { get; set; }
        public string FlNaturaladdressRoom { get; set; }
        public string FlPerformerName { get; set; }
        public string FlPerformerPost { get; set; }
        public string FlPhone { get; set; }
    }
}
