﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerStatusesReason
    {
        public int Id { get; set; }
        public int CustomerStatusId { get; set; }
        public string Name { get; set; }
    }
}
