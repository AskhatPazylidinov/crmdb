﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferType
    {
        public TransferType()
        {
            AllowedCurrencies = new HashSet<AllowedCurrency>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AllowedCurrency> AllowedCurrencies { get; set; }
    }
}
