﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerVerificationDate
    {
        public int CustomerId { get; set; }
        public DateTime VerifyDate { get; set; }
        public DateTime VerifyTime { get; set; }
    }
}
