﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsTranchesLimit
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public int CurrencyId { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
