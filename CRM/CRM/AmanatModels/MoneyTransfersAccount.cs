﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneyTransfersAccount
    {
        public int OfficeId { get; set; }
        public int SystemId { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual MoneySystem System { get; set; }
    }
}
