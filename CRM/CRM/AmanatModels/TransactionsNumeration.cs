﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsNumeration
    {
        public int Id { get; set; }
        public bool? IsBalanceGroupRecalculate { get; set; }
        public bool IsNextDayOfficeExist { get; set; }
    }
}
