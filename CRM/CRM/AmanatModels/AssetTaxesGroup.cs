﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AssetTaxesGroup
    {
        public AssetTaxesGroup()
        {
            Types = new HashSet<Type>();
        }

        public int TaxGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? TaxDepreciationRate { get; set; }

        public virtual ICollection<Type> Types { get; set; }
    }
}
