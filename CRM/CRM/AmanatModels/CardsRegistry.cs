﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardsRegistry
    {
        public int CardId { get; set; }
        public int OfficeId { get; set; }
        public DateTime WithdrawDate { get; set; }
        public string CheckNumber { get; set; }
        public int CurrencyId { get; set; }
        public decimal WithdrawSum { get; set; }
        public decimal CommissionSum { get; set; }
        public int UserId { get; set; }
        public int CardType { get; set; }
        public string CardBin { get; set; }
        public string LogonCode { get; set; }
        public int OperType { get; set; }
        public DateTime OperationTime { get; set; }
        public int? SpecialCashierType { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
    }
}
