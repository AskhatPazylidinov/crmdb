﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChequeBookOperationsView
    {
        public DateTime TransactionDate { get; set; }
        public int CustomerId { get; set; }
        public string ChequeSeries { get; set; }
        public string ChequePage { get; set; }
        public decimal OperationSum { get; set; }
        public int CurrencyId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }
    }
}
