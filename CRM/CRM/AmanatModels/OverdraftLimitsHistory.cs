﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OverdraftLimitsHistory
    {
        public int CardId { get; set; }
        public int CreditId { get; set; }
        public DateTime CloseDate { get; set; }
        public int UserId { get; set; }

        public virtual Card Card { get; set; }
        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
