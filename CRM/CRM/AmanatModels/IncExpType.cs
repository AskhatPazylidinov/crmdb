﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IncExpType
    {
        public IncExpType()
        {
            NonResidentTaxIncExpTypes = new HashSet<NonResidentTaxIncExpType>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal? DefaultValue { get; set; }
        public bool? DefaultIsPercent { get; set; }
        public int DefaultAccountTypeId { get; set; }
        public bool? DefaultTax { get; set; }
        public bool? DefaultErSs { get; set; }
        public bool? DefaultEeSs { get; set; }
        public bool? DefaultTdc { get; set; }
        public bool? DefaultEd { get; set; }
        public int? Direction { get; set; }
        public bool? Tmp { get; set; }
        public bool? Per { get; set; }
        public string Comment { get; set; }
        public bool? UseInCalcVac { get; set; }
        public bool? UseInCalcIll { get; set; }
        public bool? UseInCalcHigh { get; set; }
        public bool UseInOtherIncome { get; set; }
        public bool UseInOtherOut { get; set; }
        public int? Period { get; set; }

        public virtual ICollection<NonResidentTaxIncExpType> NonResidentTaxIncExpTypes { get; set; }
    }
}
