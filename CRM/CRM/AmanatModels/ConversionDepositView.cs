﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConversionDepositView
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal? CurrentBalance { get; set; }
        public string CurrencyName { get; set; }
        public int CustomerId { get; set; }
        public DateTime OperationDate { get; set; }
    }
}
