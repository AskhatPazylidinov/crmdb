﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PercentsTaxRate
    {
        public int CustomerTypeId { get; set; }
        public DateTime ChangeDate { get; set; }
        public decimal TaxRate { get; set; }
        public bool IsResident { get; set; }
    }
}
