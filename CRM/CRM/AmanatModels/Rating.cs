﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Rating
    {
        public int RatingId { get; set; }
        public string Name { get; set; }
        public int Coefficient { get; set; }
    }
}
