﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Bank2
    {
        public int RiaBankId { get; set; }
        public int CountryId { get; set; }
        public string BankName { get; set; }

        public virtual Country3 Country { get; set; }
    }
}
