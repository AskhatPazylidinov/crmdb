﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsTest
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int CurrencyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public byte OperationTypeId { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }
        public byte TransactionTypeId { get; set; }
        public byte ExtendedTypeId { get; set; }
        public string Comment { get; set; }
        public byte StatusId { get; set; }
        public bool IsFinalTransaction { get; set; }
        public byte CashSymbol { get; set; }
        public DateTime DateV { get; set; }
        public int UserId { get; set; }
        public string ResponsiblePerson { get; set; }
        public int OfficeId { get; set; }
        public int? AttorneyId { get; set; }
        public string OperCode { get; set; }
        public int? CountryId { get; set; }
        public string AddComment { get; set; }
        public bool? CreditPayment { get; set; }
    }
}
