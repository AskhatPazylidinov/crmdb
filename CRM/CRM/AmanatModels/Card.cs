﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Card
    {
        public Card()
        {
            Accounts = new HashSet<Account>();
            CodeWordChangeLogs = new HashSet<CodeWordChangeLog>();
            OverdraftLimits = new HashSet<OverdraftLimit>();
            OverdraftLimitsHistories = new HashSet<OverdraftLimitsHistory>();
            PerformPayOffRequestsLogs = new HashSet<PerformPayOffRequestsLog>();
            PreparePayOffRequestsLogs = new HashSet<PreparePayOffRequestsLog>();
            Status2s = new HashSet<Status2>();
        }

        public int CardId { get; set; }
        public int ProductId { get; set; }
        public int CompanyId { get; set; }
        public string CardNo { get; set; }
        public int CardType { get; set; }
        public string Codeword { get; set; }
        public string PromoCode { get; set; }
        public string CustomerNameForPrint { get; set; }
        public int? CustomerCategoryId { get; set; }
        public bool AutoLoad { get; set; }
        public int OpenUserId { get; set; }
        public int LastChangeUserId { get; set; }
        public int? CloseUserId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool UseSmsInforming { get; set; }
        public int? RiskGroupId { get; set; }
        public int? SourceInformationId { get; set; }
        public int? CreditId { get; set; }

        public virtual CardType CardTypeNavigation { get; set; }
        public virtual User CloseUser { get; set; }
        public virtual Company Company { get; set; }
        public virtual History Credit { get; set; }
        public virtual CustomerCategory CustomerCategory { get; set; }
        public virtual User OpenUser { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductsCompany ProductsCompany { get; set; }
        public virtual RiskGroup RiskGroup { get; set; }
        public virtual InformationSource SourceInformation { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<CodeWordChangeLog> CodeWordChangeLogs { get; set; }
        public virtual ICollection<OverdraftLimit> OverdraftLimits { get; set; }
        public virtual ICollection<OverdraftLimitsHistory> OverdraftLimitsHistories { get; set; }
        public virtual ICollection<PerformPayOffRequestsLog> PerformPayOffRequestsLogs { get; set; }
        public virtual ICollection<PreparePayOffRequestsLog> PreparePayOffRequestsLogs { get; set; }
        public virtual ICollection<Status2> Status2s { get; set; }
    }
}
