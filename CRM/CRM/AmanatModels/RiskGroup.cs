﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiskGroup
    {
        public RiskGroup()
        {
            Cards = new HashSet<Card>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Card> Cards { get; set; }
    }
}
