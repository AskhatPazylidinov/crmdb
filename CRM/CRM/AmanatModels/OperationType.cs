﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationType
    {
        public OperationType()
        {
            ComissinEcvayers = new HashSet<ComissinEcvayer>();
            FileOperations = new HashSet<FileOperation>();
            IndividualTariffCompanies = new HashSet<IndividualTariffCompany>();
            OperationDeviceTypes = new HashSet<OperationDeviceType>();
            OperationTypeComments = new HashSet<OperationTypeComment>();
            ProductChanges = new HashSet<ProductChange>();
            SpecialAccount1s = new HashSet<SpecialAccount1>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CodeMpc { get; set; }
        public byte IsMain { get; set; }
        public byte IsComsission { get; set; }
        public byte IsExchange { get; set; }
        public byte? Priorety { get; set; }
        public bool InCash { get; set; }
        public bool IsAvailableForBackOffice { get; set; }

        public virtual ICollection<ComissinEcvayer> ComissinEcvayers { get; set; }
        public virtual ICollection<FileOperation> FileOperations { get; set; }
        public virtual ICollection<IndividualTariffCompany> IndividualTariffCompanies { get; set; }
        public virtual ICollection<OperationDeviceType> OperationDeviceTypes { get; set; }
        public virtual ICollection<OperationTypeComment> OperationTypeComments { get; set; }
        public virtual ICollection<ProductChange> ProductChanges { get; set; }
        public virtual ICollection<SpecialAccount1> SpecialAccount1s { get; set; }
    }
}
