﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NettingOperation
    {
        public NettingOperation()
        {
            NettingOperationsTransactions = new HashSet<NettingOperationsTransaction>();
        }

        public long NettingId { get; set; }
        public DateTime BankDate { get; set; }
        public int OfficeId { get; set; }
        public int SystemId { get; set; }
        public int CurrencyId { get; set; }
        public int Direction { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsStornoOperation { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual MoneySystem System { get; set; }
        public virtual ICollection<NettingOperationsTransaction> NettingOperationsTransactions { get; set; }
    }
}
