﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElectroServiceType
    {
        public UtilitiesElectroServiceType()
        {
            UtilitiesElectroPayments = new HashSet<UtilitiesElectroPayment>();
            UtilitiesElectroPaymentsExternalAccounts = new HashSet<UtilitiesElectroPaymentsExternalAccount>();
        }

        public int TypeId { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<UtilitiesElectroPayment> UtilitiesElectroPayments { get; set; }
        public virtual ICollection<UtilitiesElectroPaymentsExternalAccount> UtilitiesElectroPaymentsExternalAccounts { get; set; }
    }
}
