﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElectroPaymentsExternalAccount
    {
        public int DepartmentId { get; set; }
        public int ServiceTypeId { get; set; }
        public string ExternalAccountNo { get; set; }
        public string ExternalBikcode { get; set; }

        public virtual UtilitiesElectroDepartment Department { get; set; }
        public virtual UtilitiesElectroServiceType ServiceType { get; set; }
    }
}
