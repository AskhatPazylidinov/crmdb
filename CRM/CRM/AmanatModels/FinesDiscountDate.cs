﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FinesDiscountDate
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
    }
}
