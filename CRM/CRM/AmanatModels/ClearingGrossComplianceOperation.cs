﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ClearingGrossComplianceOperation
    {
        public long PaymentId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
        public bool IsApproved { get; set; }
    }
}
