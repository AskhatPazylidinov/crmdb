﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBranchConversionsSumm
    {
        public int OperationId { get; set; }
        public bool IsBranchBuy { get; set; }
        public int CurrencyId { get; set; }
        public decimal OperationSumm { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual CrossBranchConversion Operation { get; set; }
    }
}
