﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Purpose1
    {
        public int PurposeId { get; set; }
        public string PurposeName { get; set; }
    }
}
