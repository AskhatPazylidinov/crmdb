﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionAuthorizer
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int UserId { get; set; }
        public DateTime AuthorizeDate { get; set; }
    }
}
