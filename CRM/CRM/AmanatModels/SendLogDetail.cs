﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SendLogDetail
    {
        public int LogId { get; set; }
        public string XmlFileName { get; set; }
        public string XmlFile { get; set; }
    }
}
