﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PotentialBlackVisitorOperationView
    {
        public long Id { get; set; }
        public DateTime CheckDate { get; set; }
        public int CustomerId { get; set; }
        public int PersonId { get; set; }
        public int? OperationUserId { get; set; }
        public string OperationNote { get; set; }
        public string BlackListName { get; set; }
        public string PersonName { get; set; }
        public string CustomerFullname { get; set; }
        public string UserName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
    }
}
