﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductChange
    {
        public int ProductId { get; set; }
        public DateTime DateChange { get; set; }
        public int OperationType { get; set; }
        public int DeviceType { get; set; }
        public byte BankType { get; set; }
        public byte ComisionType { get; set; }
        public decimal BankValue { get; set; }
        public decimal NbkrValue { get; set; }
        public decimal CashBack { get; set; }

        public virtual DeviceType DeviceTypeNavigation { get; set; }
        public virtual OperationType OperationTypeNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
