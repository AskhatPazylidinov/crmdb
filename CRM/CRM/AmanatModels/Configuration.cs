﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Configuration
    {
        public int ConfigurationId { get; set; }
        public string ConfigValue { get; set; }
    }
}
