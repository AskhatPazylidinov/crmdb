﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate2
    {
        public TariffsChangeDate2()
        {
            Tariff1s = new HashSet<Tariff1>();
        }

        public DateTime ChangeDate { get; set; }

        public virtual ICollection<Tariff1> Tariff1s { get; set; }
    }
}
