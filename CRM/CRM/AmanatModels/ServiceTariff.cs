﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceTariff
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ServiceTypeId { get; set; }
        public int Interval { get; set; }
        public decimal ComissionSumm { get; set; }
        public bool IsActive { get; set; }

        public virtual BankService1 ServiceType { get; set; }
    }
}
