﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondService
    {
        public SocialFondService()
        {
            UtilitiesSocialFondPayments = new HashSet<UtilitiesSocialFondPayment>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ShortServiceName { get; set; }
        public string PaymentCode { get; set; }
        public int PaymentTypeId { get; set; }
        public int? PayerTypeId { get; set; }

        public virtual ICollection<UtilitiesSocialFondPayment> UtilitiesSocialFondPayments { get; set; }
    }
}
