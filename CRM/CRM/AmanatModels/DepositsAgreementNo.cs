﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsAgreementNo
    {
        public int Id { get; set; }
        public int OfficeId { get; set; }
        public int CurrentNo { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
