﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsBalanceGroup1
    {
        public int ProductId { get; set; }
        public byte AccountTypeId { get; set; }
        public string ResidentGroup { get; set; }
        public string NonResidentGroup { get; set; }
        public byte CurrencyTypeId { get; set; }

        public virtual BalanceGroup NonResidentGroupNavigation { get; set; }
        public virtual Product2 Product { get; set; }
        public virtual BalanceGroup ResidentGroupNavigation { get; set; }
    }
}
