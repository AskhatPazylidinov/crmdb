﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesContactPointCode
    {
        public int OfficeId { get; set; }
        public string PointCode { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
