﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsProduct
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public bool? IsForIndividuals { get; set; }
        public bool IsForCompanies { get; set; }
        public bool? IsForResidents { get; set; }
        public bool IsForNonResidents { get; set; }
        public bool IsPromo { get; set; }
        public DateTime? OpenDate { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime? CloseDate { get; set; }
        public string CloseOrderNo { get; set; }
        public int Period { get; set; }

        public virtual User User { get; set; }
    }
}
