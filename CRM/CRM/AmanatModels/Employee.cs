﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Employee
    {
        public Employee()
        {
            Aliments = new HashSet<Aliment>();
            Children = new HashSet<Child>();
            EmployeesAllowancesIgnores = new HashSet<EmployeesAllowancesIgnore>();
            EmployeesOffices = new HashSet<EmployeesOffice>();
            EmployeesPhones = new HashSet<EmployeesPhone>();
            EmployeesTypes = new HashSet<EmployeesType>();
            HigherEducations = new HashSet<HigherEducation>();
            Illnesses = new HashSet<Illness>();
            LoansSettings = new HashSet<LoansSetting>();
            OrdersEmployees = new HashSet<OrdersEmployee>();
            OverallPhonesLimits = new HashSet<OverallPhonesLimit>();
            PayrollsEmployees = new HashSet<PayrollsEmployee>();
            PaysheetsEmployees = new HashSet<PaysheetsEmployee>();
            Penalties = new HashSet<Penalty>();
            PensionsFundsSettings = new HashSet<PensionsFundsSetting>();
            Petrols = new HashSet<Petrol>();
            PhonesExpenses = new HashSet<PhonesExpense>();
            Positions = new HashSet<Position>();
            Salaries = new HashSet<Salary>();
            TrainingCourses = new HashSet<TrainingCourse>();
            WorkIntervals = new HashSet<WorkInterval>();
            WorkedHours = new HashSet<WorkedHour>();
        }

        public int EmployeeId { get; set; }

        public virtual Customer EmployeeNavigation { get; set; }
        public virtual Pensioner Pensioner { get; set; }
        public virtual ICollection<Aliment> Aliments { get; set; }
        public virtual ICollection<Child> Children { get; set; }
        public virtual ICollection<EmployeesAllowancesIgnore> EmployeesAllowancesIgnores { get; set; }
        public virtual ICollection<EmployeesOffice> EmployeesOffices { get; set; }
        public virtual ICollection<EmployeesPhone> EmployeesPhones { get; set; }
        public virtual ICollection<EmployeesType> EmployeesTypes { get; set; }
        public virtual ICollection<HigherEducation> HigherEducations { get; set; }
        public virtual ICollection<Illness> Illnesses { get; set; }
        public virtual ICollection<LoansSetting> LoansSettings { get; set; }
        public virtual ICollection<OrdersEmployee> OrdersEmployees { get; set; }
        public virtual ICollection<OverallPhonesLimit> OverallPhonesLimits { get; set; }
        public virtual ICollection<PayrollsEmployee> PayrollsEmployees { get; set; }
        public virtual ICollection<PaysheetsEmployee> PaysheetsEmployees { get; set; }
        public virtual ICollection<Penalty> Penalties { get; set; }
        public virtual ICollection<PensionsFundsSetting> PensionsFundsSettings { get; set; }
        public virtual ICollection<Petrol> Petrols { get; set; }
        public virtual ICollection<PhonesExpense> PhonesExpenses { get; set; }
        public virtual ICollection<Position> Positions { get; set; }
        public virtual ICollection<Salary> Salaries { get; set; }
        public virtual ICollection<TrainingCourse> TrainingCourses { get; set; }
        public virtual ICollection<WorkInterval> WorkIntervals { get; set; }
        public virtual ICollection<WorkedHour> WorkedHours { get; set; }
    }
}
