﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualWithdrawalLimitsDate
    {
        public IndividualWithdrawalLimitsDate()
        {
            IndividualWithdrawalLimits = new HashSet<IndividualWithdrawalLimit>();
        }

        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
        public virtual ICollection<IndividualWithdrawalLimit> IndividualWithdrawalLimits { get; set; }
    }
}
