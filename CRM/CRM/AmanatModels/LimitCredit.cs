﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitCredit
    {
        public int LimitId { get; set; }
        public int CreditId { get; set; }

        public virtual History Credit { get; set; }
        public virtual History Limit { get; set; }
    }
}
