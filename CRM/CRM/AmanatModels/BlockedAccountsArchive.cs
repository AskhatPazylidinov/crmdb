﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BlockedAccountsArchive
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public byte LockTypeId { get; set; }
        public DateTime ArchiveDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string LockMessage { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public double? MinBalance { get; set; }
        public bool? IsApprove { get; set; }
        public int? ApprovedUserId { get; set; }
    }
}
