﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Region3
    {
        public int Id { get; set; }
        public int? CountryId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public int? VirtualBankId { get; set; }
        public bool? Status { get; set; }
        public string NameLat { get; set; }

        public virtual Country5 Country { get; set; }
    }
}
