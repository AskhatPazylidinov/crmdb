﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrBanksTemplateDocument
    {
        public int DocumentId { get; set; }
        public string CorrBankCode { get; set; }
        public string Filename { get; set; }
    }
}
