﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GaranteesMarketPrice
    {
        public int GaranteeId { get; set; }
        public DateTime MarketDate { get; set; }
        public decimal MarketSumm { get; set; }
        public int CurrencyId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Garantee Garantee { get; set; }
    }
}
