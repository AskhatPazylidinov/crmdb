﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesCancelIssueLog
    {
        public int CreditId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? UserId { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
