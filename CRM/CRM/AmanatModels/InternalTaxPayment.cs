﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InternalTaxPayment
    {
        public int OperationId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual TaxPayment1 Operation { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
