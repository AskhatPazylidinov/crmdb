﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GarantType1
    {
        public int? OldTypeId { get; set; }
        public int? NewTypeId { get; set; }
    }
}
