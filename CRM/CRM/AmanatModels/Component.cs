﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Component
    {
        public Component()
        {
            ComponentToSecuredOperations = new HashSet<ComponentToSecuredOperation>();
            InverseParent = new HashSet<Component>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public bool? IsOnMenu { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public bool InDialog { get; set; }

        public virtual Component Parent { get; set; }
        public virtual ICollection<ComponentToSecuredOperation> ComponentToSecuredOperations { get; set; }
        public virtual ICollection<Component> InverseParent { get; set; }
    }
}
