﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBaseElectroProviderDepartment
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int DepartmentId { get; set; }

        public virtual UtilitiesElectroDepartment Department { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
