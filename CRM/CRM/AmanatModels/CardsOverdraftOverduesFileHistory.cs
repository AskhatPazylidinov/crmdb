﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardsOverdraftOverduesFileHistory
    {
        public CardsOverdraftOverduesFileHistory()
        {
            CardsOverdraftOverduesHistories = new HashSet<CardsOverdraftOverduesHistory>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public string FileName { get; set; }

        public virtual ICollection<CardsOverdraftOverduesHistory> CardsOverdraftOverduesHistories { get; set; }
    }
}
