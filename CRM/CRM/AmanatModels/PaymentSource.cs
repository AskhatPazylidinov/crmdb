﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentSource
    {
        public PaymentSource()
        {
            Histories = new HashSet<History>();
            ProductsPaymentSources = new HashSet<ProductsPaymentSource>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<ProductsPaymentSource> ProductsPaymentSources { get; set; }
    }
}
