﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsFine
    {
        public int LogId { get; set; }
        public int FinesId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? FineForLatePercentsPayment { get; set; }
        public byte? FineForLatePercentsPaymentType { get; set; }
        public byte? FineForLatePercentsCalculationType { get; set; }
        public byte? FineForLatePercentsBaseType { get; set; }
        public decimal? FineForLateMainSummPayment { get; set; }
        public byte? FineForLateMainSummPaymentType { get; set; }
        public byte? FineForLateMainSummCalculationType { get; set; }
        public byte? FineForLateMainSummBaseType { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
