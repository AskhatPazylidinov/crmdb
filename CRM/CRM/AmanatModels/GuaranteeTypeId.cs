﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GuaranteeTypeId
    {
        public string Code { get; set; }
        public int GuaranteeTypeId1 { get; set; }

        public virtual GuaranteeType CodeNavigation { get; set; }
        public virtual GarantType GuaranteeTypeId1Navigation { get; set; }
    }
}
