﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExcludingScoreParameterValue
    {
        public int Id { get; set; }
        public byte ScoringParameterTypeId { get; set; }
        public byte OperationId { get; set; }
        public string ScoreParameterValue { get; set; }
        public string Comment { get; set; }
    }
}
