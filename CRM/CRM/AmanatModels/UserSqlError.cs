﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UserSqlError
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
