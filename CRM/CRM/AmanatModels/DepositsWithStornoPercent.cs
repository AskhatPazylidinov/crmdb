﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsWithStornoPercent
    {
        public string MainAccountNo { get; set; }
        public int? CurrencyId { get; set; }
    }
}
