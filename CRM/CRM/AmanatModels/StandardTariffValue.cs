﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class StandardTariffValue
    {
        public StandardTariffValue()
        {
            StandardTariffDetails = new HashSet<StandardTariffDetail>();
        }

        public int TariffId { get; set; }
        public decimal? TariffRate { get; set; }
        public bool? IsPercent { get; set; }
        public byte CustomerTypeId { get; set; }
        public decimal AddTarifRate { get; set; }

        public virtual StandardTariff Tariff { get; set; }
        public virtual ICollection<StandardTariffDetail> StandardTariffDetails { get; set; }
    }
}
