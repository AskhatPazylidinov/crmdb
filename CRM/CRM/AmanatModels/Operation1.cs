﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Operation1
    {
        public Operation1()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int Id { get; set; }
        public DateTime DateOper { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public decimal SumV { get; set; }
        public string TypeOper { get; set; }
        public decimal? SumFee { get; set; }
        public string DeviceCode { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string CardAccountNo { get; set; }
        public string CodeLogon { get; set; }
        public byte FileType { get; set; }
        public string ParentDeviceCode { get; set; }
        public string LineNumber { get; set; }
        public string Cmi { get; set; }
        public string DeviceType { get; set; }
        public string DeviceName { get; set; }
        public string MerchantId { get; set; }
        public int? ParentId { get; set; }
        public string Mcc { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
