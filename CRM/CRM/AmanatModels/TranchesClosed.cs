﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TranchesClosed
    {
        public int CreditId { get; set; }
        public DateTime TranchesCloseDate { get; set; }
        public string Description { get; set; }

        public virtual History Credit { get; set; }
    }
}
