﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesView
    {
        public int EmployeeId { get; set; }
        public string CustomerName { get; set; }
    }
}
