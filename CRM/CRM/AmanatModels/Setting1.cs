﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Setting1
    {
        public string BankName { get; set; }
        public string ManagerPosition { get; set; }
        public string ManagerName { get; set; }
        public string AccountantPosition { get; set; }
        public string AccountantName { get; set; }
        public string NationalBankAccountNo { get; set; }
        public string BankNameEn { get; set; }
        public string NationalBankLicenseNo { get; set; }
        public string SwiftBankCode { get; set; }
        public string SwiftBankShortCode { get; set; }
        public string WebSite { get; set; }
        public string Email { get; set; }
        public string Customization { get; set; }
    }
}
