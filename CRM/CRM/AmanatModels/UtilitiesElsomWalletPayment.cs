﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesElsomWalletPayment
    {
        public int OperationId { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public decimal Amount { get; set; }
        public decimal Commission { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
