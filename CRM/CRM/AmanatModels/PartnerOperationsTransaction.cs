﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnerOperationsTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
