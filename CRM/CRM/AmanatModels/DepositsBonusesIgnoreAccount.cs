﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DepositsBonusesIgnoreAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
