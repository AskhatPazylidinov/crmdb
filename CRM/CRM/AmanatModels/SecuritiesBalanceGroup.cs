﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesBalanceGroup
    {
        public int SecuritiesTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public string BalanceGroup { get; set; }

        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
