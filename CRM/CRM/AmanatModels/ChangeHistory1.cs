﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeHistory1
    {
        public int Id { get; set; }
        public long TransferId { get; set; }
        public string ChangedField { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int UserId { get; set; }
        public DateTime ChangedDate { get; set; }

        public virtual SwiftTransfer Transfer { get; set; }
    }
}
