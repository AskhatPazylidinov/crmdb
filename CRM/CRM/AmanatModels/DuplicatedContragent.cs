﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DuplicatedContragent
    {
        public int OriginatorCustomerId { get; set; }
        public int ContragentCustomerId { get; set; }
        public int MatchedCustomerId { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual Customer ContragentCustomer { get; set; }
        public virtual Customer MatchedCustomer { get; set; }
        public virtual Customer OriginatorCustomer { get; set; }
    }
}
