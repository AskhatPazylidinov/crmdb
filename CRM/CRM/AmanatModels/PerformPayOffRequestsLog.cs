﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PerformPayOffRequestsLog
    {
        public int CardId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime RequestTime { get; set; }
        public int UserId { get; set; }

        public virtual Card Card { get; set; }
    }
}
