﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesEarlyPaymentComission
    {
        public int CreditId { get; set; }
        public byte Period { get; set; }
        public decimal? RequestPartialComission { get; set; }
        public byte? RequestPartialComissionType { get; set; }
        public decimal? RequestFullPaymentComission { get; set; }
        public byte? RequestFullPaymentComissionType { get; set; }
        public decimal? ApprovedPartialComission { get; set; }
        public byte? ApprovedPartialComissionType { get; set; }
        public decimal? ApprovedFullPaymentComission { get; set; }
        public byte? ApprovedFullPaymentComissionType { get; set; }

        public virtual History Credit { get; set; }
    }
}
