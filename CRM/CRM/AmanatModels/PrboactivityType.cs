﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PrboactivityType
    {
        public int NationalBankActivityTypeId { get; set; }
        public string ActivityTypeName { get; set; }
    }
}
