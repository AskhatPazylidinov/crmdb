﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportedUtilitiesPayment
    {
        public int UtilityId { get; set; }
        public int ImportFileId { get; set; }

        public virtual UtilitiesImportFile ImportFile { get; set; }
        public virtual UtilitiesRegistry Utility { get; set; }
    }
}
