﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankType
    {
        public int Id { get; set; }
        public string BankTypeName { get; set; }
    }
}
