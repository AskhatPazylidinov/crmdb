﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceLog
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }

        public virtual BankService1 ServiceType { get; set; }
    }
}
