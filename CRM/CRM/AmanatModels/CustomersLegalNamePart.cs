﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersLegalNamePart
    {
        public int CustomerId { get; set; }
        public int WordId { get; set; }
        public string Word { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
