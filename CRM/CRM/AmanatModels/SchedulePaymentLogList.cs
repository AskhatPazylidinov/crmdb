﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SchedulePaymentLogList
    {
        public int LogId { get; set; }
        public int BranchId { get; set; }
        public int OfficeId { get; set; }
        public int PaymentId { get; set; }
        public byte? PaymentDay { get; set; }
        public int DebetCurrencyId { get; set; }
        public string DebitAccountNo { get; set; }
        public string DebetAccountName { get; set; }
        public int CreditCurrencyId { get; set; }
        public string CreditAccountNo { get; set; }
        public string CreditAccountName { get; set; }
        public byte TransactionDirection { get; set; }
        public decimal Summa { get; set; }
        public int UserId { get; set; }
        public DateTime LogDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool Result { get; set; }
        public string Comment { get; set; }
    }
}
