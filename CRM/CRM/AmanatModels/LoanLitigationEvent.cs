﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LoanLitigationEvent
    {
        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public int LoanLitigationId { get; set; }
        public int LitigationReferenceId { get; set; }
        public DateTime? PlanDate { get; set; }
        public DateTime? FactDate { get; set; }

        public virtual LitigationReference LitigationReference { get; set; }
        public virtual LoanLitigation LoanLitigation { get; set; }
    }
}
