﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccessToken
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime IssueDate { get; set; }
    }
}
