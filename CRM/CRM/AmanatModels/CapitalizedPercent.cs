﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CapitalizedPercent
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime CapitalizeDate { get; set; }
        public decimal CapitalizeSumm { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
