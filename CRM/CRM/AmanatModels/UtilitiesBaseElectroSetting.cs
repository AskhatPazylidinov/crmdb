﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBaseElectroSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
    }
}
