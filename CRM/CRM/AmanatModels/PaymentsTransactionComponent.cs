﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsTransactionComponent
    {
        public int? CreditId { get; set; }
        public int? TranchId { get; set; }
        public int? PaymentType { get; set; }
        public int? ComponentId { get; set; }
        public decimal? PaymentSumm { get; set; }
        public string CalculationComment { get; set; }
        public long? Position { get; set; }
    }
}
