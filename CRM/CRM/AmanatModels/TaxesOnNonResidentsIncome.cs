﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxesOnNonResidentsIncome
    {
        public long TaxOperationId { get; set; }
        public int OfficeId { get; set; }
        public int SystemId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual MoneySystem System { get; set; }
    }
}
