﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountsRevaluation
    {
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public string RevaluationAccount { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Account1 Account1Navigation { get; set; }
    }
}
