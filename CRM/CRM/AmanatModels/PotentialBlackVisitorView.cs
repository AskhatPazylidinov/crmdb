﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PotentialBlackVisitorView
    {
        public int PersonId { get; set; }
        public bool? IsApproved { get; set; }
        public int BlackListId { get; set; }
        public string Department { get; set; }
        public byte? StopActionId { get; set; }
        public DateTime CheckDate { get; set; }
        public int CustomerId { get; set; }
        public int? OperationUserId { get; set; }
        public DateTime CheckDateTime { get; set; }
        public string Description { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportIssueDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public string IdentificationNo { get; set; }
        public int? BlockTypeId { get; set; }
    }
}
