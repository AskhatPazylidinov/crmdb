﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsApproveType
    {
        public int ProductId { get; set; }
        public int ApproveTypeId { get; set; }

        public virtual IncomeApproveType ApproveType { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
