﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CommonCreditLineScheduleView
    {
        public int CreditId { get; set; }
        public DateTime OperationDate { get; set; }
        public decimal? MainSum { get; set; }
        public decimal? Interests { get; set; }
        public DateTime? BillingStartDate { get; set; }
        public decimal? LimitSumm { get; set; }
        public decimal? OffBalance { get; set; }
        public decimal? LoanBalance { get; set; }
    }
}
