﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Attestation
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public string Result { get; set; }
        public string ShortDescription { get; set; }
        public DateTime? NextDate { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
