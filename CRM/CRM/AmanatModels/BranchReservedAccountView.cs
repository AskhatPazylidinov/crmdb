﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchReservedAccountView
    {
        public int BranchId { get; set; }
        public int AccountTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public string BalanceGroup { get; set; }
    }
}
