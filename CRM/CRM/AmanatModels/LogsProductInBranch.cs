﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsProductInBranch
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int BranchId { get; set; }

        public virtual User User { get; set; }
    }
}
