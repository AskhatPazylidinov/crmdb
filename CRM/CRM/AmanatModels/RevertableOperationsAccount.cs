﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperationsAccount
    {
        public long RevertId { get; set; }
        public int TranchId { get; set; }
        public int AccountTypeId { get; set; }
        public DateTime PrevLastCalcDate { get; set; }
        public decimal PrevSumV { get; set; }

        public virtual RevertableOperation Revert { get; set; }
    }
}
