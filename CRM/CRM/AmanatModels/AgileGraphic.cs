﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgileGraphic
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public int GraphicId { get; set; }
        public DateTime AgileDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
