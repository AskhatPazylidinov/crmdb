﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RegionCode
    {
        public int RegionId { get; set; }
        public string RegionCode1 { get; set; }

        public virtual Region1 Region { get; set; }
    }
}
