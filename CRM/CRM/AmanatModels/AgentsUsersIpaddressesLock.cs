﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsUsersIpaddressesLock
    {
        public long Id { get; set; }
        public DateTime LockDate { get; set; }
        public string Ipaddress { get; set; }
        public bool? Enabled { get; set; }
    }
}
