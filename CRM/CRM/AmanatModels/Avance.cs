﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Avance
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Summa { get; set; }
        public long? Position { get; set; }
        public string Comment { get; set; }
        public int? Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public bool? Tax { get; set; }
        public bool? Sf { get; set; }
        public decimal? SummaToPay { get; set; }
        public bool? OfficeScope { get; set; }
        public bool? Bonus { get; set; }
        public int? ExpId { get; set; }
        public long? PositionPay { get; set; }
        public int? IsRetired { get; set; }
        public int? GroupId { get; set; }
        public int? Period { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
