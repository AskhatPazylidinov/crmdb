﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsFileOperation
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public byte FileType { get; set; }
        public int OperType { get; set; }

        public virtual User User { get; set; }
    }
}
