﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FilePath
    {
        public byte PaymentTypeId { get; set; }
        public byte PaymentDirectionId { get; set; }
        public string Path { get; set; }
    }
}
