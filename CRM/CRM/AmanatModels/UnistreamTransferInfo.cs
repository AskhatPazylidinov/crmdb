﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UnistreamTransferInfo
    {
        public int Id { get; set; }
        public string UnistreamIdentity { get; set; }
        public DateTime TransactionDate { get; set; }
        public string UnistreamSignDocument { get; set; }
        public Guid UnistreamOperationId { get; set; }
        public long TransferId { get; set; }
        public string Document { get; set; }
        public Guid? RevokedTransferOperationId { get; set; }
        public string RevokedTransferSignDocument { get; set; }
        public string ChangeReceiverDocument { get; set; }
        public string ExpectedRecipientName { get; set; }

        public virtual MoneyTransfer Transfer { get; set; }
    }
}
