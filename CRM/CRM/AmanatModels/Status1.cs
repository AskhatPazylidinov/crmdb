﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Status1
    {
        public int Id { get; set; }
        public int BlackListId { get; set; }
        public int BlockTypeId { get; set; }
        public bool UseAlert { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }

        public virtual BlackList BlackList { get; set; }
    }
}
