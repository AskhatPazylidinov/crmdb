﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SalariesSetting
    {
        public DateTime ActualDate { get; set; }
        public decimal MonthPremium { get; set; }
        public decimal AdvancePayment { get; set; }
    }
}
