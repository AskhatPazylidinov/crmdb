﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SafeTariffsValue
    {
        public DateTime ChangeDate { get; set; }
        public int SafeTypeId { get; set; }
        public int CustomerTypeId { get; set; }
        public int DaysFrom { get; set; }
        public int? DaysTo { get; set; }
        public decimal SummPerDay { get; set; }

        public virtual SafeTariff SafeTariff { get; set; }
    }
}
