﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonCashOperation
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public byte OperationTypeId { get; set; }
        public decimal? Rate { get; set; }

        public virtual NonCashOperationsType OperationType { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
