﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Bank4
    {
        public Bank4()
        {
            AllowedCurrencies = new HashSet<AllowedCurrency>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string NameLat { get; set; }
        public int? PhoneCountryId { get; set; }
        public string PhoneAreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExt { get; set; }
        public byte? PhoneType { get; set; }
        public int? RegionId { get; set; }
        public string Street { get; set; }
        public string StreetLat { get; set; }
        public string House { get; set; }
        public string HouseLat { get; set; }
        public string Building { get; set; }
        public string BuildingLat { get; set; }
        public string PostalCode { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longtitude { get; set; }
        public byte? BankType { get; set; }
        public bool? FlagsSendsTransfer { get; set; }
        public bool? FlagsPaysTransfer { get; set; }
        public bool? FlagsSendsAddressTransfer { get; set; }
        public bool? FlagsPaysAddressTransfer { get; set; }
        public bool? FlagsSendsAddresslessTransfer { get; set; }
        public bool? FlagsPaysAddresslessTransfer { get; set; }
        public bool? FlagsSendsCreditPayments { get; set; }
        public bool? FlagsPaysCreditPayments { get; set; }
        public bool? FlagsTestMode { get; set; }
        public string SourceId { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<AllowedCurrency> AllowedCurrencies { get; set; }
    }
}
