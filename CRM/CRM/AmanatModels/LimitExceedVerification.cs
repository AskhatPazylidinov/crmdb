﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LimitExceedVerification
    {
        public long TransferId { get; set; }
        public DateTime VerifyDate { get; set; }
        public string Comment { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }

        public virtual Swift103Transfer Transfer { get; set; }
    }
}
