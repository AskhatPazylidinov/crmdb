﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Order
    {
        public Order()
        {
            OrdersEmployees = new HashSet<OrdersEmployee>();
            OrdersPositions = new HashSet<OrdersPosition>();
            OrdersStatuses = new HashSet<OrdersStatus>();
        }

        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public int BranchId { get; set; }
        public byte TypeId { get; set; }
        public string OrderNo { get; set; }
        public string OrderText { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Vacation Vacation { get; set; }
        public virtual ICollection<OrdersEmployee> OrdersEmployees { get; set; }
        public virtual ICollection<OrdersPosition> OrdersPositions { get; set; }
        public virtual ICollection<OrdersStatus> OrdersStatuses { get; set; }
    }
}
