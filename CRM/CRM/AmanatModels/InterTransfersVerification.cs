﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterTransfersVerification
    {
        public long TransferId { get; set; }
        public DateTime ConfirmDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual InterTransfer Transfer { get; set; }
    }
}
