﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Setting3
    {
        public string BicCode { get; set; }
        public string BranchCode { get; set; }
        public string LogicalTerminalCode { get; set; }
        public string InstitutionName { get; set; }
        public string CityHeading { get; set; }
        public string CountryCode { get; set; }
        public string InstitutionNameRu { get; set; }
        public string CityHeadingRu { get; set; }
    }
}
