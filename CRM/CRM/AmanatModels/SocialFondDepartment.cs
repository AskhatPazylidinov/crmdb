﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondDepartment
    {
        public SocialFondDepartment()
        {
            SocialFondDepartmentsExternalAccounts = new HashSet<SocialFondDepartmentsExternalAccount>();
            UtilitiesSocialFondPayments = new HashSet<UtilitiesSocialFondPayment>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentAddress { get; set; }
        public bool TakeUserCommentAsClearingComment { get; set; }
        public bool? IsBudgetPayments { get; set; }
        public int? GovernmentId { get; set; }

        public virtual SocialFondGovernment Government { get; set; }
        public virtual ICollection<SocialFondDepartmentsExternalAccount> SocialFondDepartmentsExternalAccounts { get; set; }
        public virtual ICollection<UtilitiesSocialFondPayment> UtilitiesSocialFondPayments { get; set; }
    }
}
