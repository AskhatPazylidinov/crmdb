﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RowsetRow
    {
        public int KyId { get; set; }
        public int KyRowsetId { get; set; }
        public int? FlMsgNum { get; set; }
        public DateTime? FlMsgDate { get; set; }
        public int? FlMsgType { get; set; }
        public int? FlMsgNumref { get; set; }
        public DateTime? FlMsgDateref { get; set; }
        public long? Position { get; set; }
        public bool? FlMsgToSfr { get; set; }
    }
}
