﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RequestsLog
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int UserId { get; set; }
        public DateTime RequestDate { get; set; }
        public string Grsresponse { get; set; }
        public bool IsApirequest { get; set; }
        public string IdentificationNumber { get; set; }
    }
}
