﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Experience
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string OrganisationName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Position { get; set; }
        public string Reason { get; set; }
        public bool? IsAlpine { get; set; }
        public bool? IsFinance { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
