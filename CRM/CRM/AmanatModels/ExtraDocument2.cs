﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocument2
    {
        public int ExtraDocumentId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string Filename { get; set; }
        public string DisplayName { get; set; }
    }
}
