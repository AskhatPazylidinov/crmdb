﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountStatementEmailSetting
    {
        public int Id { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
    }
}
