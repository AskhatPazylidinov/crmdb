﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportTransactionsFile
    {
        public ImportTransactionsFile()
        {
            ImportTransactions = new HashSet<ImportTransaction>();
        }

        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime ImportBankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ImportTransaction> ImportTransactions { get; set; }
    }
}
