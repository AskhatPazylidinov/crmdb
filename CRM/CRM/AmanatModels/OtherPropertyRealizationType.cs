﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OtherPropertyRealizationType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
