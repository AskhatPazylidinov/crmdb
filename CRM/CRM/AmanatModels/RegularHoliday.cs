﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RegularHoliday
    {
        public byte Month { get; set; }
        public byte Day { get; set; }
    }
}
