﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Customer
    {
        public Customer()
        {
            Account1s = new HashSet<Account1>();
            AccountsAttorneys = new HashSet<AccountsAttorney>();
            Affiliates = new HashSet<Affiliate>();
            Agents = new HashSet<Agent>();
            AllGuarantors = new HashSet<AllGuarantor>();
            BerekeUserInfos = new HashSet<BerekeUserInfo>();
            BranchesCustomers = new HashSet<BranchesCustomer>();
            ChequeBooks = new HashSet<ChequeBook>();
            ContractContragentCustomers = new HashSet<Contract>();
            ContractCustomers = new HashSet<Contract>();
            Conversions = new HashSet<Conversion>();
            Correspondents = new HashSet<Correspondent>();
            CustomerAwayReasons = new HashSet<CustomerAwayReason>();
            CustomerDocuments = new HashSet<CustomerDocument>();
            CustomerManagerCompanies = new HashSet<CustomerManager>();
            CustomerManagerPeople = new HashSet<CustomerManager>();
            CustomersChangeLogs = new HashSet<CustomersChangeLog>();
            CustomersChanges = new HashSet<CustomersChange>();
            CustomersFiles = new HashSet<CustomersFile>();
            CustomersLegalNameParts = new HashSet<CustomersLegalNamePart>();
            CustomersNotifications = new HashSet<CustomersNotification>();
            CustomersRisqueTypes = new HashSet<CustomersRisqueType>();
            CustomersShareholderCompanies = new HashSet<CustomersShareholder>();
            CustomersShareholderShareholders = new HashSet<CustomersShareholder>();
            CustomersStatuses = new HashSet<CustomersStatus>();
            DuplicatedContragentContragentCustomers = new HashSet<DuplicatedContragent>();
            DuplicatedContragentMatchedCustomers = new HashSet<DuplicatedContragent>();
            DuplicatedContragentOriginatorCustomers = new HashSet<DuplicatedContragent>();
            EstimatedMonthTurnovers = new HashSet<EstimatedMonthTurnover>();
            Garantees = new HashSet<Garantee>();
            Histories = new HashSet<History>();
            HistoriesCustomers = new HashSet<HistoriesCustomer>();
            HistoriesRatings = new HashSet<HistoriesRating>();
            IndividualGuaranteeSumms = new HashSet<IndividualGuaranteeSumm>();
            IndividualOverdueTariffs = new HashSet<IndividualOverdueTariff>();
            IndividualSafeTariffsValues = new HashSet<IndividualSafeTariffsValue>();
            IndividualTariff3s = new HashSet<IndividualTariff3>();
            IndividualWithdrawTariffs = new HashSet<IndividualWithdrawTariff>();
            Insiders = new HashSet<Insider>();
            InterTransferReceiverCustomers = new HashSet<InterTransfer>();
            InterTransferSenderAttorneys = new HashSet<InterTransfer>();
            InterTransferSenderCustomers = new HashSet<InterTransfer>();
            InternalPeople = new HashSet<InternalPerson>();
            InverseBeneficiary = new HashSet<Customer>();
            InviteCustomer1s = new HashSet<InviteCustomer1>();
            InviteCustomers = new HashSet<InviteCustomer>();
            InviteInternetBankingUsers = new HashSet<InviteInternetBankingUser>();
            JointAccountCustomers = new HashSet<JointAccountCustomer>();
            LeftCustomers = new HashSet<LeftCustomer>();
            LinkedCustomerLinkedCustomerNavigations = new HashSet<LinkedCustomer>();
            LinkedCustomerMainCustomers = new HashSet<LinkedCustomer>();
            MarketRates = new HashSet<MarketRate>();
            MoneyTransferReceiverCustomers = new HashSet<MoneyTransfer>();
            MoneyTransferSenderCustomers = new HashSet<MoneyTransfer>();
            NonCashAccounts = new HashSet<NonCashAccount>();
            OfficesFlowOperations = new HashSet<OfficesFlowOperation>();
            ParallelLoans = new HashSet<ParallelLoan>();
            PartnerBanks = new HashSet<PartnerBank>();
            PartnersHistories = new HashSet<PartnersHistory>();
            PayBalance1s = new HashSet<PayBalance1>();
            PayBalanceTemps = new HashSet<PayBalanceTemp>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
            PotentialBlackVisitors = new HashSet<PotentialBlackVisitor>();
            PotentialBlackVisitorsOperations = new HashSet<PotentialBlackVisitorsOperation>();
            PublicOfferDocumentReferences = new HashSet<PublicOfferDocumentReference>();
            ReceiverChangeLogNewReceiverCustomers = new HashSet<ReceiverChangeLog>();
            ReceiverChangeLogPrevReceiverCustomers = new HashSet<ReceiverChangeLog>();
            RelativeCustomers = new HashSet<Relative>();
            RelativeRelativeNavigations = new HashSet<Relative>();
            RelativesLoans = new HashSet<RelativesLoan>();
            ReliableCustomers = new HashSet<ReliableCustomer>();
            ReplenishmentIndividualTariffs = new HashSet<ReplenishmentIndividualTariff>();
            RequestsLog1s = new HashSet<RequestsLog1>();
            RequestsLog2s = new HashSet<RequestsLog2>();
            SafesAttorneys = new HashSet<SafesAttorney>();
            SafesLeasings = new HashSet<SafesLeasing>();
            SecuritiesCustomers = new HashSet<SecuritiesCustomer>();
            ServiceBalances = new HashSet<ServiceBalance>();
            ServiceCustomers = new HashSet<ServiceCustomer>();
            ServiceIndividualTariffs = new HashSet<ServiceIndividualTariff>();
            ServiceProviders = new HashSet<ServiceProvider>();
            Swift103TransferReceiverCustomers = new HashSet<Swift103Transfer>();
            Swift103TransferSenderCustomers = new HashSet<Swift103Transfer>();
            Transaction2s = new HashSet<Transaction2>();
            TransferReceiverCustomers = new HashSet<Transfer>();
            TransferSenderCustomers = new HashSet<Transfer>();
            UnistreamUserInfos = new HashSet<UnistreamUserInfo>();
            UsersCustomers = new HashSet<UsersCustomer>();
            Verifications = new HashSet<Verification>();
        }

        public int CustomerId { get; set; }
        public int CustomerTypeId { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public bool? Sex { get; set; }
        public string IdentificationNumber { get; set; }
        public int? NationalityId { get; set; }
        public bool? IsResident { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? BirthCityId { get; set; }
        public int? BirthCountryId { get; set; }
        public string BirthCityName { get; set; }
        public string RegistrationPostalCode { get; set; }
        public int? RegistrationCityId { get; set; }
        public int? RegistrationCountryId { get; set; }
        public string RegistrationCityName { get; set; }
        public string RegistrationStreet { get; set; }
        public string RegistrationHouse { get; set; }
        public string RegistrationFlat { get; set; }
        public string ResidencePostalCode { get; set; }
        public int? ResidenceCityId { get; set; }
        public int? ResidenceCountryId { get; set; }
        public string ResidenceCityName { get; set; }
        public string ResidenceStreet { get; set; }
        public string ResidenceHouse { get; set; }
        public string ResidenceFlat { get; set; }
        public string WorkName { get; set; }
        public string WorkPosition { get; set; }
        public string WorkPhone { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? DocumentValidTill { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactPhone2 { get; set; }
        public string ContactAddress { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonPhone { get; set; }
        public string Email { get; set; }
        public int? PersonActivityTypeId { get; set; }
        public bool? IsIndividualEnterpreneur { get; set; }
        public bool? IsPrivateEnterpreneur { get; set; }
        public string Okpo { get; set; }
        public int? LegalFormId { get; set; }
        public int? ActivityTypeId { get; set; }
        public int? OwnershipTypeId { get; set; }
        public int? TaxOfficeId { get; set; }
        public string SocialRegistrationNo { get; set; }
        public string RegistrationAuthority { get; set; }
        public DateTime? DateOfRegistration { get; set; }
        public int? BusinessCityId { get; set; }
        public int? BusinessCountryId { get; set; }
        public string BusinessCityName { get; set; }
        public string BusinessStreet { get; set; }
        public string BusinessHouse { get; set; }
        public string BusinessFlat { get; set; }
        public string BusinessPostalCode { get; set; }
        public DateTime? StartBusinessDate { get; set; }
        public int? PartnersCount { get; set; }
        public int? EmployeesCount { get; set; }
        public double? WorkSalary { get; set; }
        public int? WorkCurrencyId { get; set; }
        public string WorkAddress { get; set; }
        public DateTime? WorkStartDate { get; set; }
        public DateTime? TotalWorkStartDate { get; set; }
        public bool? HasExtraWork { get; set; }
        public string ExtraWorkName { get; set; }
        public string ExtraWorkAddress { get; set; }
        public string ExtraWorkPosition { get; set; }
        public string ExtraWorkPhone { get; set; }
        public double? ExtraWorkSalary { get; set; }
        public int? ExtraWorkCurrencyId { get; set; }
        public DateTime? ExtraWorkStartDate { get; set; }
        public int? ExtraWorkActivityTypeId { get; set; }
        public int? FamilyStatusId { get; set; }
        public int? EducationTypeId { get; set; }
        public byte? WorkTypeId { get; set; }
        public string BusinessAdressRemarks { get; set; }
        public bool? UseBankClient { get; set; }
        public string ActivityNote { get; set; }
        public DateTime? FirstRegistrationDate { get; set; }
        public string EvidenceRegistrationNo { get; set; }
        public bool? IsPoliticalPerson { get; set; }
        public int? PoliticalCountryId { get; set; }
        public int? PoliticalPositionId { get; set; }
        public int? OriginOfFundsTypeId { get; set; }
        public string OriginOfFundsComment { get; set; }
        public bool? UseOnlineStatement { get; set; }
        public int? BeneficiaryId { get; set; }
        public bool? IsFinancialCompany { get; set; }
        public bool? IsExchangeOffice { get; set; }
        public string HighRiskReasonComment { get; set; }
        public bool? IsAllowSmsReceive { get; set; }
        public string RegistrationHousing { get; set; }
        public string RegistrationBuilding { get; set; }
        public string ResidenceHousing { get; set; }
        public string ResidenceBuilding { get; set; }
        public string BusinessHousing { get; set; }
        public string BusinessBuilding { get; set; }
        public bool? IsAllowMarketingReceive { get; set; }
        public int? FatcaCriterion { get; set; }
        public bool? ClientType { get; set; }
        public int? CustomerReasonAvayId { get; set; }
        public string Codename { get; set; }
        public byte? ReceiveLanguage { get; set; }
        public bool? IsOwnerResidence { get; set; }
        public bool? IsAllowToCibsend { get; set; }
        public bool? IsDocUnlimited { get; set; }
        public byte? LegalDocumentTypeId { get; set; }
        public bool? IsInternetBankingCustomer { get; set; }
        public bool IsBankEmployee { get; set; }
        public int? SegmentId { get; set; }
        public string WhatsAppPhone { get; set; }
        public byte? IdentificationStatus { get; set; }
        public string PrivateCustomerDocumentNo { get; set; }
        public string PrivateCustomerFullName { get; set; }

        public virtual ActivityType ActivityType { get; set; }
        public virtual Customer Beneficiary { get; set; }
        public virtual City1 BirthCity { get; set; }
        public virtual Country2 BirthCountry { get; set; }
        public virtual City1 BusinessCity { get; set; }
        public virtual Country2 BusinessCountry { get; set; }
        public virtual DocumentType1 DocumentType { get; set; }
        public virtual EducationType EducationType { get; set; }
        public virtual ActivityType ExtraWorkActivityType { get; set; }
        public virtual Currency3 ExtraWorkCurrency { get; set; }
        public virtual FamilyStatusType FamilyStatus { get; set; }
        public virtual LegalFormType LegalForm { get; set; }
        public virtual Country2 Nationality { get; set; }
        public virtual OriginOfFundsType OriginOfFundsType { get; set; }
        public virtual OwnershipType OwnershipType { get; set; }
        public virtual ActivityType PersonActivityType { get; set; }
        public virtual Country2 PoliticalCountry { get; set; }
        public virtual City1 RegistrationCity { get; set; }
        public virtual Country2 RegistrationCountry { get; set; }
        public virtual City1 ResidenceCity { get; set; }
        public virtual Country2 ResidenceCountry { get; set; }
        public virtual TaxOffice TaxOffice { get; set; }
        public virtual Currency3 WorkCurrency { get; set; }
        public virtual Contractor Contractor { get; set; }
        public virtual CustomerWallet CustomerWallet { get; set; }
        public virtual CustomersCooperationHistory CustomersCooperationHistory { get; set; }
        public virtual CustomersForeignIdentification CustomersForeignIdentification { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Employee1 Employee1 { get; set; }
        public virtual InsuranceCompany InsuranceCompany { get; set; }
        public virtual MonitoringCustomer MonitoringCustomer { get; set; }
        public virtual Partner2 Partner2 { get; set; }
        public virtual ICollection<Account1> Account1s { get; set; }
        public virtual ICollection<AccountsAttorney> AccountsAttorneys { get; set; }
        public virtual ICollection<Affiliate> Affiliates { get; set; }
        public virtual ICollection<Agent> Agents { get; set; }
        public virtual ICollection<AllGuarantor> AllGuarantors { get; set; }
        public virtual ICollection<BerekeUserInfo> BerekeUserInfos { get; set; }
        public virtual ICollection<BranchesCustomer> BranchesCustomers { get; set; }
        public virtual ICollection<ChequeBook> ChequeBooks { get; set; }
        public virtual ICollection<Contract> ContractContragentCustomers { get; set; }
        public virtual ICollection<Contract> ContractCustomers { get; set; }
        public virtual ICollection<Conversion> Conversions { get; set; }
        public virtual ICollection<Correspondent> Correspondents { get; set; }
        public virtual ICollection<CustomerAwayReason> CustomerAwayReasons { get; set; }
        public virtual ICollection<CustomerDocument> CustomerDocuments { get; set; }
        public virtual ICollection<CustomerManager> CustomerManagerCompanies { get; set; }
        public virtual ICollection<CustomerManager> CustomerManagerPeople { get; set; }
        public virtual ICollection<CustomersChangeLog> CustomersChangeLogs { get; set; }
        public virtual ICollection<CustomersChange> CustomersChanges { get; set; }
        public virtual ICollection<CustomersFile> CustomersFiles { get; set; }
        public virtual ICollection<CustomersLegalNamePart> CustomersLegalNameParts { get; set; }
        public virtual ICollection<CustomersNotification> CustomersNotifications { get; set; }
        public virtual ICollection<CustomersRisqueType> CustomersRisqueTypes { get; set; }
        public virtual ICollection<CustomersShareholder> CustomersShareholderCompanies { get; set; }
        public virtual ICollection<CustomersShareholder> CustomersShareholderShareholders { get; set; }
        public virtual ICollection<CustomersStatus> CustomersStatuses { get; set; }
        public virtual ICollection<DuplicatedContragent> DuplicatedContragentContragentCustomers { get; set; }
        public virtual ICollection<DuplicatedContragent> DuplicatedContragentMatchedCustomers { get; set; }
        public virtual ICollection<DuplicatedContragent> DuplicatedContragentOriginatorCustomers { get; set; }
        public virtual ICollection<EstimatedMonthTurnover> EstimatedMonthTurnovers { get; set; }
        public virtual ICollection<Garantee> Garantees { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<HistoriesCustomer> HistoriesCustomers { get; set; }
        public virtual ICollection<HistoriesRating> HistoriesRatings { get; set; }
        public virtual ICollection<IndividualGuaranteeSumm> IndividualGuaranteeSumms { get; set; }
        public virtual ICollection<IndividualOverdueTariff> IndividualOverdueTariffs { get; set; }
        public virtual ICollection<IndividualSafeTariffsValue> IndividualSafeTariffsValues { get; set; }
        public virtual ICollection<IndividualTariff3> IndividualTariff3s { get; set; }
        public virtual ICollection<IndividualWithdrawTariff> IndividualWithdrawTariffs { get; set; }
        public virtual ICollection<Insider> Insiders { get; set; }
        public virtual ICollection<InterTransfer> InterTransferReceiverCustomers { get; set; }
        public virtual ICollection<InterTransfer> InterTransferSenderAttorneys { get; set; }
        public virtual ICollection<InterTransfer> InterTransferSenderCustomers { get; set; }
        public virtual ICollection<InternalPerson> InternalPeople { get; set; }
        public virtual ICollection<Customer> InverseBeneficiary { get; set; }
        public virtual ICollection<InviteCustomer1> InviteCustomer1s { get; set; }
        public virtual ICollection<InviteCustomer> InviteCustomers { get; set; }
        public virtual ICollection<InviteInternetBankingUser> InviteInternetBankingUsers { get; set; }
        public virtual ICollection<JointAccountCustomer> JointAccountCustomers { get; set; }
        public virtual ICollection<LeftCustomer> LeftCustomers { get; set; }
        public virtual ICollection<LinkedCustomer> LinkedCustomerLinkedCustomerNavigations { get; set; }
        public virtual ICollection<LinkedCustomer> LinkedCustomerMainCustomers { get; set; }
        public virtual ICollection<MarketRate> MarketRates { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransferReceiverCustomers { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransferSenderCustomers { get; set; }
        public virtual ICollection<NonCashAccount> NonCashAccounts { get; set; }
        public virtual ICollection<OfficesFlowOperation> OfficesFlowOperations { get; set; }
        public virtual ICollection<ParallelLoan> ParallelLoans { get; set; }
        public virtual ICollection<PartnerBank> PartnerBanks { get; set; }
        public virtual ICollection<PartnersHistory> PartnersHistories { get; set; }
        public virtual ICollection<PayBalance1> PayBalance1s { get; set; }
        public virtual ICollection<PayBalanceTemp> PayBalanceTemps { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
        public virtual ICollection<PotentialBlackVisitor> PotentialBlackVisitors { get; set; }
        public virtual ICollection<PotentialBlackVisitorsOperation> PotentialBlackVisitorsOperations { get; set; }
        public virtual ICollection<PublicOfferDocumentReference> PublicOfferDocumentReferences { get; set; }
        public virtual ICollection<ReceiverChangeLog> ReceiverChangeLogNewReceiverCustomers { get; set; }
        public virtual ICollection<ReceiverChangeLog> ReceiverChangeLogPrevReceiverCustomers { get; set; }
        public virtual ICollection<Relative> RelativeCustomers { get; set; }
        public virtual ICollection<Relative> RelativeRelativeNavigations { get; set; }
        public virtual ICollection<RelativesLoan> RelativesLoans { get; set; }
        public virtual ICollection<ReliableCustomer> ReliableCustomers { get; set; }
        public virtual ICollection<ReplenishmentIndividualTariff> ReplenishmentIndividualTariffs { get; set; }
        public virtual ICollection<RequestsLog1> RequestsLog1s { get; set; }
        public virtual ICollection<RequestsLog2> RequestsLog2s { get; set; }
        public virtual ICollection<SafesAttorney> SafesAttorneys { get; set; }
        public virtual ICollection<SafesLeasing> SafesLeasings { get; set; }
        public virtual ICollection<SecuritiesCustomer> SecuritiesCustomers { get; set; }
        public virtual ICollection<ServiceBalance> ServiceBalances { get; set; }
        public virtual ICollection<ServiceCustomer> ServiceCustomers { get; set; }
        public virtual ICollection<ServiceIndividualTariff> ServiceIndividualTariffs { get; set; }
        public virtual ICollection<ServiceProvider> ServiceProviders { get; set; }
        public virtual ICollection<Swift103Transfer> Swift103TransferReceiverCustomers { get; set; }
        public virtual ICollection<Swift103Transfer> Swift103TransferSenderCustomers { get; set; }
        public virtual ICollection<Transaction2> Transaction2s { get; set; }
        public virtual ICollection<Transfer> TransferReceiverCustomers { get; set; }
        public virtual ICollection<Transfer> TransferSenderCustomers { get; set; }
        public virtual ICollection<UnistreamUserInfo> UnistreamUserInfos { get; set; }
        public virtual ICollection<UsersCustomer> UsersCustomers { get; set; }
        public virtual ICollection<Verification> Verifications { get; set; }
    }
}
