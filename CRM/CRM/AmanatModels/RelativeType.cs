﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RelativeType
    {
        public RelativeType()
        {
            InverseLinked = new HashSet<RelativeType>();
            Relatives = new HashSet<Relative>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int? LinkedId { get; set; }
        public bool? IsRppu { get; set; }
        public bool? IsSpouse { get; set; }

        public virtual RelativeType Linked { get; set; }
        public virtual ICollection<RelativeType> InverseLinked { get; set; }
        public virtual ICollection<Relative> Relatives { get; set; }
    }
}
