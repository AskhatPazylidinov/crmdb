﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrAccountsLimit
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal MaxLimit { get; set; }

        public virtual SwiftCorrBankAccount SwiftCorrBankAccount { get; set; }
    }
}
