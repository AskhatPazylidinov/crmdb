﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnerBanksAccount
    {
        public int PartnerId { get; set; }
        public string PartnerAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public string NostroAccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual PartnerBank Partner { get; set; }
    }
}
