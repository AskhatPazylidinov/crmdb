﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProduct1
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string OpenOrderNo { get; set; }
        public DateTime OpenDate { get; set; }
        public string CloseOrderNo { get; set; }
        public DateTime? CloseDate { get; set; }
        public byte ProductTypeId { get; set; }
        public byte OrganizationTypeId { get; set; }
        public byte CalculationTypeId { get; set; }
        public byte? CapitalizationDateTypeId { get; set; }
        public byte? CapitalizationSummTypeId { get; set; }
        public byte? CapitalizationPercentsPeriod { get; set; }
        public byte? PercentsCalculationSchemeTypeId { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
