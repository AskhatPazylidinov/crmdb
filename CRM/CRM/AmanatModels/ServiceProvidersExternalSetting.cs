﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersExternalSetting
    {
        public int ProviderId { get; set; }
        public bool? UseAutorization { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string ExternalServiceAddress { get; set; }
        public bool? UseProxy { get; set; }
        public string ProxyAddress { get; set; }
        public int? ProxyPort { get; set; }
        public bool? UseProxyAutorization { get; set; }
        public string ProxyLogin { get; set; }
        public string ProxyPassword { get; set; }

        public virtual ServiceProvider Provider { get; set; }
    }
}
