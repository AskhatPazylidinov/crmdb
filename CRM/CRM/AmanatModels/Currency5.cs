﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency5
    {
        public Currency5()
        {
            AllowedCurrencies = new HashSet<AllowedCurrency>();
        }

        public int Id { get; set; }
        public string Latin3 { get; set; }
        public string Digital { get; set; }
        public string Name { get; set; }
        public string NameLat { get; set; }
        public bool? Status { get; set; }

        public virtual ICollection<AllowedCurrency> AllowedCurrencies { get; set; }
    }
}
