﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountsAttorney
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AttorneyNo { get; set; }
        public string Description { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsActive { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
