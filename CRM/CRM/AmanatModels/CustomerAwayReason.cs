﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerAwayReason
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CustomerAwayReasonDictionaryId { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual CustomerAwayReasonDictionary CustomerAwayReasonDictionary { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
