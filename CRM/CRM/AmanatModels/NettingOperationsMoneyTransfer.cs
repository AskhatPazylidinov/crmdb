﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NettingOperationsMoneyTransfer
    {
        public long NettingId { get; set; }
        public long OperationId { get; set; }

        public virtual NettingOperation Netting { get; set; }
        public virtual MoneyTransfersStatus Operation { get; set; }
    }
}
