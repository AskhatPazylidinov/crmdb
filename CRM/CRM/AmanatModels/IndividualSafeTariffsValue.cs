﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualSafeTariffsValue
    {
        public int CustomerId { get; set; }
        public int SafeTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal SummPerDay { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual SafeType SafeType { get; set; }
    }
}
