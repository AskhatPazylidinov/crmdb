﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiaTransfer
    {
        public long TransferId { get; set; }
        public string OrderNo { get; set; }
        public int CountryId { get; set; }
        public int? BankId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string SenderInfo { get; set; }
        public string ReceiverInfo { get; set; }
        public string TransRefId { get; set; }
        public string CurrencyCodeFrom { get; set; }
        public string CurrencyCodeTo { get; set; }

        public virtual Country3 Country { get; set; }
    }
}
