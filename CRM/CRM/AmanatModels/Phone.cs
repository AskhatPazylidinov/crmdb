﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Phone
    {
        public Phone()
        {
            EmployeesPhones = new HashSet<EmployeesPhone>();
            PhonesExpenses = new HashSet<PhonesExpense>();
        }

        public string PhoneNo { get; set; }

        public virtual ICollection<EmployeesPhone> EmployeesPhones { get; set; }
        public virtual ICollection<PhonesExpense> PhonesExpenses { get; set; }
    }
}
