﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CrossBankConversionsView
    {
        public int OperationId { get; set; }
        public byte ConversionTypeId { get; set; }
        public string OrderNo { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public string ContragentBank { get; set; }
        public byte ValueDateId { get; set; }
        public DateTime ActualStatusDate { get; set; }
        public string ActualUserName { get; set; }
        public string DtBankCode { get; set; }
        public int DtCurrencyId { get; set; }
        public decimal DtOperationSumm { get; set; }
        public DateTime DtValueDate { get; set; }
        public string CtBankCode { get; set; }
        public int CtCurrencyId { get; set; }
        public decimal CtOperationSumm { get; set; }
        public DateTime CtValueDate { get; set; }
        public string FinalDtBankCode { get; set; }
        public int? FinalDtCurrencyId { get; set; }
        public decimal? FinalDtOperationSumm { get; set; }
        public DateTime? FinalDtValueDate { get; set; }
        public string FinalCtBankCode { get; set; }
        public int? FinalCtCurrencyId { get; set; }
        public decimal? FinalCtOperationSumm { get; set; }
        public DateTime? FinalCtValueDate { get; set; }
    }
}
