﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionTemplate
    {
        public byte OperationType { get; set; }
        public int TemplateOperationTypeId { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }
        public short? CashSymbol { get; set; }
        public string Comment { get; set; }
        public int? CountryId { get; set; }
        public string OperationCode { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsTemplateAvailableForAnotherOffice { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual TemplateOperationType TemplateOperationType { get; set; }
    }
}
