﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Country2
    {
        public Country2()
        {
            CorrBankTransactions = new HashSet<CorrBankTransaction>();
            Currency3s = new HashSet<Currency3>();
            CustomerBirthCountries = new HashSet<Customer>();
            CustomerBusinessCountries = new HashSet<Customer>();
            CustomerNationalities = new HashSet<Customer>();
            CustomerPoliticalCountries = new HashSet<Customer>();
            CustomerRegistrationCountries = new HashSet<Customer>();
            CustomerResidenceCountries = new HashSet<Customer>();
            MoneyTransfers = new HashSet<MoneyTransfer>();
            OffshoreTransferWarningContrBusinessCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningContrNationalityCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningContrRegistrationCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningContrResidenceCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningProxCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningRecCountries = new HashSet<OffshoreTransferWarning>();
            OffshoreTransferWarningSendCountries = new HashSet<OffshoreTransferWarning>();
            SwiftInvoicesToTheBanks = new HashSet<SwiftInvoicesToTheBank>();
        }

        public int CountryId { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public int Code { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        public int CountryTypeId { get; set; }
        public string ShortNameEn { get; set; }
        public string Capital { get; set; }
        public int? CountryCode { get; set; }
        public int? FatfTypeId { get; set; }
        public bool? IsHighRisk { get; set; }
        public string PhoneCode { get; set; }

        public virtual CountryType CountryType { get; set; }
        public virtual SettingsCountry SettingsCountry { get; set; }
        public virtual ICollection<CorrBankTransaction> CorrBankTransactions { get; set; }
        public virtual ICollection<Currency3> Currency3s { get; set; }
        public virtual ICollection<Customer> CustomerBirthCountries { get; set; }
        public virtual ICollection<Customer> CustomerBusinessCountries { get; set; }
        public virtual ICollection<Customer> CustomerNationalities { get; set; }
        public virtual ICollection<Customer> CustomerPoliticalCountries { get; set; }
        public virtual ICollection<Customer> CustomerRegistrationCountries { get; set; }
        public virtual ICollection<Customer> CustomerResidenceCountries { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransfers { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningContrBusinessCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningContrNationalityCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningContrRegistrationCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningContrResidenceCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningProxCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningRecCountries { get; set; }
        public virtual ICollection<OffshoreTransferWarning> OffshoreTransferWarningSendCountries { get; set; }
        public virtual ICollection<SwiftInvoicesToTheBank> SwiftInvoicesToTheBanks { get; set; }
    }
}
