﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsFieldsCalcValue
    {
        public int Id { get; set; }
        public int FieldValueId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public byte OperationId { get; set; }

        public virtual TransactionsField FieldNameNavigation { get; set; }
        public virtual TransactionsFieldsValue FieldValueNavigation { get; set; }
    }
}
