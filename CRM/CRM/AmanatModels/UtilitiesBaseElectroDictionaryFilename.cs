﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBaseElectroDictionaryFilename
    {
        public int FileId { get; set; }
        public int ProviderId { get; set; }
        public string DictionaryFilename { get; set; }

        public virtual ServiceProvider Provider { get; set; }
    }
}
