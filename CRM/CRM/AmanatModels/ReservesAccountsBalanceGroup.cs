﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesAccountsBalanceGroup
    {
        public bool IsFinancialCompany { get; set; }
        public byte ShortTypeId { get; set; }
        public string ReserveBalanceGroup { get; set; }
        public string ExpenseBalanceGroup { get; set; }
        public string ReserveAccountGroup { get; set; }
        public string ExpenseAccountGroup { get; set; }

        public virtual AccountGroup ExpenseAccountGroupNavigation { get; set; }
        public virtual BalanceGroup ExpenseBalanceGroupNavigation { get; set; }
        public virtual AccountGroup ReserveAccountGroupNavigation { get; set; }
        public virtual BalanceGroup ReserveBalanceGroupNavigation { get; set; }
    }
}
