﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OnlineWalletType
    {
        public OnlineWalletType()
        {
            CustomerWallets = new HashSet<CustomerWallet>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public string OperatorName { get; set; }
        public byte Placeholder { get; set; }

        public virtual ICollection<CustomerWallet> CustomerWallets { get; set; }
    }
}
