﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NotToCloseLoan
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? UserId { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
    }
}
