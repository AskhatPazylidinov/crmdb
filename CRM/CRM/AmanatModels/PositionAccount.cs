﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PositionAccount
    {
        public int BranchId { get; set; }
        public int CurrencyId { get; set; }
        public string PositionAccountNo { get; set; }
        public string NationalAccountNo { get; set; }
        public int PositionId { get; set; }
    }
}
