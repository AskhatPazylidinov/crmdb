﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservedAccount
    {
        public int OfficeId { get; set; }
        public int TypeId { get; set; }
        public string AccountNo { get; set; }
        public string AccountNoOut { get; set; }
        public string AccountNoGonorar { get; set; }
    }
}
