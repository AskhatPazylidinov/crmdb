﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionLog
    {
        public int Id { get; set; }
        public int ExecuteTransactionId { get; set; }
        public int UserId { get; set; }
        public string AccountNo { get; set; }
        public int AccountCurrencyId { get; set; }
        public int TransactionTypeId { get; set; }
        public decimal SumV { get; set; }
        public bool IsSuccess { get; set; }
        public string ResultMessage { get; set; }
        public DateTime Date { get; set; }
        public DateTime BankDate { get; set; }
        public bool IsPayment { get; set; }
    }
}
