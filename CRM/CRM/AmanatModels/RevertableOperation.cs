﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperation
    {
        public RevertableOperation()
        {
            RevertableOperationsAccounts = new HashSet<RevertableOperationsAccount>();
            RevertableOperationsTransactions = new HashSet<RevertableOperationsTransaction>();
        }

        public long RevertId { get; set; }
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime BankDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public byte OperationTypeId { get; set; }

        public virtual History Credit { get; set; }
        public virtual User User { get; set; }
        public virtual RevertableOperationsChange RevertableOperationsChange { get; set; }
        public virtual RevertableOperationsGraphic RevertableOperationsGraphic { get; set; }
        public virtual ICollection<RevertableOperationsAccount> RevertableOperationsAccounts { get; set; }
        public virtual ICollection<RevertableOperationsTransaction> RevertableOperationsTransactions { get; set; }
    }
}
