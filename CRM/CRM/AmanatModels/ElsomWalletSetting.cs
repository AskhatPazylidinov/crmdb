﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ElsomWalletSetting
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
    }
}
