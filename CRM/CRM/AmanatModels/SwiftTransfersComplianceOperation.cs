﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfersComplianceOperation
    {
        public long TransferId { get; set; }
        public DateTime StatusDate { get; set; }
        public DateTime OperationDate { get; set; }
        public int UserId { get; set; }
    }
}
