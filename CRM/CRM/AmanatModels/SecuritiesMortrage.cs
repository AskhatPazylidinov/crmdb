﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuritiesMortrage
    {
        public int MortrageId { get; set; }
        public int CreditId { get; set; }
        public string SecuritiesName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime SecuritiesPaymentDate { get; set; }
        public int SecuritiesCount { get; set; }
        public decimal PricePerItem { get; set; }
        public int SecuritiesCurrencyId { get; set; }
        public decimal? DiscountRate { get; set; }

        public virtual History Credit { get; set; }
        public virtual Currency3 SecuritiesCurrency { get; set; }
    }
}
