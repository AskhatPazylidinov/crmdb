﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocumentGroup
    {
        public ExtraDocumentGroup()
        {
            ExtraDocuments = new HashSet<ExtraDocument>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ExtraDocument> ExtraDocuments { get; set; }
    }
}
