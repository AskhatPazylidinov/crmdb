﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Rank
    {
        public int RankId { get; set; }
        public string RankName { get; set; }
        public byte? TypeId { get; set; }
    }
}
