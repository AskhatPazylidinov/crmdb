﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxService
    {
        public TaxService()
        {
            ImportTaxPaymentCodes = new HashSet<ImportTaxPaymentCode>();
            SpecialServicesBankDetails = new HashSet<SpecialServicesBankDetail>();
            TaxPayment1s = new HashSet<TaxPayment1>();
            TaxServicesComissions = new HashSet<TaxServicesComission>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string ShortServiceName { get; set; }
        public string PaymentCode { get; set; }
        public byte? CustomerTypeId { get; set; }
        public string TaxCode { get; set; }

        public virtual ICollection<ImportTaxPaymentCode> ImportTaxPaymentCodes { get; set; }
        public virtual ICollection<SpecialServicesBankDetail> SpecialServicesBankDetails { get; set; }
        public virtual ICollection<TaxPayment1> TaxPayment1s { get; set; }
        public virtual ICollection<TaxServicesComission> TaxServicesComissions { get; set; }
    }
}
