﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TaxOffice
    {
        public TaxOffice()
        {
            Customers = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string OfficeName { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
