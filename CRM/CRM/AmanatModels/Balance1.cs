﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Balance1
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal Sumv { get; set; }
        public decimal Sumn { get; set; }
        public decimal DtSumV { get; set; }
        public decimal DtSumN { get; set; }
        public decimal CtSumV { get; set; }
        public decimal CtSumN { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
