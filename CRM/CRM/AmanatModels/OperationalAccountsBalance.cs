﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalAccountsBalance
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
