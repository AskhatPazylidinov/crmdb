﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DetailedOperation
    {
        public DetailedOperation()
        {
            OperationsCards = new HashSet<OperationsCard>();
            OperationsDenominations = new HashSet<OperationsDenomination>();
            OperationsTransactions = new HashSet<OperationsTransaction>();
            OperationsUtilities = new HashSet<OperationsUtility>();
        }

        public int OperationId { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }
        public DateTime BankDate { get; set; }
        public int OfficeId { get; set; }

        public virtual OperationsDifference OperationsDifference { get; set; }
        public virtual ICollection<OperationsCard> OperationsCards { get; set; }
        public virtual ICollection<OperationsDenomination> OperationsDenominations { get; set; }
        public virtual ICollection<OperationsTransaction> OperationsTransactions { get; set; }
        public virtual ICollection<OperationsUtility> OperationsUtilities { get; set; }
    }
}
