﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CheckPerson
    {
        public string Surname { get; set; }
        public string Fullname { get; set; }
        public string Otchestvo { get; set; }
        public string IdentificationNo { get; set; }
    }
}
