﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ImportedTaxPayment
    {
        public int OperationId { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public decimal TaxSumm { get; set; }
        public string CustomerName { get; set; }
        public string Comment { get; set; }
        public int ImportFileId { get; set; }

        public virtual UtilitiesImportFile ImportFile { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual TaxPayment1 Operation { get; set; }
        public virtual User User { get; set; }
    }
}
