﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AutoPaymentsLog
    {
        public long LogId { get; set; }
        public int BranchId { get; set; }
        public DateTime BankDate { get; set; }
        public int UserId { get; set; }
        public DateTime OperationDate { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual User User { get; set; }
    }
}
