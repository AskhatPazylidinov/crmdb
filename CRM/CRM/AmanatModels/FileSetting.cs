﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FileSetting
    {
        public int TFilePartA { get; set; }
        public int TFilePartB { get; set; }
    }
}
