﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesOffice
    {
        public int EmployeeId { get; set; }
        public DateTime ActualDate { get; set; }
        public int OfficeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
