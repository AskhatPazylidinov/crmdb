﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Property
    {
        public Property()
        {
            MortgagesFields = new HashSet<MortgagesField>();
            PropertyFields = new HashSet<PropertyField>();
        }

        public int PropertyId { get; set; }
        public int GarantType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentPropertyId { get; set; }
        public int OrderNumber { get; set; }
        public bool IsTable { get; set; }

        public virtual GarantType GarantTypeNavigation { get; set; }
        public virtual ICollection<MortgagesField> MortgagesFields { get; set; }
        public virtual ICollection<PropertyField> PropertyFields { get; set; }
    }
}
