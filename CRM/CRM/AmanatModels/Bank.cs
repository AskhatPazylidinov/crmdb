﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Bank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte BankType { get; set; }
        public int? DeviceType { get; set; }
        public string StartId { get; set; }
        public int? CountSymbol { get; set; }
        public string BinCard { get; set; }
        public int? CardType { get; set; }
        public string Cmi { get; set; }
    }
}
