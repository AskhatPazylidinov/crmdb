﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExtraDocumentGroup1
    {
        public ExtraDocumentGroup1()
        {
            ExtraDocument1s = new HashSet<ExtraDocument1>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ExtraDocument1> ExtraDocument1s { get; set; }
    }
}
