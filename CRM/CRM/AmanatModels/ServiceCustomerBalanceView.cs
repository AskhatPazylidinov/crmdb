﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceCustomerBalanceView
    {
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public decimal? Amount { get; set; }
        public int CustomerTypeId { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
