﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesStatus1
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime StatusDate { get; set; }
        public string CreditStatusEx { get; set; }
        public int StatusId { get; set; }
        public string Message { get; set; }
        public int PaymentNumber { get; set; }
        public string RepayStatus { get; set; }
        public int CountOfDelinquency { get; set; }
    }
}
