﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwapSpotAccount
    {
        public int AccountId { get; set; }
        public int CurrencyId { get; set; }
        public byte AccountTypeId { get; set; }
        public string OdbaccountNo { get; set; }
    }
}
