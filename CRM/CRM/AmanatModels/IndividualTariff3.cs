﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariff3
    {
        public int CustomerId { get; set; }
        public int CurrencyId { get; set; }
        public int SenderAccountTypeId { get; set; }
        public int ReceiverAccountTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal TotalComission { get; set; }
        public decimal SenderComission { get; set; }
        public int ComissionTypeId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
