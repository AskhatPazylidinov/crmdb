﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsUsersLog
    {
        public long LogId { get; set; }
        public DateTime LogDate { get; set; }
        public short LogStatusId { get; set; }
        public string Description { get; set; }
        public string Ipaddress { get; set; }
        public int? AgentUserId { get; set; }

        public virtual AgentsUser AgentUser { get; set; }
    }
}
