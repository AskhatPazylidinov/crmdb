﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Region2
    {
        public Region2()
        {
            Office2s = new HashSet<Office2>();
        }

        public int Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Office2> Office2s { get; set; }
    }
}
