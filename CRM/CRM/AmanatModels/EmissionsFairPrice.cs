﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmissionsFairPrice
    {
        public int EmissionId { get; set; }
        public DateTime PriceDate { get; set; }
        public decimal FairValueRatio { get; set; }

        public virtual Emission Emission { get; set; }
    }
}
