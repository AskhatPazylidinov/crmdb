﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowedCurrency
    {
        public int BankId { get; set; }
        public int CurrencyId { get; set; }
        public byte? Direction { get; set; }
        public byte? TransferType { get; set; }

        public virtual Bank4 Bank { get; set; }
        public virtual Currency5 Currency { get; set; }
        public virtual TransferType TransferTypeNavigation { get; set; }
    }
}
