﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class EducationType1
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
