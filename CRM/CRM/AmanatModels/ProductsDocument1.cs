﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsDocument1
    {
        public int DocumentId { get; set; }
        public int ProductId { get; set; }
        public string Filename { get; set; }
        public bool IsGroupFile { get; set; }
        public byte DocumentTypeId { get; set; }
        public bool IsGuarantorFile { get; set; }
        public bool IsGraphic { get; set; }
        public bool ToPdf { get; set; }

        public virtual Product1 Product { get; set; }
    }
}
