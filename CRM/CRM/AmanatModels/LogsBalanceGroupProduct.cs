﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LogsBalanceGroupProduct
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public DateTime BankDate { get; set; }
        public DateTime OperationDate { get; set; }
        public byte LogChangeTypeId { get; set; }
        public int ProductId { get; set; }
        public string ResidentGroup { get; set; }
        public string NonResidentGroup { get; set; }
        public int TypeAccount { get; set; }

        public virtual User User { get; set; }
    }
}
