﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Schedule
    {
        public int OfficeId { get; set; }
        public int Day { get; set; }
        public string WorkTime { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
