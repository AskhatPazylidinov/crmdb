﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariffValuesHistory
    {
        public int TariffId { get; set; }
        public int CustomerId { get; set; }
        public string MainAccountNo { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal? TariffRate { get; set; }
        public bool? IsPercent { get; set; }
        public decimal TariffMonth { get; set; }

        public virtual IndividualTariffValue IndividualTariffValue { get; set; }
    }
}
