﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RatesBasedOnBalance
    {
        public string DepositAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal StartBalance { get; set; }
        public decimal? EndBalance { get; set; }
        public decimal Rate { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual RatesBasedOnBalanceDate RatesBasedOnBalanceDate { get; set; }
    }
}
