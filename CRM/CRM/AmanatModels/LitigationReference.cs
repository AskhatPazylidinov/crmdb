﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class LitigationReference
    {
        public LitigationReference()
        {
            LoanLitigationEvents = new HashSet<LoanLitigationEvent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public byte ReferenceType { get; set; }

        public virtual ICollection<LoanLitigationEvent> LoanLitigationEvents { get; set; }
    }
}
