﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            DocumentTypesMapping1s = new HashSet<DocumentTypesMapping1>();
        }

        public int Id { get; set; }
        public int Version { get; set; }
        public int Erased { get; set; }
        public string TypeCode { get; set; }
        public string NameEng { get; set; }
        public string NameRus { get; set; }
        public int CountryId { get; set; }

        public virtual Country1 Country { get; set; }
        public virtual ICollection<DocumentTypesMapping1> DocumentTypesMapping1s { get; set; }
    }
}
