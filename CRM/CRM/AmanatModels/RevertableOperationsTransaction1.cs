﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RevertableOperationsTransaction1
    {
        public int RevertId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual RevertableOperation1 Revert { get; set; }
    }
}
