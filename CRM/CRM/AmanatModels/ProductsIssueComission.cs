﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsIssueComission
    {
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int MortrageTypeId { get; set; }
        public decimal MinSumm { get; set; }
        public decimal? MaxSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionType { get; set; }
        public int CurrencyId { get; set; }
        public byte MinPeriod { get; set; }
        public byte MaxPeriod { get; set; }
        public decimal MinRate { get; set; }
        public decimal? MaxRate { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual MortrageType MortrageType { get; set; }
        public virtual ProductsChange ProductsChange { get; set; }
    }
}
