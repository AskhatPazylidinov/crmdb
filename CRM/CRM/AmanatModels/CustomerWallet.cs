﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerWallet
    {
        public int CustomerId { get; set; }
        public int WalletTypeId { get; set; }
        public string WalletNo { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual OnlineWalletType WalletType { get; set; }
    }
}
