﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InviteCustomer1
    {
        public int InviteEmployeeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
        public virtual Customer InviteEmployee { get; set; }
        public virtual User User { get; set; }
    }
}
