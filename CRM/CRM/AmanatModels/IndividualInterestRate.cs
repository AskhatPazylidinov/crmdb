﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualInterestRate
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal InterestRate { get; set; }
        public bool? IsApproved { get; set; }
        public int? ApprovedUserId { get; set; }
        public int? OriginatorUserId { get; set; }

        public virtual User ApprovedUser { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
