﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RatingType
    {
        public RatingType()
        {
            HistoriesRatings = new HashSet<HistoriesRating>();
            InternalPeople = new HashSet<InternalPerson>();
        }

        public int TypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool ToBlackList { get; set; }
        public bool IsFraudCodeRequired { get; set; }

        public virtual ICollection<HistoriesRating> HistoriesRatings { get; set; }
        public virtual ICollection<InternalPerson> InternalPeople { get; set; }
    }
}
