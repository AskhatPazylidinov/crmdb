﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeletedRevolvingWindow
    {
        public int CreditId { get; set; }
        public DateTime BillingStartDate { get; set; }
        public DateTime? BillingEndDate { get; set; }
        public DateTime DeletedDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
