﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FuzzyCompareCoeff
    {
        public int CustomerTypeId { get; set; }
        public bool IsOtchestvoUsed { get; set; }
        public bool IsTransliterationUsed { get; set; }
        public decimal MatchCoeff { get; set; }
        public int Id { get; set; }
        public int? BlackListId { get; set; }

        public virtual BlackList BlackList { get; set; }
    }
}
