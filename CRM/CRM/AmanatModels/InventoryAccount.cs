﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InventoryAccount
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime CloseDate { get; set; }
        public decimal? Balances { get; set; }
        public int? BranchId { get; set; }
    }
}
