﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BranchReservedAccount
    {
        public int BranchId { get; set; }
        public int AccountTypeId { get; set; }
        public int CurrencyId { get; set; }
        public string AccountNo { get; set; }

        public virtual Account1 Account1 { get; set; }
    }
}
