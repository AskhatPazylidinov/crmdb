﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CardType
    {
        public CardType()
        {
            Cards = new HashSet<Card>();
            ComissinEcvayers = new HashSet<ComissinEcvayer>();
            ProductCardTypes = new HashSet<ProductCardType>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Bin { get; set; }
        public string CompanyName { get; set; }

        public virtual ICollection<Card> Cards { get; set; }
        public virtual ICollection<ComissinEcvayer> ComissinEcvayers { get; set; }
        public virtual ICollection<ProductCardType> ProductCardTypes { get; set; }
    }
}
