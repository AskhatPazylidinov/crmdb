﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CorrectiveRule
    {
        public CorrectiveRule()
        {
            CorrectiveRuleParameterValues = new HashSet<CorrectiveRuleParameterValue>();
        }

        public int CorrectiveRuleId { get; set; }
        public int CurrentScoreTypeId { get; set; }
        public int NewScoreTypeId { get; set; }
        public string Comment { get; set; }

        public virtual ScoreType CurrentScoreType { get; set; }
        public virtual ScoreType NewScoreType { get; set; }
        public virtual ICollection<CorrectiveRuleParameterValue> CorrectiveRuleParameterValues { get; set; }
    }
}
