﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OnlineOperatorsWithdrawTariff
    {
        public int Id { get; set; }
        public int OfficeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public int? OnlineOperatorId { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public int ComissionCalculationTypeId { get; set; }
        public decimal? MinSumm { get; set; }
        public byte MinSummCurrencyType { get; set; }
        public decimal? MaxSumm { get; set; }
        public byte MaxSummCurrencyType { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
