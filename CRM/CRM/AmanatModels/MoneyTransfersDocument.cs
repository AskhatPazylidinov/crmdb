﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MoneyTransfersDocument
    {
        public int DocumentId { get; set; }
        public byte EventTypeId { get; set; }
        public string Filename { get; set; }
        public int MoneySystemId { get; set; }
        public byte SchemaAccountTypeId { get; set; }

        public virtual MoneySystem MoneySystem { get; set; }
    }
}
