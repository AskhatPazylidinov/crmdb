﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Correspondent
    {
        public int BranchId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
