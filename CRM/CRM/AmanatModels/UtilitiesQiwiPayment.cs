﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesQiwiPayment
    {
        public int OperationId { get; set; }
        public int ContragentTypeId { get; set; }
        public string PersonalAccountNo { get; set; }
        public decimal ToPaySumm { get; set; }

        public virtual ContragentType ContragentType { get; set; }
    }
}
