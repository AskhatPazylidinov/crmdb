﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesWaterProviderDepartment
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public int DepartmentId { get; set; }

        public virtual UtilitiesWaterDepartment Department { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
