﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsFieldsValue
    {
        public TransactionsFieldsValue()
        {
            TransactionsFieldsCalcValues = new HashSet<TransactionsFieldsCalcValue>();
        }

        public int Id { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }

        public virtual TransactionsField FieldNameNavigation { get; set; }
        public virtual ICollection<TransactionsFieldsCalcValue> TransactionsFieldsCalcValues { get; set; }
    }
}
