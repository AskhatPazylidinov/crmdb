﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate
    {
        public TariffsChangeDate()
        {
            Tariffs = new HashSet<Tariff>();
        }

        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }

        public virtual ICollection<Tariff> Tariffs { get; set; }
    }
}
