﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeviceType
    {
        public DeviceType()
        {
            ComissinEcvayers = new HashSet<ComissinEcvayer>();
            IndividualTariffCompanies = new HashSet<IndividualTariffCompany>();
            Offices = new HashSet<Office>();
            OperationDeviceTypes = new HashSet<OperationDeviceType>();
            ProductChanges = new HashSet<ProductChange>();
            SpecialAccount1s = new HashSet<SpecialAccount1>();
        }

        public int Code { get; set; }
        public string NameDevice { get; set; }

        public virtual ICollection<ComissinEcvayer> ComissinEcvayers { get; set; }
        public virtual ICollection<IndividualTariffCompany> IndividualTariffCompanies { get; set; }
        public virtual ICollection<Office> Offices { get; set; }
        public virtual ICollection<OperationDeviceType> OperationDeviceTypes { get; set; }
        public virtual ICollection<ProductChange> ProductChanges { get; set; }
        public virtual ICollection<SpecialAccount1> SpecialAccount1s { get; set; }
    }
}
