﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Form36BalGroup
    {
        public string BalanceGroup { get; set; }
        public string Article { get; set; }

        public virtual Form36 ArticleNavigation { get; set; }
        public virtual BalanceGroup BalanceGroupNavigation { get; set; }
    }
}
