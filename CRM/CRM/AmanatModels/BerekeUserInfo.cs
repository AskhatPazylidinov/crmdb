﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BerekeUserInfo
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ClientKey { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
