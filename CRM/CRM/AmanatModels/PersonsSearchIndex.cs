﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PersonsSearchIndex
    {
        public int Id { get; set; }
        public string Word { get; set; }
        public int ParentItemId { get; set; }
        public bool? WordStart { get; set; }

        public virtual Person ParentItem { get; set; }
    }
}
