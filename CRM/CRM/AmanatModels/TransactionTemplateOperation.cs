﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionTemplateOperation
    {
        public int TemplateOperationTypeId { get; set; }
        public int TransactionTemplateId { get; set; }
        public int BranchId { get; set; }
        public int OfficeId { get; set; }
        public int CurrencyId { get; set; }
        public short? CashSymbol { get; set; }
        public string Comment { get; set; }
        public decimal Amount { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual TemplateOperationType TemplateOperationType { get; set; }
        public virtual Currency3 TransactionTemplate { get; set; }
    }
}
