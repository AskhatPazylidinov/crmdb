﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ConfirmedPaymentsView
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public string AccountNo { get; set; }
        public string CardNo { get; set; }
        public int CurrencyId { get; set; }
        public decimal Amount { get; set; }
        public bool? IsWithdraw { get; set; }
        public DateTime TransactionDate { get; set; }
        public long? PaymentId { get; set; }
        public bool IsSendToIpc { get; set; }
        public bool? IsThroughCash { get; set; }
    }
}
