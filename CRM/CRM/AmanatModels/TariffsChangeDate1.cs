﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TariffsChangeDate1
    {
        public TariffsChangeDate1()
        {
            SafeTariffs = new HashSet<SafeTariff>();
        }

        public DateTime ChangeDate { get; set; }
        public string OrderNo { get; set; }

        public virtual ICollection<SafeTariff> SafeTariffs { get; set; }
    }
}
