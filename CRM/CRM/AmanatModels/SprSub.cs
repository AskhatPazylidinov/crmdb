﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SprSub
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Mfo { get; set; }
        public string LcSub { get; set; }
    }
}
