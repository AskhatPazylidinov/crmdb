﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Chapter21Stat
    {
        public DateTime Date { get; set; }
        public int Dir { get; set; }
        public string TrSys { get; set; }
        public string Cur { get; set; }
        public int? Days { get; set; }
        public int? Count { get; set; }
        public int? Maxcount { get; set; }
        public decimal? Sum { get; set; }
        public decimal? Maxsum { get; set; }
    }
}
