﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesBishkekTeploEnergoSetting
    {
        public int Id { get; set; }
        public string TemplateFilename { get; set; }
    }
}
