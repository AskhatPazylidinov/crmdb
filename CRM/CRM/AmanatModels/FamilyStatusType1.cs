﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FamilyStatusType1
    {
        public FamilyStatusType1()
        {
            SettingsFamilyStatusTypes = new HashSet<SettingsFamilyStatusType>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SettingsFamilyStatusType> SettingsFamilyStatusTypes { get; set; }
    }
}
