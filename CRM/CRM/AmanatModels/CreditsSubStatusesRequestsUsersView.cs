﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsSubStatusesRequestsUsersView
    {
        public int CreditId { get; set; }
        public int SubStatusId { get; set; }
        public int UserId { get; set; }
    }
}
