﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int ProviderId { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
        public DateTime TransactionDate { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
        public virtual User User { get; set; }
    }
}
