﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NationalBankReservesRequirement
    {
        public DateTime ActualDate { get; set; }
        public decimal PercentSumm { get; set; }
        public decimal ReserveSumm { get; set; }
    }
}
