﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeCoef
    {
        public int OfficeId { get; set; }
        public decimal? Coef { get; set; }
    }
}
