﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersStatusType
    {
        public CustomersStatusType()
        {
            CustomersStatuses = new HashSet<CustomersStatus>();
        }

        public int StatusType { get; set; }
        public string StatusDescription { get; set; }

        public virtual ICollection<CustomersStatus> CustomersStatuses { get; set; }
    }
}
