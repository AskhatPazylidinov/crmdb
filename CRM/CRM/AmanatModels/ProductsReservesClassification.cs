﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsReservesClassification
    {
        public int ProductId { get; set; }
        public DateTime ActualDate { get; set; }
        public int ClassificationId { get; set; }

        public virtual ReservesClassification Classification { get; set; }
        public virtual Product1 Product { get; set; }
    }
}
