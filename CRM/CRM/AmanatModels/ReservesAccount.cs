﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReservesAccount
    {
        public int BranchId { get; set; }
        public bool IsFinancialCompany { get; set; }
        public byte ShortTypeId { get; set; }
        public string ReserveAccountNo { get; set; }
        public string ExpenseAccountNo { get; set; }
        public int ReserveCurrencyId { get; set; }
        public int ExpenseCurrencyId { get; set; }

        public virtual Account1 Expense { get; set; }
        public virtual Account1 Reserve { get; set; }
    }
}
