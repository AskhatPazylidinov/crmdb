﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EarlyRepayStatement
    {
        public int CreditId { get; set; }
        public DateTime PaymentDate { get; set; }
        public int RepaymentType { get; set; }

        public virtual History Credit { get; set; }
    }
}
