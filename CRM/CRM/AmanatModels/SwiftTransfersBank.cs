﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfersBank
    {
        public long TransferId { get; set; }
        public byte BankType { get; set; }
        public string BankCode { get; set; }
        public string BankAccount { get; set; }
        public string AddInfo { get; set; }
        public string CountryCode { get; set; }
        public int Bictype { get; set; }
        public byte SwiftKey { get; set; }
        public string CityName { get; set; }

        public virtual SwiftTransfer Transfer { get; set; }
    }
}
