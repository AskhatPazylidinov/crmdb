﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class GarantInBranch
    {
        public int GarantType { get; set; }
        public int BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual GarantType GarantTypeNavigation { get; set; }
    }
}
