﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Account1
    {
        public Account1()
        {
            AccountsAttorneys = new HashSet<AccountsAttorney>();
            AccountsRevaluationAccount1Navigations = new HashSet<AccountsRevaluation>();
            AccountsRevaluationAccount1s = new HashSet<AccountsRevaluation>();
            AccountsSourceCurrencies = new HashSet<AccountsSourceCurrency>();
            AssetsAccounts = new HashSet<AssetsAccount>();
            Balance1s = new HashSet<Balance1>();
            BlockedAccounts = new HashSet<BlockedAccount>();
            BranchReservedAccounts = new HashSet<BranchReservedAccount>();
            ConversionAccounts = new HashSet<ConversionAccount>();
            ConversionsAccounts = new HashSet<ConversionsAccount>();
            CorrAccountsReserveAccounts = new HashSet<CorrAccountsReserveAccount>();
            CreditsAccounts = new HashSet<CreditsAccount>();
            CustomizableComissionsAccounts = new HashSet<CustomizableComissionsAccount>();
            DepositAccountCurrens = new HashSet<DepositAccount>();
            DepositAccumulativeAccounts = new HashSet<DepositAccumulativeAccount>();
            DepositoryAccounts = new HashSet<DepositoryAccount>();
            ExternalLoansPaymentAccounts = new HashSet<ExternalLoansPaymentAccount>();
            ImportTransactionCredits = new HashSet<ImportTransaction>();
            ImportTransactionDebets = new HashSet<ImportTransaction>();
            IncomeAccounts = new HashSet<IncomeAccount>();
            InfoSystemTransitAccountsForCommissions = new HashSet<InfoSystemTransitAccountsForCommission>();
            InsurancesIncomesAccounts = new HashSet<InsurancesIncomesAccount>();
            InterBranchTransfersAccounts = new HashSet<InterBranchTransfersAccount>();
            InterBranchTransfersIncomeAccounts = new HashSet<InterBranchTransfersIncomeAccount>();
            InterTransfers = new HashSet<InterTransfer>();
            K3depositsInBanks = new HashSet<K3depositsInBank>();
            MigrationAccounts = new HashSet<MigrationAccount>();
            MoneyTransfers = new HashSet<MoneyTransfer>();
            MoneyTransfersAccounts = new HashSet<MoneyTransfersAccount>();
            NonCashAccounts = new HashSet<NonCashAccount>();
            Odbaccounts = new HashSet<Odbaccount>();
            OnlineOperations = new HashSet<OnlineOperation>();
            OperationalAccountsBalances = new HashSet<OperationalAccountsBalance>();
            PartnerBanksAccounts = new HashSet<PartnerBanksAccount>();
            PartnersAccount1s = new HashSet<PartnersAccount1>();
            PayBalanceToEditAccount1s = new HashSet<PayBalanceToEdit>();
            PayBalanceToEditCs = new HashSet<PayBalanceToEdit>();
            ProductsSpecialAccounts = new HashSet<ProductsSpecialAccount>();
            ProvidersIncomeAccounts = new HashSet<ProvidersIncomeAccount>();
            ProvidersTransitAccounts = new HashSet<ProvidersTransitAccount>();
            ReplacedAccounts = new HashSet<ReplacedAccount>();
            ReservesAccountExpenses = new HashSet<ReservesAccount>();
            ReservesAccountReserves = new HashSet<ReservesAccount>();
            SavingAccounts = new HashSet<SavingAccount>();
            ScheduledPaymentCredits = new HashSet<ScheduledPayment>();
            ScheduledPaymentDebs = new HashSet<ScheduledPayment>();
            ServiceBlockedAccounts = new HashSet<ServiceBlockedAccount>();
            ServiceCustomers = new HashSet<ServiceCustomer>();
            ServiceProvidersInternalAccounts = new HashSet<ServiceProvidersInternalAccount>();
            SpecialAccount1s = new HashSet<SpecialAccount1>();
            SpecialAccounts = new HashSet<SpecialAccount>();
            SpecialCashierTypeAccounts = new HashSet<SpecialCashierTypeAccount>();
            SpecializedAccounts = new HashSet<SpecializedAccount>();
            SwiftAccounts = new HashSet<SwiftAccount>();
            TaxDepartmentsIncomeAccounts = new HashSet<TaxDepartmentsIncomeAccount>();
            TaxDepartmentsTransitAccounts = new HashSet<TaxDepartmentsTransitAccount>();
            Transaction2Account1s = new HashSet<Transaction2>();
            Transaction2Cs = new HashSet<Transaction2>();
            TransferAccount1Account1Navigations = new HashSet<TransferAccount1>();
            TransferAccount1Account1s = new HashSet<TransferAccount1>();
            Transfers = new HashSet<Transfer>();
        }

        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CustomerId { get; set; }
        public int OfficeId { get; set; }
        public string BalanceGroup { get; set; }
        public string AccountName { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public byte AccountTypeId { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal CurrentNationalBalance { get; set; }
        public string Codename { get; set; }
        public string AccountGroup { get; set; }
        public decimal DtSumV { get; set; }
        public decimal DtSumN { get; set; }
        public decimal CtSumV { get; set; }
        public decimal CtSumN { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual Account Account { get; set; }
        public virtual AccountStatementScheduler AccountStatementScheduler { get; set; }
        public virtual DepositAccount DepositAccountAccount1 { get; set; }
        public virtual DepositAccount DepositAccountAccount1Navigation { get; set; }
        public virtual DepositsBonusesIgnoreAccount DepositsBonusesIgnoreAccount { get; set; }
        public virtual DepositsBroughtByOther DepositsBroughtByOther { get; set; }
        public virtual DisabledAccount DisabledAccount { get; set; }
        public virtual OrganizationCorrAccount OrganizationCorrAccount { get; set; }
        public virtual SpecialTaxSettingsForAccount SpecialTaxSettingsForAccount { get; set; }
        public virtual SwiftCorrBankAccount SwiftCorrBankAccount { get; set; }
        public virtual ICollection<AccountsAttorney> AccountsAttorneys { get; set; }
        public virtual ICollection<AccountsRevaluation> AccountsRevaluationAccount1Navigations { get; set; }
        public virtual ICollection<AccountsRevaluation> AccountsRevaluationAccount1s { get; set; }
        public virtual ICollection<AccountsSourceCurrency> AccountsSourceCurrencies { get; set; }
        public virtual ICollection<AssetsAccount> AssetsAccounts { get; set; }
        public virtual ICollection<Balance1> Balance1s { get; set; }
        public virtual ICollection<BlockedAccount> BlockedAccounts { get; set; }
        public virtual ICollection<BranchReservedAccount> BranchReservedAccounts { get; set; }
        public virtual ICollection<ConversionAccount> ConversionAccounts { get; set; }
        public virtual ICollection<ConversionsAccount> ConversionsAccounts { get; set; }
        public virtual ICollection<CorrAccountsReserveAccount> CorrAccountsReserveAccounts { get; set; }
        public virtual ICollection<CreditsAccount> CreditsAccounts { get; set; }
        public virtual ICollection<CustomizableComissionsAccount> CustomizableComissionsAccounts { get; set; }
        public virtual ICollection<DepositAccount> DepositAccountCurrens { get; set; }
        public virtual ICollection<DepositAccumulativeAccount> DepositAccumulativeAccounts { get; set; }
        public virtual ICollection<DepositoryAccount> DepositoryAccounts { get; set; }
        public virtual ICollection<ExternalLoansPaymentAccount> ExternalLoansPaymentAccounts { get; set; }
        public virtual ICollection<ImportTransaction> ImportTransactionCredits { get; set; }
        public virtual ICollection<ImportTransaction> ImportTransactionDebets { get; set; }
        public virtual ICollection<IncomeAccount> IncomeAccounts { get; set; }
        public virtual ICollection<InfoSystemTransitAccountsForCommission> InfoSystemTransitAccountsForCommissions { get; set; }
        public virtual ICollection<InsurancesIncomesAccount> InsurancesIncomesAccounts { get; set; }
        public virtual ICollection<InterBranchTransfersAccount> InterBranchTransfersAccounts { get; set; }
        public virtual ICollection<InterBranchTransfersIncomeAccount> InterBranchTransfersIncomeAccounts { get; set; }
        public virtual ICollection<InterTransfer> InterTransfers { get; set; }
        public virtual ICollection<K3depositsInBank> K3depositsInBanks { get; set; }
        public virtual ICollection<MigrationAccount> MigrationAccounts { get; set; }
        public virtual ICollection<MoneyTransfer> MoneyTransfers { get; set; }
        public virtual ICollection<MoneyTransfersAccount> MoneyTransfersAccounts { get; set; }
        public virtual ICollection<NonCashAccount> NonCashAccounts { get; set; }
        public virtual ICollection<Odbaccount> Odbaccounts { get; set; }
        public virtual ICollection<OnlineOperation> OnlineOperations { get; set; }
        public virtual ICollection<OperationalAccountsBalance> OperationalAccountsBalances { get; set; }
        public virtual ICollection<PartnerBanksAccount> PartnerBanksAccounts { get; set; }
        public virtual ICollection<PartnersAccount1> PartnersAccount1s { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEditAccount1s { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEditCs { get; set; }
        public virtual ICollection<ProductsSpecialAccount> ProductsSpecialAccounts { get; set; }
        public virtual ICollection<ProvidersIncomeAccount> ProvidersIncomeAccounts { get; set; }
        public virtual ICollection<ProvidersTransitAccount> ProvidersTransitAccounts { get; set; }
        public virtual ICollection<ReplacedAccount> ReplacedAccounts { get; set; }
        public virtual ICollection<ReservesAccount> ReservesAccountExpenses { get; set; }
        public virtual ICollection<ReservesAccount> ReservesAccountReserves { get; set; }
        public virtual ICollection<SavingAccount> SavingAccounts { get; set; }
        public virtual ICollection<ScheduledPayment> ScheduledPaymentCredits { get; set; }
        public virtual ICollection<ScheduledPayment> ScheduledPaymentDebs { get; set; }
        public virtual ICollection<ServiceBlockedAccount> ServiceBlockedAccounts { get; set; }
        public virtual ICollection<ServiceCustomer> ServiceCustomers { get; set; }
        public virtual ICollection<ServiceProvidersInternalAccount> ServiceProvidersInternalAccounts { get; set; }
        public virtual ICollection<SpecialAccount1> SpecialAccount1s { get; set; }
        public virtual ICollection<SpecialAccount> SpecialAccounts { get; set; }
        public virtual ICollection<SpecialCashierTypeAccount> SpecialCashierTypeAccounts { get; set; }
        public virtual ICollection<SpecializedAccount> SpecializedAccounts { get; set; }
        public virtual ICollection<SwiftAccount> SwiftAccounts { get; set; }
        public virtual ICollection<TaxDepartmentsIncomeAccount> TaxDepartmentsIncomeAccounts { get; set; }
        public virtual ICollection<TaxDepartmentsTransitAccount> TaxDepartmentsTransitAccounts { get; set; }
        public virtual ICollection<Transaction2> Transaction2Account1s { get; set; }
        public virtual ICollection<Transaction2> Transaction2Cs { get; set; }
        public virtual ICollection<TransferAccount1> TransferAccount1Account1Navigations { get; set; }
        public virtual ICollection<TransferAccount1> TransferAccount1Account1s { get; set; }
        public virtual ICollection<Transfer> Transfers { get; set; }
    }
}
