﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeductionSetting
    {
        public byte DeductionTypeId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal Summ { get; set; }
    }
}
