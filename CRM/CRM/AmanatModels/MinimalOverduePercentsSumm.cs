﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class MinimalOverduePercentsSumm
    {
        public int CurrencyId { get; set; }
        public decimal MinimalSumm { get; set; }

        public virtual Currency3 Currency { get; set; }
    }
}
