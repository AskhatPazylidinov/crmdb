﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RetireReason
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
