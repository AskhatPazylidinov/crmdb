﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllowedDepositsWithdrawOffice
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int AllowedOfficeId { get; set; }
        public string Description { get; set; }

        public virtual Office1 AllowedOffice { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
