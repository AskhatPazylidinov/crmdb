﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class RoleCustomerStatus
    {
        public int RoleId { get; set; }
        public int StatusId { get; set; }

        public virtual Role Role { get; set; }
        public virtual CustomerStatus Status { get; set; }
    }
}
