﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonResidentTaxIncExpType
    {
        public int TaxId { get; set; }
        public int IncExpTypeId { get; set; }

        public virtual IncExpType IncExpType { get; set; }
        public virtual NonResidentTaxis Tax { get; set; }
    }
}
