﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsOperation
    {
        public PaymentsOperation()
        {
            Transaction1s = new HashSet<Transaction1>();
        }

        public long OperationId { get; set; }
        public long? PaymentId { get; set; }
        public byte? StatusId { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? OperationDate { get; set; }
        public int? UserId { get; set; }
        public int? OfficeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual Payment1 Payment { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Transaction1> Transaction1s { get; set; }
    }
}
