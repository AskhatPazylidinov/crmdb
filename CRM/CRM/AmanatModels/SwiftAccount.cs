﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftAccount
    {
        public int OfficeId { get; set; }
        public int SystemId { get; set; }
        public int ComissionId { get; set; }
        public byte CustomerTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual SwiftComission Comission { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual SwiftSystem System { get; set; }
    }
}
