﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class NettingOperationsTransaction
    {
        public long NettingId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual NettingOperation Netting { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
