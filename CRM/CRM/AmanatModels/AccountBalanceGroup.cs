﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class AccountBalanceGroup
    {
        public int Id { get; set; }
        public string BalanceGroup { get; set; }
    }
}
