﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditsSubStatusesUsersView
    {
        public int CreditId { get; set; }
        public int SubStatusId { get; set; }
        public int UserId { get; set; }
    }
}
