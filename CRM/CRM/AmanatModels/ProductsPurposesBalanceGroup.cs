﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsPurposesBalanceGroup
    {
        public int ProductId { get; set; }
        public int PurposeTypeId { get; set; }
        public byte BalanceTypeId { get; set; }
        public string ResidentGroup { get; set; }
        public string NonResidentGroup { get; set; }
        public bool IsTranchUse { get; set; }

        public virtual BalanceGroup NonResidentGroupNavigation { get; set; }
        public virtual Product1 Product { get; set; }
        public virtual Purpose PurposeType { get; set; }
        public virtual BalanceGroup ResidentGroupNavigation { get; set; }
    }
}
