﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankClosedDayTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public bool Deleted { get; set; }
    }
}
