﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class HeightExpInter
    {
        public int Age { get; set; }
        public decimal? Inter { get; set; }
    }
}
