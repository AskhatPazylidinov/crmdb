﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuredOperationView
    {
        public int ComparisonResult { get; set; }
        public bool? IsChecked { get; set; }
        public int OperationId { get; set; }
        public string Namespace { get; set; }
        public string ClassName { get; set; }
        public DateTime CreateDate { get; set; }
        public string ClassDescription { get; set; }
        public string NamespaceGroup { get; set; }
        public string MethodName { get; set; }
        public string Description { get; set; }
        public string OldDescription { get; set; }
        public string ExtendedDescription { get; set; }
        public string OldExtendedDescription { get; set; }
        public byte CriticalTypeId { get; set; }
    }
}
