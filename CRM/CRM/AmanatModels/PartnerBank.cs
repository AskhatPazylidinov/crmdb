﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnerBank
    {
        public PartnerBank()
        {
            MoneySystems = new HashSet<MoneySystem>();
            PartnerBanksAccounts = new HashSet<PartnerBanksAccount>();
        }

        public int PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string SwiftCode { get; set; }
        public int? PartnerCustomerId { get; set; }

        public virtual Customer PartnerCustomer { get; set; }
        public virtual ICollection<MoneySystem> MoneySystems { get; set; }
        public virtual ICollection<PartnerBanksAccount> PartnerBanksAccounts { get; set; }
    }
}
