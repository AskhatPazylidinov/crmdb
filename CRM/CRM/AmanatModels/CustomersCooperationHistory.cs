﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomersCooperationHistory
    {
        public int CustomerId { get; set; }
        public DateTime StartCooperationDate { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
