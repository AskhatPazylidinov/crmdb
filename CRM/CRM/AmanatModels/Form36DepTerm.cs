﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Form36DepTerm
    {
        public string AdditionalArticle { get; set; }
        public string Article { get; set; }
        public byte TypeId { get; set; }
    }
}
