﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PropertyField
    {
        public PropertyField()
        {
            FieldsValue1s = new HashSet<FieldsValue1>();
            TableFieldsValues = new HashSet<TableFieldsValue>();
        }

        public int FieldId { get; set; }
        public int PropertyId { get; set; }
        public string Name { get; set; }
        public int DataType { get; set; }
        public int Len { get; set; }
        public bool NewLine { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public string RaplaceSymbol { get; set; }
        public bool? UsedInGrid { get; set; }

        public virtual Property Property { get; set; }
        public virtual ICollection<FieldsValue1> FieldsValue1s { get; set; }
        public virtual ICollection<TableFieldsValue> TableFieldsValues { get; set; }
    }
}
