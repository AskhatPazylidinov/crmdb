﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Branch
    {
        public Branch()
        {
            ActiveBranches = new HashSet<ActiveBranch>();
            AutoPaymentsLogs = new HashSet<AutoPaymentsLog>();
            BalanceGroupsBalances = new HashSet<BalanceGroupsBalance>();
            BranchIds = new HashSet<BranchId>();
            BranchesCloseDays = new HashSet<BranchesCloseDay>();
            BranchesDeclensions = new HashSet<BranchesDeclension>();
            BranchesMinimalTaxesBases = new HashSet<BranchesMinimalTaxesBasis>();
            Correspondents = new HashSet<Correspondent>();
            CrossBranchConversions = new HashSet<CrossBranchConversion>();
            CustomizableComissionsAccounts = new HashSet<CustomizableComissionsAccount>();
            DepositoryAccounts = new HashSet<DepositoryAccount>();
            GarantInBranches = new HashSet<GarantInBranch>();
            InsuranceBranches = new HashSet<InsuranceBranch>();
            InterBranchTransfersAccounts = new HashSet<InterBranchTransfersAccount>();
            InterTransferReceiverBranches = new HashSet<InterTransfer>();
            InterTransferSenderBranches = new HashSet<InterTransfer>();
            Limit1s = new HashSet<Limit1>();
            MpccardsNos = new HashSet<MpccardsNo>();
            Office1s = new HashSet<Office1>();
            OperationalBalanceGroupBalances = new HashSet<OperationalBalanceGroupBalance>();
            Orders = new HashSet<Order>();
            OrganizationCorrAccounts = new HashSet<OrganizationCorrAccount>();
            OtherProperties = new HashSet<OtherProperty>();
            Partner2s = new HashSet<Partner2>();
            PayBalanceToEdits = new HashSet<PayBalanceToEdit>();
            Paysheets = new HashSet<Paysheet>();
            PositionAccountPairs = new HashSet<PositionAccountPair>();
            ProductInBranches = new HashSet<ProductInBranch>();
            ProductsBranches = new HashSet<ProductsBranch>();
            ProvidersTransitAccounts = new HashSet<ProvidersTransitAccount>();
            Saves = new HashSet<Safe>();
            ServiceProvidersBranches = new HashSet<ServiceProvidersBranch>();
            ServiceProvidersContragentsBranches = new HashSet<ServiceProvidersContragentsBranch>();
            ServiceProvidersInternalAccounts = new HashSet<ServiceProvidersInternalAccount>();
            Tariff2ReceiverBranches = new HashSet<Tariff2>();
            Tariff2SenderBranches = new HashSet<Tariff2>();
            TaxDepartmentsTransitAccounts = new HashSet<TaxDepartmentsTransitAccount>();
            TransfersNettingOperations = new HashSet<TransfersNettingOperation>();
            WithdrawTariffsByBranchAccountBranches = new HashSet<WithdrawTariffsByBranch>();
            WithdrawTariffsByBranchTransactionBranches = new HashSet<WithdrawTariffsByBranch>();
            WriteOffContrAccounts = new HashSet<WriteOffContrAccount>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public DateTime OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? CityId { get; set; }
        public string Bik { get; set; }
        public string HeadOfLegalDepartment { get; set; }
        public string ShortName { get; set; }
        public int DefaultCustomerId { get; set; }
        public string AccountantPosition { get; set; }
        public string AccountantName { get; set; }

        public virtual City1 City { get; set; }
        public virtual BranchCode BranchCode { get; set; }
        public virtual BranchesCode BranchesCode { get; set; }
        public virtual BranchesCustomer BranchesCustomer { get; set; }
        public virtual BranchesOrdersNo BranchesOrdersNo { get; set; }
        public virtual InfoSystemTransitAccountsForCommission InfoSystemTransitAccountsForCommission { get; set; }
        public virtual SavingAccount SavingAccount { get; set; }
        public virtual ICollection<ActiveBranch> ActiveBranches { get; set; }
        public virtual ICollection<AutoPaymentsLog> AutoPaymentsLogs { get; set; }
        public virtual ICollection<BalanceGroupsBalance> BalanceGroupsBalances { get; set; }
        public virtual ICollection<BranchId> BranchIds { get; set; }
        public virtual ICollection<BranchesCloseDay> BranchesCloseDays { get; set; }
        public virtual ICollection<BranchesDeclension> BranchesDeclensions { get; set; }
        public virtual ICollection<BranchesMinimalTaxesBasis> BranchesMinimalTaxesBases { get; set; }
        public virtual ICollection<Correspondent> Correspondents { get; set; }
        public virtual ICollection<CrossBranchConversion> CrossBranchConversions { get; set; }
        public virtual ICollection<CustomizableComissionsAccount> CustomizableComissionsAccounts { get; set; }
        public virtual ICollection<DepositoryAccount> DepositoryAccounts { get; set; }
        public virtual ICollection<GarantInBranch> GarantInBranches { get; set; }
        public virtual ICollection<InsuranceBranch> InsuranceBranches { get; set; }
        public virtual ICollection<InterBranchTransfersAccount> InterBranchTransfersAccounts { get; set; }
        public virtual ICollection<InterTransfer> InterTransferReceiverBranches { get; set; }
        public virtual ICollection<InterTransfer> InterTransferSenderBranches { get; set; }
        public virtual ICollection<Limit1> Limit1s { get; set; }
        public virtual ICollection<MpccardsNo> MpccardsNos { get; set; }
        public virtual ICollection<Office1> Office1s { get; set; }
        public virtual ICollection<OperationalBalanceGroupBalance> OperationalBalanceGroupBalances { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<OrganizationCorrAccount> OrganizationCorrAccounts { get; set; }
        public virtual ICollection<OtherProperty> OtherProperties { get; set; }
        public virtual ICollection<Partner2> Partner2s { get; set; }
        public virtual ICollection<PayBalanceToEdit> PayBalanceToEdits { get; set; }
        public virtual ICollection<Paysheet> Paysheets { get; set; }
        public virtual ICollection<PositionAccountPair> PositionAccountPairs { get; set; }
        public virtual ICollection<ProductInBranch> ProductInBranches { get; set; }
        public virtual ICollection<ProductsBranch> ProductsBranches { get; set; }
        public virtual ICollection<ProvidersTransitAccount> ProvidersTransitAccounts { get; set; }
        public virtual ICollection<Safe> Saves { get; set; }
        public virtual ICollection<ServiceProvidersBranch> ServiceProvidersBranches { get; set; }
        public virtual ICollection<ServiceProvidersContragentsBranch> ServiceProvidersContragentsBranches { get; set; }
        public virtual ICollection<ServiceProvidersInternalAccount> ServiceProvidersInternalAccounts { get; set; }
        public virtual ICollection<Tariff2> Tariff2ReceiverBranches { get; set; }
        public virtual ICollection<Tariff2> Tariff2SenderBranches { get; set; }
        public virtual ICollection<TaxDepartmentsTransitAccount> TaxDepartmentsTransitAccounts { get; set; }
        public virtual ICollection<TransfersNettingOperation> TransfersNettingOperations { get; set; }
        public virtual ICollection<WithdrawTariffsByBranch> WithdrawTariffsByBranchAccountBranches { get; set; }
        public virtual ICollection<WithdrawTariffsByBranch> WithdrawTariffsByBranchTransactionBranches { get; set; }
        public virtual ICollection<WriteOffContrAccount> WriteOffContrAccounts { get; set; }
    }
}
