﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Region1
    {
        public Region1()
        {
            Areas = new HashSet<Area>();
            City1s = new HashSet<City1>();
            TaxDistricts = new HashSet<TaxDistrict>();
        }

        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public string Code { get; set; }
        public string RegionCode { get; set; }
        public byte RegionTypeId { get; set; }
        public int? RegionIdbyNbkr { get; set; }

        public virtual Prbo37regionsCode Prbo37regionsCode { get; set; }
        public virtual RegionCode RegionCodeNavigation { get; set; }
        public virtual ICollection<Area> Areas { get; set; }
        public virtual ICollection<City1> City1s { get; set; }
        public virtual ICollection<TaxDistrict> TaxDistricts { get; set; }
    }
}
