﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsurancesIncomesAccount
    {
        public int OfficeId { get; set; }
        public string BalanceGroup { get; set; }
        public string IncomeAccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
