﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PreviousLoansPayment
    {
        public int CreditId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal MainSumm { get; set; }
        public decimal PercentSumm { get; set; }

        public virtual History Credit { get; set; }
    }
}
