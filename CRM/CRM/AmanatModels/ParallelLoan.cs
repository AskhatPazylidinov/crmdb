﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ParallelLoan
    {
        public int ParallelId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
        public string CibagreementNo { get; set; }
        public string CreditType { get; set; }
        public decimal Amount { get; set; }
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
