﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Garantee
    {
        public Garantee()
        {
            CreditsGarantees = new HashSet<CreditsGarantee>();
            FieldsValue1s = new HashSet<FieldsValue1>();
            GaranteesMarketPrices = new HashSet<GaranteesMarketPrice>();
            GaranteesMonitoringsSettings = new HashSet<GaranteesMonitoringsSetting>();
            GaranteesOfficers = new HashSet<GaranteesOfficer>();
            StoredDocuments = new HashSet<StoredDocument>();
            TableFieldsValues = new HashSet<TableFieldsValue>();
        }

        public int GaranteeId { get; set; }
        public int GarantTypeId { get; set; }
        public int CustomerId { get; set; }
        public int OfficeId { get; set; }
        public string AgreementNo { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual GarantType GarantType { get; set; }
        public virtual Office1 Office { get; set; }
        public virtual ICollection<CreditsGarantee> CreditsGarantees { get; set; }
        public virtual ICollection<FieldsValue1> FieldsValue1s { get; set; }
        public virtual ICollection<GaranteesMarketPrice> GaranteesMarketPrices { get; set; }
        public virtual ICollection<GaranteesMonitoringsSetting> GaranteesMonitoringsSettings { get; set; }
        public virtual ICollection<GaranteesOfficer> GaranteesOfficers { get; set; }
        public virtual ICollection<StoredDocument> StoredDocuments { get; set; }
        public virtual ICollection<TableFieldsValue> TableFieldsValues { get; set; }
    }
}
