﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Safe
    {
        public Safe()
        {
            SafesLeasings = new HashSet<SafesLeasing>();
        }

        public int SafeId { get; set; }
        public int SafeTypeId { get; set; }
        public int BranchId { get; set; }
        public string DisplayedNumber { get; set; }
        public bool IsActive { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual SafeType SafeType { get; set; }
        public virtual ICollection<SafesLeasing> SafesLeasings { get; set; }
    }
}
