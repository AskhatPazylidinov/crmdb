﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SpecializedAccountView
    {
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int OfficeId { get; set; }
        public int SpecialTypeId { get; set; }
        public string BalanceGroup { get; set; }
    }
}
