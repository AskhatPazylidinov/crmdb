﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficeSalary
    {
        public int OfficeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Status { get; set; }
        public int? TotalWorkDays { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? Position { get; set; }
    }
}
