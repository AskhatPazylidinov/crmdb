﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OfficesMinimalTaxesBasis
    {
        public int OfficeId { get; set; }
        public DateTime ActualDate { get; set; }
        public decimal MinIncomeTaxBase { get; set; }
        public decimal MinSocialTaxBase { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
