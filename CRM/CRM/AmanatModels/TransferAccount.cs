﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransferAccount
    {
        public int EmplNumber { get; set; }
        public decimal SumV { get; set; }
    }
}
