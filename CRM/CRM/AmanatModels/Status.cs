﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Status
    {
        public string Status1 { get; set; }
        public string StatusDesc { get; set; }
    }
}
