﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceSetting
    {
        public int ServiceId { get; set; }
        public string Address { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsHttps { get; set; }
    }
}
