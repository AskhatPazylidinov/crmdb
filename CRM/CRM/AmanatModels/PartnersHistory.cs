﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PartnersHistory
    {
        public int CompanyId { get; set; }
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public decimal SumV { get; set; }
        public decimal ComissionSum { get; set; }
        public byte IssueComissionPaymentTypeId { get; set; }

        public virtual Customer Company { get; set; }
    }
}
