﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerSearchView
    {
        public int CustomerId { get; set; }
        public int CustomerTypeId { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string CompanyName { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ResidenceStreet { get; set; }
        public string ResidenceHouse { get; set; }
        public string ResidenceFlat { get; set; }
        public string DocumentSeries { get; set; }
        public string RegistrationStreet { get; set; }
        public string RegistrationHouse { get; set; }
        public string RegistrationFlat { get; set; }
        public string DocumentNo { get; set; }
        public string AgreementNo { get; set; }
        public string AccountNo { get; set; }
        public string GroupName { get; set; }
        public bool? IsPrivateEnterpreneur { get; set; }
        public bool? IsIndividualEnterpreneur { get; set; }
        public string OldAccountNo { get; set; }
    }
}
