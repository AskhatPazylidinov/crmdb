﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ReplenishmentIndividualTariff
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public int ComissionCalculationTypeId { get; set; }
        public bool IsConfirmed { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
