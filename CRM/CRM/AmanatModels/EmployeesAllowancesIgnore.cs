﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesAllowancesIgnore
    {
        public int EmployeeId { get; set; }
        public int AllowanceTypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual AllowancesType AllowanceType { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
