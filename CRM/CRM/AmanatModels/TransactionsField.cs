﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsField
    {
        public TransactionsField()
        {
            TransactionsFieldsCalcValues = new HashSet<TransactionsFieldsCalcValue>();
            TransactionsFieldsValues = new HashSet<TransactionsFieldsValue>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsCalculated { get; set; }

        public virtual ICollection<TransactionsFieldsCalcValue> TransactionsFieldsCalcValues { get; set; }
        public virtual ICollection<TransactionsFieldsValue> TransactionsFieldsValues { get; set; }
    }
}
