﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankCloseDayModule
    {
        public DateTime CloseDate { get; set; }
        public int ModuleId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
