﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Employee1
    {
        public Employee1()
        {
            Absences = new HashSet<Absence>();
            Aliment1s = new HashSet<Aliment1>();
            Attestations = new HashSet<Attestation>();
            Avances = new HashSet<Avance>();
            Awards = new HashSet<Award>();
            BusinessTrips = new HashSet<BusinessTrip>();
            Child1s = new HashSet<Child1>();
            CurrentIncExps = new HashSet<CurrentIncExp>();
            Delays = new HashSet<Delay>();
            DepositsSettings = new HashSet<DepositsSetting>();
            Educations = new HashSet<Education>();
            EmployeeAccounts = new HashSet<EmployeeAccount>();
            EmployeeSalaries = new HashSet<EmployeeSalary>();
            EmployeeWorkedDays = new HashSet<EmployeeWorkedDay>();
            EmployeesTables = new HashSet<EmployeesTable>();
            EmployeesTablesApproveds = new HashSet<EmployeesTablesApproved>();
            Experiences = new HashSet<Experience>();
            HolidayWorkdays = new HashSet<HolidayWorkday>();
            Illness1s = new HashSet<Illness1>();
            OfficeGroupIncExpOperations = new HashSet<OfficeGroupIncExpOperation>();
            Order1s = new HashSet<Order1>();
            Position1s = new HashSet<Position1>();
            SalariesIncsExps = new HashSet<SalariesIncsExp>();
            VacationsApproveds = new HashSet<VacationsApproved>();
            VacationsPlanneds = new HashSet<VacationsPlanned>();
            training = new HashSet<Training>();
        }

        public int EmployeeId { get; set; }
        public bool? LangKg { get; set; }
        public bool? LangRu { get; set; }
        public bool? LangEn { get; set; }
        public string LangOther { get; set; }
        public int? CurrentPositionId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public bool? IsPensioner { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueAuthority { get; set; }
        public int? DiplomaTypeId { get; set; }
        public int? TypeId { get; set; }
        public string No { get; set; }
        public string Ssstatus { get; set; }
        public bool? VoinStatus { get; set; }
        public string VoinRayon { get; set; }
        public string VoinVus { get; set; }
        public int? CategoryId { get; set; }
        public DateTime? RetiredDate { get; set; }
        public bool? Retired { get; set; }
        public string VoinSpecReg { get; set; }
        public decimal? DoshCardPercentage { get; set; }
        public string Nationality { get; set; }
        public decimal? AddDeduction { get; set; }
        public bool? IsNotTaxDeduction { get; set; }
        public bool? IsDeduction { get; set; }
        public int? RankId { get; set; }
        public int? VacationsDayCount { get; set; }
        public DateTime? VacationsDayStart { get; set; }
        public string VoinZvanie { get; set; }
        public bool? IsDisability { get; set; }

        public virtual Customer Employee { get; set; }
        public virtual OrdersVacation OrdersVacation { get; set; }
        public virtual ICollection<Absence> Absences { get; set; }
        public virtual ICollection<Aliment1> Aliment1s { get; set; }
        public virtual ICollection<Attestation> Attestations { get; set; }
        public virtual ICollection<Avance> Avances { get; set; }
        public virtual ICollection<Award> Awards { get; set; }
        public virtual ICollection<BusinessTrip> BusinessTrips { get; set; }
        public virtual ICollection<Child1> Child1s { get; set; }
        public virtual ICollection<CurrentIncExp> CurrentIncExps { get; set; }
        public virtual ICollection<Delay> Delays { get; set; }
        public virtual ICollection<DepositsSetting> DepositsSettings { get; set; }
        public virtual ICollection<Education> Educations { get; set; }
        public virtual ICollection<EmployeeAccount> EmployeeAccounts { get; set; }
        public virtual ICollection<EmployeeSalary> EmployeeSalaries { get; set; }
        public virtual ICollection<EmployeeWorkedDay> EmployeeWorkedDays { get; set; }
        public virtual ICollection<EmployeesTable> EmployeesTables { get; set; }
        public virtual ICollection<EmployeesTablesApproved> EmployeesTablesApproveds { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual ICollection<HolidayWorkday> HolidayWorkdays { get; set; }
        public virtual ICollection<Illness1> Illness1s { get; set; }
        public virtual ICollection<OfficeGroupIncExpOperation> OfficeGroupIncExpOperations { get; set; }
        public virtual ICollection<Order1> Order1s { get; set; }
        public virtual ICollection<Position1> Position1s { get; set; }
        public virtual ICollection<SalariesIncsExp> SalariesIncsExps { get; set; }
        public virtual ICollection<VacationsApproved> VacationsApproveds { get; set; }
        public virtual ICollection<VacationsPlanned> VacationsPlanneds { get; set; }
        public virtual ICollection<Training> training { get; set; }
    }
}
