﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DeletedTransactionsOperationInfoView
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int CurrencyId { get; set; }
        public DateTime TransactionDate { get; set; }
        public byte OperationTypeId { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal SumV { get; set; }
        public decimal SumN { get; set; }
        public byte TransactionTypeId { get; set; }
        public byte ExtendedTypeId { get; set; }
        public string Comment { get; set; }
        public byte StatusId { get; set; }
        public byte IsFinalTransaction { get; set; }
        public byte CashSymbol { get; set; }
        public DateTime DateV { get; set; }
        public string UserName { get; set; }
        public string ResponsiblePerson { get; set; }
        public int OfficeId { get; set; }
        public int? AttorneyId { get; set; }
        public DateTime DeleteDate { get; set; }
        public string DeleteUserName { get; set; }
        public string ApproveUserName { get; set; }
        public bool IsReviewed { get; set; }
        public bool? IsComplex { get; set; }
        public string CtBalanceGroup { get; set; }
        public string DtBalanceGroup { get; set; }
        public int? Id { get; set; }
        public bool? IsApproved { get; set; }
        public int? BranchId { get; set; }
        public int? CountryId { get; set; }
        public string OperCode { get; set; }
        public string OfficeName { get; set; }
        public string BranchName { get; set; }
        public int RoleId { get; set; }
        public string UserRoleName { get; set; }
        public int UserId { get; set; }
        public string ReasonForDelete { get; set; }
    }
}
