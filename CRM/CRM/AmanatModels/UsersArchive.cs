﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersArchive
    {
        public int UserId { get; set; }
        public DateTime ArchiveDate { get; set; }
        public DateTime DateOd { get; set; }
        public int OfficeId { get; set; }
        public string UserName { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public int? DefaultModuleId { get; set; }
        public string WorkDir { get; set; }
        public DateTime PasswordExpiryDate { get; set; }
        public bool HasToChangePasswordAfterLogin { get; set; }
        public bool SignIn { get; set; }
        public string DomainUsername { get; set; }
    }
}
