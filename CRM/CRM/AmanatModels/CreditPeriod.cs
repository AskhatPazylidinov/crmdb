﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreditPeriod
    {
        public int Period { get; set; }
        public string AccountNo { get; set; }
    }
}
