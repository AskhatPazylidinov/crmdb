﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExpenseType
    {
        public ExpenseType()
        {
            OtherPropertyExpenses = new HashSet<OtherPropertyExpense>();
            OtherPropertyRealExpenses = new HashSet<OtherPropertyRealExpense>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OtherPropertyExpense> OtherPropertyExpenses { get; set; }
        public virtual ICollection<OtherPropertyRealExpense> OtherPropertyRealExpenses { get; set; }
    }
}
