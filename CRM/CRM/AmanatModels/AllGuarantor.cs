﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AllGuarantor
    {
        public int CreditId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte Status { get; set; }
        public decimal GuaranteeAmount { get; set; }

        public virtual History Credit { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
