﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ParallelLoansTemp
    {
        public int Id { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StatusDate { get; set; }
        public string AgreementNo { get; set; }
        public int OverdueDays { get; set; }
        public string CibCustomerId { get; set; }
        public string CustomerInn { get; set; }
        public string CreditStatus { get; set; }
        public string CreditType { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public string RepaymentStatus { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ReasonOverdueDay { get; set; }
    }
}
