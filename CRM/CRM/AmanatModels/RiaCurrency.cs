﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RiaCurrency
    {
        public RiaCurrency()
        {
            BankCurrencies = new HashSet<BankCurrency>();
            CountryCurrencies = new HashSet<CountryCurrency>();
        }

        public string RiaCurrencyCode { get; set; }
        public string CountryName { get; set; }
        public string CurrencyName { get; set; }

        public virtual ICollection<BankCurrency> BankCurrencies { get; set; }
        public virtual ICollection<CountryCurrency> CountryCurrencies { get; set; }
    }
}
