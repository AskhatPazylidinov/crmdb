﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class PenalityType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
