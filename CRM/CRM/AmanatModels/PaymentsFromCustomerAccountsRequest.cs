﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaymentsFromCustomerAccountsRequest
    {
        public int CreditId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public DateTime LogDate { get; set; }

        public virtual History Credit { get; set; }
    }
}
