﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IndividualTariffValue
    {
        public IndividualTariffValue()
        {
            IndividualTariffDetails = new HashSet<IndividualTariffDetail>();
            IndividualTariffValuesHistories = new HashSet<IndividualTariffValuesHistory>();
        }

        public int TariffId { get; set; }
        public int CustomerId { get; set; }
        public string MainAccountNo { get; set; }
        public decimal? TariffRate { get; set; }
        public bool? IsPercent { get; set; }
        public decimal TariffMonth { get; set; }

        public virtual StandardTariff Tariff { get; set; }
        public virtual ICollection<IndividualTariffDetail> IndividualTariffDetails { get; set; }
        public virtual ICollection<IndividualTariffValuesHistory> IndividualTariffValuesHistories { get; set; }
    }
}
