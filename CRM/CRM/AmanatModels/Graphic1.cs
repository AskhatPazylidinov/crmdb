﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Graphic1
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal ReplenishmentAmount { get; set; }
        public decimal? MainSumm { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? Rate { get; set; }
        public decimal? CapitalizationSumm { get; set; }
        public decimal? Replenished { get; set; }
        public bool? IsCapitalization { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
