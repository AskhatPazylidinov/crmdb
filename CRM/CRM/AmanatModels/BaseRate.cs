﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class BaseRate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Rate { get; set; }
        public bool? IsPercent { get; set; }
        public string BalanceGroup { get; set; }
    }
}
