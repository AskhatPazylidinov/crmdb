﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsLogsProductsCurrency1
    {
        public int LogId { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public decimal? MinDepositSumm { get; set; }
        public decimal? MaxDepositSumm { get; set; }
        public decimal MinSummOnAccount { get; set; }
        public int MinSummOnAccountTypeId { get; set; }
        public bool? IsActive { get; set; }
        public int LogUserId { get; set; }
        public DateTime LogDate { get; set; }
        public byte LogChangeTypeId { get; set; }

        public virtual User LogUser { get; set; }
    }
}
