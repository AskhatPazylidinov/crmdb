﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class WalletOperationTransaction
    {
        public long WalletOperationId { get; set; }
        public long Position { get; set; }
        public short Positionn { get; set; }

        public virtual Transaction2 PositionNavigation { get; set; }
        public virtual WalletOperation WalletOperation { get; set; }
    }
}
