﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class DuplicatesMatch
    {
        public int SrcId { get; set; }
        public int MatchId { get; set; }
        public int? OfficeId { get; set; }
    }
}
