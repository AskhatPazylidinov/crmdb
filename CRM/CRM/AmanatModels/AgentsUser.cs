﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsUser
    {
        public AgentsUser()
        {
            AgentsUsersLocks = new HashSet<AgentsUsersLock>();
            AgentsUsersLogs = new HashSet<AgentsUsersLog>();
            AgentsUsersPasswords = new HashSet<AgentsUsersPassword>();
            TransfersOperations = new HashSet<TransfersOperation>();
        }

        public int AgentUserId { get; set; }
        public int AgentBranchId { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string HashPassword { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime PasswordExpiryDate { get; set; }
        public bool HasToChangePasswordAfterLogin { get; set; }

        public virtual AgentsBranch AgentBranch { get; set; }
        public virtual ICollection<AgentsUsersLock> AgentsUsersLocks { get; set; }
        public virtual ICollection<AgentsUsersLog> AgentsUsersLogs { get; set; }
        public virtual ICollection<AgentsUsersPassword> AgentsUsersPasswords { get; set; }
        public virtual ICollection<TransfersOperation> TransfersOperations { get; set; }
    }
}
