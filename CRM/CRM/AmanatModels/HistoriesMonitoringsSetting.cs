﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class HistoriesMonitoringsSetting
    {
        public int CreditId { get; set; }
        public int TypeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Interval { get; set; }
        public int? MaxMonitorings { get; set; }

        public virtual History Credit { get; set; }
        public virtual MonitoringsType Type { get; set; }
    }
}
