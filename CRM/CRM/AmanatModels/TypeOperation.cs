﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TypeOperation
    {
        public TypeOperation()
        {
            OperationTemplates = new HashSet<OperationTemplate>();
        }

        public int TypeOperationId { get; set; }
        public int OperationId { get; set; }
        public int GroupId { get; set; }
        public int? TypeId { get; set; }

        public virtual AssetGroup Group { get; set; }
        public virtual Operation Operation { get; set; }
        public virtual Type Type { get; set; }
        public virtual ICollection<OperationTemplate> OperationTemplates { get; set; }
    }
}
