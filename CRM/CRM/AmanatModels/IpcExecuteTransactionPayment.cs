﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class IpcExecuteTransactionPayment
    {
        public int Id { get; set; }
        public string AccountNo { get; set; }
        public int AccountCurrencyId { get; set; }
        public int TransactionTypeId { get; set; }

        public virtual Account Account { get; set; }
        public virtual IpcExecuteTransaction IdNavigation { get; set; }
        public virtual EnrollmentIpcExecuteTransaction EnrollmentIpcExecuteTransaction { get; set; }
        public virtual TransactionsIpcExecuteTransactionPayment TransactionsIpcExecuteTransactionPayment { get; set; }
    }
}
