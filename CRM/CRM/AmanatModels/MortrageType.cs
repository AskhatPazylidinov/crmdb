﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class MortrageType
    {
        public MortrageType()
        {
            GuaranteeIssueComissions = new HashSet<GuaranteeIssueComission>();
            Histories = new HashSet<History>();
            InverseParent = new HashSet<MortrageType>();
            ProductsInterests = new HashSet<ProductsInterest>();
            ProductsIssueComissions = new HashSet<ProductsIssueComission>();
            ProductsMortrageTypes = new HashSet<ProductsMortrageType>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int? ParentId { get; set; }

        public virtual MortrageType Parent { get; set; }
        public virtual ICollection<GuaranteeIssueComission> GuaranteeIssueComissions { get; set; }
        public virtual ICollection<History> Histories { get; set; }
        public virtual ICollection<MortrageType> InverseParent { get; set; }
        public virtual ICollection<ProductsInterest> ProductsInterests { get; set; }
        public virtual ICollection<ProductsIssueComission> ProductsIssueComissions { get; set; }
        public virtual ICollection<ProductsMortrageType> ProductsMortrageTypes { get; set; }
    }
}
