﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PercentBalances18012021
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal PercentBalance { get; set; }
    }
}
