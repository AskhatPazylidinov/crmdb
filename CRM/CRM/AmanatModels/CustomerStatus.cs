﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomerStatus
    {
        public CustomerStatus()
        {
            RoleCustomerStatuses = new HashSet<RoleCustomerStatus>();
        }

        public int StatusId { get; set; }
        public string Name { get; set; }
        public bool ShowInReport { get; set; }

        public virtual ICollection<RoleCustomerStatus> RoleCustomerStatuses { get; set; }
    }
}
