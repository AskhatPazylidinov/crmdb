﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SocialFondDepartmentsExternalAccount
    {
        public int SocialFondDepartmentId { get; set; }
        public string ExternalBikcode { get; set; }
        public string ExternalAccountNo { get; set; }
        public int PaymentTypeId { get; set; }

        public virtual SocialFondDepartment SocialFondDepartment { get; set; }
    }
}
