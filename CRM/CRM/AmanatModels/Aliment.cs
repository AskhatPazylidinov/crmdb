﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Aliment
    {
        public Aliment()
        {
            PaysheetsEmployeesAliments = new HashSet<PaysheetsEmployeesAliment>();
        }

        public int AlimentId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal PercentSumm { get; set; }
        public string Description { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual ICollection<PaysheetsEmployeesAliment> PaysheetsEmployeesAliments { get; set; }
    }
}
