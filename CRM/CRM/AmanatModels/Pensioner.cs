﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Pensioner
    {
        public int EmployeeId { get; set; }
        public string DocumentNo { get; set; }
        public DateTime IssueDate { get; set; }
        public string IssueAuthority { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
