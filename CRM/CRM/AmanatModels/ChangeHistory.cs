﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ChangeHistory
    {
        public int Id { get; set; }
        public long PaymentId { get; set; }
        public string ChangedField { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int UserId { get; set; }
        public DateTime ChangedDate { get; set; }

        public virtual Payment1 Payment { get; set; }
    }
}
