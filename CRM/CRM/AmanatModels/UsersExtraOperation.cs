﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UsersExtraOperation
    {
        public int UserId { get; set; }
        public int OperationId { get; set; }
        public bool IsAllowed { get; set; }

        public virtual SecuredOperation Operation { get; set; }
        public virtual User User { get; set; }
    }
}
