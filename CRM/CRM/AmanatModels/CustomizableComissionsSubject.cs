﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomizableComissionsSubject
    {
        public int ComissionId { get; set; }
        public int SubjectTypeId { get; set; }
        public int? PaymentTypeId { get; set; }

        public virtual CustomizableComission Comission { get; set; }
    }
}
