﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SalariesIncsExp
    {
        public int Id { get; set; }
        public int? EmployeeId { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public int? IncExpTypeId { get; set; }
        public int? Direction { get; set; }
        public decimal? Summa { get; set; }
        public string DebetAccountNo { get; set; }
        public string CreditAccontNo { get; set; }
        public decimal? Tax { get; set; }
        public decimal? EeSf { get; set; }
        public decimal? ErSf { get; set; }
        public string Reason { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
