﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Office2
    {
        public Office2()
        {
            Aimags = new HashSet<Aimag>();
            TerminalTaxPayments = new HashSet<TerminalTaxPayment>();
        }

        public int Code { get; set; }
        public string Name { get; set; }
        public int RegionCode { get; set; }
        public string AccountNo { get; set; }

        public virtual Region2 RegionCodeNavigation { get; set; }
        public virtual ICollection<Aimag> Aimags { get; set; }
        public virtual ICollection<TerminalTaxPayment> TerminalTaxPayments { get; set; }
    }
}
