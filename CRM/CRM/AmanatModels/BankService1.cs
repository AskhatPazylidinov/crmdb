﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class BankService1
    {
        public BankService1()
        {
            ServiceBalances = new HashSet<ServiceBalance>();
            ServiceBlockedAccounts = new HashSet<ServiceBlockedAccount>();
            ServiceCustomers = new HashSet<ServiceCustomer>();
            ServiceIndividualTariffs = new HashSet<ServiceIndividualTariff>();
            ServiceLogs = new HashSet<ServiceLog>();
            ServiceTariffs = new HashSet<ServiceTariff>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ServiceType { get; set; }
        public int IncomeOperationType { get; set; }
        public int FirstEnableNotChargeDays { get; set; }
        public int DisableAfterOverdueMonth { get; set; }
        public decimal ExchangeRateError { get; set; }
        public int? OverdueInterval { get; set; }
        public int? ProgramModuleId { get; set; }

        public virtual ICollection<ServiceBalance> ServiceBalances { get; set; }
        public virtual ICollection<ServiceBlockedAccount> ServiceBlockedAccounts { get; set; }
        public virtual ICollection<ServiceCustomer> ServiceCustomers { get; set; }
        public virtual ICollection<ServiceIndividualTariff> ServiceIndividualTariffs { get; set; }
        public virtual ICollection<ServiceLog> ServiceLogs { get; set; }
        public virtual ICollection<ServiceTariff> ServiceTariffs { get; set; }
    }
}
