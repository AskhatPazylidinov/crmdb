﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OtherPropertyExpense
    {
        public int Id { get; set; }
        public int? ProchkaId { get; set; }
        public int? ExpenseTypeId { get; set; }
        public decimal? Val { get; set; }

        public virtual ExpenseType ExpenseType { get; set; }
        public virtual OtherProperty Prochka { get; set; }
    }
}
