﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class JointAccountCustomer
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
