﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class GaranteesMonitoring
    {
        public GaranteesMonitoring()
        {
            GaranteesMonitoringsStatuses = new HashSet<GaranteesMonitoringsStatus>();
        }

        public int MonitoringId { get; set; }
        public int CreditGaranteeRelationId { get; set; }
        public int MonitoringTypeId { get; set; }
        public int Mmonth { get; set; }
        public int Myear { get; set; }

        public virtual CreditsGarantee CreditGaranteeRelation { get; set; }
        public virtual MonitoringsType1 MonitoringType { get; set; }
        public virtual ICollection<GaranteesMonitoringsStatus> GaranteesMonitoringsStatuses { get; set; }
    }
}
