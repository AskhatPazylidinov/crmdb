﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceIndividualTariff
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ServiceTypeId { get; set; }
        public int Interval { get; set; }
        public decimal ComissionSumm { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsApprove { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual BankService1 ServiceType { get; set; }
    }
}
