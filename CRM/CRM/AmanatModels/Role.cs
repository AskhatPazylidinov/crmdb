﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Role
    {
        public Role()
        {
            AllowedOperationsInRoles = new HashSet<AllowedOperationsInRole>();
            ConversionLimitsByRoles = new HashSet<ConversionLimitsByRole>();
            CreateTransactionsInModuleRoles = new HashSet<CreateTransactionsInModuleRole>();
            EditApprovedParamsLimitsOnRoles = new HashSet<EditApprovedParamsLimitsOnRole>();
            IssueLoanLimitsOnRoles = new HashSet<IssueLoanLimitsOnRole>();
            RoleCustomerStatuses = new HashSet<RoleCustomerStatus>();
            RoleLimits = new HashSet<RoleLimit>();
            RolesAllowedViewAccounts = new HashSet<RolesAllowedViewAccount>();
            SubStatusesLimitsOnRoles = new HashSet<SubStatusesLimitsOnRole>();
            SubStatusesRequestsLimitsOnRoles = new HashSet<SubStatusesRequestsLimitsOnRole>();
            UsersRoles = new HashSet<UsersRole>();
            UsersRolesChanges = new HashSet<UsersRolesChange>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AllowedOperationsInRole> AllowedOperationsInRoles { get; set; }
        public virtual ICollection<ConversionLimitsByRole> ConversionLimitsByRoles { get; set; }
        public virtual ICollection<CreateTransactionsInModuleRole> CreateTransactionsInModuleRoles { get; set; }
        public virtual ICollection<EditApprovedParamsLimitsOnRole> EditApprovedParamsLimitsOnRoles { get; set; }
        public virtual ICollection<IssueLoanLimitsOnRole> IssueLoanLimitsOnRoles { get; set; }
        public virtual ICollection<RoleCustomerStatus> RoleCustomerStatuses { get; set; }
        public virtual ICollection<RoleLimit> RoleLimits { get; set; }
        public virtual ICollection<RolesAllowedViewAccount> RolesAllowedViewAccounts { get; set; }
        public virtual ICollection<SubStatusesLimitsOnRole> SubStatusesLimitsOnRoles { get; set; }
        public virtual ICollection<SubStatusesRequestsLimitsOnRole> SubStatusesRequestsLimitsOnRoles { get; set; }
        public virtual ICollection<UsersRole> UsersRoles { get; set; }
        public virtual ICollection<UsersRolesChange> UsersRolesChanges { get; set; }
    }
}
