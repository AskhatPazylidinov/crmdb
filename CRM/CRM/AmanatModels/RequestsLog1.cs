﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RequestsLog1
    {
        public long LogId { get; set; }
        public int UserId { get; set; }
        public int CustomerId { get; set; }
        public DateTime RequestDate { get; set; }
        public Guid RequestId { get; set; }
        public byte[] Report { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
