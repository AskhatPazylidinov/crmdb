﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InfoSystemSetting
    {
        public int SettingId { get; set; }
        public string UrlAddress { get; set; }
        public bool UseIntegration { get; set; }
    }
}
