﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Doccode
    {
        public int KyCode { get; set; }
        public int KyParentcode { get; set; }
        public string FlName { get; set; }
        public int? Id { get; set; }
    }
}
