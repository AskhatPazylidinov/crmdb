﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NonCashOperationsView
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public DateTime TransactionDate { get; set; }
        public string DocumentNo { get; set; }
        public string DebetAccountId { get; set; }
        public string CreditAccountId { get; set; }
        public decimal SumV { get; set; }
        public DateTime DateV { get; set; }
        public string Comment { get; set; }
        public string DtBalanceGroup { get; set; }
        public string CtBalanceGroup { get; set; }
        public string Symbol { get; set; }
        public string Fullname { get; set; }
        public bool? IsNotSet { get; set; }
        public byte? NonCashOperationTypeId { get; set; }
        public decimal? NonCashRate { get; set; }
        public int IsIncomeOperation { get; set; }
        public byte? TypeId { get; set; }
        public string NameIncome { get; set; }
        public string CommentIncome { get; set; }
        public string NameWithdraw { get; set; }
        public string CommentWithdraw { get; set; }
        public bool? IsRateRequired { get; set; }
        public string NonCashOperationType { get; set; }
        public string NonCashOperationTypeComment { get; set; }
    }
}
