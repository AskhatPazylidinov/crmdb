﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class InsuranceInterest
    {
        public int InsuranceId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int InsureTypeId { get; set; }
        public decimal Interest { get; set; }
        public bool? IsInsuredInBank { get; set; }
        public string Filename { get; set; }
        public decimal Comission { get; set; }

        public virtual InsuranceChange InsuranceChange { get; set; }
        public virtual InsuranceType InsureType { get; set; }
    }
}
