﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ComissionDiscountRate
    {
        public int CreditId { get; set; }
        public decimal DiscountRate { get; set; }

        public virtual History Credit { get; set; }
    }
}
