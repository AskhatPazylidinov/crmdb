﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationsCard
    {
        public int OperationId { get; set; }
        public int CardId { get; set; }

        public virtual DetailedOperation Operation { get; set; }
    }
}
