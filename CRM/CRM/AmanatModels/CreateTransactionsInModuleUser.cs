﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CreateTransactionsInModuleUser
    {
        public int UserId { get; set; }
        public int ProgramModuleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual ProgramModule ProgramModule { get; set; }
        public virtual User User { get; set; }
    }
}
