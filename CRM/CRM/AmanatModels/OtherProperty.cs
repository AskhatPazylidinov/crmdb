﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OtherProperty
    {
        public OtherProperty()
        {
            OtherPropertyExpenses = new HashSet<OtherPropertyExpense>();
            OtherPropertyInstallments = new HashSet<OtherPropertyInstallment>();
            OtherPropertyRealExpenses = new HashSet<OtherPropertyRealExpense>();
        }

        public int Id { get; set; }
        public int? BranchId { get; set; }
        public string CustomerFullname { get; set; }
        public int? Status { get; set; }
        public decimal? PrimarySum { get; set; }
        public decimal? ProcSum { get; set; }
        public decimal? FineSum { get; set; }
        public string GuaranteeName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public decimal? MarketVal { get; set; }
        public string DocData { get; set; }
        public string Reason { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? ProchkaDate { get; set; }
        public decimal? BalanceCost { get; set; }
        public decimal? AuctionOrgFee { get; set; }
        public decimal? NotaryPublicFee { get; set; }
        public decimal? OtherExpenses { get; set; }
        public decimal? RealizationCost { get; set; }
        public int? RestrictionType { get; set; }
        public string ClientFullname { get; set; }
        public string RealReason { get; set; }
        public DateTime? RealDocDate { get; set; }
        public string RealDocData { get; set; }
        public decimal? RealizationCost2 { get; set; }
        public string RealCondition { get; set; }
        public decimal? CreditSum { get; set; }
        public string Comment { get; set; }
        public decimal? RasrochkaPrimarySum { get; set; }
        public decimal? RasrochkaLoanSum { get; set; }
        public int? RasrochkaSrok { get; set; }
        public DateTime? RasrochkaContractDate { get; set; }
        public DateTime? RasrochkaRegDate { get; set; }
        public string ProtokolKkno { get; set; }
        public DateTime? ProtokolKkdate { get; set; }
        public string ReshenieNo { get; set; }
        public DateTime? ReshenieDate { get; set; }
        public int? RealizationType { get; set; }
        public decimal? CreditSum1 { get; set; }
        public DateTime? CreditIssueDate { get; set; }
        public decimal? CreditProc { get; set; }
        public int? CreditPeriod { get; set; }
        public string CreditDeptor { get; set; }
        public int? CreditId { get; set; }
        public int? CreditCurrencyId { get; set; }
        public int? UserId { get; set; }
        public decimal? Classification { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual OtherPropertyRestrictionType RestrictionTypeNavigation { get; set; }
        public virtual OtherPropertyStatus StatusNavigation { get; set; }
        public virtual ICollection<OtherPropertyExpense> OtherPropertyExpenses { get; set; }
        public virtual ICollection<OtherPropertyInstallment> OtherPropertyInstallments { get; set; }
        public virtual ICollection<OtherPropertyRealExpense> OtherPropertyRealExpenses { get; set; }
    }
}
