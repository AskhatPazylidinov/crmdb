﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsOfficesRegion
    {
        public int OfficeId { get; set; }
        public int CibregionId { get; set; }

        public virtual Office1 Office { get; set; }
    }
}
