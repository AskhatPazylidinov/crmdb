﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class CashFlowTransaction
    {
        public long Position { get; set; }
        public short Positionn { get; set; }
        public int OperationTypeId { get; set; }

        public virtual CashFlowOperationType OperationType { get; set; }
        public virtual Transaction2 PositionNavigation { get; set; }
    }
}
