﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CustomField
    {
        public CustomField()
        {
            CustomFieldsModules = new HashSet<CustomFieldsModule>();
        }

        public int Id { get; set; }
        public string FieldModule { get; set; }
        public string FieldName { get; set; }
        public string FieldDescription { get; set; }
        public string FieldType { get; set; }
        public string ListType { get; set; }
        public int? FieldOrder { get; set; }

        public virtual ICollection<CustomFieldsModule> CustomFieldsModules { get; set; }
    }
}
