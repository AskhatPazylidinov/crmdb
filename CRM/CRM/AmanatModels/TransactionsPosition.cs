﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TransactionsPosition
    {
        public long Position { get; set; }
        public DateTime GenerateDatetime { get; set; }
    }
}
