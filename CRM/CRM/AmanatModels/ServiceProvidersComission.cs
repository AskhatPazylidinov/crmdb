﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersComission
    {
        public int ProviderId { get; set; }
        public int ContactTypeId { get; set; }
        public int OfficeId { get; set; }
        public decimal StartSumm { get; set; }
        public decimal? EndSumm { get; set; }
        public decimal Comission { get; set; }
        public byte ComissionTypeId { get; set; }
        public byte DirectionTypeId { get; set; }

        public virtual Office1 Office { get; set; }
        public virtual ServiceProvider Provider { get; set; }
    }
}
