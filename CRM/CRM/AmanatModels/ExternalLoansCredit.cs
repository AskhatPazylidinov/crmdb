﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ExternalLoansCredit
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public int CreditId { get; set; }

        public virtual History Credit { get; set; }
        public virtual DepositAccount DepositAccount { get; set; }
    }
}
