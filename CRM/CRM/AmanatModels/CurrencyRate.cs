﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class CurrencyRate
    {
        public int CurrencyId { get; set; }
        public DateTime RateDate { get; set; }
        public decimal Rate { get; set; }
        public int Nominal { get; set; }
        public int UserId { get; set; }
        public int CrossCurrencyId { get; set; }
        public int? ApprovedUserId { get; set; }

        public virtual Currency3 CrossCurrency { get; set; }
        public virtual Currency3 Currency { get; set; }
        public virtual User User { get; set; }
    }
}
