﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class FinesBalance
    {
        public int CreditId { get; set; }
        public int TranchId { get; set; }
        public byte FinesTypeId { get; set; }
        public DateTime BalanceDate { get; set; }
        public decimal FinesBalance1 { get; set; }

        public virtual History Credit { get; set; }
    }
}
