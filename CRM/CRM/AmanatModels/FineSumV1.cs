﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class FineSumV1
    {
        public int CurrencyId { get; set; }
        public decimal FineSumV { get; set; }
    }
}
