﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class TrainingCourse
    {
        public int CourseId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CourseName { get; set; }
        public string OrganisationName { get; set; }
        public bool? IsPaidByBank { get; set; }
        public decimal? CourseSumm { get; set; }
        public int? CurrencyId { get; set; }
        public string OrderNo { get; set; }
        public string Comment { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
