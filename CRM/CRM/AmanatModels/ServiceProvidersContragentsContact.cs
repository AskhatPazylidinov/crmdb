﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class ServiceProvidersContragentsContact
    {
        public int ProviderId { get; set; }
        public int ContragentTypeId { get; set; }
        public int ContactTypeId { get; set; }

        public virtual ServiceProvidersContragent ServiceProvidersContragent { get; set; }
    }
}
