﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Currency
    {
        public string Symbol { get; set; }
        public string CurrencyName { get; set; }
    }
}
