﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class DbfpayBalance
    {
        public decimal? Nn { get; set; }
        public decimal? Kodp { get; set; }
        public decimal? Skods1 { get; set; }
        public decimal? Schet { get; set; }
        public string Nval { get; set; }
        public DateTime? Dt { get; set; }
        public string Transkd { get; set; }
        public decimal? Skods2 { get; set; }
        public decimal? Skod { get; set; }
        public decimal? Skods3 { get; set; }
        public decimal? Krd { get; set; }
        public decimal? Deb { get; set; }
        public decimal? Sums { get; set; }
        public decimal? Sumd { get; set; }
        public string Oper { get; set; }
        public string Filial { get; set; }
    }
}
