﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AuthorizedOperation
    {
        public int AuthorizationId { get; set; }
        public int ContractId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionSumm { get; set; }
        public int AuthorizerId { get; set; }
        public string AuthorizeComment { get; set; }

        public virtual User Authorizer { get; set; }
        public virtual Contract Contract { get; set; }
    }
}
