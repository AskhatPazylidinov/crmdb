﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftFileHeader
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
