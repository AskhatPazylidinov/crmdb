﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class NightCashierUser
    {
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SpecialCashierType { get; set; }

        public virtual User User { get; set; }
    }
}
