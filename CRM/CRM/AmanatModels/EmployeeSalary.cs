﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeeSalary
    {
        public int EmployeeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string TotalSum { get; set; }
        public decimal? TotalErSs { get; set; }
        public decimal? TotalEeSs { get; set; }
        public decimal? TotalTax { get; set; }
        public decimal? TotalIncSum { get; set; }
        public decimal? TotalExpSum { get; set; }
        public decimal? TotalNetSum { get; set; }
        public int Status { get; set; }
        public int? StatusUserId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? TotalDays { get; set; }
        public decimal? TotalWorkedHours { get; set; }
        public decimal? OverTimeHours { get; set; }
        public long? Position { get; set; }

        public virtual Employee1 Employee { get; set; }
    }
}
