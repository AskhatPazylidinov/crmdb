﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class ProductsCurrenciesReplenishmentLimit
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int CurrencyId { get; set; }
        public byte LimitType { get; set; }
        public int PeriodCount { get; set; }
        public decimal MaxSumm { get; set; }
        public byte PeriodType { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual ProductsCurrency ProductsCurrency { get; set; }
    }
}
