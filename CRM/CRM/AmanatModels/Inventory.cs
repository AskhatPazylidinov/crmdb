﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class Inventory
    {
        public int AssetId { get; set; }
        public int InvYear { get; set; }
        public int Deficit { get; set; }
        public string Description { get; set; }
        public bool WriteOff { get; set; }
        public decimal? WriteOffSumm { get; set; }
        public bool? WritenOff { get; set; }

        public virtual Asset Asset { get; set; }
    }
}
