﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class EmployeesPhone
    {
        public string PhoneNo { get; set; }
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal LimitSumm { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Phone PhoneNoNavigation { get; set; }
    }
}
