﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PreTransactionsError
    {
        public long? Position { get; set; }
        public string ErrMessage { get; set; }
        public DateTime? ErrDate { get; set; }
    }
}
