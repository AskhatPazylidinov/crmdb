﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class InterBranchTransfersIncomeAccount
    {
        public int AccountId { get; set; }
        public int OfficeId { get; set; }
        public int AccountTypeId { get; set; }
        public byte? CustomerTypeId { get; set; }
        public string AccountNo { get; set; }
        public int CurrencyId { get; set; }

        public virtual Account1 Account1 { get; set; }
        public virtual Office1 Office { get; set; }
    }
}
