﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class RotationReason
    {
        public RotationReason()
        {
            HistoriesOfficers = new HashSet<HistoriesOfficer>();
        }

        public int ReasonId { get; set; }
        public string ReasonName { get; set; }

        public virtual ICollection<HistoriesOfficer> HistoriesOfficers { get; set; }
    }
}
