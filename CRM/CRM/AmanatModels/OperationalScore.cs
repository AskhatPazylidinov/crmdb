﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class OperationalScore
    {
        public int CustomerId { get; set; }
        public int ScoringId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int? CurrentScoreTypeId { get; set; }
        public int? NewScoreTypeId { get; set; }
        public int? CurrentScore { get; set; }
        public int? NewScore { get; set; }
        public int Cycle { get; set; }
        public int Age { get; set; }
        public int LoanProductId { get; set; }
        public int RatingCode { get; set; }
        public int RatingOverdue { get; set; }
        public int WorstOverduesL6m { get; set; }
        public int WorstPercentOverduesL6m { get; set; }
        public int OverdueDays { get; set; }
        public int PercentOverdueDays { get; set; }
        public int NoPayments { get; set; }
        public string Comment { get; set; }
        public bool? IsExclude { get; set; }
        public int? RatingOverdueDaysCount { get; set; }
    }
}
