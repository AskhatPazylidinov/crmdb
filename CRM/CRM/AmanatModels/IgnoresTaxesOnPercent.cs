﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class IgnoresTaxesOnPercent
    {
        public string MainAccountNo { get; set; }
        public int CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual DepositAccount DepositAccount { get; set; }
    }
}
