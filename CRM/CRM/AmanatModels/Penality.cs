﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Penality
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public int? Tip { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }
    }
}
