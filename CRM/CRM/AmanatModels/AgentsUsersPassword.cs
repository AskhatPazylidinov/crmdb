﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class AgentsUsersPassword
    {
        public int AgentUserId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string HashPassword { get; set; }

        public virtual AgentsUser AgentUser { get; set; }
    }
}
