﻿using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class PaysheetsEmployee
    {
        public PaysheetsEmployee()
        {
            PaysheetsEmployeesAliments = new HashSet<PaysheetsEmployeesAliment>();
            PaysheetsEmployeesExtraAllowances = new HashSet<PaysheetsEmployeesExtraAllowance>();
            PaysheetsEmployeesExtraTransfers = new HashSet<PaysheetsEmployeesExtraTransfer>();
            PaysheetsEmployeesLoansTransfers = new HashSet<PaysheetsEmployeesLoansTransfer>();
            PaysheetsEmployeesTaxes = new HashSet<PaysheetsEmployeesTaxis>();
        }

        public int PaysheetId { get; set; }
        public int EmployeeId { get; set; }
        public decimal? Summ { get; set; }
        public int? CurrencyId { get; set; }

        public virtual Currency3 Currency { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Paysheet Paysheet { get; set; }
        public virtual ICollection<PaysheetsEmployeesAliment> PaysheetsEmployeesAliments { get; set; }
        public virtual ICollection<PaysheetsEmployeesExtraAllowance> PaysheetsEmployeesExtraAllowances { get; set; }
        public virtual ICollection<PaysheetsEmployeesExtraTransfer> PaysheetsEmployeesExtraTransfers { get; set; }
        public virtual ICollection<PaysheetsEmployeesLoansTransfer> PaysheetsEmployeesLoansTransfers { get; set; }
        public virtual ICollection<PaysheetsEmployeesTaxis> PaysheetsEmployeesTaxes { get; set; }
    }
}
