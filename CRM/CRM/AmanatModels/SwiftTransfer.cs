﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SwiftTransfer
    {
        public SwiftTransfer()
        {
            ChangeHistory1s = new HashSet<ChangeHistory1>();
            SwiftTransfersBanks = new HashSet<SwiftTransfersBank>();
            SwiftTransfersOperations = new HashSet<SwiftTransfersOperation>();
        }

        public long TransferId { get; set; }
        public DateTime TransferDate { get; set; }
        public DateTime TradeDate { get; set; }
        public byte DirectionTypeId { get; set; }
        public string CorrBankCode { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string AddInfo { get; set; }
        public string NumberInFile { get; set; }

        public virtual Swift202Transfer Swift202Transfer { get; set; }
        public virtual Swift300Transfer Swift300Transfer { get; set; }
        public virtual ICollection<ChangeHistory1> ChangeHistory1s { get; set; }
        public virtual ICollection<SwiftTransfersBank> SwiftTransfersBanks { get; set; }
        public virtual ICollection<SwiftTransfersOperation> SwiftTransfersOperations { get; set; }
    }
}
