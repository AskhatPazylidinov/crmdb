﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class UtilitiesDoshcardPayment
    {
        public int OperationId { get; set; }
        public string WalletNo { get; set; }
        public string CustomerName { get; set; }
        public string GroupCode { get; set; }

        public virtual UtilitiesDoshcardGroup GroupCodeNavigation { get; set; }
    }
}
