﻿using System;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class Tariff1
    {
        public int TariffId { get; set; }
        public DateTime ChangeDate { get; set; }
        public int ComissionId { get; set; }
        public int OfficeId { get; set; }
        public byte CustomerTypeId { get; set; }
        public int CurrencyId { get; set; }
        public decimal TariffStart { get; set; }
        public decimal? TariffEnd { get; set; }
        public decimal ComissionSumm { get; set; }
        public byte ComissionTypeId { get; set; }
        public bool IsApproved { get; set; }

        public virtual TariffsChangeDate2 ChangeDateNavigation { get; set; }
        public virtual StandardTariff Comission { get; set; }
        public virtual Currency3 Currency { get; set; }
    }
}
