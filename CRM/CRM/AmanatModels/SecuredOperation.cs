﻿using System;
using System.Collections.Generic;
#nullable disable

namespace CRM.AmanatModels
{
    public partial class SecuredOperation
    {
        public SecuredOperation()
        {
            AllowedOperationsInRoles = new HashSet<AllowedOperationsInRole>();
            UsersExtraOperations = new HashSet<UsersExtraOperation>();
        }

        public int OperationId { get; set; }
        public string ProviderNamespace { get; set; }
        public string ProviderName { get; set; }
        public string OperationName { get; set; }
        public string Description { get; set; }
        public string ExtendedDescription { get; set; }
        public byte CriticalTypeId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual SecurityProvider ProviderNameNavigation { get; set; }
        public virtual ICollection<AllowedOperationsInRole> AllowedOperationsInRoles { get; set; }
        public virtual ICollection<UsersExtraOperation> UsersExtraOperations { get; set; }
    }
}
