﻿#nullable disable

namespace CRM.AmanatModels
{
    public partial class SettingsFamilyStatusType
    {
        public int FamilyTypeId { get; set; }
        public int GrsfamilyTypeId { get; set; }

        public virtual FamilyStatusType1 GrsfamilyType { get; set; }
    }
}
