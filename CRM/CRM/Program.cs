﻿using System;
using System.Threading;
using CRM.Logs;
using CRM.ScriptOptions;
using CRM.ShceduleOfScript;

namespace CRM
{
    class Program
    {
        private static void Main(string[] args)
        {
            Logger.Logging($"Скрипт запущен: {DateTime.Now}");

            Options.CheckOptions();

            // UpdateExecutor.Start();
            Shceduler.Start();

            new ManualResetEventSlim(false).Wait();
        }
    }
}