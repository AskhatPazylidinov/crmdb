﻿using System;
using System.IO;
namespace CRM.Logs
{
    public static class Logger
    {
        private static readonly string _pathToLogs = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs/Logs.txt");

        public static void Logging(string message)
        { 
            using (var file = new StreamWriter(_pathToLogs, true))
            {
                file.WriteLine(message);
            }
        }
    }
}