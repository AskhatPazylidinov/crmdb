﻿using System;
using System.Linq;
using CRM.AmanatModels;
using CRM.Logs;
using Microsoft.EntityFrameworkCore;

namespace CRM
{
    class UpdateExecutor
    {
        public static void Start()
        {
            using var amanatContext = new AmanatCreditDevelopContext();
            using var crmContext = new CrmDbContext();

            var updateSchedule = crmContext.UpdateDatabaseSchedule.FirstOrDefault(x => x.DateTime.Date == DateTime.Now.Date);
            var sysInfo = amanatContext.SysInfos.FirstOrDefault();

            if(updateSchedule != null || sysInfo?.DateOd.Date != DateTime.Now.Date)
                return;
            
            Console.WriteLine("Старт обновления");
            Logger.Logging($"--------------------------------------Старт обновления: {DateTime.Now}");
            
            amanatContext.Database.SetCommandTimeout(0);

            amanatContext.Database.ExecuteSqlRaw("EXEC Crm.UpdateImportCrmDB");
            
            Console.WriteLine("Обновление завершено!");
        }
    }
}