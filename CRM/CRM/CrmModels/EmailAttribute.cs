﻿#nullable disable

namespace CRM.CrmModels
{
    public partial class EmailAttribute
    {
        public int? CustomerId { get; set; }
        public string Email { get; set; }
    }
}
