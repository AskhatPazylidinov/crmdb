﻿#nullable disable

namespace CRM.CrmModels
{
    public partial class PhoneAttribute
    {
        public int? CustomerId { get; set; }
        public string ContactPhone1 { get; set; }
        public string ContactPhone2 { get; set; }
    }
}
