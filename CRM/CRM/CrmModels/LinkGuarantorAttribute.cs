﻿using System;
#nullable disable

namespace CRM.CrmModels
{
    public partial class LinkGuarantorAttribute
    {
        public int? CustomerId { get; set; }
        public int? CreditId { get; set; }
        
        public int GaranteeID { get; set; }
        
        public int? GarantTypeId { get; set; }
        public string GarantTypeName { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string IdentificationNumber { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNo { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? IssueDate { get; set; }
    }
}
