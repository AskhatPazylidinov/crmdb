﻿namespace CRM.CrmModels
{
    public class GarantsFieldsValue
    {
        public int GaranteeId { get; set; }
        
        public int FieldId { get; set; }
        
        public bool? ValueBoolean { get; set; }
        
        public int? ValueInteger { get; set; }
        
        public decimal? ValueDecimal { get; set; }
        
        public string ValueString { get; set; }
    }
}