﻿using System;
namespace CRM.CrmModels
{
    public class Guarantor
    {
        public int CreditID { get; set; }
        public int CustomerID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}