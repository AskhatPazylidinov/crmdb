﻿using System;
using System.Net.Http;
namespace CRM.CrmModels
{
    public class UpdateDatabaseSchedule
    {
        public DateTime DateTime { get; set; }
        
        public string Message { get; set; }
    }
}