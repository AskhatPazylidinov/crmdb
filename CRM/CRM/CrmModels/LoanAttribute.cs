﻿using System;
#nullable disable

namespace CRM.CrmModels
{
    public partial class LoanAttribute
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int CreditId { get; set; }
        public string AgreementNo { get; set; }
        public DateTime? IssueDate { get; set; }
        public int? ApprovedPeriod { get; set; }
        public string Name { get; set; }
        public decimal? ApprovedSumm { get; set; }
        
        public int? TranchIdHistoryOverdues { get; set; }
        
        public decimal? CurrentBody { get; set; }
        public decimal? ApprovedRate { get; set; }
        public decimal? MainOverdueSumm { get; set; }
        public decimal? PercentBalanceOverdueSumm { get; set; }
        public decimal? Peny { get; set; }
        public decimal? Rvd { get; set; }
        public int? NumDiscount { get; set; }
        public decimal? SumGosSubsid { get; set; }
        public decimal? MonthlyPay { get; set; }
        public decimal? HaveToPay { get; set; }
        public int? MainSummOverdueDays { get; set; }
        public int? PercentSummOverdueDays { get; set; }
        public int? OverdueDays { get; set; }
        public string BrancheName { get; set; }
        public int? StatusId { get; set; }
        public DateTime? ChangeDate { get; set; }
        public string OfficeName { get; set; }
        public string UserName { get; set; }
        public DateTime? EndDate { get; set; }
        public string LoanPurpose { get; set; }
    }
}
