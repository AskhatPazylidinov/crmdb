﻿#nullable disable

namespace CRM.CrmModels
{
    public partial class LinkType
    {
        public int RelativeTypeId { get; set; }
        public string RelativeName { get; set; }
    }
}
