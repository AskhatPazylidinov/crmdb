﻿#nullable disable

namespace CRM.CrmModels
{
    public partial class LinkRelativeAttribute
    {
        public int? CustomerId { get; set; }
        public int? RelativeId { get; set; }
        public int? RelativeTypeId { get; set; }
        public string RelativeName { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
    }
}
