﻿using System;
#nullable disable

namespace CRM.CrmModels
{
    public partial class PaymentsAttribute
    {
        public int? CreditId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public decimal? PayBody { get; set; }
        
        public long? Code { get; set; }
        public decimal? PayInterests { get; set; }
        public decimal? SumN { get; set; }
        public string Comment { get; set; }
    }
}
