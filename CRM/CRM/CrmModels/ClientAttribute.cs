﻿using System;
#nullable disable

namespace CRM.CrmModels
{
    public partial class ClientAttribute
    {
        public int CustomerId { get; set; }
        public string Surname { get; set; }
        public string CustomerName { get; set; }
        public string Otchestvo { get; set; }
        public string IdentificationNumber { get; set; }
        public string DocumentSeries { get; set; }
        public string DocumentNo { get; set; }
        public string IssueAuthority { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool? Sex { get; set; }
        public string WorkName { get; set; }
        public string WorkPosition { get; set; }
        public string WorkPhone { get; set; }
    }
}
