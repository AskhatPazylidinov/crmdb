﻿using System;
#nullable disable

namespace CRM.CrmModels
{
    public partial class SheduleOfPlanesRepayment
    {
        public int? CreditId { get; set; }
        public DateTime? IssueDate { get; set; }
        public string AgreementNo { get; set; }
        public DateTime? PayDate { get; set; }
        public decimal? MainSumm { get; set; }
        public decimal? PercentsSumm { get; set; }
    }
}
