﻿using System;
namespace CRM.CrmModels
{
    public class HistoryStopCalculated
    {
        public int CreditId { get; set; }
        public string StopType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
    }
}