﻿namespace CRM.CrmModels
{
    public partial class Documents
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public string FileName { get; set; }
        public byte[] DATA { get; set; }
        public byte Type { get; set; }
    }
}
