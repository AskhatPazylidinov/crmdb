﻿#nullable disable

namespace CRM.CrmModels
{
    public partial class AddressAttribute
    {
        public int? CustomerId { get; set; }
        public int? RegistrationCountryId { get; set; }
        public int? ResidenceCountryId { get; set; }
        public string RegistrationPostalCode { get; set; }
        public string ResidencePostalCode { get; set; }
        public string RegistrationCityName { get; set; }
        public string ResidenceCityName { get; set; }
        public string RegistrationStreet { get; set; }
        public string ResidenceStreet { get; set; }
        public string RegistrationHouse { get; set; }
        public string ResidenceHouse { get; set; }
        public string RegistrationFlat { get; set; }
        public string ResidenceFlat { get; set; }
    }
}
