﻿using CRM.ScriptOptions;
using Quartz;
using Quartz.Impl;

namespace CRM.ShceduleOfScript
{
    public class Shceduler
    {
        public static async void Start()
        {
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            var job = JobBuilder.Create<ShceduleExecutor>().Build();

            var trigger = TriggerBuilder.Create()
                                        .WithCronSchedule(Options.ShceduleTime)
                                        .Build();

            await scheduler.ScheduleJob(job, trigger);
        }
    }
}