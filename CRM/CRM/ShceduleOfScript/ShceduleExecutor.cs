﻿using System.Threading.Tasks;
using Quartz;

namespace CRM.ShceduleOfScript
{
    class ShceduleExecutor : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(UpdateExecutor.Start);
        }
    }
}