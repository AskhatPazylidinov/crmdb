﻿using System;
using System.IO;
using System.Text;
namespace CRM.ScriptOptions
{
    public class Options
    {
        /// <summary>
        /// Расписание
        /// </summary>
        public static string ShceduleTime { get; private set; }

        /// <summary>
        /// Строка подключения к БД
        /// </summary>
        public static string ConnectionString { get; private set; }

        public static void CheckOptions()
        {
            var pathToShceduleTime = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Options/ShceduleTime.txt");
            var pathToConnectionString = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Options/ConnectionString.txt");

            using (var fstream = File.OpenRead($"{pathToShceduleTime}"))
            {
                var array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                var optionsValue = Encoding.Default.GetString(array);
                ShceduleTime = optionsValue;
            }

            using (var fstream = File.OpenRead($"{pathToConnectionString}"))
            {
                var array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                var optionsValue = Encoding.Default.GetString(array);
                ConnectionString = optionsValue;
            }
        }
    }
}